package sound;

import java.io.*;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.EnumMap;
import java.util.TreeMap;

/**
 * Singleton class for accessing sound resources.
 * 
 * Loads different sound sets from a predefined location.
 * Each sub-directory under SOUND_DIR is assumed to be a sound set containing
 * .wav files with appropriate names.
 * 
 * The sound sets are stored in a nested Map. The current sound set is kept
 * track with a member variable, and playing a SOUND_EVENT extracts the sound
 * from the nest map using the combination of keys.
 * 
 * @author tyuki
 */
public class SoundContainer {

    private final Map<String, Map<SOUND_EVENT, SoundPlayer>> sounds;
    private static final String SOUND_DIR = "./sounds/";
    private String currentSoundSet = "default";
    public static final SoundContainer INSTANCE = new SoundContainer();

    protected SoundContainer() {
        sounds = new TreeMap<>();
    }

    public static void loadSounds() {

        File dir = new File(SOUND_DIR);
        if (!dir.isDirectory()) {
            return;
        }

        List<String> subdirs = new LinkedList<>();

        //First path
        for (File file : dir.listFiles()) {
            if (!file.isDirectory()) {
                continue;
            }
            if (file.getName().matches(".+\\..+$")) {
                return;
            }

            subdirs.add(file.getName());
        }

        //
        for (String subdir : subdirs) {
            for (SOUND_EVENT se : SOUND_EVENT.values()) {
                SoundContainer.addFile(subdir, se, dir + "/" + subdir + "/" + se.filename + ".wav");
            }
        }
    }
    
    public Collection<String> getSoundSets() {
        return sounds.keySet();
    }
    
    public boolean setSoundSet(String setName) {
        if (sounds.containsKey(setName)) {
            currentSoundSet = setName;
            return true;
        }
        
        return false;
    }

    private static void addFile(String setName, SOUND_EVENT key, String file) {
        try {
            File soundFile = new File(file);
            SoundPlayer sound = SoundPlayer.loadFile(soundFile);
            
            if (!INSTANCE.sounds.containsKey(setName)) {
                INSTANCE.sounds.put(setName, new EnumMap<>(SOUND_EVENT.class));
            }
            
            INSTANCE.sounds.get(setName).put(key, sound);
        } catch (Exception fnfe) {
//            fnfe.printStackTrace();;
//                      UrsuppeApp.debug(fnfe);
        }
    }

    public void play(SOUND_EVENT key, float volume) {
        try {
            if (sounds.containsKey(currentSoundSet)) {
                if (sounds.get(currentSoundSet).containsKey(key)) {
                    ((SoundPlayer) sounds.get(currentSoundSet).get(key)).play(volume);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void play(SOUND_EVENT key, int volume) {
        play(key, (float) ((float) volume / 10.0f));
    }
}
