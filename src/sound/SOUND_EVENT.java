package sound;

public enum SOUND_EVENT {
    TURN("turn", TYPE.NOTICE), 
    TURN_END("turn_end", TYPE.NOTICE), 
    NOTICE("notice", TYPE.NOTICE), 
    GAME_END("game_end", TYPE.NOTICE), 
    
    MOVE("move", TYPE.EFFECTS), 
    DAMAGE("damage", TYPE.EFFECTS), 
    DIVISION("division", TYPE.EFFECTS), 
    ALERT("alert", TYPE.EFFECTS), 
    SCORE("score", TYPE.EFFECTS), 
    
    TEN("ten", TYPE.GENE_EFFECTS), 
    HEAL("heal", TYPE.GENE_EFFECTS), 
    SFS_DAMAGE("sfs_damage", TYPE.GENE_EFFECTS), 
    DEATH("death", TYPE.EFFECTS), 
    BANG("bang", TYPE.GENE_EFFECTS), 
    DICEROLL("diceroll", TYPE.EFFECTS);

    public static enum TYPE {

        NOTICE, EFFECTS, GENE_EFFECTS
    }
    public final String filename;
    public final TYPE type;

    private SOUND_EVENT(String filename, TYPE type) {
        this.filename = filename;
        this.type = type;
    }
    
}
