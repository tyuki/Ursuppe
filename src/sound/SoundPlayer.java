package sound;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.*;

/**
 * SoundPlayer simply plays a clip loaded from .wav files.
 * 
 * @author tyuki
 */
public class SoundPlayer implements LineListener {

    private Clip _clip;

    protected SoundPlayer() {
    }
    
    private void load(File file) throws UnsupportedAudioFileException, IOException {
        AudioInputStream input = null;
        try {
            //open wav
            input = AudioSystem.getAudioInputStream(file);
            DataLine.Info info = new DataLine.Info(Clip.class, input.getFormat());
            //create clip
            _clip = (Clip) AudioSystem.getLine(info);
            _clip.addLineListener(this);
            _clip.open(input);
            input.close();
        } catch (LineUnavailableException ex) {
//            ex.printStackTrace();
           if (input != null)
                input.close();
        } catch (IOException ex) {
//            ex.printStackTrace();
           if (input != null)
                input.close();
           throw ex;
        }
    }
    
    public static SoundPlayer loadFile(File file) throws UnsupportedAudioFileException, IOException {
        SoundPlayer player = new SoundPlayer();
        player.load(file);
        return player;
    }

    public void play(float volume) {
        if (volume < 1.0f) {
            FloatControl control = (FloatControl)_clip.getControl(FloatControl.Type.MASTER_GAIN);
            control.setValue((float)Math.log10(volume) * 20);
        }
        _clip.start();
    }

    @Override
    public void update(LineEvent event) {
        if (event.getType() == LineEvent.Type.STOP) {
            Clip clip = (Clip) event.getSource();
            clip.stop();
            clip.setFramePosition(0);
        }
    }


}

