package ursuppe.ai;

import java.awt.Point;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import ursuppe.game.Amoeba;
import ursuppe.game.EnvironmentCard;
import ursuppe.game.GameState;
import ursuppe.game.GameState.ATTACK_RESPONSE;
import ursuppe.game.GameState.DIRECTION;
import ursuppe.game.Gene;
import ursuppe.game.GenePool.GENE;
import ursuppe.game.Player;
import ursuppe.game.Soup;

/**
 *
 * @author Personal
 */
public class GameView {

    private final GameState _game;
    private final int _playerID;

    public GameView(GameState game, int playerID) {
        _game = game;
        _playerID = playerID;
    }

    /**
     * Returns the Player info of this player.
     * 
     * @return
     */
    public Player getPlayer() {
        return _game.getPlayerByID(_playerID);
    }

    /**
     * Returns Player info for the playerID given
     *
     * @param playerID
     * @return
     */
    public Player getPlayerByID(int playerID) {
        return _game.getPlayerByID(playerID);
    }

    /**
     * Returns Player info for the order given.
     * The order starts from 0 to number of players - 1, and 0 is the player with highest score.
     *
     * @param order
     * @return
     */
    public Player getPlayerByOrder(int order) {
        return _game.getPlayerByOrder(order);
    }

    /**
     * Returns list of players.
     *
     * @return
     */
    public List<Player> getPlayers() {
        return Collections.unmodifiableList(_game.getPlayers());
    }

    /**
     * Returns current environment
     *
     * @return
     */
    public EnvironmentCard getCurrentEnvironment() {
        return _game.getCurrentEnvironment();
    }

    /**
     * Returns current round
     *
     * @return
     */
    public int getCurrentRound() {
        return _game.getProgress().currentRound;
    }

    public boolean isCloseToGameEnd() {
        Player top = _game.getPlayerByOrder(0);
        return (top.getAdvance() + _game.computeScore(top) >= GameState.ENDING_SCORE);
    }

    public Gene getGene(GENE gene) {
        return _game.getGenePool().getGene(gene);
    }

    public int getRemainingAmoebas() {
        return _game.getRemainingAmoebaNumbers(_playerID).size();
    }

    /**
     * Returns income expected at the beginning of phase4
     *
     * @return
     */
    public int getPhase4Income() {
        return _game.getPhase4Income();
    }

    /**
     * Reeturns the cost for cell division.
     *
     * @return
     */
    public int getDivisionCost() {
        return _game.getDivisionCost(_game.getCurrentPlayer());
    }

    /**
     * Returns food colors.
     *
     * @return
     */
    public int[] getFoodColors() {
        return _game.getSoup().getFoodColors();
    }

    /**
     * Returns the number parasitizing you can do at the given location.
     *
     * @param loc
     * @return
     */
    public int getPARusableNum(Point loc) {
        return _game.getPARusableNum(_game.getCurrentPlayer(), loc);
    }

    /**
     * Returns the players which you can parasite in a given location.
     * The Map returns contains players as keys, and values are the number of parasitism
     * possible for that player.
     *
     * @param loc
     * @return
     */
    public Map<Player, Integer> getPARtargets(Point loc) {
        return _game.getPARtargets(_game.getCurrentPlayer(), loc);
    }

    //Soup Cell accesses

    /**
     * Returns true if the given location is a valid point in the game.
     *
     * @param loc
     * @return
     */
    public boolean isValidLocation(Point loc) {
        return Soup.isValidLocation(loc);
    }

    /**
     * Returns true if the given location is a valid point in the game.
     *
     * @param x
     * @param y
     * @return
     */
    public boolean isValidLocation(int x, int y) {
        return Soup.isValidLocation(x, y);
    }

    /**
     * Returns a list of amoebas in a location.
     *
     * @param loc
     * @return
     */
    public List<Amoeba> getAmoebasAt(Point loc) {
        return Collections.unmodifiableList(_game.getSoup().getCell(loc).getAmoebas());
    }

    /**
     * Returns the foods available at a location.
     *
     * @param loc
     * @return
     */
    public int[] getFoodAt(Point loc) {
        return _game.getSoup().getCell(loc).getFood();
    }

    /**
     * Returns the number of DPs placed at a location
     *
     * @param loc
     * @return
     */
    public int getDPAt(Point loc) {
        return _game.getSoup().getCell(loc).getDP();
    }

    /**
     * Returns a new location if moved from the current location to the direction given.
     *
     * @param current
     * @param dir
     * @return
     */
    public Point getDestination(Point current, DIRECTION dir) {
        return _game.getDestiation(current, dir);
    }

    /**
     * Returns true if moving to the given direction will hit a wall
     *
     * @param loc
     * @param dir
     * @return
     */
    public boolean doesHitWall(Point loc, DIRECTION dir) {
        return _game.doesHitWall(loc, dir);
    }

    /**
     * Returns the opposite direction of the given direction
     *
     * @param dir
     * @return
     */
    public DIRECTION getOppositeDirection(DIRECTION dir) {
        return _game.getOppositeDirection(dir);
    }


    /**
     * Returns true if BANG can be used when eaten by SFS
     *
     * @return
     */
    public boolean isBANGusableAgainstSFS() {
        return _game.isBANGusableAgainstSFS();
    }

    /**
     * Returns true if ESC followed by HOLD makes the attack a success.
     *
     *
     * @return
     */
    public boolean isESCchasableByHOLD() {
        return _game.isESCchasableByHOLD();
    }


    /**
     * Returns true if the player is currently asked to decide wether or not to chae ESC with HOLD
     *
     * @return
     */
    public boolean isChasingESCByHOLD() {
        return _game.getProgress().defenderActive && _game.getProgress().attackerID == _playerID;
    }

    /**
     * Returns the BP required to move.
     *
     * @return
     */
    public int getMovementCost() {
        return _game.getMovementCost(_playerID);
    }

    /**
     * Returns the number of dice to roll when moving.
     *
     * @return
     */
    public int getMovementDiceNum() {
        return _game.getMovementDiceNum(_playerID);
    }

    /**
     * Returns true if enough food is available at a given location.
     *
     * @param loc
     * @return
     */
    public boolean canFeedAt(Point loc) {
        Player player = _game.getPlayerByID(_playerID);

        return _game.canFeed(player, loc, true) || _game.canFeed(player, loc, false);
    }

    /**
     * Returns list of possible SFS targets in a location
     * Amoebas of your own will also be included when 
     * the flag is set to true.
     *
     * @param loc
     * @param attackerAmoebaNum
     * @param includeOwnColor
     * @return
     */
    public List<Amoeba> getSFSTargets(Point loc, int attackerAmoebaNum, boolean includeOwnColor) {
        Player player = getPlayer();
        
        List<Amoeba> targets = new LinkedList<>();

        for (Amoeba amoeba : getAmoebasAt(loc)) {
            //own color
            if (amoeba.playerID == player.getID() && !includeOwnColor) continue;
            //own amoeba
            if (amoeba.playerID == player.getID() && attackerAmoebaNum == amoeba.number) continue;
            //Skip if blocked
            List<ATTACK_RESPONSE> atkRes = getAttackResponse(amoeba);
            if (atkRes.contains(ATTACK_RESPONSE.BLOCKED)) continue;

            //else add
            targets.add(amoeba);
        }


        return targets;
    }

    /**
     * Returns possible response from an attack against target.
     * You cannot attack if the response contains BLOCKED.
     * The attack will instantly succeed if the response contains NO_DEFENSE
     *
     * @param target
     * @return
     */
    public List<ATTACK_RESPONSE> getAttackResponse(Amoeba target) {
        Player attacker = _game.getCurrentPlayer();
        Amoeba attackerAmoeba = _game.getCurrentAmoeba();

        Player defender = _game.getPlayerByID(target.playerID);

        return _game.getAttackResponse(attacker, attackerAmoeba, defender, target, true);
    }

    /**
     * Returns true if the given location has amoebas to eat.
     *
     * @param loc
     * @param attackerAmoebaNumber
     * @param easyTargetOnly
     * @return
     */
    public boolean canFeedWithSFS(Point loc, int attackerAmoebaNumber, boolean easyTargetOnly) {
        if (getPlayer().getBP() == 0) return false;

        List<Amoeba> targets = this.getSFSTargets(loc, attackerAmoebaNumber, false);
        //No if #targets is 0
        if (targets.isEmpty()) return false;
        //Else true if not looking for easty targets
        if (!easyTargetOnly) return true;
        //Look for easy targets
        for (Amoeba amoeba : targets) {
            List<ATTACK_RESPONSE> atkRes = getAttackResponse(amoeba);
            if (atkRes.contains(ATTACK_RESPONSE.NO_DEFENSE)) return true;
            if (!atkRes.contains(ATTACK_RESPONSE.DEFENSE) && !atkRes.contains(ATTACK_RESPONSE.ESCAPE) && !atkRes.contains(ATTACK_RESPONSE.HARD_CRUST)) return true;
        }

        return false;
    }

    /**
     * Returns true if some one other than your self has SFS
     *
     * @return
     */
    public boolean isSFSinPlay() {
        Player player = getPlayer();
        for (Player p : getPlayers()) {
            if (p.equals(player)) continue;
            if (p.hasGene(GENE.SFS) || p.hasGene(GENE.AGR)) return true;
        }
        return false;
    }

    /**
     * Returns true if some one other than your self has SFS
     *
     * @return
     */
    public boolean isCAMinPlay() {
        Player player = getPlayer();
        for (Player p : getPlayers()) {
            if (p.equals(player)) continue;
            if (p.hasGene(GENE.SFS) || p.hasGene(GENE.AGR)) {
                if (p.hasGene(GENE.CAM1) || p.hasGene(GENE.CAM2)) return true;
            }
        }
        return false;
    }

    /**
     * Returns true if there is an amoeba with SFS capability
     * at the given location
     *
     * @param loc
     * @return
     */
    public boolean isSFSthreatAt(Point loc) {
        Player player = getPlayer();

        for (Amoeba amoeba : _game.getSoup().getCell(loc).getAmoebas()) {
            //skip own
            if (amoeba.playerID == player.getID()) continue;
            //find owner
            Player owner = _game.getPlayerByID(_playerID);
            if ((owner.hasGene(GENE.SFS) || owner.hasGene(GENE.AGR)) && owner.getBP() > 0) return true;
        }

        return false;
    }

    /**
     * Returns true if the player has some kind of defense
     *
     * @return
     */
    public boolean hasDefense() {
        Player player = getPlayer();

        if (player.hasGene(GENE.DEF)) return true;
        if (player.hasGene(GENE.ESC)) return true;
        if (player.hasGene(GENE.HARD)) return true;
        if (player.hasGene(GENE.ALM)) return true;
        if (player.hasGene(GENE.DIS)) return true;
        if (player.hasGene(GENE.BANG) && _game.isBANGusableAgainstSFS()) return true;

        return false;
    }

    /**
     * Return true if the selected amoeba will die this round
     *
     * @param amoeba
     * @return
     */
    public boolean isDead(Amoeba amoeba) {
        Player player = _game.getPlayerByID(amoeba.playerID);
        return _game.isAmoebaDead(player, amoeba);
    }

    /**
     * Returns true if there is at least one dead amoeba in the given location.
     *
     * @param loc
     * @return
     */
    public boolean isDeadAmoebaAt(Point loc) {
        for (Amoeba amoeba : getAmoebasAt(loc)) {
            if (isDead(amoeba)) return true;
        }
        return false;
    }


    /**
     * Returns feeding requirements. First element of the returned array is the number of foods,
     * and the second element is the number of colors required.
     *
     * @param loc
     * @param withFRUPAR
     * @param withSUB
     * @return
     */
    public int[] getFeedingRequirement(Point loc, boolean withFRUPAR, boolean withSUB) {
        return _game.calculateFoodRequirement(getPlayer(), loc, withFRUPAR, withSUB);
    }

    /**
     * Returns neighboring locations that have food to let you feed.
     *
     * @param loc
     * @return
     */
    public List<Point> getFeedableSUClocations(Point loc) {
        List<Point> feedable = new LinkedList<>();

        Player player = getPlayer();

        //FRU PAR only, No SUB
        int[] foodReq = _game.calculateFoodRequirement(player, loc, true, false);
        //Try SUC
        if (_game.canFeedWithSUC(player, loc, foodReq[0], foodReq[1], DIRECTION.NORTH)) feedable.add(getDestination(loc, DIRECTION.NORTH));
        if (_game.canFeedWithSUC(player, loc, foodReq[0], foodReq[1], DIRECTION.SOUTH)) feedable.add(getDestination(loc, DIRECTION.SOUTH));
        if (_game.canFeedWithSUC(player, loc, foodReq[0], foodReq[1], DIRECTION.EAST)) feedable.add(getDestination(loc, DIRECTION.EAST));
        if (_game.canFeedWithSUC(player, loc, foodReq[0], foodReq[1], DIRECTION.WEST)) feedable.add(getDestination(loc, DIRECTION.WEST));

        //FRU PAR SUB all used
        foodReq = _game.calculateFoodRequirement(player, loc, true, true);
        //Try SUC
        if (_game.canFeedWithSUC(player, loc, foodReq[0], foodReq[1], DIRECTION.NORTH)) feedable.add(getDestination(loc, DIRECTION.NORTH));
        if (_game.canFeedWithSUC(player, loc, foodReq[0], foodReq[1], DIRECTION.SOUTH)) feedable.add(getDestination(loc, DIRECTION.SOUTH));
        if (_game.canFeedWithSUC(player, loc, foodReq[0], foodReq[1], DIRECTION.EAST)) feedable.add(getDestination(loc, DIRECTION.EAST));
        if (_game.canFeedWithSUC(player, loc, foodReq[0], foodReq[1], DIRECTION.WEST)) feedable.add(getDestination(loc, DIRECTION.WEST));

        return feedable;
    }
}
