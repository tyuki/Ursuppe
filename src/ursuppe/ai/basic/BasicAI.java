package ursuppe.ai.basic;

import java.awt.Point;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import ursuppe.ai.AIInterface;
import ursuppe.ai.GameView;
import ursuppe.game.Amoeba;
import ursuppe.game.EnvironmentCard;
import ursuppe.game.GameState.DIRECTION;
import ursuppe.game.GameState.PHASE1_ACTION;
import ursuppe.game.Gene;
import ursuppe.game.GenePool.GENE;
import ursuppe.game.Player;

/**
 *
 * @author Personal
 */
public class BasicAI implements AIInterface {

    private final Random _rand;
    private final BasicAIOption _option;


    public BasicAI(BasicAIOption option) {
        _rand = new Random();
        _option = option;
    }

    @Override
    public int selectInitialScore(List<Integer> remainingChoice, GameView game) {
        return remainingChoice.get(_rand.nextInt(remainingChoice.size()));
    }

    @Override
    public Point selectInitialAmoebaLocation(List<Point> availableLocs, GameView game) {
        List<LocationRanking> ranked = rankSpawnLocations(availableLocs, game);
        return ranked.get(0).loc;
    }

    @Override
    public int selectAmoebaNum(List<Integer> choice, Point location, GameView game) {
        return choice.get(0);
    }

    @Override
    public PHASE1_ACTION selectPhase1Action(Amoeba amoeba, List<PHASE1_ACTION> choice, GameView game) {
        Player player = game.getPlayer();
        //Drift if it can feed you
        if (choice.contains(PHASE1_ACTION.DRIFT)) {
            DIRECTION dir = game.getCurrentEnvironment().direction;
            Point dst = game.getDestination(amoeba.getLocation(), dir);
            if (game.canFeedAt(dst)) return PHASE1_ACTION.DRIFT;
            if ((player.hasGene(GENE.SFS) || player.hasGene(GENE.AGR)) && game.canFeedWithSFS(dst, amoeba.number, true)) return PHASE1_ACTION.DRIFT;

            //Rebound
            if (game.doesHitWall(amoeba.getLocation(), dir) && player.hasGene(GENE.REB)) {
                Point REBdst = game.getDestination(amoeba.getLocation(), game.getOppositeDirection(dir));
                if (game.canFeedAt(REBdst)) return PHASE1_ACTION.DRIFT;
                if ((player.hasGene(GENE.SFS) || player.hasGene(GENE.AGR)) && game.canFeedWithSFS(REBdst, amoeba.number, true)) return PHASE1_ACTION.DRIFT;
            }
        }
        //Hold if can feeed
        if (choice.contains(PHASE1_ACTION.HOLD)) {
            if (game.canFeedAt(amoeba.getLocation())) return PHASE1_ACTION.HOLD;
            if ((player.hasGene(GENE.SFS) || player.hasGene(GENE.AGR)) && game.canFeedWithSFS(amoeba.getLocation(), amoeba.number, true)) return PHASE1_ACTION.HOLD;
        }
        if (choice.contains(PHASE1_ACTION.EAT)) return PHASE1_ACTION.EAT;
        if (choice.contains(PHASE1_ACTION.EAT_WITH_CHOICE)) return PHASE1_ACTION.EAT_WITH_CHOICE;
        if (choice.contains(PHASE1_ACTION.EAT_WITH_GENES)) return PHASE1_ACTION.EAT_WITH_GENES;
        //SFS
        if (choice.contains(PHASE1_ACTION.SFS) && decideSFS(amoeba, amoeba.getLocation(), game)) return PHASE1_ACTION.SFS;
        //Move
        if (choice.contains(PHASE1_ACTION.MOVE)) {
            if ((computeMovementSuccessRate(amoeba.getLocation(), game) >= _option.moveProbabilityLB) || player.hasGene(GENE.STR)) return PHASE1_ACTION.MOVE;
        }
        if (choice.contains(PHASE1_ACTION.STARVE)) return PHASE1_ACTION.STARVE;

        //Take the only choice
        return choice.get(0);
    }

    @Override
    public int[] selectTENfoods(int[] foodColors, int[] srcFood, int[] dstFood, Point srcLoc, Point dstLoc, GameView game) {
        return null;
    }

    @Override
    public boolean selectREB(Point current, DIRECTION wallDirection, GameView game) {
        DIRECTION REBdir = game.getOppositeDirection(wallDirection);
        double REBused = rankMovementDirection(current, REBdir, game);
        double REBunused = rankMovementDirection(current, DIRECTION.NONE, game);

        //true if destination is better and no SFS threat present
        return (REBused > REBunused && !game.isSFSthreatAt(game.getDestination(current, REBdir)));
    }

    @Override
    public boolean selectTOX(Point loc, GameView game) {
        return true;
    }

    @Override
    public Point selectSUCLocation(Point current, List<Point> availableLocs, GameView game) {
        List<Point> locs = game.getFeedableSUClocations(current);
        if (locs.isEmpty()) return null;

        Collections.shuffle(locs);

        for (int i = 0; i < locs.size(); i++) {
            if (availableLocs.contains(locs.get(i))) {
                return locs.get(i);
            }
        }

        return null;
    }

    @Override
    public int[] selectSUCFoods(Point current, Point SUCloc, int[] foodColors, int[] SUClocFoods, GameView game) {
        Player player = game.getPlayer();

        int[] avail = game.getFoodAt(current);
        int[] foodReq = game.getFeedingRequirement(current, true, true);
        int reqNum = foodReq[0];
        int reqCol = foodReq[1];

        int availNum = 0;
        int availCol = 0;
        int sucSum = 0;
        int sucMax= 1;
        if (player.hasGene(GENE.SUC2)) sucMax++;
        int[] sucFoods = new int[foodColors.length];

        //Count availability and missing color
        for (int i = 0; i < foodColors.length; i++) {
            //skip own
            if (foodColors[i] == (player.getColorID())) continue;
            //avail
            availNum+=avail[i];
            if (avail[i] > 0) availCol++;
            //missing color
            if (avail[i] == 0 && SUClocFoods[i] > 0 && sucSum < sucMax) {
                sucSum++;
                sucFoods[i]++;
                availCol++;
                availNum++;
            }
        }

        //check
        //missing colors are used but not enough
        if (availCol < reqCol) return sucFoods;
        //fill in the numbers
        while (availNum < reqNum && sucSum < sucMax) {
            boolean changed = false;
            //Add any color available to meet the number requirement
            for (int i = 0; i < foodColors.length; i++) {
                //skip own
                if (foodColors[i] == (player.getColorID())) continue;
                //missing color
                if (SUClocFoods[i] > sucFoods[i] && sucSum < sucMax) {
                    sucFoods[i]++;
                    sucSum++;
                    availNum++;
                    changed = true;
                }
                
            }

            if (!changed) break;
        }
        
        return sucFoods;
    }

    @Override
    public int[] selectFoods(Point loc, int[] foodColors, int[] foods, int[] sucFoods, GameView game, List<Integer> parTargets) {
        return null;
    }

    @Override
    public Amoeba selectSFStarget(Amoeba attacker, List<Player> players, List<Amoeba> amoebas, GameView game) {
        List<AmoebaRanking> ranked = rankSFSTargets(attacker, amoebas, game);
        
        return ranked.get(0).target;
    }

    @Override
    public boolean selectSPD(Point loc, GameView game) {
        return !game.canFeedAt(loc);
    }

    @Override
    public Point selectExcreteDestination(List<Point> validDsts, GameView game) {
        return null;
    }

    @Override
    public DIRECTION selectMovementDirection(Point loc, List<DIRECTION> availableDirs, GameView game) {
        List<DirectionRanking> ranked = rankMovementDirections(loc, availableDirs, game);

        return ranked.get(0).dir;
    }


    @Override
    public GENE[] handleGeneDeffects(int exceeding, List<Gene> genes, int currentBP, GameView game) {
        List<GENE> discards = new LinkedList<>();

        List<GeneRanking> ranked = rankDiscardGenes(genes, game);
        
        int discardValue = 0;
        int i = 0;
        while (exceeding - discardValue > currentBP) {
            discards.add(ranked.get(i).gene);
            discardValue += game.getGene(ranked.get(i).gene).getMPReleaseValue();
            i++;
        }

        if (discards.isEmpty()) return null;

        GENE[] d = new GENE[discards.size()];
        for (i = 0; i < discards.size(); i++) {
            d[i] = discards.get(i);
        }

        return d;
    }

    @Override
    public GENE[] selectADAExchange(Map<GENE, List<GENE>> exchangable, int currentBP, GameView game) {
        return null;
    }

    @Override
    public GENE selectGENEPurchase(List<GENE> purchasable, int currentBP, GameView game) {
        Player player = game.getPlayer();

        if (!purchaseDecision(currentBP, game)) return null;
        if (purchasable.isEmpty()) return null;

        List<GeneRanking> ranked = rankGenes(purchasable, game);

        Collections.sort(ranked);

        List<GeneRanking> highranked = new LinkedList<>();
        int max = ranked.get(0).rank;

        for (GeneRanking rank : ranked) {
            if (rank.rank > max - 10 && rank.rank > 0) highranked.add(rank);
        }

        return weightedRandom(highranked);
    }

    @Override
    public Point selectCellDivision(List<Point> availableLocs, int currentBP, GameView game) {

        if ((currentBP - game.getDivisionCost() >= _option.BPSavedPhase4 && availableLocs.size() > 0) || game.isCloseToGameEnd() ||
                game.getDivisionCost() == 0) {
            List<LocationRanking> ranked = rankSpawnLocations(availableLocs, game);
             return ranked.get(0).loc;
        }

        return null;
    }

    @Override
    public Amoeba selectHealing1Target(List<Amoeba> amoebas, int currentBP, GameView game) {
        Player player = game.getPlayer();
        int cost = game.getDivisionCost();

        if (currentBP >= cost + _option.BPSavedPhase4 && game.getRemainingAmoebas() > 0) return null;

        if (currentBP < 3 + _option.BPSavedPhase4 && !game.isCloseToGameEnd()) return null;

        for (Amoeba amoeba : amoebas) {
            if (game.isDead(amoeba) && amoeba.getDP() <= 3) {
                return amoeba;
            }
        }
        
        return null;
    }

    @Override
    public Amoeba selectHealing2Target(List<Amoeba> amoebas, int currentBP, GameView game) {
        return null;
    }

    @Override
    public boolean selectBANG(Point loc, GameView game) {
        Player player = game.getPlayer();
        for (Amoeba amoeba : game.getAmoebasAt(loc)) {
            //If their is any amoeba that is not dead, and is not its own, explode
            if (!game.isDead(amoeba) && amoeba.playerID != player.getID()) return true;
        }

        return false;
    }

    @Override
    public boolean selectHOL(Amoeba amoeba, DIRECTION dir, GameView game) {
        //when chasing HOLD, definately chase
        if (game.isChasingESCByHOLD() && game.isESCchasableByHOLD()) return true;

        double HOLused = rankMovementDirection(amoeba.getLocation(), dir, game);
        double HOLunused = rankMovementDirection(amoeba.getLocation(), DIRECTION.NONE, game);

        //true if destination is better and no SFS threat present
        return (HOLused > HOLunused && !game.isSFSthreatAt(game.getDestination(amoeba.getLocation(), dir)));
    }

    @Override
    public boolean selectPOP(int currentBP, int spawnableNumber, GameView game) {
        Player top = game.getPlayerByOrder(0);
        Player player = game.getPlayer();

        if (player.getID() == top.getID()) return false;
        if (game.getPhase4Income() + currentBP < game.getDivisionCost() * (top.getNumAmoebas() - player.getNumAmoebas())) return true;

        return false;
    }

    @Override
    public Amoeba selectAGR(List<Amoeba> AGRtargets, int currentBP, GameView game) {
        if (currentBP < 1 || AGRtargets.isEmpty()) return null;

        List<AmoebaRanking> ranked = rankAGRTargets(AGRtargets, game);

        if (ranked.get(0).rank < 0) return null;

        return ranked.get(0).target;
    }

    @Override
    public boolean selectESC(Player attacker, Amoeba attackerAmoeba, Player defender, Amoeba defenderAmoeba, GameView game) {
        return !(attacker.hasGene(GENE.HOL) && game.isESCchasableByHOLD());
    }

    @Override
    public boolean selectDEF(Player attacker, Amoeba attackerAmoeba, Player defender, Amoeba defenderAmoeba, GameView game) {
        return true;
    }

    @Override
    public boolean selectPayHARD(Player attacker, Amoeba attackerAmoeba, Player defender, Amoeba defenderAmoeba, GameView game) {
        return true;
    }

    @Override
    public DIRECTION selectEscapeDirection(Point loc, List<DIRECTION> availableDirs, GameView game) {
        return availableDirs.get(0);
    }

    @Override
    public boolean selectESCSPD(Point loc, Point attackerLoc, GameView game) {
        return loc.equals(attackerLoc);
    }

    @Override
    public boolean decideSFSBANG(Point loc, GameView game) {
        return true;
    }

    @Override
    public void nextEnvironment(EnvironmentCard card) {
        //NOP
    }

    public boolean decideSFS(Amoeba attacker, Point loc, GameView game) {
        List<Amoeba> targets = game.getSFSTargets(loc, attacker.number, true);
        List<AmoebaRanking> ranked = rankSFSTargets(attacker, targets, game);

        return ranked.size() > 0 && ranked.get(0).rank >= 0;
    }

    protected boolean purchaseDecision(int currentBP, GameView game) {
        Player player = game.getPlayer();

        //Defense is high priority
        if (game.isSFSinPlay() && !game.hasDefense()) return true;

        //Near end
        if (game.isCloseToGameEnd() && player.getNumGenesForScore() < 6) return true;


        //MP check
        if (player.getMP() > _option.maxMP + 2) return false;

//random by available BP
        //First gene, 50% chance
        if (game.getCurrentRound() == 1) {
            return _rand.nextBoolean();
        }


        return true;
    }

    protected GENE weightedRandom(List<GeneRanking> ranked) {
        if (ranked.isEmpty()) return null;

        List<GENE> genes = new LinkedList<>();
        List<Integer> scans = new LinkedList<>();

        int scan = 0;
        //calculate the scan
        for (GeneRanking rank : ranked) {
            genes.add(rank.gene);
            scan += rank.rank;
            scans.add(scan);
        }

        if (scan > 0) {
            //select a number from 0 to max scan
            int sel = _rand.nextInt(scan);
            //pick one accorng to the random number
            for (int i = 0; i < scans.size(); i++) {
                if (scans.get(i) >= sel) return genes.get(i);
            }
        }

        Collections.shuffle(genes);
        
        return genes.get(0);
    }

    protected int rankDiscardGene(Gene gene, GameView game) {
        int rank = gene.getMPReleaseValue()*100 / gene.getBPCost();

        if (gene.getCardScoreCount() > 1) return 0;

        //Throw away RAY in the last round
        if (game.isCloseToGameEnd() && gene.gene == GENE.RAY) return 100;

        //some special cases
        Player player = game.getPlayer();
        //POP that may not be useful anymore
        if (gene.gene == GENE.POP && player.getNumAmoebas() >= 4) rank++;

        return rank;
    }

    protected List<GeneRanking> rankDiscardGenes(List<Gene> genes, GameView game) {
        List<GeneRanking> ranked = new LinkedList<>();

        for (Gene gene : genes) {
            int rank = rankDiscardGene(gene, game);
            ranked.add(new GeneRanking(gene.gene, rank));
        }

        Collections.sort(ranked);
        return ranked;
    }

    protected int rankGene(GENE gene, GameView game) {
        int rank = 1;

        Player player = game.getPlayer();
        Gene gn = game.getGene(gene);

        //Near end game, things change
        if (game.isCloseToGameEnd()) {
            //RAY is not necessary anymore
            if (gn.gene == GENE.RAY) return 0;
            //buy buch of cheap genes
            return 100 - gn.getBPCost()*10;
        }

        //SFS makes DEF important
        if (game.isSFSinPlay() && !game.hasDefense()) {
            switch (gene) {
                case ESC:
                    rank+=20;
                    if (player.hasGene(GENE.MV1) || player.hasGene(GENE.MV2)) rank+=2;
                    if (player.hasGene(GENE.STR)) rank+=2;
                    break;
                case DEF:
                    rank+=15;
                    if (game.isESCchasableByHOLD()) rank+=10;
                    break;
                case ALM:
                    rank+=10;
                    if (player.hasGene(GENE.MV1) || player.hasGene(GENE.MV2)) rank+=2;
                    if (player.hasGene(GENE.STR)) rank+=2;
                    break;
                case HARD:
                    rank+=20;
                    break;
                case MOU:
                    rank+=15;
                    break;
                case DIS:
                    rank+=15;
                    if (game.isCAMinPlay()) rank+=1;
                    break;
                case BANG:
                    if (!game.hasDefense() && game.isBANGusableAgainstSFS()) rank+=20;
                    break;

            }
        }

        //normal ranking

        //discourage MP exceeding
        rank += Math.min(_option.maxMP - (player.getMP()+gn.getMPCost()), 0);

        //combination bonus
        switch (gene) {
            //can't use it now
            case ADA:
            case CLE:
                rank = 0;
                break;
            //not what you should by in the beginning
            case INT:
                if (player.getNumGenes() < 2) rank = 0;
                break;
            //not useful unless SFS is around
            case DEF:
            case ESC:
            case ALM:
            case HARD:
            case MOU:
            case DIS:
                if (!game.isSFSinPlay()) rank = 0;
                break;
            case POP:
                //not useful most of the time
                rank = 0;
                //when there is significant difference in number of amoebas, get it
                if (game.getPlayerByOrder(0).getNumAmoebas() - player.getNumAmoebas() >= 3) rank+=5;
                if (game.getPlayerByOrder(0).getNumAmoebas() - player.getNumAmoebas() >= 4) rank+=2;
                if (game.getPlayerByOrder(0).getNumAmoebas() - player.getNumAmoebas() >= 5) rank+=2;
                break;
            //always good ones
            case DIV:
            case ENE:
                rank +=2;
            case SPO:
                rank++;
                break;
            case STR:
                if (player.hasGene(GENE.MV1) || player.hasGene(GENE.MV2) || player.hasGene(GENE.SPD)) rank+=5;
                break;
                //MV1 is good
            case MV1:
                rank++;
            case SPD:
                //even better with STR
                if (player.hasGene(GENE.STR)) rank += 5;
                break;
            case ARM:
                if (game.isCAMinPlay()) rank+=5;
                break;
            case SFS:
                if (player.hasGene(GENE.PAR) || player.hasGene(GENE.FRU) || player.hasGene(GENE.SUB) || player.hasGene(GENE.SUC1) || player.hasGene(GENE.SUC2)) rank-= 8;
                if (!game.isSFSinPlay()) rank+= 5;
                //slight encouragement with GLU
                if (player.hasGene(GENE.GLU)) rank++;
                break;
            case CAM1:
            case CAM2:
                if (player.hasGene(gene.SFS) || player.hasGene(gene.AGR)) rank+=5;
                if (!player.hasGene(gene.SFS) && !player.hasGene(gene.AGR)) rank = 0;
                break;
            case TEN:
            case FRU:
            case PAR:
            case SUB:
                if (player.hasGene(GENE.TEN) || player.hasGene(GENE.FRU) || player.hasGene(GENE.PAR) || player.hasGene(GENE.SUB)) rank+=3;
                if (player.hasGene(gene.SFS)) rank -= 8;
                break;
            case GLU:
                //don't buy if there isn't much MP
                if (player.getMP() <= 7) rank = 0;
                //works well with SFS or AGR
                if (player.hasGene(GENE.SFS) || player.hasGene(GENE.AGR)) rank+=2;
                break;
            case RAY:
                //don't buy if there isn't much MP
                if (player.getMP() <= 6) rank = 0;
                break;
            case HOL:
                //pretty important if ESC can be disabled
                if ((player.hasGene(GENE.SFS) || player.hasGene(GENE.AGR)) && game.isESCchasableByHOLD()) rank+=5;
                //not so good with other movement genes or Fast Food
                if (player.hasGene(gene.MV1) || player.hasGene(gene.MV2) || player.hasGene(gene.SPD) || player.hasGene(gene.PER) || player.hasGene(gene.REB) || player.hasGene(gene.FAST)) rank-=5;
                //TEN, PAR, BANG may go well
                if (player.hasGene(gene.TEN) || player.hasGene(gene.PAR) || player.hasGene(gene.BANG)) rank+=3;
                break;
            default:
                break;
        }

        return rank;
    }

    protected List<GeneRanking> rankGenes(List<GENE> genes, GameView game) {
        List<GeneRanking> ranked = new LinkedList<>();

        for (GENE gene : genes) {
            int rank = rankGene(gene, game);
            ranked.add(new GeneRanking(gene, rank));
        }

        Collections.sort(ranked);
        return ranked;
    }

    protected double rankMovementDirection(Point loc, DIRECTION dir, GameView game) {
        double rank = 0.0;

        Point dst = game.getDestination(loc, dir);
        //Food
        if (game.canFeedAt(dst)) rank++;
        if (game.canFeedWithSFS(dst, -1, true)) rank++;
        if (game.canFeedWithSFS(dst, -1, false)) rank++;
        //TOX
        rank -= game.getDPAt(dst)*5;

        return rank;
    }

    protected List<DirectionRanking> rankMovementDirections(Point loc, List<DIRECTION> dirs, GameView game) {
        List<DirectionRanking> ranked = new LinkedList<>();

        for (DIRECTION dir : dirs) {
            ranked.add(new DirectionRanking(dir, rankMovementDirection(loc, dir, game)));
        }

        Collections.sort(ranked);

        return ranked;
    }

    protected double computeMovementSuccessRate(Point loc, GameView game) {
        double rate = 0;

        Player player = game.getPlayer();
        int count = 0;
        //4 directions
        if (game.canFeedAt(game.getDestination(loc, DIRECTION.NORTH))) count++;
        if (game.canFeedAt(game.getDestination(loc, DIRECTION.SOUTH))) count++;
        if (game.canFeedAt(game.getDestination(loc, DIRECTION.WEST))) count++;
        if (game.canFeedAt(game.getDestination(loc, DIRECTION.EAST))) count++;
        //MV2 is success if any of the above work
        if (player.hasGene(GENE.MV2) && count > 0) return 1;
        //rolling 6
        if (count > 0) count++;
        //rolling 5
        if (game.canFeedAt(loc)) count++;

        //with 2 dies, the probability increases
        if (player.hasGene(GENE.MV1)) {
            rate = 1.0 - (1.0 - ((double)count / 6.0))*(1.0 - ((double)count / 6.0));
        //compute probability of one die
        } else {
            rate = (double)count / 6.0;
        }

        //with speed, blindy add 0.2
        if (player.hasGene(GENE.SPD) || player.hasGene(GENE.PER)) {
            rate = Math.min(rate + 0.2, 1.0);
        }

        return rate;
    }


    protected int getNumberOfSurroundingGoodLocations(Point loc, GameView game) {
        int count = 0;

        if (!game.doesHitWall(loc, DIRECTION.NORTH) && game.canFeedAt(game.getDestination(loc, DIRECTION.NORTH))) count++;
        if (!game.doesHitWall(loc, DIRECTION.SOUTH) && game.canFeedAt(game.getDestination(loc, DIRECTION.SOUTH))) count++;
        if (!game.doesHitWall(loc, DIRECTION.WEST) && game.canFeedAt(game.getDestination(loc, DIRECTION.WEST))) count++;
        if (!game.doesHitWall(loc, DIRECTION.EAST) && game.canFeedAt(game.getDestination(loc, DIRECTION.EAST))) count++;

        return count;
    }

    protected double rankSpawnLocation(Point loc, GameView game) {
            Point dst = game.getDestination(loc, game.getCurrentEnvironment().direction);
            double rank = 0.0;
            //Neighbors
            rank += getNumberOfSurroundingGoodLocations(dst,game);
            //Food
            if (game.canFeedAt(dst)) rank++;
            //Dead can be thought as food
            if (game.isDeadAmoebaAt(dst)) rank+=2;
            //TOX
            rank -= game.getDPAt(loc)*5;
            //Distance
            double dist = 0;
            for (Amoeba amoeba  : game.getPlayer().getAmoebas()) {
                dist += Point.distance(amoeba.getLocation().x, amoeba.getLocation().y, loc.x, loc.y);
            }
            if (dist>0) dist /= (7.0*game.getPlayer().getNumAmoebas());
            rank += dist;
            //Randomness
            rank += Math.random();
            
            return rank;
    }

    protected List<LocationRanking> rankSpawnLocations(List<Point> availableLocs, GameView game) {
        List<LocationRanking> ranked = new LinkedList<>();

        for (Point loc : availableLocs) {
            ranked.add(new LocationRanking(loc, rankSpawnLocation(loc, game)));
        }

        Collections.sort(ranked);

        return ranked;
    }

    protected double rankSFSTarget(Amoeba attacker, Amoeba defender, GameView game) {
        double rank = 1;

        Player player = game.getPlayer();
        Player owner = game.getPlayerByID(defender.playerID);
        //Your own
        if (owner.equals(player)) {
            //dead amoeba of your own could be a good target
            if (game.isDead(defender) && player.getNumAmoebas() > 4) {
                rank += 5;
            } else {
                rank -= 10;
            }
        }
        //Owner doen't care about other defenses
        if (owner.equals(player)) return rank;

        //Don't attack bang
        if (owner.hasGene(GENE.BANG) && owner.getBP() > 0) rank-=2;
        //Try to avoid MOU
        if (owner.hasGene(GENE.MOU) && owner.getBP() > 0) rank--;

        //Don't attack HARD unless in dander
        if (owner.hasGene(GENE.HARD) && (owner.getBP() < 2 || attacker.getDP() < 1)) rank-=2;
        //ESC
        if (!game.isESCchasableByHOLD() || !player.hasGene(GENE.HOL)) {
            if (owner.hasGene(GENE.ESC) && owner.getBP() > 0 && !player.hasGene(GENE.CAM2)) rank-=2;
            if (owner.hasGene(GENE.ESC) && owner.hasGene(GENE.STR) && !player.hasGene(GENE.CAM2)) rank-=2;
            //CAM
            if (owner.hasGene(GENE.ESC) && player.hasGene(GENE.CAM1)) {
                rank++;
                //If close to death, take chances
                if (attacker.getDP() == 1) rank++;
            }
            if (owner.hasGene(GENE.ESC) && owner.hasGene(GENE.STR) && player.hasGene(GENE.CAM1)) {
                rank++;
                //If close to death, take chances
                if (attacker.getDP() == 1) rank++;
            }
        }
        //Defense
        if (owner.hasGene(GENE.DEF) && owner.getBP() > 0) rank-=2;
        //CAM
        if (owner.hasGene(GENE.DEF) && player.hasGene(GENE.CAM1)) {
            rank++;
            //If close to death, take chances
            if (attacker.getDP() == 1) rank++;
        }
        if (owner.hasGene(GENE.DEF) && player.hasGene(GENE.CAM2)) rank+=2;

        return rank;
    }

    protected List<AmoebaRanking> rankSFSTargets(Amoeba attacker, List<Amoeba> targets, GameView game) {
        List<AmoebaRanking> ranked = new LinkedList<>();

        for (Amoeba target : targets) {
            ranked.add(new AmoebaRanking(target, rankSFSTarget(attacker, target, game)));
        }

        Collections.sort(ranked);

        return ranked;
    }

    protected double rankAGRTarget(Amoeba defender, GameView game) {
        double rank = 0.0;

        Player player = game.getPlayer();
        Player owner = game.getPlayerByID(defender.playerID);
        //don't attack dead amoeba
        if (game.isDead(defender)) rank = -10000;
        //Your own
        if (owner.equals(player)) rank -= 10000;
        //Defense
        if (owner.hasGene(GENE.DEF) && owner.getBP() > 0) rank-=2;
        if (owner.hasGene(GENE.BANG) && owner.getBP() > 0) rank--;
        if (owner.hasGene(GENE.HARD)) rank-=2;
        //ESC
        if (!game.isESCchasableByHOLD() || !player.hasGene(GENE.HOL)) {
            if (owner.hasGene(GENE.ESC) && owner.getBP() > 0 && !player.hasGene(GENE.CAM2)) rank-=2;
            if (owner.hasGene(GENE.ESC) && owner.hasGene(GENE.STR) && !player.hasGene(GENE.CAM2)) rank-=2;
            //CAM
            if (owner.hasGene(GENE.ESC) && player.hasGene(GENE.CAM1)) rank++;
            if (owner.hasGene(GENE.ESC) && owner.hasGene(GENE.STR) && player.hasGene(GENE.CAM1)) rank++;
        }
        //CAM
        if (owner.hasGene(GENE.DEF) && player.hasGene(GENE.CAM1)) rank++;
        if (owner.hasGene(GENE.DEF) && player.hasGene(GENE.CAM2)) rank+=2;
        //top player and number of amoebas play a roll
        rank += (owner.getAdvance() - game.getPlayerByOrder(game.getPlayers().size()-1).getAdvance());
        rank += owner.getNumAmoebas();

        return rank;
    }

    protected List<AmoebaRanking> rankAGRTargets(List<Amoeba> targets, GameView game) {
        List<AmoebaRanking> ranked = new LinkedList<>();

        for (Amoeba target : targets) {
            ranked.add(new AmoebaRanking(target, rankAGRTarget(target, game)));
        }

        Collections.sort(ranked);

        return ranked;
    }

    protected class GeneRanking implements Comparable<GeneRanking> {
        public final GENE gene;
        public final int rank;
        
        public GeneRanking(GENE gene, int rank) {
            this.gene = gene;
            this.rank = Math.max(rank, 0);
        }

        @Override
        public int compareTo(GeneRanking o) {
            if (this.rank > o.rank) return -1;
            if (this.rank < o.rank) return 1;
            return 0;
        }
    }

    protected class AmoebaRanking implements Comparable<AmoebaRanking> {
        public final Amoeba target;
        public final double rank;

        public AmoebaRanking(Amoeba target, double rank) {
            this.target = target;
            this.rank = rank;
        }

        @Override
        public int compareTo(AmoebaRanking o) {
            if (this.rank > o.rank) return -1;
            if (this.rank < o.rank) return 1;
            return 0;
        }

    }

    protected class LocationRanking implements Comparable<LocationRanking> {
        public final Point loc;
        public final double rank;

        public LocationRanking(Point loc, double rank) {
            this.loc = loc;
            this.rank = rank;
        }

        @Override
        public int compareTo(LocationRanking o) {
            if (this.rank > o.rank) return -1;
            if (this.rank < o.rank) return 1;
            return 0;
        }

    }

    protected class DirectionRanking implements Comparable<DirectionRanking> {
        public final DIRECTION dir;
        public final double rank;

        public DirectionRanking(DIRECTION dir, double rank) {
            this.dir = dir;
            this.rank = rank;
        }

        @Override
        public int compareTo(DirectionRanking o) {
            if (this.rank > o.rank) return -1;
            if (this.rank < o.rank) return 1;
            return 0;
        }

    }
}
