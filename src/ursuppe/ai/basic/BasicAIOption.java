package ursuppe.ai.basic;

/**
 *
 * @author Personal
 */
public class BasicAIOption {
    public double moveProbabilityLB;
    public int BPSavedPhase4;
    public int maxMP;

    public BasicAIOption() {
        moveProbabilityLB = 0.5;
        BPSavedPhase4 = 4;
        maxMP = 10;
    }

    public BasicAIOption(int BPP4) {
        BPSavedPhase4 = BPP4;
    }
}
