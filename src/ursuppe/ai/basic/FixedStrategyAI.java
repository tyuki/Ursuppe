package ursuppe.ai.basic;

import java.util.List;
import ursuppe.ai.GameView;
import ursuppe.game.GenePool.GENE;

/**
 *
 * @author Personal
 */
public class FixedStrategyAI extends BasicAI {

    public static enum STRATEGY {
        MV1STR
    }

    public FixedStrategyAI() {
        super(new BasicAIOption());
    }

    @Override
    public GENE selectGENEPurchase(List<GENE> purchasable, int currentBP, GameView game) {
        return null;
    }

    
}
