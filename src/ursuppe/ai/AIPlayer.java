package ursuppe.ai;

import ursuppe.ai.basic.BasicAI;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import ursuppe.ai.basic.BasicAIOption;
import ursuppe.control.Organizer;
import ursuppe.control.UrsuppeController;
import ursuppe.game.Amoeba;
import ursuppe.game.EnvironmentCard;
import ursuppe.game.GameState;
import ursuppe.game.GameState.ATTACK_RESPONSE;
import ursuppe.game.GameState.DIRECTION;
import ursuppe.game.GameState.PHASE1_ACTION;
import ursuppe.game.Gene;
import ursuppe.game.GenePool.GENE;
import ursuppe.game.Player;
import ursuppe.game.Progress;
import ursuppe.game.Soup;
import ursuppe.gui.icon.IconContainer;

/**
 *
 * @author Personal
 */
public class AIPlayer implements UrsuppeController {

    private final AIContainer container;
    
    protected Organizer _organizer;
    protected final String _name;
    protected int _playerID;
    protected AIInterface _interface;

    protected int _wait = 500;

    protected AIPlayer(Organizer organizer, String name, int playerID) {
        container = null;
        _organizer = organizer;
        _name = name;
        _playerID = playerID;
        _interface = new BasicAI(new BasicAIOption());
    }

    public static final AIPlayer getPlayerSubstitute(Organizer organizer, String name, int playerID) {
        return new AIPlayer(organizer, name, playerID);
    }

    public AIPlayer(AIContainer container, String name, int port) {
        this.container = container;
        _interface = new BasicAI(new BasicAIOption());
        _name = name;
        _organizer = new Organizer();
        _organizer.setController(this);

        //Amoeba Icons
        List<String> amoebaIcons = new LinkedList<>();
        amoebaIcons.addAll(IconContainer.getCustomIconNames());
        List<String> rmAList = new LinkedList<String>(){{add(IconContainer.DEFAULT_CUSTOM_ICON);}};
        for (String iconName : amoebaIcons) {
            if (iconName.matches(IconContainer.CUSTOM_ICON_PREFIX+"\\d+")) rmAList.add(iconName);
        }
        amoebaIcons.removeAll(rmAList);
        amoebaIcons.add(null);
        

        //Player Background
        List<String> playerBackgrounds = new LinkedList<>();
        playerBackgrounds.addAll(IconContainer.getCustomPlayerBackgroundNames());
        List<String> rmBList = new LinkedList<String>(){{add(IconContainer.DEFAULT_BACKGROUND);}};
        for (String bgName : playerBackgrounds) {
            if (bgName.matches(IconContainer.CUSTOM_BACKGROUND_PREFIX+"\\d+")) rmBList.add(bgName);
        }
        playerBackgrounds.removeAll(rmBList);
        playerBackgrounds.add(null);

        Collections.shuffle(amoebaIcons);
        Collections.shuffle(playerBackgrounds);

        _organizer.connectServer(_name, "127.0.0.1", port, true, amoebaIcons.get(0), playerBackgrounds.get(0), -1, -1);
    }

    public void disconnect() {
        _organizer.disconnectFromServer();
    }

    @Override
    public void setID(int id) {
        _playerID = id;
    }

    public String getName() {
        return _name;
    }

    @Override
    public void gameReset() {
        if (container != null) {
            _organizer.disconnectFromServer();
            container.selfDestruct(this);
        }
    }

    private void AIwait() {
        if (_wait <= 0) return;
        try {
            Thread.sleep(_wait);
        } catch (InterruptedException ex) {
        }
    }

    @Override
    public void selectInitialScore(int playerID, GameState game) {
        if (playerID != _playerID) return;
        AIwait();
        GameView gview = new GameView(game, _playerID);

        List<Integer> cand = new LinkedList<>();
        boolean[] availability = game.getAvailableInitialScores();
        for (int i = 0; i < availability.length; i++) {
            if (availability[i]) cand.add(i);
        }

        cand = Collections.unmodifiableList(cand);

        int num = _interface.selectInitialScore(cand, gview);

        //Check validity
        if (cand.contains(num)) {
            _organizer.initialScoreSelected(num);
        //If invalid, pick one
        } else {
            Collections.sort(cand);
            _organizer.initialScoreSelected(cand.get(0));
        }
    }

    @Override
    public void selectInitialAmoeba(int playerID, GameState game) {
        if (playerID != _playerID) return;
        AIwait();
        GameView gview = new GameView(game, _playerID);
        
        //Ask for location
        List<Point> available = getInitialPlacementAvailability(game);

        Point loc = _interface.selectInitialAmoebaLocation(Collections.unmodifiableList(available), gview);

        //When the return value is invalid, pick here
        if (loc == null || !available.contains(loc)) {
            //Random choice
            Collections.shuffle(available);
            loc = available.get(0);
        }

        //Ask for number
        List<Integer> nums = game.getRemainingAmoebaNumbers(playerID);
        int num = _interface.selectAmoebaNum(Collections.unmodifiableList(nums), loc, gview);

        //When invalid, pick the youngest
        if (!nums.contains(num)) {
            num = nums.get(0);
        }

        _organizer.initialAmoebaSelected(num, loc);
    }

    @Override
    public void selectPhase1Action(int playerID, int amoebaIndex, GameState game) {
        if (playerID != _playerID) return;
        AIwait();
        GameView gview = new GameView(game, _playerID);

        Player player = game.getCurrentPlayer();
        Amoeba amoeba = game.getCurrentAmoeba();
        
        List<PHASE1_ACTION> actions = game.getAvailablePhase1Actions(playerID, amoebaIndex);

        PHASE1_ACTION selected = _interface.selectPhase1Action(amoeba, actions, gview);

        Point srcLoc = amoeba.getLocation();
        int[] TENfoods = null;
        boolean useREB = false;
        Point exLoc = null;
        boolean useTOX = false;

        switch (selected) {
            case DRIFT:
                Point dstLoc = game.getDestiation(srcLoc, game.getCurrentEnvironment().direction);
                //Rebound
                if (player.hasGene(GENE.REB) && game.doesHitWall(amoeba.getLocation(), game.getCurrentEnvironment().direction)) {
                    useREB = _interface.selectREB(amoeba.getLocation(), game.getCurrentEnvironment().direction, gview);
                }
                //Tentacle
                if (player.hasGene(GENE.TEN)) {
                    DIRECTION dir = game.getCurrentEnvironment().direction;
                    //REB
                    if (useREB) dir = game.getOppositeDirection(dir);
                    //Get food if not moving to a wall
                    if (!game.doesHitWall(srcLoc, dir) && dir != DIRECTION.NONE) {
                        TENfoods = _interface.selectTENfoods(gview.getFoodColors(), gview.getFoodAt(srcLoc), gview.getFoodAt(dstLoc), srcLoc, dstLoc, gview);
                    }
                }
                _organizer.amoebaDriftSelected(TENfoods, useREB);
                break;
            case MOVE:
                _organizer.amoebaMoveSelected();
                break;
            case HOLD:
                _organizer.HOLSelected();
                break;
            case EAT_WITH_CHOICE:
            case EAT_WITH_GENES:
            case EAT:
                //SUC
                Point sucLoc = null;
                int[] sucFoods = null;
                int[] foods = null;
                int[] parIDs = null;

                //Eat with choice/genes specific
                if (selected != PHASE1_ACTION.EAT) {
                    if (player.hasGene(GENE.SUC1) || player.hasGene(GENE.SUC2)) {
                        List<Point> sucLocs = getSuctionLocations(game);
                        sucLoc = _interface.selectSUCLocation(srcLoc, Collections.unmodifiableList(sucLocs), gview);
                    }
                    if (sucLoc != null) sucFoods = _interface.selectSUCFoods(srcLoc, sucLoc, game.getSoup().getFoodColors(), game.getSoup().getCell(sucLoc).getFood(), gview);
                    //Select food
                    List<Integer> parTargets = new LinkedList<>();
                    foods = _interface.selectFoods(srcLoc, gview.getFoodColors(), game.getSoup().getCell(srcLoc).getFood(), sucFoods, gview, parTargets);
                    if (parTargets.size() > 0) {
                        parIDs = new int[parTargets.size()];
                        //convert to array
                        for (int i = 0; i < parTargets.size(); i++) parIDs[i] = parTargets.get(i);
                    }
                }
                
                //CLE select
                List<Point> CLEtarget = getAvailableDestination(game, player, amoeba.getLocation(),
                    new ArrayList<DIRECTION>(){{add(DIRECTION.NONE); add(DIRECTION.NORTH); add(DIRECTION.SOUTH); add(DIRECTION.EAST); add(DIRECTION.WEST);}});
                if (player.hasGene(GENE.CLE)) exLoc = _interface.selectExcreteDestination(CLEtarget, gview);
                //TOX + CLE
                if (exLoc != null && player.hasGene(GENE.TOX)) useTOX = _interface.selectTOX(exLoc, gview);
                //TOX only
                if (exLoc == null && player.hasGene(GENE.TOX)) useTOX = _interface.selectTOX(amoeba.getLocation(), gview);

                //EAT
                if (selected == PHASE1_ACTION.EAT) {
                    _organizer.eatSelected(exLoc, useTOX);
                //EAT_CHOICE / EAT_GENES
                } else {
                    //Auto eat
                    if (foods == null) {
                        _organizer.autoEat(player, amoeba, sucLoc, sucFoods, exLoc, useTOX);
                    } else {
                        _organizer.eatSelected(foods, parIDs, sucLoc, sucFoods, exLoc, useTOX);
                    }
                }
                break;
            case SFS:
                Amoeba target = _interface.selectSFStarget(amoeba, gview.getPlayers(), gview.getSFSTargets(amoeba.getLocation(), amoeba.number, true), gview);
                _organizer.SFSSelected(target.playerID, target.number);
                break;
            case SPEED:
                if (_interface.selectSPD(srcLoc, gview)) {
                    _organizer.amoebaMoveSelected();
                } else {
                    _organizer.cancelSPDSelected();
                }
                break;
            case STARVE:
                _organizer.starveSelected();
                break;

                
        }
    }

    @Override
    public void moveResponse(int playerID, int amoebaIndex, List<DIRECTION> dirs, GameState game) {
        if (playerID != _playerID) return;
        AIwait();
        GameView gview = new GameView(game, _playerID);

        Player current;
        Amoeba amoeba;

        switch (game.getProgress().diceState) {
            case NONE:
            case MOVE:
            case MOVE_MV1:
            case MOVE_MV2:
                current = game.getCurrentPlayer();
                amoeba = game.getCurrentAmoeba();
                break;
            case ESC:
            case ESC_MV1:
            case ESC_MV2:
                current = game.getPlayerByID(game.getProgress().defenderID);
                amoeba = current.getAmoebaByNumber(game.getProgress().defenderAmoebaNum);
                break;
            default:
                throw new RuntimeException("Unexpected DICE_STATE: " + game.getProgress().diceState);
        }

        //Select direction
        DIRECTION dir = _interface.selectMovementDirection(amoeba.getLocation(), dirs, gview);
        if (dir == null) dir = dirs.get(0);

        int[] TENfoods = null;
        boolean useREB = false;

        //Rebound
        if (current.hasGene(GENE.REB) && game.doesHitWall(amoeba.getLocation(), dir)) {
            useREB = _interface.selectREB(amoeba.getLocation(), dir, gview);
        }
        //Tentacle
        if (current.hasGene(GENE.TEN)) {
            DIRECTION TENdir = dir;
            //REB
            if (useREB) TENdir = game.getOppositeDirection(TENdir);
            //Get food if not moving to a wall
            Point srcLoc = amoeba.getLocation();
            Point dstLoc = game.getDestiation(srcLoc, TENdir);
            if (!game.doesHitWall(srcLoc, TENdir) && TENdir != DIRECTION.NONE) {
                TENfoods = _interface.selectTENfoods(gview.getFoodColors(), gview.getFoodAt(srcLoc), gview.getFoodAt(dstLoc), srcLoc, dstLoc, gview);
            }
        }

        _organizer.amoebaMoveSelected(dir, TENfoods, useREB);

    }


    @Override
    public void handleGeneDefect(int playerID, GameState game) {
        if (playerID != _playerID) return;
        GameView gview = new GameView(game, _playerID);
        Player current = game.getCurrentPlayer();
        int maxMP = game.getCurrentEnvironment().maxMP;
        int exceeding = current.getMP() - maxMP;

        _organizer.geneDefectsHandled(_interface.handleGeneDeffects(exceeding, current.getGenes(), current.getBP(), gview));
    }


    @Override
    public void selectPhase3Action(int playerID, GameState game) {
        if (playerID != _playerID) return;
        AIwait();

        GameView gview = new GameView(game, _playerID);
        Player player = game.getCurrentPlayer();

        //ADA
        if (player.hasGene(GENE.ADA)) {
            Map<GENE, List<GENE>> exchangable = new HashMap<>();
            
            //Get the list of exchangable genes for each card in hand
            for (Gene hand : player.getGenes()) {
                Map<GENE, Boolean> exgenes = game.getExchangableGenes(player, hand.gene);
                List<GENE> exList = new LinkedList<>();
                //add all exchangable genes
                for (GENE exgn : exgenes.keySet()) {
                    if (exgenes.get(exgn)) exList.add(exgn);
                }
                //put it intot the map
                exchangable.put(hand.gene, exList);
            }

            //ask the interface
            GENE[] exRes = _interface.selectADAExchange(exchangable, player.getBP(), gview);
            //verify
            if (exRes != null && exRes.length == 2 && exchangable.containsKey(exRes[0]) && exchangable.get(exRes[0]).contains(exRes[1])) {
                _organizer.ADASelected(exRes[0], exRes[1]);
            }
            
        }

        //Normal Purchase
        Map<GENE, Boolean> genes = game.getPurchasableGenes(player);
        List<GENE> purchasable = new LinkedList<>();
        for (GENE gene : genes.keySet()) {
            if (genes.get(gene)) purchasable.add(gene);
        }

        if (purchasable.isEmpty()) {
            _organizer.endGenePurchase();
            return;
        }

        GENE gene = _interface.selectGENEPurchase(purchasable, playerID, gview);

        if (gene != null) {
            _organizer.genePurchase(gene);
        } else {
            _organizer.endGenePurchase();
        }
    }

    @Override
    public void selectPhase4Action(int playerID, GameState game) {
        if (playerID != _playerID) return;
        AIwait();

        GameView gview = new GameView(game, _playerID);
        Player player = game.getCurrentPlayer();

        //Healing 1
        if ((player.hasGene(GENE.HEA1) || player.hasGene(GENE.HEA2)) && player.getBP() > 2) {
            List<Amoeba> hea1targets = new LinkedList<>();
            for (Amoeba amoeba : player.getAmoebas()) {
                if (amoeba.getDP() > 0) hea1targets.add(amoeba);
            }
            Amoeba heal1 = _interface.selectHealing1Target(hea1targets, player.getBP(), gview);
            //When healing1 selected
            if (heal1 != null) {
                //Healing 2
                Amoeba heal2 = null;
                if (player.hasGene(GENE.HEA2) && player.getBP() > 3) {
                    List<Amoeba> hea2targets = new LinkedList<>();
                    //Amoebas in the same cell
                    for (Amoeba amoeba : game.getSoup().getCell(heal1.getLocation()).getAmoebas()) {
                        if (!amoeba.equals(heal1)) hea2targets.add(amoeba);
                    }
                    
                     heal2 = _interface.selectHealing2Target(hea2targets, player.getBP(), gview);
                }
                //Use healing
                _organizer.HEALselected(heal1, heal2);
                return;
            }
        }


        List<Point> spawnable = getSpawnableLocations(game);

        //Cell division
        Point spawnLoc = null;
        if (spawnable.size() > 0) spawnLoc = _interface.selectCellDivision(spawnable, player.getBP(), gview);
        if (spawnLoc != null) {
            int num = _interface.selectAmoebaNum(game.getRemainingAmoebaNumbers(playerID), spawnLoc, gview);
            _organizer.cellDivision(num, spawnLoc);
            return;
        }
        
        _organizer.endCellDivision();
    }

    @Override
    public void death(int playerID, int amoebaID, GameState game) {
        if (playerID != _playerID) return;

        GameView gview = new GameView(game, _playerID);
        Player player = game.getPlayerByID(playerID);

        boolean useBANG = false;
        if (player.hasGene(GENE.BANG)) {
            useBANG = _interface.selectBANG(game.getProgress().BANGlocation, gview);
        }
        
        _organizer.deathAck(useBANG);
    }

    @Override
    public void scoring(int playerID, GameState game) {
        if (playerID != _playerID) return;
    }


    @Override
    public void decideHOL(int playerID, int amoebaIndex, DIRECTION dir, GameState game) {
        if (playerID != _playerID) return;
        AIwait();

        GameView gview = new GameView(game, _playerID);

        Player player = game.getPlayerByID(game.getProgress().HOLPlayerID);
        Amoeba amoeba = player.getAmoebas().get(game.getProgress().HOLAmoebaIndex);

        boolean useHOL = _interface.selectHOL(amoeba, dir, gview);


        int[] TENfoods = null;
        //Tentacle
        if (player.hasGene(GENE.TEN)) {
            //Get food if not moving to a wall
            Point srcLoc = amoeba.getLocation();
            Point dstLoc = game.getDestiation(srcLoc, dir);
            if (!game.doesHitWall(srcLoc, dir) && dir != DIRECTION.NONE) {
                TENfoods = _interface.selectTENfoods(gview.getFoodColors(), gview.getFoodAt(srcLoc), gview.getFoodAt(dstLoc), srcLoc, dstLoc, gview);
            }
        }
        
        _organizer.HOLSelected(useHOL, TENfoods);
    }

    @Override
    public void decidePOP(int playerID, GameState game) {
        if (playerID != _playerID) return;

        GameView gview = new GameView(game, _playerID);
        Player player = game.getCurrentPlayer();
        Player top = game.getPlayerByOrder(0);

        _organizer.POPdecided(_interface.selectPOP(player.getBP(), top.getNumAmoebas() - player.getNumAmoebas(), gview));
    }

    @Override
    public void decideAGR(int playerID, GameState game) {
        if (playerID != _playerID) return;
        
        GameView gview = new GameView(game, _playerID);
        Player player = game.getCurrentPlayer();
        List<Amoeba> usable = game.getAGRusableAmoebas(player);

        //Not usable
        if (usable.isEmpty()) {
            _organizer.AGRCanceled();
            return;
        }

        List<Amoeba> targets = new LinkedList<>();
        //For each usable amoeba
        for (Amoeba attacker : usable) {
            //for each potential target
            for (Amoeba defender : gview.getAmoebasAt(attacker.getLocation())) {
                //Skip your self
                if (attacker.equals(defender)) continue;
                //See if it is attackable
                Player owner = game.getPlayerByID(defender.playerID);
                List<ATTACK_RESPONSE> atkRes = game.getAttackResponse(player, attacker, owner, defender);
                //If attack is not blocked, add as a target
                if (!atkRes.contains(ATTACK_RESPONSE.BLOCKED))  targets.add(defender);
            }
        }

        //select target
        Amoeba attacked = _interface.selectAGR(targets, player.getBP(), gview);

        //no AGR
        if (attacked == null || !targets.contains(attacked)) {
            _organizer.AGRCanceled();
        //Yse AGR
        } else {
            for (Amoeba attacker : usable) {
                if (attacker.getLocation().equals(attacked.getLocation())) {
                    _organizer.AGRSelected(attacker.number, attacked.playerID, attacked.number);
                    return;
                }
            }
            _organizer.AGRCanceled();
        }
    }


    @Override
    public void defenderAction(int attackerID, int attackerAmoebaNum, int defenderID, int defenderAmoebaNum, GameState game) {
        if (defenderID != _playerID) return;

        GameView gview = new GameView(game, _playerID);
        Player attacker = game.getPlayerByID(attackerID);
        Amoeba attackerAmoeba = attacker.getAmoebaByNumber(attackerAmoebaNum);

        Player defender = game.getPlayerByID(defenderID);
        Amoeba defenderAmoeba = defender.getAmoebaByNumber(defenderAmoebaNum);

        //Get list of defense
        List<ATTACK_RESPONSE> atkRes = game.getAttackResponse(attacker, attackerAmoeba, defender, defenderAmoeba);

        //Select Escape
        if (atkRes.contains(ATTACK_RESPONSE.ESCAPE) && _interface.selectESC(attacker, attackerAmoeba, defender, defenderAmoeba, gview)) {
            _organizer.ESCSelected();
            return;
        }

        //Select Defendse
        if (atkRes.contains(ATTACK_RESPONSE.DEFENSE) && _interface.selectDEF(attacker, attackerAmoeba, defender, defenderAmoeba, gview)) {
            _organizer.DEFSelected();
            return;
        }

        //_organizer.ESCDirectionSelected(DIRECTION.WEST, TENfoods, true)
        //_organizer
        
        _organizer.defenderGiveup();
    }

    @Override
    public void attackerAction(int attackerID, int attackerAmoebaNum, int defenderID, int defenderAmoebaNum, boolean HARD, boolean MOUexcretes, GameState game) {
        if (attackerID != _playerID) return;

        GameView gview = new GameView(game, _playerID);
        Player attacker = game.getPlayerByID(attackerID);
        Amoeba attackerAmoeba = attacker.getAmoebaByNumber(attackerAmoebaNum);

        Player defender = game.getPlayerByID(defenderID);
        Amoeba defenderAmoeba = defender.getAmoebaByNumber(defenderAmoebaNum);
        
        boolean payHARD = false;
        Point exLoc = null;
        boolean useTOX = false;


        if (HARD) {
            payHARD = _interface.selectPayHARD(attacker, attackerAmoeba, defender, defenderAmoeba, gview);
        }

        if ((HARD && payHARD) || !HARD) {
            //CLE select
            List<Point> CLEtarget = getAvailableDestination(game, attacker, attackerAmoeba.getLocation(),
                new ArrayList<DIRECTION>(){{add(DIRECTION.NONE); add(DIRECTION.NORTH); add(DIRECTION.SOUTH); add(DIRECTION.EAST); add(DIRECTION.WEST);}});
            if (attacker.hasGene(GENE.CLE)) exLoc = _interface.selectExcreteDestination(CLEtarget, gview);
            //TOX + CLE
            if (exLoc != null && attacker.hasGene(GENE.TOX)) useTOX = _interface.selectTOX(exLoc, gview);
            //TOX only
            if (exLoc == null && attacker.hasGene(GENE.TOX)) useTOX = _interface.selectTOX(attackerAmoeba.getLocation(), gview);
        }

        _organizer.attackerActionSelected(payHARD, exLoc, useTOX);
    }

    @Override
    public void selectEscapeDirection(int defenderID, int defenderAmoebaNum, List<DIRECTION> dirs, GameState game) {
        if (defenderID != _playerID) return;

        GameView gview = new GameView(game, _playerID);

        Player current;
        Amoeba amoeba;

        switch (game.getProgress().diceState) {
            case NONE:
            case MOVE:
            case MOVE_MV1:
            case MOVE_MV2:
                current = game.getCurrentPlayer();
                amoeba = game.getCurrentAmoeba();
                break;
            case ESC:
            case ESC_MV1:
            case ESC_MV2:
                current = game.getPlayerByID(game.getProgress().defenderID);
                amoeba = current.getAmoebaByNumber(game.getProgress().defenderAmoebaNum);
                break;
            default:
                throw new RuntimeException("Unexpected DICE_STATE: " + game.getProgress().diceState);
        }

        //Select direction
        DIRECTION dir = _interface.selectEscapeDirection(amoeba.getLocation(), dirs, gview);
        if (dir == null) dir = dirs.get(0);

        int[] TENfoods = null;
        boolean useREB = false;

        //Rebound
        if (current.hasGene(GENE.REB) && game.doesHitWall(amoeba.getLocation(), dir)) {
            useREB = _interface.selectREB(amoeba.getLocation(), dir, gview);
        }
        //Tentacle
        if (current.hasGene(GENE.TEN)) {
            DIRECTION TENdir = dir;
            //REB
            if (useREB) TENdir = game.getOppositeDirection(TENdir);
            //Get food if not moving to a wall
            Point srcLoc = amoeba.getLocation();
            Point dstLoc = game.getDestiation(srcLoc, TENdir);
            if (!game.doesHitWall(srcLoc, TENdir) && TENdir != DIRECTION.NONE) {
                TENfoods = _interface.selectTENfoods(gview.getFoodColors(), gview.getFoodAt(srcLoc), gview.getFoodAt(dstLoc), srcLoc, dstLoc, gview);
            }
        }
        
        _organizer.ESCDirectionSelected(dir, TENfoods, useREB);
    }

    @Override
    public void decideESCSPD(int defenderID, GameState game) {
        if (defenderID != _playerID) return;

        GameView gview = new GameView(game, _playerID);

        Progress progress = game.getProgress();
        Player attacker = game.getPlayerByID(progress.attackerID);
        Amoeba attackerAmoeba = attacker.getAmoebaByNumber(progress.attackerAmoebaNum);

        Player defender = game.getPlayerByID(defenderID);
        Amoeba defenderAmoeba = defender.getAmoebaByNumber(progress.defenderAmoebaNum);
        

        if (_interface.selectESCSPD(defenderAmoeba.getLocation(), attackerAmoeba.getLocation(), gview)) {
            _organizer.ESCSelected();
        } else {
            _organizer.ESCAck();
        }
    }

    @Override
    public void decideSFSBANG(int defenderID, int defenderAmoebaNum, GameState game) {
        if (defenderID != _playerID) return;

        GameView gview = new GameView(game, _playerID);
        Progress progress = game.getProgress();

        Player defender = game.getPlayerByID(defenderID);
        Amoeba defenderAmoeba = defender.getAmoebaByNumber(progress.defenderAmoebaNum);

        _organizer.SFSAck(_interface.decideSFSBANG(defenderAmoeba.getLocation(), gview));
    }

    @Override
    public void showNextEnvironment(EnvironmentCard env) {
        _interface.nextEnvironment(env);
    }





    /**
     * Returns list of Points where initial amoebas can be placed
     *
     * @return
     */
    private List<Point> getInitialPlacementAvailability(GameState game) {
        boolean[][] available = game.getInitialPlacementAvailability();
        List<Point> cand = new LinkedList<>();
        for (int x = 0; x < Soup.SOUP_WIDTH; x++) {
            for (int y = 0; y < Soup.SOUP_HEIGHT; y++) {
                if (available[x][y]) {
                    cand.add(new Point(x,y));
                }
            }
        }
        return cand;
    }

    /**
     * Returns list of Points where current player can place new amoebas
     *
     * @return
     */
    private List<Point> getSpawnableLocations(GameState game) {
        boolean[][] available = game.getSpawnableLocations(game.getCurrentPlayer());
        List<Point> cand = new LinkedList<>();
        for (int x = 0; x < Soup.SOUP_WIDTH; x++) {
            for (int y = 0; y < Soup.SOUP_HEIGHT; y++) {
                if (available[x][y]) {
                    cand.add(new Point(x,y));
                }
            }
        }
        return cand;
    }

    /**
     * Returns list points where foods can be taken with Suction.
     *
     * @return
     */
    private List<Point> getSuctionLocations(GameState game) {
        boolean[][] available = game.getSuctionLocation(game.getCurrentAmoeba().getLocation());
        List<Point> cand = new LinkedList<>();
        for (int x = 0; x < Soup.SOUP_WIDTH; x++) {
            for (int y = 0; y < Soup.SOUP_HEIGHT; y++) {
                if (available[x][y]) {
                    cand.add(new Point(x,y));
                }
            }
        }
        return cand;
    }

    private List<Point> getAvailableDestination(GameState game, Player player, Point loc, List<DIRECTION> directions) {
        boolean[][] available = game.getAvailableDestination(player, loc, directions);
        List<Point> cand = new LinkedList<>();
        for (int x = 0; x < Soup.SOUP_WIDTH; x++) {
            for (int y = 0; y < Soup.SOUP_HEIGHT; y++) {
                if (available[x][y]) {
                    cand.add(new Point(x,y));
                }
            }
        }
        return cand;
    }
}
