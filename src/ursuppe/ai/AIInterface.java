package ursuppe.ai;

import java.awt.Point;
import java.util.List;
import java.util.Map;
import ursuppe.game.Amoeba;
import ursuppe.game.EnvironmentCard;
import ursuppe.game.GameState.DIRECTION;
import ursuppe.game.GameState.PHASE1_ACTION;
import ursuppe.game.Gene;
import ursuppe.game.GenePool.GENE;
import ursuppe.game.Player;

/**
 * All AIs must implement all functions in this interface.
 * 
 * All information that you may need can be accessed through the GameView class that is 
 * an argument for all of the functions defined in this interface.
 * The convention is that selected important information for a function is given before GameView,
 * and any argument after GameView are additional outputs from the function required,
 * which cannot be simply returned since a function can only return one type.
 *
 *
 */
public interface AIInterface {

    /**
     * Select initial from the remaining choice.
     * Return a integer that is in the given list of choices.
     *
     * @param remainingChoice
     * @param game
     * @return initial score choice
     */
    public int selectInitialScore(List<Integer> remainingChoice, GameView game);

    /**
     * Selects location of initial amoeba. Return a valid Point from the available coordinates
     * specified as the availability 2D array.
     *
     * @param availableLocs
     * @param game
     * @return initial amoeba location
     */
    public Point selectInitialAmoebaLocation(List<Point> availableLocs, GameView game);

    /**
     * Select a number for your new amoeba. The remaining numbers are given as List, choice.
     * location is the coordinate of your new amoeba
     *
     * @param choice
     * @param location
     * @param game
     * @return amoeba number
     */
    public int selectAmoebaNum(List<Integer> choice, Point location, GameView game);

    /**
     * Select an action to take for an amoeba in Phase 1.
     * All possible actions for given amoeba is listed in the List choice.
     * Return one of the actions in the list, which will be followed by further calls
     * to determine the detail when required.
     *
     * @param amoeba
     * @param choice
     * @param game
     * @return an action to take
     */
    public PHASE1_ACTION selectPhase1Action(Amoeba amoeba, List<PHASE1_ACTION> choice, GameView game);


    /**
     * When moving by any means, you have an option to take up to 3 foods if you have TEN gene.
     * Return an array of ints, which length is equal to arrays of Colors and ints given as input to this function (or the number of players).
     * The number in the returned array is the number of foods you wish to take.
     *
     * @param foodColors
     * @param srcFood
     * @param dstFood
     * @param srcLoc
     * @param dstLoc
     * @param game
     * @return list of foods to take
     */
    public int[] selectTENfoods(int[] foodColors, int[] srcFood, int[] dstFood, Point srcLoc, Point dstLoc, GameView game);

    /**
     * When moving against a wall, you have an option to bounce back to the opposite direction.
     * Return true if you wish to bounce.
     *
     * @param current
     * @param game
     * @return true if rebound is used
     */
    public boolean selectREB(Point current, DIRECTION wallDirection, GameView game);

    /**
     * When excreting, you can excrete on a neighboring cell with Cleanliness gene.
     * Select a valid location specified as validDsts and return the corresponding Point.
     * Return null if you wish to excrete normally.
     *
     * @param validDsts
     * @param game
     * @return
     */
    public Point selectExcreteDestination(List<Point> validDsts, GameView game);

    /**
     * When excreting, you can also place DP along with your excrete, which will damage the next
     * amoeba entering the cell with DP. Return if you wish to use Toxic Excretions.
     *
     * @param loc
     * @param game
     * @return
     */
    public boolean selectTOX(Point loc, GameView game);

    /**
     * With Suction, you can take foods from a neighboring cell. Select a location specified as availableLoc,
     * if you wish to use suction. Return null to not use this gene.
     *
     * @param current
     * @param availableLocs
     * @param game
     * @return
     */
    public Point selectSUCLocation(Point current, List<Point> availableLocs, GameView game);

    /**
     * When Suction location was selected, now the foods to take must be selected.
     * Select foods to take (1 with SUC1 and 2 with SUC2) from the foods array given.
     * Return an array of ints (same size as foods) where numbers corrspond to the food to take.
     *
     * @param SUCloc
     * @param foodColors
     * @param foods
     * @param game
     * @return
     */
    public int[] selectSUCFoods(Point current, Point SUCloc, int[] foodColors, int[] SUClocFoods, GameView game);

    /**
     * This function is called when eating with genes or which food to eat needs to be seelcted.
     * Return an array of ints denoting the combination of foods to eat.
     * Eating combinations can be verifies through calls to GameView
     *
     * Return null if you wish to use AutoEat, which will select a eating combination that eats the
     * least amount of food automatically.
     *
     * When using par, the ID of players that you wish to parasite needs to be added to parTargets List.
     * When parasitizing a same player twice, you need to add the ID twice.
     * The number of players you can parasite and available targets can be obtained from calls to GameView
     *
     * @param loc
     * @param foodColors
     * @param foods
     * @param sucFoods foods taken from neighboring cells with Suction. May be null.
     * @param game
     * @param parTargets
     * @return foods to eat
     */
    public int[] selectFoods(Point loc, int[] foodColors, int[] foods, int[] sucFoods, GameView game, List<Integer> parTargets);

    /**
     * Select the target amoeba for your SFS attempt.
     * Pick a target from the list of amoebas, and return the Amoeba object.
     *
     * GameView has a function (getAttackResponse) that checks if you can
     * attack or if the target amoeba has any defense.
     *
     * @param attacker
     * @param players
     * @param amoebas
     * @param game
     * @return target amoeba
     */
    public Amoeba selectSFStarget(Amoeba attacker, List<Player> players, List<Amoeba> amoebas, GameView game);

    /**
     * Return true if you wish to take the second movement (without any cost) using th SPD gene.
     *
     * @param loc
     * @param game
     * @return return true if taking second movement
     */
    public boolean selectSPD(Point loc, GameView game);

    /**
     * Select a direction you wish to move from the available directions.
     * Return direction of the movement.
     *
     * @param loc
     * @param availableDirs
     * @param game
     * @return
     */
    public DIRECTION selectMovementDirection(Point loc, List<DIRECTION> availableDirs, GameView game);


    /**
     * Handle gene defects by discarding genes to make up for MPs exceeding the maxMP of the new environment.
     * If you have enough BP to pay for the remaining amount, you don't need to discard genes to fully make up for
     * the exceeding amount (or no discards at all).
     * Return the list of genes you wish to discard in an array. If the discard is not enough,
     * three remaining excess will be paid by BP.
     *
     * @param exceeding
     * @param genes
     * @param currentBP
     * @param game
     * @return
     */
    public GENE[] handleGeneDeffects(int exceeding, List<Gene> genes, int currentBP, GameView game);

    /**
     * Select a gene you wish to exchange with any gene already in possession.
     * The input exchangeable is a map where the key is a gene in possession, and value
     * is the list of genes that can be exchanged with that gene (key).
     *
     * Return null if you do not wish to exchange
     * Return an array of genes, where the first element is the gene in possession, and the second element
     * is the gene you want to get.
     *
     * @param exchangable
     * @param currentBP
     * @param game
     * @return
     */
    public GENE[] selectADAExchange(Map<GENE, List<GENE>> exchangable, int currentBP, GameView game);

    /**
     * Select a GENE you wish to purchase.
     * Return null if you don't want any more genes.
     *
     * @param purchasable
     * @param currentBP
     * @param game
     * @return
     */
    public GENE selectGENEPurchase(List<GENE> purchasable, int currentBP, GameView game);

    /**
     * Select a location to spawn a new amoeba from the available coordinates.
     * Return null if you don't want anymore amoebas
     *
     * @param availableLocs
     * @param currentBP
     * @param game
     * @return initial amoeba location
     */
    public Point selectCellDivision(List<Point> availableLocs, int currentBP, GameView game);

    /**
     * Select an amoeba to heal. Healing cost 3 BP and, can only be used to an amoeba of your own.
     * Return null if you don't want to heal
     *
     * @param amoebas
     * @param currentBP
     * @param game
     * @return
     */
    public Amoeba selectHealing1Target(List<Amoeba> amoebas, int currentBP, GameView game);

    /**
     * If you have Healing 2, you can transfer the healed DP to another amoeba when healing.
     * Select an amoeba you wish to transfer the DP to from the list of amoebas.
     * Healing 2 costs 1 extra BP on top of Healing 1 (total 4 BP).
     * Return null if you just want to use Healing 1
     *
     *
     * @param amoebas
     * @param currentBP
     * @param game
     * @return
     */
    public Amoeba selectHealing2Target(List<Amoeba> amoebas, int currentBP, GameView game);

    /**
     * Select if you want to use BANG gene to give 1 DP to every amoeba in the given location.
     * Return true to use BANG.
     *
     * @param loc
     * @param game
     * @return
     */
    public boolean selectBANG(Point loc, GameView game);


    /**
     * Select if the amoeba given wants to hold on to another amoeba
     * to move towards the direction given
     * Return true to use Holding and move.
     *
     * @param amoeba
     * @param dir
     * @param game
     * @return
     */
    public boolean selectHOL(Amoeba amoeba, DIRECTION dir, GameView game);

    /**
     * Select if you want to use Population Explosion.
     * Return true to use POP
     *
     * Population Explosion will lets you spawn amoebas for free
     * until you match the number of amoebas of the player with highest score.
     * However, you must give up all BPs you currently have, including the income
     * you are about to receive.
     *
     * @param currentBP
     * @param spawnableNumber number of amoebas you will be able to spawn for free
     * @param game
     * @return
     */
    public boolean selectPOP(int currentBP, int spawnableNumber, GameView game);

    /**
     * Select an amoeba to attempt an Agression attack from the list of available targets
     * AGR attack costs 1 BP.
     * Return null if you don't want to use AGR
     *
     * @param AGRtargets
     * @param currentBP
     * @param game
     * @return
     */
    public Amoeba selectAGR(List<Amoeba> AGRtargets, int currentBP, GameView game);


    /**
     * Select if you wish to use Escape to avoid attacks.
     * Return true if you wish to use Escape
     *
     * @param attacker
     * @param attackerAmoeba
     * @param defender
     * @param defenderAmoeba
     * @param game
     * @return
     */
    public boolean selectESC(Player attacker, Amoeba attackerAmoeba, Player defender, Amoeba defenderAmoeba, GameView game);

    /**
     * Select if you wish to use Defense to defend yourslef.
     * Return true if you wish to use Defend
     *
     * @param attacker
     * @param attackerAmoeba
     * @param defender
     * @param defenderAmoeba
     * @param game
     * @return
     */
    public boolean selectDEF(Player attacker, Amoeba attackerAmoeba, Player defender, Amoeba defenderAmoeba, GameView game);

    /**
     * Select if you want to pay additional 1 BP to carry through the attack when
     * the targets have Hard Crust
     * Return true if you wish to carry through by paying 1 BP
     *
     * @param attacker
     * @param attackerAmoeba
     * @param defender
     * @param defenderAmoeba
     * @param game
     * @return
     */
    public boolean selectPayHARD(Player attacker, Amoeba attackerAmoeba, Player defender, Amoeba defenderAmoeba, GameView game);

    /**
     * Select the direction to move using Escape from the available directions,
     *
     * @param loc
     * @param availableDirs
     * @param game
     * @return
     */
    public DIRECTION selectEscapeDirection(Point loc, List<DIRECTION> availableDirs, GameView game);

    /**
     * Decide if you wish to use Speed and move again while Escaping.
     * Return true if you wish to move again.
     *
     * @param loc
     * @param attackerLoc
     * @param game
     * @return
     */
    public boolean selectESCSPD(Point loc, Point attackerLoc, GameView game);

    /**
     * Decide if you wish to use Go Out With a Bang and explose when
     * killed by Struggle For Survival
     *
     * @param loc
     * @param game
     * @return
     */
    public boolean decideSFSBANG(Point loc, GameView game);

    /**
     * When you have Migration Sensing or Environment Sensing,
     * this function will be called when the next environment card
     * became visible.
     *
     * @param card
     */
    public void nextEnvironment(EnvironmentCard card);
}
