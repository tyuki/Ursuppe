package ursuppe.ai;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import ursuppe.game.GameManager;

/**
 *
 * @author Personal
 */
public class AIContainer {

    private final List<AIPlayer> AIs = Collections.synchronizedList(new LinkedList<>());
    private static final List<String> NAMES = new LinkedList<String>() {
        {
            add("AI_1");
            add("AI_2");
            add("AI_3");
            add("AI_4");
            add("AI_5");
            add("AI_6");
            add("AI_7");
            add("AI_8");
            /*add("ふぇいいぇん");
            add("あふぁーむど");
            add("べるぐどる");
            add("らいでん");
            add("てむじん");
            add("ばう");
            add("どるかす");
            add("ばいぱー");*/
            Collections.shuffle(this);
        }
    };

    private final GameManager manager;

    public AIContainer(GameManager manager) {
        this.manager = manager;
    }

    public synchronized void addAI(int port) {
            AIs.add(new AIPlayer(this, NAMES.get(AIs.size()), port));
    }

    public synchronized void removeAI() {
        if (AIs.size() > 0) {
            AIs.get(AIs.size()-1).disconnect();
            AIs.remove(AIs.size()-1);
        }
    }

    public synchronized void selfDestruct(AIPlayer player) {
        AIs.remove(player);
        manager.cleanupConnection(player.getName());
    }

    public int getCount() {
        return AIs.size();
    }

}
