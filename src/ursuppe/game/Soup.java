/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.game;

import java.awt.Color;
import java.awt.Point;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Personal
 */
public class Soup implements Serializable {


    public static final long serialVersionUID = 98153841398L;

    public static final int SOUP_WIDTH = 5;
    public static final int SOUP_HEIGHT = 5;

    private SoupCell[][] _soup;
    private int[] _foodColors;

    public Soup() {
        _soup = new SoupCell[SOUP_WIDTH][SOUP_HEIGHT];

        for (int i = 0; i < SOUP_WIDTH; i++) {
            for (int j = 0; j < SOUP_HEIGHT; j++) {
                _soup[i][j] = new SoupCell();
            }
        }
    }

    public static boolean isValidLocation(int x, int y) {

        if (x < 0 || x >= SOUP_WIDTH) return false;
        if (y < 0 || y >= SOUP_HEIGHT) return false;

        if (x==2&&y==2) {
            return false;
        } else if ((x != 1 && y == 0) || (x==4 && y==4)) {
            return false;
        }
        return true;
    }

    public static boolean isValidLocation(Point loc) {
        return isValidLocation(loc.x, loc.y);
    }

    public void initialize(List<Player> players) {

        //Food color
        _foodColors = new int[players.size()];
        for (int i = 0; i < _foodColors.length; i++) {
            _foodColors[i] = players.get(i).getColorID();
        }

        //Each cell
        for (int x = 0; x < SOUP_WIDTH; x++) {
            for (int y = 0; y < SOUP_HEIGHT; y++) {
                if (isValidLocation(x,y)) {
                    _soup[x][y].initialize(players.size());
                }
            }
        }
    }

    public SoupCell getCell(int x, int y) {
        if (isValidLocation(x,y)) return _soup[x][y];

        return null;
    }

    public SoupCell getCell(Point loc) {
        return getCell(loc.x, loc.y);
    }

    public int[] getFoodColors() {
        return _foodColors;
    }
}
