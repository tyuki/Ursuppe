/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.game.message;

import java.io.Serializable;

/**
 *
 * @author Personal
 */
public class RelevantAmoeba implements Serializable {


    public static final long serialVersionUID = 15657484854L;

    public final int playerID;
    public final int amoebaNum;

    public RelevantAmoeba(int playerID, int amoebaNum) {
        this.playerID = playerID;
        this.amoebaNum = amoebaNum;
    }
}
