/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.game.message;

import java.io.Serializable;

/**
 *
 * @author Personal
 */
public class GameEventMessage extends GameMessage implements Serializable {

    public static final long serialVersionUID = 48785248991389L;

    public static enum EVENT {
        MOVE,
        SCORE_STEP
    }

    public final EVENT event;
    public final Object[] data;

    public GameEventMessage(EVENT event, Object[] data) {
        this.event = event;
        this.data = data;
    }
    
}
