/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.game.message;

import java.io.Serializable;
import java.util.Locale;
import ursuppe.UrsuppeResource;

/**
 *
 * @author Personal
 */
public class GameTextMessage extends GameMessage implements Serializable {

    public static enum COLOR_TYPE {
        DEFAULT,
        PLAYER,
        ERROR,
        EMPHASIZE
    }

    public static enum MESSAGE {
        GAME_START,
        INITIAL_SCORE,
        ROUND_START,
        NEW_PHASE,
        GAME_END,
        ADD_AMOEBA,

        DIE_ROLL,
        DICE_ROLL,
        DICE_DEF,

        DRIFT,
        MOVE,
        MOVE_STAY,
        EAT,
        EXCRETE,
        STARVE,
        END_PHASE_1,

        DEFECT_PAY_BP,
        DEFECT_PAY_GENE,

        GENE_PURCHASED,
        GENE_SOC_PURCHASED,
        END_GENE_PURCHASE,

        PHASE4_INCOME,
        END_CELL_DIVISION,

        DEAD_AMOEBA,

        SCORE,
        
        FINAL_SCORE,

        TURN_SIGNAL,

        GENE_TEN,
        GENE_ENE,
        GENE_BANG,
        GENE_BANG_NOTICE,
        GENE_TOX,
        GENE_TOX_HIT,
        GENE_REB_DRIFT,
        GENE_REB_MOVE,
        GENE_HOL_STAY,
        GENE_HOL,
        GENE_HOL_NOTICE,
        GENE_SOC,
        GENE_POP,
        GENE_HEA1,
        GENE_HEA2,
        GENE_ADA_FREE,
        GENE_ADA,
        GENE_SUC,
        GENE_PAR,
        GENE_ALM,
        GENE_CAM1_SUCCESS,
        GENE_CAM1_FAIL,

        GENE_SFS_SUCCESS,
        GENE_AGR_SUCCESS,
        GENE_SFS_FAILURE,
        GENE_AGR_FAILURE,
        GENE_SFS_SUCCESS_MOU,
        GENE_HARD_PAID,
        GENE_HARD_GIVEUP,
        GENE_DEF_ATTACKER_WIN,
        GENE_DEF_DEFENDER_WIN,
        GENE_DEF_TIE,
        GENE_PER


    }

    public final MESSAGE message;
    public final String[] argv;
    public final COLOR_TYPE colorType;
    public final int colorID;
    public final RelevantGene[] genes;
    public final RelevantAmoeba[] amoebas;



    public GameTextMessage(MESSAGE message, String[] argv, COLOR_TYPE colorType, int colorID) {
        this.message = message;
        this.argv = argv;
        this.colorType = colorType;
        this.colorID = colorID;
        this.genes = null;
        this.amoebas = null;
    }

    public GameTextMessage(MESSAGE message, String[] argv, COLOR_TYPE colorType, int colorID, RelevantGene[] genes) {
        this.message = message;
        this.argv = argv;
        this.colorType = colorType;
        this.colorID = colorID;
        this.genes = genes;
        this.amoebas = null;
    }

    public GameTextMessage(MESSAGE message, String[] argv, COLOR_TYPE colorType, int colorID,  RelevantAmoeba[] amoebas) {
        this.message = message;
        this.argv = argv;
        this.colorType = colorType;
        this.colorID = colorID;
        this.genes = null;
        this.amoebas = amoebas;
    }

    public GameTextMessage(MESSAGE message, String[] argv, COLOR_TYPE colorType, int colorID, RelevantGene[] genes, RelevantAmoeba[] amoebas) {
        this.message = message;
        this.argv = argv;
        this.colorType = colorType;
        this.colorID = colorID;
        this.genes = genes;
        this.amoebas = amoebas;
    }
    
    @Override
    public String toString() {

        Locale local = Locale.getDefault();

        if (local == Locale.JAPAN || local == Locale.JAPANESE) {
            return toStringJP();
        } else {
            return toStringEN();
        }
    }
    
    private String toStringEN() {
            switch (message) {
                case GAME_START:
                    return "GAME START";
                case ROUND_START:
                    return "Starting ROUND " + argv[0];
                case NEW_PHASE:
                    return "Statring Phase " + argv[0] + " : " + UrsuppeResource.getString("phase"+argv[0]+".name");
                case INITIAL_SCORE:
                    return argv[0] + " selected initial score " + argv[1];
                case GAME_END:
                    return "GAME END";
                case ADD_AMOEBA:
                    return argv[0] + " spawned a new amoeba in " + argv[1];

                case DIE_ROLL:
                    return argv[0] + " rolled a die ... " + argv[1];
                case DICE_ROLL:
                    return argv[0] + " rolled two dice ... " + argv[1] + "," + argv[2];
                case DICE_DEF:
                    return "Dice rolled for a fight\nAttacker:" + argv[0] + " Defender:" + argv[1];

                case DRIFT:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " drifted with the wind (" + UrsuppeResource.getString("term."+argv[2]) + ")";
                case MOVE:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " moved (" + UrsuppeResource.getString("term."+argv[2]) + ")";
                case MOVE_STAY:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " stayed at the same cell";
                case EAT:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " successfully fed itself " + argv[2];
                case EXCRETE:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " excreted at " + argv[2];
                case STARVE:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " got 1 DP due to starvation";
                case END_PHASE_1:
                    return argv[0] + " finished phase 1.";

                case DEFECT_PAY_BP:
                    return argv[0] + " paid " + argv[1] + " BP " + " for having excessive MP in the new environment";
                case DEFECT_PAY_GENE:
                    return argv[0] + " gave up " + UrsuppeResource.getString("gene."+argv[1]+".name") + " for having excessive MP in the new environment";

                case GENE_PURCHASED:
                    return argv[0] + " purchased a new gene " + UrsuppeResource.getString("gene."+argv[1]+".name");
                case GENE_SOC_PURCHASED:
                    return argv[0] + " purchased Social Gene from " + argv[0];
                case END_GENE_PURCHASE:
                    return argv[0] + " finished buying new genes ";

                case PHASE4_INCOME:
                    return argv[0] + " earned " + argv[1] + " BP";
                case END_CELL_DIVISION:
                    return argv[0] + " finished cell division ";

                case DEAD_AMOEBA:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " died";

                case SCORE:
                    return argv[0] + " scored " + argv[1] + " points";

                case FINAL_SCORE:
                    return argv[0] + " : " + argv[1];

                case TURN_SIGNAL:
                    return "Turn of "+argv[0];

                case GENE_TEN:
                    return argv[0] + " moved foods " + argv[1] + " with Tentacles";
                case GENE_ENE:
                    return argv[0] + " earned 1 BP with Energy Conservation";
                case GENE_BANG:
                    return "All other amoebas in the cell " + argv[0] +  " got 1 DP from the Go Out With a Bang gene";
                case GENE_BANG_NOTICE:
                    return argv[0] + " is deciding if Amoeba #" + argv[1] + " is going to explode or not";
                case GENE_TOX:
                    return "A toxic excrete was left in " + argv[0];
                case GENE_TOX_HIT:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " got " + argv[2] + " DPs from Toxic Excretes";
                case GENE_REB_DRIFT:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " drifted with the wind " + "(" + UrsuppeResource.getString("term."+argv[2]) + ")" + " and then bounced from the wall with Rebound";
                case GENE_REB_MOVE:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " moved " + UrsuppeResource.getString("term."+argv[2]) + " and then bounced from the wall with Rebound";
                case GENE_HOL_STAY:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " used Holding to stay put";
                case GENE_HOL:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " moved " + UrsuppeResource.getString("term."+argv[2]) + " by holding on to another amoeba";
                case GENE_HOL_NOTICE:
                    return argv[0] + " is now deciding if Amoeba #" + argv[1] + " should move along with Holding or not";
                case GENE_SOC:
                    return argv[0] + " received 1 BP from " + argv[1] + " using Social Gene";
                case GENE_POP:
                    return argv[0] + " activated Population Explosion";
                case GENE_HEA1:
                    return argv[0] + " paid 3 BP to heal amoeba #" + argv[1];
                case GENE_HEA2:
                    return argv[0] + " paid 4 BP to transfer a DP from amoeba #" + argv[1] + " to amoeba #" + argv[3] + " of " + argv[2];
                case GENE_ADA_FREE:
                    return argv[0] + " exchanged " + UrsuppeResource.getString("gene."+argv[1]+".name") + " with " +  UrsuppeResource.getString("gene."+argv[2]+".name");
                case GENE_ADA:
                    return argv[0] + " exchanged " + UrsuppeResource.getString("gene."+argv[1]+".name") + " with " +  UrsuppeResource.getString("gene."+argv[2]+".name") + " and " + argv[3] + " BPs";
                case GENE_SUC:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " took foods " + argv[2] + " from " + argv[3] + " using Suction";
                case GENE_PAR:
                    return argv[0] + " lost 1 BP due to parasitism";
                case GENE_ALM:
                    return "Alarm Gene activated";
                case GENE_CAM1_SUCCESS:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " is now camouflaged";
                case GENE_CAM1_FAIL:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " failed to camouflage";

                case GENE_SFS_SUCCESS:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " ate Amoeba #" + argv[3] + " of " + argv[2];
                case GENE_AGR_SUCCESS:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " killed Amoeba #" + argv[3] + " of " + argv[2];
                case GENE_SFS_FAILURE:
                    return "SFS Attempt Failed";
                case GENE_AGR_FAILURE:
                    return "AGR Attempt Failed";
                case GENE_SFS_SUCCESS_MOU:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " ate a bite of Amoeba #" + argv[3] + " of " + argv[2];
                case GENE_HARD_PAID:
                    return argv[0] + " paid additional 1 BP to carry through its attack";
                case GENE_HARD_GIVEUP:
                    return argv[0] + " gave up attacking due to Hard Crust";
                case GENE_DEF_ATTACKER_WIN:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " was unable to defend itself from the attack";
                case GENE_DEF_DEFENDER_WIN:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " successfully defended itself from the attack";
                case GENE_DEF_TIE:
                    return "The rolls were tied. Fight continues";
                case GENE_PER:
                    return "Amoeba #" + argv[1] + " of " + argv[0] + " gets another try with its Persitence";

            }
        return "";
    }

    private  String toStringJP() {
            switch (message) {
                case GAME_START:
                    return "ゲーム開始";
                case ROUND_START:
                    return "ラウンド" + argv[0] + "開始";
                case NEW_PHASE:
                    return "フェイズ" + argv[0] + " : " + UrsuppeResource.getString("phase"+argv[0]+".name");
                case INITIAL_SCORE:
                    return argv[0] + "は初期スコアとして " + argv[1] + " を選択しました";
                case GAME_END:
                    return "ゲーム終了";
                case ADD_AMOEBA:
                    return argv[1] + "に" + argv[0]+"の新しいアメーバが発生しました";

                case DIE_ROLL:
                    return argv[0] + "はサイコロを振った ... " + argv[1];
                case DICE_ROLL:
                    return argv[0] + "はサイコロを2つ振った ... " + argv[1] + "," + argv[2];
                case DICE_DEF:
                    return "ダイスによる戦闘\n攻撃側:" + argv[0] + " 防御側" + argv[1];

                case DRIFT:
                    return argv[0]+"のアメーバ#" + argv[1] + "は風に乗って漂流 (" + UrsuppeResource.getString("term."+argv[2]) + ")しました";
                case MOVE:
                    return argv[0]+"のアメーバ#" + argv[1] + "は" + UrsuppeResource.getString("term."+argv[2]) + "に移動しました";
                case MOVE_STAY:
                    return argv[0]+"のアメーバ#" + argv[1] + "は同じ位置に留まりました";
                case EAT:
                    return argv[0]+"のアメーバ#" + argv[1] + "は食事" + argv[2] + "をしました";
                case EXCRETE:
                    return argv[0]+"のアメーバ#" + argv[1] + "は排泄" + argv[2] + "しました";
                case STARVE:
                    return argv[0]+"のアメーバ#" + argv[1] + "は餌を見つけられず 1 DPを受けました";
                case END_PHASE_1:
                    return argv[0] + "の移動・食事ターン終了";

                case DEFECT_PAY_BP:
                    return argv[0] + "は遺伝子異常克服のため " + argv[1] + " BPを支払いました";
                case DEFECT_PAY_GENE:
                    return argv[0] + "は遺伝子異常克服のため " + UrsuppeResource.getString("gene."+argv[1]+".name") + " を手放しました";

                case GENE_PURCHASED:
                    return argv[0] + "は新しい遺伝子、" + UrsuppeResource.getString("gene."+argv[1]+".name")+" を獲得しました";
                case GENE_SOC_PURCHASED:
                    return argv[0] + "はSocial Geneを" + argv[1] + "から買取りました";
                case END_GENE_PURCHASE:
                    return argv[0] + "の遺伝子獲得ターン終了";

                case PHASE4_INCOME:
                    return argv[0] + "は " + argv[1] + " BPを得ました";
                case END_CELL_DIVISION:
                    return argv[0] + "の細胞分裂ターン終了";

                case DEAD_AMOEBA:
                    return argv[0]+"のアメーバ#" + argv[1] + "は死亡";

                case SCORE:
                    return argv[0] + "は" + argv[1] + "点を獲得";

                case FINAL_SCORE:
                    return argv[0] + " : " + argv[1];

                case TURN_SIGNAL:
                    return argv[0] + "のターン";

                case GENE_TEN:
                    return argv[0] + "は触手を使って餌" + argv[1] + "を移動しました";
                case GENE_ENE:
                    return argv[0] + "は省エネ行動により 1 BPを得ました";
                case GENE_BANG:
                    return argv[0] + "にいる全てのアメーバは爆発により 1 DP受けました";
                case GENE_BANG_NOTICE:
                    return argv[0] + "はアメーバ#" + argv[1] + "を死に際に爆発させるか考え中";
                case GENE_TOX:
                    return argv[0] + "に毒が置かれました";
                //case MOVE_STAY:
                //    return argv[0]+"の アメーバ#" + argv[1] + "は同じ位置に留まった";
                case GENE_TOX_HIT:
                    return argv[0] + "のアメーバ#" + argv[1] + "は毒により" + argv[2] + " DPを受けました";
                case GENE_REB_DRIFT:
                    return argv[0] + "のアメーバ#" + argv[1] + "は風に乗って漂流" + "(" + UrsuppeResource.getString("term."+argv[2]) + ")" + "、壁にあたって跳ね返りました";
                case GENE_REB_MOVE:
                    return argv[0] + "のアメーバ#" + argv[1] + "は" + UrsuppeResource.getString("term."+argv[2]) + "に移動、壁にあたって跳ね返りました";
                case GENE_HOL_STAY:
                    return argv[0] + "のアメーバ#" + argv[1] + "は現在地に留まりました";
                case GENE_HOL:
                    return argv[0] + "のアメーバ#" + argv[1] + "は他のアメーバにくっついて" + UrsuppeResource.getString("term."+argv[2]) + "に移動しました";
                case GENE_HOL_NOTICE:
                    return argv[0] + "はアメーバ#" + argv[1] + "をくっついて移動させようか考え中";
                case GENE_SOC:
                    return argv[0] + "は持ち前の社交性により" + argv[1] + "から1BP受け取りました";
                case GENE_POP:
                    return argv[0] + "は人口爆発を発動しました";
                case GENE_HEA1:
                    return argv[0] + "は 3 BPを支払いアメーバ#" + argv[1] + "を治療しました";
                case GENE_HEA2:
                    return argv[0] + "は 4 BPを支払いアメーバ#" + argv[1] + "から" + argv[2] + "のアメーバ#" + argv[3] + "にDPを移動させました";
                case GENE_ADA_FREE:
                    return argv[0] + "は " + UrsuppeResource.getString("gene."+argv[1]+".name") + " と " +  UrsuppeResource.getString("gene."+argv[2]+".name") + " を交換しました";
                case GENE_ADA:
                    return argv[0] + "は " + UrsuppeResource.getString("gene."+argv[1]+".name") + " と " +  UrsuppeResource.getString("gene."+argv[2]+".name") + " を " +argv[3]+ " BP支払って交換しました";
                case GENE_SUC:
                    return argv[0] + "のアメーバ#" + argv[1] + "は餌" + argv[2] + "を" + argv[3] + "からSuctionを使って吸い取りました";
                case GENE_PAR:
                    return argv[0] + "は寄生されて 1 BP失いました";
                case GENE_ALM:
                    return "Alarm遺伝子により警報が出されました";
                case GENE_CAM1_SUCCESS:
                    return argv[0] + "のアメーバ#" + argv[1] + "は奇襲に成功しました";
                case GENE_CAM1_FAIL:
                    return argv[0] + "のアメーバ#" + argv[1] + "は奇襲に失敗しました";

                case GENE_SFS_SUCCESS:
                    return argv[0] + "のアメーバ#" + argv[1] +  "は" + argv[2] + "のアメーバ#" + argv[3] + "を捕食しました";
                case GENE_AGR_SUCCESS:
                    return argv[0] + "のアメーバ#" + argv[1] + "は" + argv[2] + "のアメーバ#" + argv[3] + "を殺しました";
                case GENE_SFS_FAILURE:
                    return "捕食に失敗";
                case GENE_AGR_FAILURE:
                    return "殺害に失敗";
                case GENE_SFS_SUCCESS_MOU:
                    return argv[0] + "のアメーバ#" + argv[1] + "は" + argv[2] + "のアメーバ#" + argv[3] + "をひとかじりしました";
                case GENE_HARD_PAID:
                    return argv[0] + "は 1 BPを追加で支払い攻撃を続行させました";
                case GENE_HARD_GIVEUP:
                    return argv[0] + "は硬い殻に阻まれ、攻撃を諦めました";
                case GENE_DEF_ATTACKER_WIN:
                    return argv[0] + "のアメーバ#" + argv[1] + "は防御に失敗";
                case GENE_DEF_DEFENDER_WIN:
                    return argv[0] + "のアメーバ#" + argv[1] + "は防御に成功";
                case GENE_DEF_TIE:
                    return "引き分けのため振り直し";
                case GENE_PER:
                    return argv[0] + "のアメーバ#" + argv[1] + "は諦めずに再戦";

            }
        return toStringEN();
    }
}
