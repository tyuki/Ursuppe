/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.game.message;

import java.io.Serializable;
import ursuppe.game.GenePool.GENE;

/**
 *
 * @author Personal
 */
public class RelevantGene implements Serializable {

    public static final long serialVersionUID = 348945348745L;

    public final int playerID;
    public final GENE gene;

    public RelevantGene(int playerID, GENE gene) {
        this.playerID = playerID;
        this.gene = gene;
    }
}
