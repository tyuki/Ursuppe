/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.game.message;

import java.io.Serializable;
import java.util.Locale;

/**
 *
 * @author Personal
 */
public class ServerMessage implements Serializable {
    
    public static final long serialVersionUID = 9841248341752L;

     public static enum COLOR_TYPE {
        DEFAULT,
        ERROR,
        EMPHASIZE
    }

    public static enum ARGC0_MESSAGE {
        NEWGAME,
        OPTION_CHANGED
    }

    public static enum ARGC1_MESSAGE {
        CONNECTED,
        DISCONNECTED,
        JOINED,
        LEAVE,
        RESET_VOTE,
        RESET_COUNTDOWN,
        RESET_CANCEL
    }


    public final String string;
    public final COLOR_TYPE colorType;

    public ServerMessage(String string, COLOR_TYPE colorType) {
        this.string = string;
        this.colorType = colorType;
    }

    public static ServerMessage zeroArgMessage(ARGC0_MESSAGE msgType, COLOR_TYPE type) {

        Locale local = Locale.getDefault();

        if (local == Locale.JAPAN || local == Locale.JAPANESE) {
            return new ServerMessage(zeroArgMessageJP(msgType), type);
        } else {
            return new ServerMessage(zeroArgMessageEN(msgType), type);
        }
    }

    public static String zeroArgMessageJP(ARGC0_MESSAGE msgType) {
        switch (msgType) {
            case NEWGAME:
                return "新しいゲームがはじまりました";
            case OPTION_CHANGED:
                return "ゲーム設定が変更されました。変更はゲームのリセット後に反映されます。";
        }

        return "";
    }


    public static String zeroArgMessageEN(ARGC0_MESSAGE msgType) {
        switch (msgType) {
            case NEWGAME:
                return "New game has started";
            case OPTION_CHANGED:
                return "Game Options have been changed. The changes are reflected after game reset.";
        }

        return "";
    }


    public static ServerMessage singleArgMessage(ARGC1_MESSAGE msgType, String arg1, COLOR_TYPE type) {

        Locale local = Locale.getDefault();

        if (local == Locale.JAPAN || local == Locale.JAPANESE) {
            return new ServerMessage(singleArgMessageJP(msgType, arg1), type);
        } else {
            return new ServerMessage(singleArgMessageEN(msgType, arg1), type);
        }
    }

    private static String singleArgMessageEN(ARGC1_MESSAGE msgType, String arg1) {
        switch (msgType) {
            case CONNECTED:
                return arg1 + " has connected";
            case DISCONNECTED:
                return arg1 + " has diconnected";
            case JOINED:
                return arg1 + " has joined the game";
            case LEAVE:
                return arg1 + " has left the game";
            case RESET_VOTE:
                return arg1 + " has voted for reset";
            case RESET_COUNTDOWN:
                return "Reset in " + arg1 + " seconds...";
            case RESET_CANCEL:
                return arg1 + " has canceled reset";
        }

        return "";
    }


    private static String singleArgMessageJP(ARGC1_MESSAGE msgType, String arg1) {
        switch (msgType) {
            case CONNECTED:
                return arg1 + "が接続しました";
            case DISCONNECTED:
                return arg1 + "との接続が切断されました";
            case JOINED:
                return arg1 + "がゲームに参加しました";
            case LEAVE:
                return arg1 + "がゲームへの参加をキャンセルしました";
            case RESET_VOTE:
                return arg1 + "がゲームのリセットを申請しました";
            case RESET_COUNTDOWN:
                return arg1 + "秒後にリセット";
            case RESET_CANCEL:
                return arg1 + "がリセットをキャンセルしました";
        }
        return singleArgMessageEN(msgType, arg1);
    }


//
//
//    public static ServerMessage doubleArgMessage(ARGC2_MESSAGE msgType, String arg1, String arg2, COLOR_TYPE type) {
//
//        Locale local = Locale.getDefault();
//
//        if (local == Locale.JAPAN || local == Locale.JAPANESE) {
//            return new ServerMessage(doubleArgMessageJP(msgType, arg1, arg2), type);
//        } else {
//            return new ServerMessage(doubleArgMessageEN(msgType, arg1, arg2), type);
//        }
//    }
//
//    public static String doubleArgMessageJP(ARGC2_MESSAGE msgType, String arg1, String arg2) {
//        switch (msgType) {
//            case FINAL_SCORE:
//                return String.format("%s : %s", arg1, arg2);
//        }
//
//        return "";
//    }
//
//
//    public static String doubleArgMessageEN(ARGC2_MESSAGE msgType, String arg1, String arg2) {
//        switch (msgType) {
//            case FINAL_SCORE:
//                return String.format("%s : %s", arg1, arg2);
//        }
//
//        return "";
//    }
}
