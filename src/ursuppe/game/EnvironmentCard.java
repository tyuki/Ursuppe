package ursuppe.game;

import java.io.Serializable;

/**
 *
 * @author Personal
 */
public class EnvironmentCard implements Serializable, Comparable<EnvironmentCard> {


    public static final long serialVersionUID = 148397716746L;

        public final int maxMP;
        public final GameState.DIRECTION direction;
        public final int ID;

        public EnvironmentCard(int mp, GameState.DIRECTION dir, int ID) {
            maxMP = mp;
            direction = dir;
            this.ID = ID;
        }

    @Override
    public int compareTo(EnvironmentCard o) {
        return ID - o.ID;
    }

    @Override
    public String toString() {
        return "ID:"+ID+" MP:"+maxMP + " dir:" + direction.name();
    }


}
