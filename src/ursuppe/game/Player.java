/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.game;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import ursuppe.game.GenePool.GENE;

/**
 *
 * @author Personal
 */
public class Player implements Comparable<Player>, Serializable {


    public static final long serialVersionUID = 24894389781L;
    
    private final String _name;
    private final int _id;
    private final int _colorID;

    private int _advance;
    private int _BP;
    private List<Amoeba> _amoebas;
    private List<Gene> _genes;
    private Map<GENE, Integer> _purchasedRound;

    public Player(int id, String name, int colorID) {
        _advance = _BP = 0;
        _id = id;
        _name = name;
        _colorID = colorID;

        _amoebas = new LinkedList<>();
        _genes = new ArrayList<>();
        _purchasedRound = new HashMap<>();
    }
    
    public int getID() {
        return _id;
    }
    
    public String getName() {
        return _name;
    }
    
    public int getColorID() {
        return _colorID;
    }

    public void advance(int num) {
        _advance += num;
    }

    public int getAdvance() {
        return _advance;
    }

    public void addBP(int BP) {
        _BP += BP;
    }

    public int getBP() {
        return _BP;
    }

    public int getMP() {
        int mp = 0;

        for (Gene gene : _genes) {
            mp += gene.getMPCost();
        }

        return mp;
    }

    public int getNumAmoebas() {
        return _amoebas.size();
    }

    public int getNumGenes() {
        return _genes.size();
    }

    public int getNumGenesForScore() {
        int sum = 0;
        for (Gene gene : _genes) {
            sum += gene.getCardScoreCount();
        }
        return sum;
    }

    public void addAmoeba(Amoeba amoeba) {
        _amoebas.add(amoeba);
        Collections.sort(_amoebas);
    }

    public void removeAmoeba(Amoeba amoeba) {
        _amoebas.remove(amoeba);
    }

    public List<Amoeba> getAmoebas() {
        return _amoebas;
    }

    public Amoeba getAmoebaByNumber(int num) {
        for (Amoeba amoeba : _amoebas) {
            if (amoeba.number == num) return amoeba;
        }

        return null;
    }

    public void addGene(Gene gene, int round) {
        _genes.add(gene);
        gene.geneBought();
        _purchasedRound.put(gene.gene, round);
    }

    public void removeGene(Gene gene) {
        _genes.remove(gene);
        gene.geneReleased();
        _purchasedRound.remove(gene.gene);
    }

    public List<Gene> getGenes() {
        return _genes;
    }

    public boolean hasGene(GENE gene) {
        if (gene == null) return false;

        for (Gene g : _genes) {
            if (g.gene == gene) return true;
        }

        return false;
    }

    public int getPurchasedRound(GENE gene) {
        return _purchasedRound.get(gene);
    }

    public int compareTo(Player o) {
        if (o._advance > this._advance) {
            return 1;
        } else if (o._advance < this._advance) {
            return -1;
        }

        return 0;
    }
}
