/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.game.exception;

/**
 *
 * @author Personal
 */
public class NotEnoughPlayerException extends GameException {
    public NotEnoughPlayerException(String msg) {
        super(msg);
    }
}
