package ursuppe.game;

import java.awt.Point;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import ursuppe.UrsuppeResource;
import ursuppe.game.GameOption.PARASITISM;
import ursuppe.game.GenePool.GENE;
import ursuppe.game.Progress.DICE_STATE;
import ursuppe.game.exception.GameInProgressException;
import ursuppe.game.exception.NotEnoughPlayerException;
import ursuppe.game.exception.TooManyPlayersException;
import ursuppe.game.message.GameEventMessage;
import ursuppe.game.message.GameEventMessage.EVENT;
import ursuppe.game.message.GameMessage;
import ursuppe.game.message.GameTextMessage;
import ursuppe.game.message.GameTextMessage.COLOR_TYPE;
import ursuppe.game.message.GameTextMessage.MESSAGE;
import ursuppe.game.message.RelevantAmoeba;
import ursuppe.game.message.RelevantGene;
import ursuppe.game.message.ServerMessage;

/**
 *
 * @author Personal
 */
public class GameState implements Serializable {

    public static final long serialVersionUID = 374841856L;

    private static final int MIN_PLAYERS = 3;
    private static final int MAX_PLAYERS = 6;

    public static final int INITIAL_FOODS = 2;
    private static final int INITIAL_BP = 4;

    private static final int DEATH_WAIT = 1000;
    private static final int SCORING_WAIT = 500;
    private static final int DICE_WAIT = 1000;

    public static final int ENDING_SCORE = 42;

    public static enum ACTION_PHASE_0 {
        INITIAL_AMOEBA
    }

    public static enum DIRECTION {
        NORTH, WEST, SOUTH, EAST, NONE
    }

    public static enum PHASE1_ACTION {
        DRIFT, MOVE, HOLD, SPEED,
        EAT, EAT_WITH_CHOICE, EAT_WITH_GENES, SFS,
        STARVE
    }

    public static enum ATTACK_RESPONSE {
        NO_DEFENSE,
        BLOCKED,
        ARMOR,
        ALARM,
        ESCAPE,
        DEFENSE,
        HARD_CRUST,
        MORE_THAN_A_MOUTHFUL
    }


    private final GameOption _option;
    private final Progress _progress;
    private final List<Player> _players;
    private final Soup _soup;
    private final GenePool _genePool;
    private final Environment _environment;
    private EnvironmentCard _currentEnvironment;

    private final Random _rand = new Random();

    private int _IDcount = 0;
    private final List<Integer> _colorsLeft;

    private GameStatistics _stats = null;

    public GameState(GameOption option) {
        _option = option;
        
        _progress = new Progress();
        _players = new LinkedList<>();
        _soup = new Soup();
        _genePool = new GenePool(_option);
        _environment = new Environment();

        _colorsLeft = new LinkedList<Integer>() { { add(0);  add(1);  add(2);  add(3);  add(4);  add(5); }};
        Collections.shuffle(_colorsLeft);
    }
    
    public boolean isStarted() {
        return _progress.started;
    }

    private void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException ex) {
        }
    }

    public String getRuleDescription() {
        StringBuilder res = new StringBuilder();

        res.append(UrsuppeResource.getString("rule.gameSetting")).append("<BR>");


        //Extra Genes
        res.append(UrsuppeResource.getString("rule.extraGenes.name")).append(" : ");
        if (_option.allowExtras) {
            res.append(UrsuppeResource.getString("rule.extraGenes.yes")).append("<BR>");
        } else {
            res.append(UrsuppeResource.getString("rule.extraGenes.no")).append("<BR>");
        }

        //SFS Attacks
        res.append(UrsuppeResource.getString("rule.sfsAttack.name")).append(" : ");
        if (_option.sfsStrict) {
            res.append(UrsuppeResource.getString("rule.sfsAttack.strict")).append("<BR>");
        } else {
            res.append(UrsuppeResource.getString("rule.sfsAttack.loose")).append("<BR>");
        }

        //SFS HOL ESC
        res.append(UrsuppeResource.getString("rule.sfsHOL.name")).append(" : ");
        if (_option.SFSESCHOLisSuccess) {
            res.append(UrsuppeResource.getString("rule.sfsHOL.success")).append("<BR>");
        } else {
            res.append(UrsuppeResource.getString("rule.sfsHOL.fail")).append("<BR>");
        }

        //SFS BANG
        res.append(UrsuppeResource.getString("rule.sfsBANG.name")).append(" : ");
        if (_option.SFSESCHOLisSuccess) {
            res.append(UrsuppeResource.getString("rule.sfsBANG.yes")).append("<BR>");
        } else {
            res.append(UrsuppeResource.getString("rule.sfsBANG.no")).append("<BR>");
        }


        //PAR
        res.append(UrsuppeResource.getString("rule.par.name")).append(" : ");
        switch (_option.par) {
            case ONLY_ONCE:
                res.append(UrsuppeResource.getString("rule.par.once")).append("<BR>");
                break;
            case DIFFERENT_AMOEBA:
                res.append(UrsuppeResource.getString("rule.par.amoebas")).append("<BR>");
                break;
            case DIFFERENT_PLAYER:
                res.append(UrsuppeResource.getString("rule.par.players")).append("<BR>");
                break;
            case SAME_AMOEBA_TWICE:
                res.append(UrsuppeResource.getString("rule.par.twice")).append("<BR>");
                break;
        }

        //Score
        res.append("<BR>").append(UrsuppeResource.getString("rule.scoring")).append("<BR>");
        res.append("<pre>").append(UrsuppeResource.getString("rule.amoebaScore")).append("<BR>");
        if (_players.size() < 5) {
            res.append(UrsuppeResource.getString("rule.amoebas")).append(" 1 2 3 4 5 6 7<BR>");
            res.append(UrsuppeResource.getString("rule.score")).append(" 0 0 1 2 4 5 6<BR>");
        } else {
            res.append(UrsuppeResource.getString("rule.amoebas")).append(" 1 2 3 4 5 6<BR>");
            res.append(UrsuppeResource.getString("rule.score")).append(" 0 1 2 4 5 6<BR>");
        }
        res.append(UrsuppeResource.getString("rule.geneScore")).append("<BR>");
        res.append(UrsuppeResource.getString("rule.genes")).append(" 1 2 3 4 5 6+<BR>");
        res.append(UrsuppeResource.getString("rule.score")).append(" 0 0 1 2 3 4</pre><BR>");

        return res.toString();
    }

    /**
     * Starts the game by initializing board, genes, and players
     *
     * @throws NotEnoughPlayerException
     */
    public void startGame() throws NotEnoughPlayerException {
        if (_players.size() < MIN_PLAYERS) throw new NotEnoughPlayerException("");

        Collections.shuffle(_players);

        _progress.startGame();

        _stats = new GameStatistics(_players);

        //add BP
        for (Player player : _players) {
            player.addBP(INITIAL_BP);
        }

        _soup.initialize(_players);
        _genePool.initialize(_option, _players.size());
    }

    public void populateGameStatistics() {
        if (_stats != null && _progress.started) {
           _stats.populateStatistics(this);
        }
    }


    /**
     * Adds a new player and returns its ID.
     *
     * @param name
     * @return
     * @throws TooManyPlayersException
     * @throws GameInProgressException
     */
    public int addPlayer(String name, int preferredColor1, int preferredColor2) throws TooManyPlayersException, GameInProgressException {
        //check if same named player exists
        for (Player player : _players) {
            if (player.getName().compareTo(name) == 0) {
                return player.getID();
            }
        }
        
        //see if game is in progress
        if (_progress.started) throw new GameInProgressException("");

        //check for number of players
        if (_players.size() >= MAX_PLAYERS) throw new TooManyPlayersException("");
        
        //try to respect preferrence
        int color1 = -1;
        int color2 = -1;
        for (int color : _colorsLeft) {
            if (color == preferredColor1) color1 = color;
            if (color == preferredColor2) color2 = color;
        }

        
        final int color;
        if (color1 != -1) {
            color = color1;
        } else if (color2 != -1) {
            color = color2;
        } else {
            color = _colorsLeft.get(0);
        }
        _colorsLeft.remove(Integer.valueOf(color));

        _players.add(new Player(_IDcount, name, color));
        _IDcount++;
        
        return _players.get(_players.size()-1).getID();
    }

    /**
     * Removes a player
     *
     * @param playerID
     * @return
     */
    public ServerMessage removePlayer(int playerID) {
        if (getPlayerByID(playerID) != null) {
            String name = getPlayerByID(playerID).getName();
            Player target = getPlayerByID(playerID);
            _players.remove(target);
            _colorsLeft.add(target.getColorID());

            return ServerMessage.singleArgMessage(ServerMessage.ARGC1_MESSAGE.LEAVE, name, ServerMessage.COLOR_TYPE.DEFAULT);
        } else {
            return null;
        }
    }

    /**
     * Returns Player that have the given playerID
     *
     * @param id
     * @return
     */
    public Player getPlayerByID(int id) {
        for (Player player : _players) {
            if (player.getID() == id) return player;
        }

        return null;
    }

    /**
     * Returns the Player in the specified place, indexed from 0 and 0 has the highest score.
     *
     * @param i
     * @return
     */
    public Player getPlayerByOrder(int i) {
        if (i >= _players.size()) return null;
        return _players.get(i);
    }

    /**
     * Returns Player of the current turn.
     * Note that it will not return the Player who is deciding whether or not to hold on to another amoeba
     * of a different player who just moved. In this case, getCurrentPlayer will return the player that just moved an amoeba.
     *
     * @return
     */
    public Player getCurrentPlayer() {
        return getPlayerByOrder(_progress.currentOrder);
    }

    /**
     * Returns Amoeba of the current turn.
     * It follows the same convention as getCurrentPlayer
     *
     * @return
     */
    public Amoeba getCurrentAmoeba() {
        Player player = getCurrentPlayer();
        if (_progress.currentAmoeba < 0 || _progress.currentAmoeba >= player.getAmoebas().size()) return null;
        
        return player.getAmoebas().get(_progress.currentAmoeba);
    }

    /**
     * Returns the list of players
     *
     * @return
     */
    public List<Player> getPlayers() {
        return _players;
    }

    /**
     * Returns the Soup object
     *
     * @return
     */
    public Soup getSoup() {
        return _soup;
    }

    /**
     * Returns Progress
     *
     * @return
     */
    public Progress getProgress() {
        return _progress;
    }

    /**
     * Returns Gene Pool
     *
     * @return
     */
    public GenePool getGenePool() {
        return _genePool;
    }

    /***
     * Returns current environment
     *
     * @return
     */
    public EnvironmentCard getCurrentEnvironment() {
        return _currentEnvironment;
    }

    /**
     * Returns the environment card that it not yet played,
     * but viewable by special genes.
     *
     * @param gene
     * @return
     */
    public EnvironmentCard peekNextEnvironment(GENE gene) {
        if (gene == GENE.MIG) return _environment.peek(0);
        if (gene == GENE.ENV) return _environment.peek(1);

        return null;
    }

    public Environment getEnvironment() {
        return _environment;
    }

    /**
     * Returns the number of maximum amoebas in this game
     *
     * @return
     */
    public int maximumAmoebaCount() {
        if (_players.size() > 4) return 6;

        return 7;
    }

    /**
     * Returns the income at the beginning of Phase 4 in this game
     *
     * @return
     */
    public int getPhase4Income() {
        if (_players.size() > 4) return 11;

        return 10;
    }

    /**
     * Returns the result of a die roll
     *
     * @return
     */
    private int roll() {
        return _rand.nextInt(6)+1;
    }

    /**
     * Returns if extra gene is used in this game.
     *
     * @return
     */
    public boolean isExtraGeneUsed() {
        return _players.size() > 4 || _option.allowExtras;
    }

    public boolean isBANGusableAgainstSFS() {
        return _option.canBANGwithSFS;
    }

    public boolean isESCchasableByHOLD() {
        return _option.SFSESCHOLisSuccess;
    }

    /**
     * Returns string representation of Point.
     *
     * @param p
     * @return
     */
    public static String pointToString(Point p) {
        return "("+p.x+","+p.y+")";
    }

    /***AMOEBAS***/

    /**
     * Spawns a new amoeba in the specified location.
     *
     * @param playerID
     * @param amoebaNum
     * @param loc
     * @return
     */
    private GameMessage addAmoeba(int playerID, int amoebaNum, Point loc) {
        Player player = getCurrentPlayer();
        //do some checking
        if (player.getID() == playerID) {
            Amoeba moeba = new Amoeba(playerID, amoebaNum, getPlayerByID(playerID).getColorID(), loc);

            getPlayerByID(playerID).addAmoeba(moeba);
            _soup.getCell(loc).addAmoeba(moeba);

            //RelevantGene[] rgens = null;
            List<RelevantGene> rgenes = new LinkedList<>();
            if (player.hasGene(GENE.SPO)) rgenes.add(new RelevantGene(playerID, GENE.SPO));
            if (player.hasGene(GENE.DIV)) rgenes.add(new RelevantGene(playerID, GENE.DIV));
            if (player.hasGene(GENE.SLS)) rgenes.add(new RelevantGene(playerID, GENE.SLS));
            if (player.hasGene(GENE.POP) && _progress.POPactive)  rgenes.add(new RelevantGene(playerID, GENE.POP));

            if (rgenes.isEmpty()) {
                return new GameTextMessage(MESSAGE.ADD_AMOEBA, new String[]{getPlayerByID(playerID).getName(), loc.x+","+loc.y},
                        COLOR_TYPE.PLAYER, playerID, null,
                        new RelevantAmoeba[] {new RelevantAmoeba(playerID, amoebaNum)});
            } else {
                RelevantGene[] rgenesA = new RelevantGene[rgenes.size()];
                for (int i = 0; i < rgenesA.length; i++) {
                    rgenesA[i] = rgenes.get(i);
                }
                return new GameTextMessage(MESSAGE.ADD_AMOEBA, new String[]{getPlayerByID(playerID).getName(), loc.x+","+loc.y},
                        COLOR_TYPE.PLAYER, playerID, rgenesA,
                        new RelevantAmoeba[] {new RelevantAmoeba(playerID, amoebaNum)});
            }
        }

        return null;
    }

    @SuppressWarnings("element-type-mismatch")
    /**
     * Returns a list of numbers that can be given to amoebas
     *
     */
    public List<Integer> getRemainingAmoebaNumbers(int playerID) {
        Player player = getPlayerByID(playerID);

        List<Integer> all = new LinkedList<Integer>(){{add(1); add(2); add(3); add(4); add(5); add(6);}};

        if (_players.size() <= 4) {
            all.add(7);
        }

        for (Amoeba amoeba : player.getAmoebas()) {
           all.remove((Object)((Integer)amoeba.number));
        }

        return all;
    }

    /**
     * Moves an amoeba towards the specified direction.
     *
     * @param amoeba
     * @param direction
     * @return
     */
    private List<GameMessage> moveAmoeba(Amoeba amoeba, DIRECTION direction) {
        List<GameMessage> res = new LinkedList<>();
        Point loc = amoeba.getLocation();
        Point newLoc = getDestiation(loc, direction);

        if (loc.equals(newLoc)) return res;

        int srcIndex = _soup.getCell(loc).getAmoebas().indexOf(amoeba);

        amoeba.setLocation(newLoc);
        _soup.getCell(loc).removeAmoeba(amoeba);
        _soup.getCell(newLoc).addAmoeba(amoeba);

        int dstIndex = _soup.getCell(newLoc).getAmoebas().indexOf(amoeba);

        res.add(new GameEventMessage(EVENT.MOVE, new Object[]{amoeba, loc, srcIndex, newLoc, dstIndex}));

        int DP = _soup.getCell(newLoc).getDP();
        if (DP > 0) {
            amoeba.addDP(DP);
            _soup.getCell(newLoc).addDP(-DP);
            Player owner = getPlayerByID(amoeba.playerID);
            res.add(new GameTextMessage(MESSAGE.GENE_TOX_HIT, new String[]{owner.getName(), Integer.toString(amoeba.number), Integer.toString(DP)},
                    COLOR_TYPE.PLAYER, owner.getID()));
        }

        return res;
        
    }

    /**
     * Returns the cost of movement
     *
     * @param playerID
     * @return
     */
    public int getMovementCost(int playerID) {
        if (getPlayerByID(playerID).hasGene(GENE.STR)) return 0;

        if (_progress.amoebaMoved && !_progress.SPDused && (getPlayerByID(playerID).hasGene(GENE.SPD) || getPlayerByID(playerID).hasGene(GENE.PER))) return 0;

        return 1;
    }

    /**
     * Returns the number of dice to roll when moving
     *
     * @param playerID
     * @return
     */
    public int getMovementDiceNum(int playerID) {
        if (getPlayerByID(playerID).hasGene(GENE.MV2)) return 0;
        if (getPlayerByID(playerID).hasGene(GENE.MV1)) return 2;

        return 1;
    }

    /***INITIAL SCORE***/

    /**
     * Returns the available initial scores.
     * If res[i] = true, i is available
     *
     * @return
     */
    public boolean[] getAvailableInitialScores() {
        boolean[] res = new boolean[_players.size()+1];

        //Set every place to be available
        for (int i = 1; i < res.length; i++) res[i] = true;

        //mark off those already taken
        for (Player player : _players) {
            if (player.getAdvance() > 0) {
                res[player.getAdvance()] = false;
            }
        }

        return res;
    }

    /**
     * Initial score selected
     *
     * @param playerID
     * @param score
     * @return
     */
    public GameMessage setInitialScore(int playerID, int score) {
        if (getPlayerByID(playerID).getAdvance() == 0) {
            getPlayerByID(playerID).advance(score);

            //next player
            _progress.currentOrder++;
            //end of initial score selection
            if (_progress.currentOrder >= _players.size()) {
                _progress.currentPhase = Progress.PHASE_FIRST_AMOEBA;
                _progress.currentOrder = _players.size() - 1;
                Collections.sort(_players);

                _currentEnvironment = _environment.pop();
            }

            return new GameTextMessage(MESSAGE.INITIAL_SCORE, new String[]{getPlayerByID(playerID).getName(), Integer.toString(score)}, COLOR_TYPE.PLAYER, playerID, null, null);
        }

        return null;
    }

    /***INITIAL PLACEMENT***/
    /**
     * Returns the availabel locations for initial placement
     *
     * @return
     */
    public boolean[][] getInitialPlacementAvailability() {
        boolean[][] avail = new boolean[Soup.SOUP_WIDTH][Soup.SOUP_HEIGHT];

        for (int x = 0; x < Soup.SOUP_WIDTH; x++) {
            for (int y = 0; y < Soup.SOUP_HEIGHT; y++) {
                avail[x][y] = false;
                if (Soup.isValidLocation(x, y) && _soup.getCell(x, y).getAmoebas().isEmpty()) {
                     avail[x][y] = true;
                }
            }
        }

        return avail;
    }

    /**
     * Initial amoeba selected
     *
     * @param playerID
     * @param amoebaNum
     * @param loc
     * @return
     */
    public List<GameMessage> addInitialAmoeba(int playerID, int amoebaNum, Point loc) {

        List<GameMessage> res = new LinkedList<>();
        GameMessage gm = addAmoeba(playerID, amoebaNum, loc);

        if (gm != null) {
            res.add(gm);
            //First amoeba in ascending order
            if (_progress.currentPhase == Progress.PHASE_FIRST_AMOEBA) {
                _progress.currentOrder--;
                //Add one DP
                getPlayerByID(playerID).getAmoebas().get(0).addDP(1);

                //End of phase
                if (_progress.currentOrder < 0) {
                    _progress.currentOrder = 0;
                    _progress.currentPhase = Progress.PHASE_SECOND_AMOEBA;
                }
            //Second amoeba in descending order
            } else if (_progress.currentPhase == Progress.PHASE_SECOND_AMOEBA) {
                _progress.currentOrder++;
                //End of phase
                if (_progress.currentOrder >= _players.size()) {
                    //start of new round
                    res.addAll(startNewRound());
                }
            }
        }

        return res;
    }


    /***ROUND***/

    /**
     * Starts a new round
     *
     * @return
     */
    private List<GameMessage> startNewRound() {
        List<GameMessage> res = new LinkedList<>();

        Collections.sort(_players);

        _progress.currentPhase = Progress.PHASE_1_MOVE_AND_FEED;
        _progress.currentOrder = _players.size()-1;
        _progress.currentAmoeba = 0;

        _progress.clearRoundProgress();

        _progress.currentRound++;

        res.add(new GameTextMessage(MESSAGE.ROUND_START, new String[]{Integer.toString(_progress.currentRound)}, COLOR_TYPE.EMPHASIZE, 0, null, null));

        int migID = -1;
        for (Player player : _players) {
            if (player.hasGene(GENE.MIG)) migID = player.getID();
        }

        if (migID != -1) {
            res.add(new GameTextMessage(MESSAGE.NEW_PHASE, new String[]{"1"}, COLOR_TYPE.EMPHASIZE, 0, new RelevantGene[]{new RelevantGene(migID, GENE.MIG)}));
        } else {
            res.add(new GameTextMessage(MESSAGE.NEW_PHASE, new String[]{"1"}, COLOR_TYPE.EMPHASIZE, 0));
        }

        res.add(findNextPhase1Player());

        return res;
    }

    /**
     * Find next player in Phase 1
     *
     * @return
     */
    private GameMessage findNextPhase1Player() {

        while (_progress.currentOrder >= 0) {
            Player player = getCurrentPlayer();
            _progress.currentAmoeba = 0;
            //IF some one has no amoeba
            if (player.getNumAmoebas() == 0) {
                _progress.currentOrder--;
            } else {
               return new GameTextMessage(MESSAGE.TURN_SIGNAL, new String[]{player.getName()}, COLOR_TYPE.PLAYER, player.getID());
            }
        }

        return startPhase2();
    }

    /***PHASE 1***/

    /**
     * Returns available actions in phase1
     *
     * @param playerID
     * @param amoebaIndex
     * @return
     */
    public List<PHASE1_ACTION> getAvailablePhase1Actions(int playerID, int amoebaIndex) {
        List<PHASE1_ACTION> avail = new LinkedList<>();

        if (playerID != getPlayerByOrder(_progress.currentOrder).getID()) return null;

        Player player = getPlayerByID(playerID);
        Amoeba amoeba = player.getAmoebas().get(amoebaIndex);

        if (amoeba == null) return null;

        //When not moved yet
        if (!_progress.amoebaMoved) {
            //Drift can be done any time if amoeba hasn't moved
            avail.add(PHASE1_ACTION.DRIFT);
            //needs 1 BP unless has STR
            if (player.getBP() > 0 || player.hasGene(GenePool.GENE.STR)) avail.add(PHASE1_ACTION.MOVE);

            //If hold is available
            if (player.hasGene(GenePool.GENE.HOL)) avail.add(PHASE1_ACTION.HOLD);

            //If fast food is available
            if (!_progress.amoebaFed && player.hasGene(GenePool.GENE.FAST)) {
                //Normal feeding
                List<PHASE1_ACTION> feedingOptions = getFeedingOptions(player, amoeba);
                if (feedingOptions.contains(PHASE1_ACTION.EAT)) avail.add(PHASE1_ACTION.EAT);
                if (feedingOptions.contains(PHASE1_ACTION.EAT_WITH_CHOICE)) avail.add(PHASE1_ACTION.EAT_WITH_CHOICE);
                if (feedingOptions.contains(PHASE1_ACTION.EAT_WITH_GENES)) avail.add(PHASE1_ACTION.EAT_WITH_GENES);
                //SFS
                 if (canUseSFS(player, amoeba, feedingOptions)) avail.add(PHASE1_ACTION.SFS);
            }
        //If already moved
        } else {
            //check if speed is available
            if (!_progress.SPDused && (player.hasGene(GenePool.GENE.SPD) || player.hasGene(GenePool.GENE.PER))) {
                avail.add(PHASE1_ACTION.SPEED);
                //Then it has to decide if SPD is used or not
                return avail;
            }

            //Now it has to eat, unless it has already eaten
            if (!_progress.amoebaFed) avail.addAll(getFeedingOptions(player, amoeba));

            //SFS
            if (canUseSFS(player, amoeba, avail)) avail.add(PHASE1_ACTION.SFS);
        }

        return avail;
    }

    /**
     * Returns if the given amoeba can attack with SFS.
     *
     * @param attacker
     * @param attackerAmoeba
     * @param feedingOptions
     * @return
     */
    private boolean canUseSFS(Player attacker, Amoeba attackerAmoeba, List<PHASE1_ACTION> feedingOptions) {

        //Feeding option check
        if (!feedingOptions.contains(PHASE1_ACTION.STARVE)) return false;
        //If strict, eating of any sort is not allowed
        if (_option.sfsStrict && (feedingOptions.contains(PHASE1_ACTION.EAT) || feedingOptions.contains(PHASE1_ACTION.EAT_WITH_CHOICE) || feedingOptions.contains(PHASE1_ACTION.EAT_WITH_GENES))) return false;
        //Whe not strict, normal eating is not allowed
        if (!_option.sfsStrict && (feedingOptions.contains(PHASE1_ACTION.EAT) || feedingOptions.contains(PHASE1_ACTION.EAT_WITH_CHOICE))) return false;
        //Gene check
        if (!attacker.hasGene(GENE.SFS) && !attacker.hasGene(GENE.AGR)) return false;
        //BP check
        if (attacker.getBP() == 0) return false;

        for (Amoeba defenderAmoeba : _soup.getCell(attackerAmoeba.getLocation()).getAmoebas()) {
            //Skip your own
            if (attackerAmoeba.equals(defenderAmoeba)) continue;
            //get defending player
            Player defender = getPlayerByID(defenderAmoeba.playerID);
            //If the attack is not blocked
            List<ATTACK_RESPONSE> res = getAttackResponse(attacker, attackerAmoeba, defender, defenderAmoeba);
            if (res.size() > 0 && !res.contains(ATTACK_RESPONSE.BLOCKED)) return true;
        }

        return false;
    }

    public List<GameMessage> amoebaDrifted(int playerID, int[] TENfoods, boolean useREB) {
        List<GameMessage> res = new LinkedList<>();

        Player player = getCurrentPlayer();
        //verification
        if (!_progress.amoebaMoved && playerID == player.getID() && _progress.currentPhase == Progress.PHASE_1_MOVE_AND_FEED) {
            _progress.amoebaMoved = true;
            _progress.SPDused = true; //SPD can't be used anymore
            Amoeba amoeba = getCurrentAmoeba();

            DIRECTION dir = _currentEnvironment.direction;
            if (useREB) dir = getOppositeDirection(dir);

            //Tentacle
            if (player.hasGene(GenePool.GENE.TEN) && TENfoods != null) {
                res.add(useTEN(player, TENfoods, amoeba.getLocation(), dir));
            }

            Point oldLoc = (Point)amoeba.getLocation().clone();

           //Drift message
            if (useREB) {
                res.add(new GameTextMessage(MESSAGE.GENE_REB_DRIFT, new String[]{player.getName(), Integer.toString(amoeba.number), getDirectionString(dir)},
                        COLOR_TYPE.PLAYER, player.getID(), new RelevantGene[]{new RelevantGene(player.getID(), GENE.REB)}));
            } else {
                res.add(new GameTextMessage(MESSAGE.DRIFT, new String[]{player.getName(), Integer.toString(amoeba.number), getDirectionString(dir)},
                        COLOR_TYPE.PLAYER, player.getID()));
            }

            //Move
            res.addAll(moveAmoeba(amoeba, dir));

            //Energy conservation
            if (player.hasGene(GenePool.GENE.ENE)) {
                player.addBP(1);
                res.add(new GameTextMessage(MESSAGE.GENE_ENE, new String[]{player.getName()}, COLOR_TYPE.PLAYER, player.getID(),
                        new RelevantGene[] {new RelevantGene(player.getID(), GenePool.GENE.ENE)}));
            }

            res.addAll(endOfMovementHandling(player, amoeba, oldLoc));
            
            //With FAST, this may be the end
            res.addAll(amoebaFinished());
        }

        return res;
    }


    public List<GameMessage> amoebaMoved(int playerID, DIRECTION dir, int[] TENfoods, boolean useREB) {
        List<GameMessage> res = new LinkedList<>();

        Player player = getCurrentPlayer();
        //verification
        if ((playerID == player.getID() && _progress.currentPhase == Progress.PHASE_1_MOVE_AND_FEED) &&
                //hasn't moved or SPD ability usable
              ((!_progress.amoebaMoved || (_progress.amoebaMoved && !_progress.SPDused && (player.hasGene(GENE.SPD) || player.hasGene(GENE.PER)))))) {
            if (_progress.amoebaMoved) _progress.SPDused = true;
            _progress.amoebaMoved = true;
            
            Amoeba amoeba = player.getAmoebas().get(_progress.currentAmoeba);

            //Tentacle
            if (player.hasGene(GenePool.GENE.TEN) && TENfoods != null) {
                res.add(useTEN(player, TENfoods, amoeba.getLocation(), dir));
            }

           //MOVE message
            if (dir != DIRECTION.NONE) {
                if (useREB) {
                    res.add(new GameTextMessage(MESSAGE.GENE_REB_MOVE, new String[]{player.getName(), Integer.toString(amoeba.number), getDirectionString(getOppositeDirection(dir))},
                            COLOR_TYPE.PLAYER, player.getID(), new RelevantGene[]{new RelevantGene(player.getID(), GENE.REB)}));
                } else {
                    res.add(new GameTextMessage(MESSAGE.MOVE, new String[]{player.getName(), Integer.toString(amoeba.number), getDirectionString(dir)},
                            COLOR_TYPE.PLAYER, player.getID()));
                }
            } else {
                res.add(new GameTextMessage(MESSAGE.MOVE_STAY, new String[]{player.getName(), Integer.toString(amoeba.number)},
                        COLOR_TYPE.PLAYER, player.getID()));
            }


            Point oldLoc = (Point)amoeba.getLocation().clone();
            
            //Move
            res.addAll(moveAmoeba(amoeba, dir));

            _progress.clearDice();
            
            res.addAll(endOfMovementHandling(player, amoeba, oldLoc));
            
            //With FAST, this may be the end
            res.addAll(amoebaFinished());
        }

        return res;
    }


    public List<GameMessage> HOLDSelected(int playerID) {
        List<GameMessage> res = new LinkedList<>();

        Player player = getCurrentPlayer();
        //verification
        if (playerID == player.getID() && _progress.currentPhase == Progress.PHASE_1_MOVE_AND_FEED && player.hasGene(GENE.HOL)) {
            _progress.amoebaMoved = true;
            _progress.SPDused = true;
            Amoeba amoeba = getCurrentAmoeba();
            
            res.add(new GameTextMessage(MESSAGE.GENE_HOL_STAY, new String[]{player.getName(), Integer.toString(amoeba.number)},
                    COLOR_TYPE.PLAYER, player.getID(), new RelevantGene[]{new RelevantGene(player.getID(), GENE.HOL)}));
            
            //With FAST, this may be the end
            res.addAll(amoebaFinished());

        }

        return res;
        
    }

    public List<GameMessage> HOLDSelected(int playerID, boolean useHOL, int[] TENfoods) {
        List<GameMessage> res = new LinkedList<>();

        Player player = getPlayerByID(_progress.HOLPlayerID);
        Amoeba amoeba = player.getAmoebas().get(_progress.HOLAmoebaIndex);

        //verify
        if (player.getID() == playerID && _progress.HOLactive) {
            if (useHOL) {
                //Tentacle
                if (player.hasGene(GenePool.GENE.TEN) && TENfoods != null) {
                    res.add(useTEN(player, TENfoods, amoeba.getLocation(), _progress.HOLdirection));
                }
                //HOL message
                res.add(new GameTextMessage(MESSAGE.GENE_HOL, new String[]{player.getName(), Integer.toString(amoeba.number), getDirectionString(_progress.HOLdirection)},
                        COLOR_TYPE.PLAYER, player.getID(), new RelevantGene[]{new RelevantGene(player.getID(), GENE.HOL)}));

                //Move
                res.addAll(moveAmoeba(amoeba, _progress.HOLdirection));
            }

            _progress.HOLAmoebaIndex = findNextHOLDCandidate();
            //finish
            if (_progress.HOLAmoebaIndex == -1) {
                _progress.clearHOLProgress();
            }
        }

        return res;
    }

    public List<GameMessage> cancelSpeed(int playerID) {
        List<GameMessage> res = new LinkedList<>();

        Player player = getCurrentPlayer();
        //verification
        if (playerID == player.getID() && _progress.currentPhase == Progress.PHASE_1_MOVE_AND_FEED && (player.hasGene(GENE.SPD) || player.hasGene(GENE.PER))) {
            _progress.SPDused = true;
            //With FAST, this may be the end
            res.addAll(amoebaFinished());
        }

        return res;
    }

    private List<GameMessage> endOfMovementHandling(Player player, Amoeba amoeba, Point oldLoc) {
        List<GameMessage> res = new LinkedList<>();
        //HOL handling
        if (_genePool.getGene(GENE.HOL).getAvailableNumber() < _genePool.getGene(GENE.HOL).max && !amoeba.getLocation().equals(oldLoc)) {
            _progress.HOLactive = true;
            _progress.HOLlocation = oldLoc;
            _progress.HOLdirection = this.getDirectionToReachNewLocation(oldLoc, amoeba.getLocation());
            //get ID of HOL
            for (Player p : _players) if (p.hasGene(GENE.HOL)) _progress.HOLPlayerID = p.getID();
            //find the next amoeba to HOLD
            _progress.HOLAmoebaIndex = -1;
            _progress.HOLAmoebaIndex = findNextHOLDCandidate();
            //No hold candidate
            if (_progress.HOLAmoebaIndex == -1) {
                _progress.clearHOLProgress();
            } else {
                Player holP = this.getPlayerByID(_progress.HOLPlayerID);
                Amoeba holA = holP.getAmoebas().get(_progress.HOLAmoebaIndex);
                res.add(new GameTextMessage(MESSAGE.GENE_HOL_NOTICE, new String[]{holP.getName(), Integer.toString(holA.number)}, COLOR_TYPE.PLAYER, holP.getID()));
            }
        }
        return res;
    }

    private int findNextHOLDCandidate() {
        Player player = getPlayerByID(_progress.HOLPlayerID);
        if (!player.hasGene(GENE.HOL)) return -1;
        
        for (int i = Math.max(0, _progress.HOLAmoebaIndex+1); i < player.getNumAmoebas(); i++) {
            if (player.getAmoebas().get(i).getLocation().equals(_progress.HOLlocation)) return i;
        }
        
        return -1;
    }

    public List<GameMessage> eat(int playerID, Point exLoc, boolean tox) {
        List<GameMessage> res = new LinkedList<>();

        Player player = getCurrentPlayer();
        Amoeba amoeba = getCurrentAmoeba();

        //
//        if (amoeba == null) return res;
        //ID check
        if (player.getID() != playerID) return res;
        //phase check
        if (_progress.currentPhase != Progress.PHASE_1_MOVE_AND_FEED) return res;
        //fed check
        if (_progress.amoebaFed) return res;
        
        //check
        List<PHASE1_ACTION> actions = getAvailablePhase1Actions(player.getID(), _progress.currentAmoeba);
        if (!actions.contains(PHASE1_ACTION.EAT)) return res;

        int[] foodsEaten = eatNormally(player, amoeba);
        res.add(new GameTextMessage(MESSAGE.EAT,  new String[]{player.getName(), Integer.toString(amoeba.number), foodToString(foodsEaten)}, COLOR_TYPE.PLAYER, player.getID()));
        res.addAll(excrete(player, amoeba, exLoc, tox));
        _progress.amoebaFed = true;

        //w/o FAST this would be the end of this amoeba
        res.addAll(amoebaFinished());

        return res;
    }

    public List<GameMessage> eat(int playerID, int[] foods, int[] parPlayerIDs, Point sucLoc, int[] sucFoods, Point exLoc, boolean tox) {
        List<GameMessage> res = new LinkedList<>();

        Player player = getCurrentPlayer();
        Amoeba amoeba = getCurrentAmoeba();

        //ID check
        if (player.getID() != playerID) return res;
        //phase check
        if (_progress.currentPhase != Progress.PHASE_1_MOVE_AND_FEED) return res;
        //fed check
        if (_progress.amoebaFed) return res;
        //input check
        if (foods == null && sucFoods == null) {
            return eat(playerID, exLoc, tox);
        }

        //when only SUC is used
        if (foods == null) {
            //make sure it can eat
            if (!canFeedAfterSUC(player, amoeba.getLocation(), sucFoods, false, false)) return res;

            //SUC
            res.add(new GameTextMessage(MESSAGE.GENE_SUC, new String[]{player.getName(), Integer.toString(amoeba.number), foodToString(sucFoods), pointToString(sucLoc)},
                        COLOR_TYPE.PLAYER, player.getID(), new RelevantGene[]{new RelevantGene(player.getID(), GENE.SUC1)}));

            _soup.getCell(sucLoc).takeFood(sucFoods);
            _soup.getCell(amoeba.getLocation()).addFood(sucFoods);

            int[] foodsEaten = eatNormally(player, amoeba);
             res.add(new GameTextMessage(MESSAGE.EAT,  new String[]{player.getName(), Integer.toString(amoeba.number), foodToString(foodsEaten)}, COLOR_TYPE.PLAYER, player.getID()));
             
        //When food number is changed
        } else {
            //PAR
            int activePars = 0;
            if (parPlayerIDs != null) activePars = parPlayerIDs.length;
            //check this combination is valid
            if (!canEat(player, foods, activePars)) return res;

            //check SUC is valid
            int[] origFoods = _soup.getCell(amoeba.getLocation()).getFood();
            int[] totalFoods = new int[origFoods.length];
            //Add SUC and make sure number to eat is less than what is there
            for (int i = 0; i < totalFoods.length; i++) {
                totalFoods[i] = origFoods[i];
                if (sucFoods != null) totalFoods[i] += sucFoods[i];
                if (totalFoods[i] < foods[i]) return res;
            }

            //SUC
            if (sucFoods != null) {
                res.add(new GameTextMessage(MESSAGE.GENE_SUC, new String[]{player.getName(), Integer.toString(amoeba.number), foodToString(sucFoods), pointToString(sucLoc)},
                            COLOR_TYPE.PLAYER, player.getID(), new RelevantGene[]{new RelevantGene(player.getID(), GENE.SUC1)}));

                _soup.getCell(sucLoc).takeFood(sucFoods);
                _soup.getCell(amoeba.getLocation()).addFood(sucFoods);
            }

            //Parsite
            if (activePars > 0) {
                for (int id : parPlayerIDs) {
                    Player parPL = getPlayerByID(id);
                    parPL.addBP(-1);
                    res.add(new GameTextMessage(MESSAGE.GENE_PAR, new String[]{parPL.getName()}, COLOR_TYPE.PLAYER, parPL.getID(),
                            new RelevantGene[]{new RelevantGene(player.getID(), GENE.PAR)}));
                }
            }

            //eat
            _soup.getCell(amoeba.getLocation()).takeFood(foods);

            //Relevant Genes
             List<RelevantGene> rg = new LinkedList<>();
             if (player.hasGene(GENE.SUB)) rg.add(new RelevantGene(player.getID(), GENE.SUB));
             if (player.hasGene(GENE.FRU)) rg.add(new RelevantGene(player.getID(), GENE.FRU));
             if (player.hasGene(GENE.GLU)) rg.add(new RelevantGene(player.getID(), GENE.GLU));
             if (player.hasGene(GENE.PAR) && activePars > 0) rg.add(new RelevantGene(player.getID(), GENE.PAR));

             //Genes
             RelevantGene[] rgenes = new RelevantGene[rg.size()];
             for (int i = 0; i < rg.size(); i++) rgenes[i] = rg.get(i);

             res.add(new GameTextMessage(MESSAGE.EAT,  new String[]{player.getName(), Integer.toString(amoeba.number), foodToString(foods)}, COLOR_TYPE.PLAYER, player.getID(), rgenes));

           
        }

        //Excretion is normal
        res.addAll(excrete(player, amoeba, exLoc, tox));
        _progress.amoebaFed = true;

        //w/o FAST this would be the end of this amoeba
        res.addAll(amoebaFinished());

        
        return res;
    }

    public List<GameMessage> amoebaStarved(int playerID) {
        List<GameMessage> res = new LinkedList<>();
        Player player = getCurrentPlayer();
        if (!_progress.amoebaFed && playerID == player.getID() && _progress.currentPhase == Progress.PHASE_1_MOVE_AND_FEED) {
            Amoeba amoeba = player.getAmoebas().get(_progress.currentAmoeba);
            amoeba.addDP(1);

            _progress.amoebaFed = true;

            res.add(new GameTextMessage(MESSAGE.STARVE, new String[]{player.getName(), Integer.toString(amoeba.number)}, COLOR_TYPE.PLAYER, player.getID()));

            res.addAll(amoebaFinished());
        }

        return res;
    }

    private List<GameMessage> amoebaFinished() {
        List<GameMessage> messages = new LinkedList<>();
        Player player = getCurrentPlayer();

        //finishing condition
        if (_progress.amoebaFed && _progress.amoebaMoved && (_progress.SPDused || !(player.hasGene(GENE.SPD) || player.hasGene(GENE.PER)))) {
            //Next amoeba
            _progress.currentAmoeba++;
            _progress.clearAmoebaProgress();

            //If no more amoeba left, move to next player
            if (getPlayerByOrder(_progress.currentOrder).getAmoebas().size() <= _progress.currentAmoeba) {
                messages.add(new GameTextMessage(MESSAGE.END_PHASE_1, new String[]{player.getName()}, COLOR_TYPE.PLAYER, player.getID()));
                _progress.currentOrder--;
                messages.add(findNextPhase1Player());
                return messages;
            }
        }

        return messages;
    }

    public GameMessage moveRequested(int playerID) {

        Player player = getCurrentPlayer();
        //valid player and
        if ((playerID == player.getID() && _progress.currentPhase == Progress.PHASE_1_MOVE_AND_FEED) &&
                //hasn't moved or SPD ability usable
              ((!_progress.amoebaMoved || (_progress.amoebaMoved && !_progress.SPDused && (player.hasGene(GENE.SPD) || player.hasGene(GENE.PER)))))) {
            int cost = getMovementCost(playerID);
            player.addBP(-cost);

            RelevantGene costRG = null;
            if (cost == 0 && !_progress.amoebaMoved && player.hasGene(GENE.STR)) {
                costRG = new RelevantGene(player.getID(), GENE.STR);
            } else if (cost == 0 && _progress.amoebaMoved && !_progress.SPDused && (player.hasGene(GENE.SPD) || player.hasGene(GENE.PER))) {
                costRG = new RelevantGene(player.getID(), GENE.SPD);
            }

            if (player.hasGene(GENE.MV2)) {
                _progress.diceState = DICE_STATE.MOVE_MV2;
            } else if (player.hasGene(GENE.MV1)) {
                _progress.diceState = DICE_STATE.MOVE_MV1;
                _progress.dice1 = roll();
                _progress.dice2 = roll();
                if (costRG != null) {
                    return new GameTextMessage(MESSAGE.DICE_ROLL, new String[]{player.getName(), Integer.toString(_progress.dice1), Integer.toString(_progress.dice2)},
                            COLOR_TYPE.PLAYER, player.getID(), new RelevantGene[] {new RelevantGene(player.getID(), GENE.MV1), costRG});
                } else {
                    return new GameTextMessage(MESSAGE.DICE_ROLL, new String[]{player.getName(), Integer.toString(_progress.dice1), Integer.toString(_progress.dice2)},
                            COLOR_TYPE.PLAYER, player.getID(), new RelevantGene[] {new RelevantGene(player.getID(), GENE.MV1)});
                }
            } else {
                _progress.diceState = DICE_STATE.MOVE;
                _progress.dice1 = roll();
                if (costRG != null) {
                    return new GameTextMessage(MESSAGE.DIE_ROLL, new String[]{player.getName(), Integer.toString(_progress.dice1)}, COLOR_TYPE.PLAYER, player.getID(),
                            new RelevantGene[]{costRG});
                } else {
                    return new GameTextMessage(MESSAGE.DIE_ROLL, new String[]{player.getName(), Integer.toString(_progress.dice1)}, COLOR_TYPE.PLAYER, player.getID());
                }
            }
        }

        return null;
    }


    /***MOVEMENT***/

    public List<DIRECTION> getMovableDirection(DICE_STATE dice, int dice1, int dice2) {
        List<DIRECTION> res = new LinkedList<>();

        if (dice == DICE_STATE.MOVE_MV2 || dice == DICE_STATE.ESC_MV2) {
                    res.add(DIRECTION.WEST);
                    res.add(DIRECTION.NORTH);
                    res.add(DIRECTION.EAST);
                    res.add(DIRECTION.SOUTH);
                    return res;
        }

        int i = 1;
        while (i < 3) {
            int d;
            if (i == 1) {
                d = dice1;
            } else {
                d = dice2;
            }
            i++;
            
            switch (d) {
                case 1:
                    res.add(DIRECTION.WEST);
                    break;
                case 2:
                     res.add(DIRECTION.NORTH);
                    break;
                case 3:
                    res.add(DIRECTION.EAST);
                    break;
                case 4:
                    res.add(DIRECTION.SOUTH);
                    break;
                case 5:
                    res.add(DIRECTION.NONE);
                    break;
                case 6:
                    res.add(DIRECTION.WEST);
                    res.add(DIRECTION.NORTH);
                    res.add(DIRECTION.EAST);
                    res.add(DIRECTION.SOUTH);
                    break;
            }
        }

        return res;
    }

    /**
     * Returns available destination of movement
     *
     * @param player
     * @param current
     * @param dir
     * @return
     */
    public boolean[][] getAvailableDestination(Player player, Point current, List<DIRECTION> dir) {
        boolean[][] avail = new boolean[Soup.SOUP_WIDTH][Soup.SOUP_HEIGHT];

        for (DIRECTION d : dir) {
            Point newLoc = getDestiation(current, d);
             avail[newLoc.x][newLoc.y] = true;
            if (doesHitWall(current, d)) {
                avail[current.x][current.y] = true;
                if (player.hasGene(GENE.REB) && !doesHitWall(current, getOppositeDirection(d))) {
                    Point rebLoc = getDestiation(current, getOppositeDirection(d));
                    avail[rebLoc.x][rebLoc.y] = true;
                }
            }
        }

        return avail;
    }

    /**
     * Returns opposite direction of the direction given.
     *
     * @param dir
     * @return
     */
    public DIRECTION getOppositeDirection(DIRECTION dir) {
        switch(dir) {
            case NORTH:
                return DIRECTION.SOUTH;
            case SOUTH:
                return DIRECTION.NORTH;
            case WEST:
                return DIRECTION.EAST;
            case EAST:
                return DIRECTION.WEST;
            default:
                return DIRECTION.NONE;
        }
    }


    public static String getDirectionString(DIRECTION direction) {

            switch (direction) {
                case NORTH:
                    return "north";
                case SOUTH:
                    return "south";
                case EAST:
                    return "east";
                case WEST:
                    return "west";
                case NONE:
                default:
                    return "none";
            }
    }

    /**
     * Returns the destination of movement
     *
     * @param loc
     * @param direction
     * @return
     */
    public Point getDestiation(Point loc, DIRECTION direction) {
        Point newLoc = new Point(loc.x, loc.y);

        switch (direction) {
            case NORTH:
                if (Soup.isValidLocation(loc.x,loc.y-1)) newLoc.y--;
                break;
            case SOUTH:
                if (Soup.isValidLocation(loc.x,loc.y+1)) newLoc.y++;
                break;
            case EAST:
                if (Soup.isValidLocation(loc.x+1,loc.y)) newLoc.x++;
                break;
            case WEST:
                if (Soup.isValidLocation(loc.x-1,loc.y)) newLoc.x--;
                break;
        }

        return newLoc;
    }

    private DIRECTION pointVectorToDirection(Point vec) {
        if (vec.x == 1 && vec.y == 0) return DIRECTION.EAST;
        if (vec.x == -1 && vec.y == 0) return DIRECTION.WEST;
        if (vec.x == 0 && vec.y == -1) return DIRECTION.NORTH;
        if (vec.x == 0 && vec.y == 1) return DIRECTION.SOUTH;

        return DIRECTION.NONE;
    }

    public DIRECTION getDirectionToReachNewLocation(Point current, Point newLoc) {
        Point diff = new Point(newLoc.x - current.x, newLoc.y - current.y);
        return pointVectorToDirection(diff);
    }

    public List<DIRECTION> getDirectionsToHitWall(Point current) {
        List<DIRECTION> dirs = new LinkedList<>();


        if (doesHitWall(current, DIRECTION.NORTH)) dirs.add(DIRECTION.NORTH);
        if (doesHitWall(current, DIRECTION.SOUTH)) dirs.add(DIRECTION.SOUTH);
        if (doesHitWall(current, DIRECTION.EAST)) dirs.add(DIRECTION.EAST);
        if (doesHitWall(current, DIRECTION.WEST)) dirs.add(DIRECTION.WEST);

        return dirs;
    }

    /**
     * Returns true if moving to the given direction will hit a wall
     *
     * @param loc
     * @param dir
     * @return
     */
    public boolean doesHitWall(Point current, DIRECTION dir) {
        if (dir == DIRECTION.NONE) return false;
        
        Point newLoc = this.getDestiation(current, dir);

        return (current.x == newLoc.x && current.y == newLoc.y);
    }

    /***FEEDING***/
    private int[] eatNormally(Player player, Amoeba amoeba) {
        int[] colors = _soup.getFoodColors();
        
        int[] foods = new int[colors.length];
        int[] excrete = new int[colors.length];

        for (int i = 0; i < foods.length; i++) {
            //skip own color
            if (colors[i] == (player.getColorID())) {
                excrete[i] = 2;
                continue;
            }
            foods[i] = 1;
        }

        _soup.getCell(amoeba.getLocation()).takeFood(foods);
        _progress.amoebaFed = true;

        return foods;
    }

    private List<GameMessage> excrete(Player player, Amoeba amoeba, Point exLoc, boolean tox) {
        List<GameMessage> res = new LinkedList<>();
        int[] colors = _soup.getFoodColors();

        int[] excretes = new int[colors.length];

        for (int i = 0; i < excretes.length; i++) {
            //skip own color
            if (colors[i] == (player.getColorID())) {
                excretes[i] = 2;
                break;
            }
        }
        
        //check TOX
        if (!player.hasGene(GENE.TOX) && tox) tox = false;

        //check loc
        if (exLoc == null || !player.hasGene(GENE.CLE) || !Soup.isValidLocation(exLoc)) {
            exLoc = amoeba.getLocation();
        } else {
           int diff = Math.abs(amoeba.getLocation().x - exLoc.x) + Math.abs(amoeba.getLocation().y - exLoc.y);
           if (diff > 1) exLoc = amoeba.getLocation();
        }

        //excrete
        _soup.getCell(exLoc).addFood(excretes);

        //excrete message
        if (exLoc.equals(amoeba.getLocation())) {
            res.add(new GameTextMessage(MESSAGE.EXCRETE, new String[]{player.getName(), Integer.toString(amoeba.number), GameState.pointToString(exLoc)}, COLOR_TYPE.PLAYER, player.getID()));
        } else {
            res.add(new GameTextMessage(MESSAGE.EXCRETE, new String[]{player.getName(), Integer.toString(amoeba.number), GameState.pointToString(exLoc)}, COLOR_TYPE.PLAYER, player.getID(),
                    new RelevantGene[] {new RelevantGene(player.getID(), GENE.CLE)}));
        }

        //TOX
        if (tox) _soup.getCell(exLoc).addDP(1);
        //tox message
        if(tox) res.add(new GameTextMessage(MESSAGE.GENE_TOX, new String[]{GameState.pointToString(exLoc)}, COLOR_TYPE.PLAYER, player.getID(), new RelevantGene[] {new RelevantGene(player.getID(), GENE.TOX)}));

        return res;
    }

    private String foodToString(int[] food) {
        StringBuffer buf = new StringBuffer("(");

        for (int i = 0; i < food.length-1; i++) {
            buf.append(food[i]).append(", ");
        }
        buf.append(food[food.length-1]);

        buf.append(")");

        return buf.toString();
    }

    /***FEEDING CALCULATIONS***/

    public List<PHASE1_ACTION> getFeedingOptions(Player player, Amoeba amoeba) {
        List<PHASE1_ACTION> action = new LinkedList<>();

        if (canFeed(player, amoeba.getLocation(), false)) {
            if (_players.size() > 3 && _players.size() < 6 && !player.hasGene(GENE.GLU)) {
                action.add(PHASE1_ACTION.EAT);
            } else {
                action.add(PHASE1_ACTION.EAT_WITH_CHOICE);
            }
        }
        if (canFeed(player, amoeba.getLocation(), true)) action.add(PHASE1_ACTION.EAT_WITH_GENES);
        if (!action.contains(PHASE1_ACTION.EAT) && !action.contains(PHASE1_ACTION.EAT_WITH_CHOICE)) action.add(PHASE1_ACTION.STARVE);

        return action;
    }

    public boolean canFeed(Player player, Point loc, boolean withGene) {

        //Cannot feed with gene, if no food gene is available
        if (withGene) {
            if (!player.hasGene(GENE.SUB) && !player.hasGene(GENE.FRU) && !player.hasGene(GENE.PAR) && !player.hasGene(GENE.SUC1) && !player.hasGene(GENE.SUC2)) {
                return false;
            }
            //Only PAR and not usable
            if (!player.hasGene(GENE.SUB) && !player.hasGene(GENE.FRU) && player.hasGene(GENE.PAR) && !player.hasGene(GENE.SUC1) && !player.hasGene(GENE.SUC2) && getPARusableNum(player, loc) == 0) {
                return false;
            }
        }

        if (withGene) {
            //FRU PAR only, No SUB
            int[] foodReq = calculateFoodRequirement(player, loc, true, false);
            if (canFeed(player, loc, foodReq[0], foodReq[1])) return true;
            //Try SUC
            if (canFeedWithSUC(player, loc, foodReq[0], foodReq[1], DIRECTION.NORTH)) return true;
            if (canFeedWithSUC(player, loc, foodReq[0], foodReq[1], DIRECTION.SOUTH)) return true;
            if (canFeedWithSUC(player, loc, foodReq[0], foodReq[1], DIRECTION.EAST)) return true;
            if (canFeedWithSUC(player, loc, foodReq[0], foodReq[1], DIRECTION.WEST)) return true;

            //FRU PAR SUB all used
            foodReq = calculateFoodRequirement(player, loc, true, true);
            if (canFeed(player, loc, foodReq[0], foodReq[1])) return true;
            //Try SUC
            if (canFeedWithSUC(player, loc, foodReq[0], foodReq[1], DIRECTION.NORTH)) return true;
            if (canFeedWithSUC(player, loc, foodReq[0], foodReq[1], DIRECTION.SOUTH)) return true;
            if (canFeedWithSUC(player, loc, foodReq[0], foodReq[1], DIRECTION.EAST)) return true;
            if (canFeedWithSUC(player, loc, foodReq[0], foodReq[1], DIRECTION.WEST)) return true;

        //NoGene
        } else {
            int[] foodReq = calculateFoodRequirement(player, loc, false, false);
            if (canFeed(player, loc, foodReq[0], foodReq[1])) return true;
        }
        return false;

    }

    private boolean canFeed(Player player, Point loc, int reqFoods, int reqColors) {

        int[] foods = _soup.getCell(loc).getFood();
        int[] colors = _soup.getFoodColors();

        int availableFoods = 0;
        int availableColors = 0;

        for (int i = 0; i < foods.length; i++) {
            //skip your own waste
            if (colors[i] == (player.getColorID())) continue;
            //available
            availableFoods += foods[i];
            if (foods[i] > 0) availableColors++;
        }

        //If enough
        if (reqFoods <= availableFoods && reqColors <= availableColors) return true;

        return false;
    }

    public boolean canFeedWithSUC(Player player, Point loc, int reqNum, int reqCol, DIRECTION sucDir) {
        if (doesHitWall(loc, sucDir)) return false;
        if (!player.hasGene(GENE.SUC1) && !player.hasGene(GENE.SUC2)) return false;

        SoupCell cell = _soup.getCell(loc);

        int[] colors = _soup.getFoodColors();
        int[] foods = cell.getFood();

        Point sucLoc = getDestiation(loc, sucDir);

        if (loc.equals(sucLoc)) return false;

        int[] exFoods = _soup.getCell(sucLoc).getFood();

        int availableFoods = 0;
        int availableColors = 0;
        int exFoodNum = 0;
        int availableColorsWithEx = 0;
        
        for (int i = 0; i < foods.length; i++) {
            //skip your own waste
            if (colors[i] == (player.getColorID())) continue;
            //available
            availableFoods += foods[i];
            exFoodNum += exFoods[i];
            if (foods[i] > 0) availableColors++;
            if (foods[i] > 0 || exFoods[i] > 0) availableColorsWithEx++;
        }

        if (reqNum <= availableFoods && reqCol <= availableColors) return true;
        if (availableColorsWithEx < reqCol) return false;

        int needCol = reqCol - availableColors;
        int needNum = reqNum - availableFoods;


        if (player.hasGene(GENE.SUC2)) {
            if (needCol > 2 || needNum > 2) return false;
            if (needNum > exFoodNum) return false;
        } else {
            if (needCol > 1 || needNum > 1) return false;
            if (needNum > exFoodNum) return false;
        }

        return true;

    }

    public boolean canFeedAfterSUC(Player player, Point loc, int[] sucFoods, boolean withFRUPAR, boolean withSUB) {
        
        //Cannot feed with gene, if no food gene is available
        if (withFRUPAR && !player.hasGene(GENE.FRU) && !player.hasGene(GENE.PAR)) return false;
        if (withSUB && !player.hasGene(GENE.SUB)) return false;

        //Number of require food, color
        int[] foodReq = calculateFoodRequirement(player, loc, withFRUPAR, withSUB);
        int reqNum = foodReq[0];
        int reqCol = foodReq[1];

        int[] origFoods = _soup.getCell(loc).getFood();
        int[] foods;

        //When SUC is not used
        if (sucFoods == null) {
            foods = origFoods;
        //When SUC is used
        } else {
            foods = new int[origFoods.length];
            for (int i = 0; i < foods.length; i++) {
                foods[i] = origFoods[i] + sucFoods[i];
            }
        }

        int[] colors = _soup.getFoodColors();

        int availableFoods = 0;
        int availableColors = 0;

        for (int i = 0; i < foods.length; i++) {
            //skip your own waste
            if (colors[i] == (player.getColorID())) continue;
            //available
            availableFoods += foods[i];
            if (foods[i] > 0) availableColors++;
        }

        //If enough
        return (reqNum <= availableFoods && reqCol <= availableColors);

    }

    public int[] calculateFoodRequirement(Player player, Point loc, boolean withFRUPAR, boolean withSUB) {
        //Sub num+1 color-1
        boolean SUB = (withSUB && player.hasGene(GenePool.GENE.SUB));

        //Fru num-1 color-1
        boolean FRU = (withFRUPAR && player.hasGene(GenePool.GENE.FRU));

        //GLU num+1
        boolean GLU = (player.hasGene(GenePool.GENE.GLU));

        //PAR
        int parNum = 0;
        if (withFRUPAR && player.hasGene(GenePool.GENE.PAR)) {
            parNum = getPARusableNum(player, loc);
        }

        return calculateFoodRequirement(SUB, FRU, GLU, parNum);
    }

    public int getMaximumRequiredFoodNumber() {
        return calculateFoodRequirement(true, false, true, 0)[0];
    }

    public int getMaximumRequiredFoodNumber(Player player) {
        return calculateFoodRequirement(player.hasGene(GENE.SUB), false, player.hasGene(GENE.GLU), 0)[0];
    }

    public int getMaximumRequiredFoodNumberPerColor(Player player) {
        int[] req = calculateFoodRequirement(player.hasGene(GENE.SUB), false, player.hasGene(GENE.GLU), 0);
        return (int)Math.ceil((double)(req[0]+1) / (double)req[1]);
    }

    private int[] calculateFoodRequirement(boolean SUB, boolean FRU, boolean GLU, int activePars) {
        assert(activePars >= 0 && activePars <= 2);
        assert((_players.size() >= 5 && _option.par != PARASITISM.ONLY_ONCE) || activePars <= 1);


        //Number of require food, color
        int numFoods, numColors;
        numFoods = Math.max(3, Math.min(_players.size()-1, 4));
        numColors = Math.min(_players.size()-1, 4);
        //Sub num+1 color-1
        if (SUB) {
            numFoods++;
            numColors--;
        }

        //Fru num-1 color-1
        if (FRU) {
            numFoods--;
            numColors--;
        }

        //GLU num+1
        if (GLU) {
            numFoods++;
        }

        //PAR
        numFoods-=activePars;
        numColors-=activePars;

        return new int[]{numFoods, numColors};
    }

    public boolean canEat(Player player, int[] foods, int activePars) {
        if (activePars > 0 && !player.hasGene(GENE.PAR)) return false;

        int[] foodColors = _soup.getFoodColors();
        int numFoods = 0;
        int numColors = 0;

        //count numbers
        for (int i = 0; i < foods.length; i++) {
            //can't if trying to eat your own
            if (foods[i] > 0 && foodColors[i] == (player.getColorID())) return false;
            //foods
            numFoods += foods[i];
            if (foods[i] > 0) numColors += 1;
        }

        //
        boolean SUB = player.hasGene(GENE.SUB);
        boolean FRU = player.hasGene(GENE.FRU);
        int[] foodReq;
        foodReq = calculateFoodRequirement(false, false, player.hasGene(GENE.GLU), activePars);
        if (foodReq[0] == numFoods && foodReq[1] <= numColors) return true;
        foodReq = calculateFoodRequirement(false, true && FRU, player.hasGene(GENE.GLU), activePars);
        if (foodReq[0] == numFoods && foodReq[1] <= numColors) return true;
        foodReq = calculateFoodRequirement(true && SUB, false, player.hasGene(GENE.GLU), activePars);
        int slack = activePars; //SUB can only be use when reducing number of clors, but combining with FRU or PAR changes things
        if (foodReq[0] == numFoods && foodReq[1] <= numColors && numColors <= foodReq[1] + slack) return true;
        foodReq = calculateFoodRequirement(true && SUB, true && FRU, player.hasGene(GENE.GLU), activePars);
        if (FRU) slack++;
        if (foodReq[0] == numFoods && foodReq[1] == numColors && numColors <= foodReq[1] + slack) return true;

        return false;
    }

    public List<int[]> getSubOptimalEatingCombinations(Player player, Amoeba amoeba, Point sucLoc, int[] sucFoods) {
        List<int[]> eatingCombinations = new LinkedList<>();

        //calculate total available food
        int[] origFoods = _soup.getCell(amoeba.getLocation()).getFood();
        int[] totalFoods = new int[origFoods.length];
        for (int i = 0; i < totalFoods.length; i++) {
            totalFoods[i] = origFoods[i];
            if (sucFoods != null) totalFoods[i] += sucFoods[i];
        }

        //Find ownColor
        int[] foodColors = _soup.getFoodColors();
        int ownIndex = -1;
        for (int i = 0; i < foodColors.length; i++) {
            if (foodColors[i] == (player.getColorID())) ownIndex = i;
        }

        //Do another run with SUB off
        int[] foodReq = calculateFoodRequirement(player, amoeba.getLocation(), true, false);
        findEatingCombinationRecursive(eatingCombinations, totalFoods, foodReq[0], foodReq[1], new int[]{}, ownIndex);
        //End if something found
        if (eatingCombinations.size() > 0) return eatingCombinations;

        //Calculate foodReq with FRU PAR SUB on
        int[] foodReqA = calculateFoodRequirement(player, amoeba.getLocation(), true, true);
        for (int i = 0; i < foodReq.length; i++) {
            //when the requirements are not the same
            if (foodReq[i] != foodReqA[i]) {
                //Find all possibel combinations
                findEatingCombinationRecursive(eatingCombinations, totalFoods, foodReqA[0], foodReqA[1], new int[]{}, ownIndex);
                break;
            }
        }
        //End if something found
        if (eatingCombinations.size() > 0) return eatingCombinations;

        //Do another run with all off
        int[] foodReqB = calculateFoodRequirement(player, amoeba.getLocation(), false, false);
        for (int i = 0; i < foodReq.length; i++) {
            //when the requirements are not the same
            if (foodReq[i] != foodReqB[i] && foodReqA[i] != foodReqB[i]) {
                findEatingCombinationRecursive(eatingCombinations, totalFoods, foodReqB[0], foodReqB[1], new int[]{}, ownIndex);
                break;
            }
        }

        return eatingCombinations;
    }

    private void findEatingCombinationRecursive(List<int[]> combinations, int[] availFoods, int reqFoods, int reqColors, int[] partial, int ownIndex) {
        int curFoods = 0;
        int curColors = 0;
        for (int i = 0; i < partial.length; i++) {
            curFoods += partial[i];
            if (partial[i] > 0) curColors++;
        }
        //terminating conditon
        if (partial.length == availFoods.length) {
            //make sure the conditon is met
            if (curFoods == reqFoods && curColors == reqColors) combinations.add(partial);
            return;
        }
        
        //extend the length
        int[] newPartial = new int[partial.length+1];
        for (int i = 0; i < partial.length; i++) {
            newPartial[i] = partial[i];
        }

        if (partial.length==ownIndex) {
            findEatingCombinationRecursive(combinations, availFoods, reqFoods, reqColors, newPartial, ownIndex);
        } else {
            for (int i = 0; i <= Math.min(availFoods[partial.length], reqFoods-curFoods); i++) {
                //end at 0 if the color condition is met
                if (curColors == reqColors && i > 0) break;
                newPartial[partial.length] = i;
                findEatingCombinationRecursive(combinations, availFoods, reqFoods, reqColors, newPartial, ownIndex);
            }
        }

    }

    public List<int[]> getPossiblePARcombinations(Player player, Point loc) {
        int parNum = getPARusableNum(player, loc);

        //Compute PAR combinations
        List<int[]> parCombinations = new LinkedList<>();
     
        //Compute list of possible targets
        Map<Player, Integer> PARtargets = getPARtargets(player, loc);
        List<Integer> parPLs = new LinkedList<>();
        for (Player p : PARtargets.keySet()) {
            parPLs.add(p.getID());
        }
        //When target is one, pick one each
        if (parNum == 1) {
            for (int i = 0; i < parPLs.size(); i++) parCombinations.add(new int[]{parPLs.get(i)});
        //When target is two, but the candidate is only one (if the option allows for it)
        } else if (parNum == 2 && parPLs.size() == 1) {
            //Then parasite twice
            parCombinations.add(new int[]{parPLs.get(0), parPLs.get(0)});
        //When two targets, enumarate all possible combinations
        } else {
            for (int i = 0; i < parPLs.size(); i++) {
                //Avoid parasiting same player twice
                for (int j = i+1; j < parPLs.size(); j++) {
//                    PARASITISM.DIFFERENT_PLAYER
                    parCombinations.add(new int[]{parPLs.get(i), parPLs.get(j)});
                }
            }

        }
        //If PAR is not used, null only
        if (parCombinations.isEmpty()) {
            parCombinations.add(null);
        }

        return parCombinations;
    }

    /**
     * Returns the number of food requirements that can be reduced by using PAR
     *
     * @param player
     * @param amoeba
     * @return
     */
    public int getPARusableNum(Player player, Point loc) {
        Map<Player, Integer> targets = getPARtargets(player, loc);

        //No target
        if (targets.isEmpty()) return 0;

        //
        int max = 0;
        int PARablePlayers = 0;
        for (int pNum : targets.values()) {
            PARablePlayers++;
            max = Math.max(pNum, max);
        }
        PARablePlayers = Math.min(2, PARablePlayers);

        if (_option.par == PARASITISM.DIFFERENT_AMOEBA || _option.par == PARASITISM.SAME_AMOEBA_TWICE) {
            return Math.max(max, PARablePlayers);
        } else if (_option.par == PARASITISM.DIFFERENT_PLAYER) {
            return PARablePlayers;
        } else {
            return max;
        }
    }

    public Map<Player, Integer> getPARtargets(Player player, Point loc) {
        Map<Player, Integer> targets = new HashMap<>();

        if (!player.hasGene(GenePool.GENE.PAR)) return targets;

        SoupCell cell = _soup.getCell(loc);


        //3,4 player game does not need to consult option
        if (_players.size() < 5 || _option.par == PARASITISM.ONLY_ONCE) {
            //go through all amoebas in this cell
            for (Amoeba a : cell.getAmoebas()) {
                if (a.playerID != player.getID() && getPlayerByID(a.playerID).getBP() > 0) {
                    //add the player to the list
                    if (!targets.containsKey(getPlayerByID(a.playerID))) targets.put(getPlayerByID(a.playerID), 1);
                }
            }
        //5+ game can change behavior based on options
        } else {

            for (Amoeba a : cell.getAmoebas()) {
                if (a.playerID != player.getID() && getPlayerByID(a.playerID).getBP() > 0) {
                    //Already in the list
                    if (targets.containsKey(getPlayerByID(a.playerID))) {
                        //but its a different amoeba
                        if (_option.par == PARASITISM.DIFFERENT_AMOEBA && getPlayerByID(a.playerID).getBP() > 1) {
                            targets.put(getPlayerByID(a.playerID), 2);
                        }
                    //First PAR, twice allowed
                    } else if (_option.par == PARASITISM.SAME_AMOEBA_TWICE && getPlayerByID(a.playerID).getBP() > 1) {
                        targets.put(getPlayerByID(a.playerID), 2);
                    //First PAR, once
                    } else {
                        targets.put(getPlayerByID(a.playerID), 1);
                    }
                }
            }
        }

        return targets;
    }


    /***PHASE 2***/
    private GameMessage startPhase2() {
        _progress.currentPhase = Progress.PHASE_2_OZONE_CHANGE;
        _progress.currentOrder = 0; //Phase 2 is descending

        //Open new Enviornment card
        _currentEnvironment = _environment.pop();

        int envID = -1;
        for (Player player : _players) {
            if (player.hasGene(GENE.ENV)) envID = player.getID();
        }

        if (envID != -1) {
            return new GameTextMessage(MESSAGE.NEW_PHASE, new String[]{"2"}, COLOR_TYPE.EMPHASIZE, 0, new RelevantGene[]{new RelevantGene(envID, GENE.ENV)});
        } else {
            return new GameTextMessage(MESSAGE.NEW_PHASE, new String[]{"2"}, COLOR_TYPE.EMPHASIZE, 0);
        }
    }

    public List<GameMessage> geneDefectHandled(int playerID, GENE[] discards) {
        List<GameMessage> res = new LinkedList<>();

        Player player = getCurrentPlayer();

        if (player.getID() == playerID && _progress.currentPhase == Progress.PHASE_2_OZONE_CHANGE) {

            int mp = player.getMP();
            if (mp > _currentEnvironment.maxMP) {
                int diff = mp - _currentEnvironment.maxMP;
                //when nothing to discard
                if (discards == null) {
                    //pay all by BP
                    if (player.getBP() >= diff) {
                        player.addBP(-diff);
                        res.add(new GameTextMessage(MESSAGE.DEFECT_PAY_BP, new String[]{player.getName(), Integer.toString(diff)}, COLOR_TYPE.PLAYER, player.getID()));
                    //if player can't pay, error
                    } else {
                        return res;
                    }
                } else {
                    //Count how many MPs the discards worth
                    int discardMP = 0;
                    for (GENE gene : discards) {
                        discardMP += _genePool.getGene(gene).getMPReleaseValue();
                    }
                    //Discard and pay the
                    if (diff - discardMP <= player.getBP()) {
                        for (GENE gene : discards) {
                            //Alarm is turned off when discarded
                            if (gene == GENE.ALM) _progress.ALMactive = false;
                            //dscard
                            res.add(new GameTextMessage(MESSAGE.DEFECT_PAY_GENE, new String[]{player.getName(), _genePool.getGene(gene).name}, COLOR_TYPE.PLAYER, player.getID()));
                            player.removeGene(_genePool.getGene(gene));
                        }

                        //Pay the rest in BP
                        int left = diff - discardMP;
                        if (left > 0) {
                            res.add(new GameTextMessage(MESSAGE.DEFECT_PAY_BP, new String[]{player.getName(), Integer.toString(left)}, COLOR_TYPE.PLAYER, player.getID()));
                            player.addBP(-left);
                        }
                    //else error
                    } else {
                        return res;
                    }
                }
            }

            _progress.currentOrder++;
            //End of phase 2
            if (_progress.currentOrder >= _players.size()) {
                res.addAll(startPhase3());
            }

        }

        return res;
    }

    /***PHASE 3***/

    private boolean isPurchasable(Player player, GENE g) {
         Gene gene = _genePool.getGene(g);

         //SOC is special
         if (g == GENE.SOC) {
            int cost = getSOCcost();
            if (player.hasGene(GENE.FLEX)) cost--;
             //has some left, and player does not have one yet

            return cost <= player.getBP() && canPurchaseSOC(player);
         } else if (gene.getAvailableNumber() > 0 && !player.hasGene(g) && !player.hasGene(gene.successor)) {
               //and has enough money
                int cost = getGeneCost(player, g);
                if (cost <= player.getBP()) {
                    //If predecessors are null then you can buy
                    if (gene.getPredecessors().isEmpty()) {
                        return true;
                    //If not, check if there are predecessors
                    } else if (hasPredecessor(player, gene)) {
                        return true;
                    }
                }
         }

         return false;
    }

    private boolean isExchangable(Player player, GENE g, GENE exchange) {
        //no gene to exchange
        if (exchange == null) return false;
        //doesn't have ADA
        if (!player.hasGene(GENE.ADA)) return false;
        //canUse check
        if (!canUseForExchange(player, exchange)) return false;

        Gene gene = _genePool.getGene(g);
        //has some left, and player does not have one yet
        if (gene.getAvailableNumber() > 0 && !player.hasGene(g) && !player.hasGene(gene.successor)) {
            int exValue = _genePool.getGene(exchange).getBPCost();
            //and has enough money
            int cost = getGeneCost(player, g) - exValue;
            if (cost <= player.getBP()) {
                //If predecessors are null then you can buy
                if (gene.getPredecessors().isEmpty()) {
                    return true;
                //If not, check if there are predecessors, and it cannot be the one being exchanged
                } else if (hasPredecessor(player, gene) && !gene.getPredecessors().contains(exchange)) {
                   return true;
                }
            }
        }

        return false;
        
    }

    public boolean canUseForExchange(Player player, GENE gene) {
        if (player == null || gene == null) return false;

        if (!player.hasGene(gene)) return false;

        return player.getPurchasedRound(gene) < _progress.currentRound;
    }

    public int getGeneCost(Player player, GENE gene) {
        int cost = _genePool.getGene(gene).getBPCost();
        if (player.hasGene(gene.FLEX)) cost--;

        return cost;
    }

    public Map<GENE, Boolean> getPurchasableGenes(Player player) {
        Map<GENE, Boolean> map = new HashMap<>();

        //Regular Genes
        for (GENE g : GenePool.getRegularGenes()) {
            map.put(g, isPurchasable(player, g));
        }
        if (_players.size() > 4 || _option.allowExtras) {
            //Extra Genes
            for (GENE g : GenePool.getExtraGenes()) {
                map.put(g, isPurchasable(player, g));
            }
        }

        return map;
    }

    public Map<GENE, Boolean> getExchangableGenes(Player player, GENE exchange) {
        Map<GENE, Boolean> map = new HashMap<>();

        //Regular Genes
        for (GENE g : GenePool.getRegularGenes()) {
            map.put(g, isExchangable(player, g, exchange));
        }
        if (_players.size() > 4 || _option.allowExtras) {
            //Extra Genes
            for (GENE g : GenePool.getExtraGenes()) {
                map.put(g, isExchangable(player, g, exchange));
            }
        }

        return map;
    }

    private boolean hasPredecessor(Player player, Gene gene) {
        for (GENE pred : gene.getPredecessors()) {
            if (player.hasGene(pred) && player.getPurchasedRound(pred) < _progress.currentRound) {
                return true;
            }
        }

        return false;
    }

    private List<GameMessage> startPhase3() {
        List<GameMessage> res = new LinkedList<>();

        _progress.currentOrder = 0;
        _progress.currentPhase = Progress.PHASE_3_NEW_GENES;

        res.add(new GameTextMessage(MESSAGE.NEW_PHASE, new String[]{"3"}, COLOR_TYPE.EMPHASIZE, 0));
        res.add(new GameTextMessage(MESSAGE.TURN_SIGNAL, new String[]{getCurrentPlayer().getName()}, COLOR_TYPE.PLAYER, getCurrentPlayer().getID()));
        return res;
    }

    public GameMessage genePurchase(int playerID, GENE g) {

        Player player = getCurrentPlayer();
        //verify
        if (playerID == player.getID() && _progress.currentPhase == Progress.PHASE_3_NEW_GENES) {
            //can't have the same gene
            if (player.hasGene(g)) return null;

            Gene gene = _genePool.getGene(g);

            //Special case with SOC
            if (gene.gene == GENE.SOC && gene.getAvailableNumber() == 0) {
                int cost = getSOCcost();
                if (player.hasGene(GENE.FLEX)) cost--;
                //Find old owner
                Player owner = null;
                for (Player p : _players) {
                    if (p.hasGene(GENE.SOC)) {
                        owner = p;
                        break;
                    }
                }
                if (owner == null) return null;
                //Old player releases it
                owner.removeGene(gene);
                owner.addBP(cost);
                //New player gets it
                player.addBP(-cost);
                player.addGene(gene, _progress.currentRound);
                if (player.hasGene(GENE.FLEX)) {
                    return  new GameTextMessage(MESSAGE.GENE_PURCHASED, new String[]{player.getName(), owner.getName()}, COLOR_TYPE.PLAYER, player.getID(),
                            new RelevantGene[]{new RelevantGene(player.getID(), GENE.FLEX)});
                } else {
                    return  new GameTextMessage(MESSAGE.GENE_PURCHASED, new String[]{player.getName(), owner.getName()}, COLOR_TYPE.PLAYER, player.getID());
                }
            }

            int cost = gene.getBPCost();
            if (player.hasGene(GENE.FLEX)) cost--;
            //can't buy without enough BP
            if (player.getBP() < cost) return null;

            //if there is no more left
            if (gene.getAvailableNumber() <= 0) return null;

            //Genes without predecessors are easy
            if (gene.getPredecessors().isEmpty()) {
                player.addBP(-cost);
                player.addGene(gene, _progress.currentRound);

                if (player.hasGene(GENE.FLEX)) {
                    return new GameTextMessage(MESSAGE.GENE_PURCHASED, new String[]{player.getName(), gene.name}, COLOR_TYPE.PLAYER, player.getID(),
                            new RelevantGene[]{new RelevantGene(player.getID(), GENE.FLEX)});
                } else {
                    return new GameTextMessage(MESSAGE.GENE_PURCHASED, new String[]{player.getName(), gene.name}, COLOR_TYPE.PLAYER, player.getID());
                }
            } else {
                player.addBP(-cost);
                player.addGene(gene, _progress.currentRound);
                for (GENE pred : gene.getPredecessors()) {
                    if (player.hasGene(pred)) {
                        player.removeGene(_genePool.getGene(pred));
                        break;
                    }
                }
                if (player.hasGene(GENE.FLEX)) {
                    return new GameTextMessage(MESSAGE.GENE_PURCHASED, new String[]{player.getName(), gene.name}, COLOR_TYPE.PLAYER, player.getID(),
                            new RelevantGene[]{new RelevantGene(player.getID(), GENE.FLEX)});
                } else {
                    return new GameTextMessage(MESSAGE.GENE_PURCHASED, new String[]{player.getName(), gene.name}, COLOR_TYPE.PLAYER, player.getID());
                }
            }

        }

        return null;
    }

    public List<GameMessage> endGenePurchase(int playerID) {

        List<GameMessage> res = new LinkedList<>();
        
        Player player = getCurrentPlayer();

        //verify
        if (playerID == player.getID() && _progress.currentPhase == Progress.PHASE_3_NEW_GENES) {

            res.add(new GameTextMessage(MESSAGE.END_GENE_PURCHASE, new String[]{player.getName()}, COLOR_TYPE.PLAYER, player.getID()));

            //next player
            _progress.currentOrder++;

            //Next phase
            if (_progress.currentOrder >= _players.size()) {
                res.addAll(startPhase4());
            } else {
                res.add(new GameTextMessage(MESSAGE.TURN_SIGNAL, new String[]{getCurrentPlayer().getName()}, COLOR_TYPE.PLAYER, getCurrentPlayer().getID()));
            }

            return res;
            
        }

        return null;
    }

    /***PHASE 4***/

    private List<GameMessage> startPhase4() {
        List<GameMessage> res = new LinkedList<>();
        _progress.currentPhase = Progress.PHASE_4_DIVISION;
        _progress.currentOrder = 0;

        res.add(new GameTextMessage(MESSAGE.NEW_PHASE, new String[]{"4"}, COLOR_TYPE.EMPHASIZE, 0));
        res.add(new GameTextMessage(MESSAGE.TURN_SIGNAL, new String[]{getCurrentPlayer().getName()}, COLOR_TYPE.PLAYER, getCurrentPlayer().getID()));

        res.add(phase4Income());

        return res;
    }

    public int getDivisionCost(Player player) {
        int cost = 6;
        //0 if POP is active
        if (player.hasGene(GENE.POP)) {
            int topNum = getPlayerByOrder(0).getNumAmoebas();
            if (_progress.POPactive && player.getNumAmoebas() < topNum) {
                return 0;
            }
        }
        //When you have no amoeba, first amoeba is free
        if (player.getNumAmoebas() == 0) {
            cost = 0;
        //With DIV or SLS, cost is 5
        } else if (player.hasGene(GENE.DIV) || player.hasGene(GENE.SLS)) {
            cost = 4;
        }
        return cost;
    }

    public GameMessage cellDivision(int playerID, int num, Point loc) {
        Player player = getCurrentPlayer();

        //verify
        if (playerID == player.getID() && _progress.currentPhase == Progress.PHASE_4_DIVISION) {
            int cost = getDivisionCost(player);

            //check cost
            if (player.getBP() < cost) return null;

            //check number
            List<Integer> amoebaNums = getRemainingAmoebaNumbers(playerID);
            if (!amoebaNums.contains(num)) return null;

            //check location
            boolean[][] spawnable = getSpawnableLocations(player);
            if (!spawnable[loc.x][loc.y]) return null;

            //spawn
            player.addBP(-cost);
            return addAmoeba(playerID, num, loc);

        }

        return null;
    }
    
    public List<GameMessage> endCellDivision(int playerID) {

        List<GameMessage> res = new LinkedList<>();

        Player player = getCurrentPlayer();

        //verify
        if (playerID == player.getID() && _progress.currentPhase == Progress.PHASE_4_DIVISION) {

            _progress.clearPOPProgress();

            res.add(new GameTextMessage(MESSAGE.END_CELL_DIVISION, new String[]{player.getName()}, COLOR_TYPE.PLAYER, player.getID()));

            //next player
            _progress.currentOrder++;

            //Next phase
            if (_progress.currentOrder >= _players.size()) {
                res.addAll(startPhase5());
            } else {
                res.add(new GameTextMessage(MESSAGE.TURN_SIGNAL, new String[]{getCurrentPlayer().getName()}, COLOR_TYPE.PLAYER, getCurrentPlayer().getID()));
                res.add(phase4Income());
            }

            return res;

        }

        return null;
    }

    private GameMessage phase4Income() {
        Player newPlayer = getPlayerByOrder(_progress.currentOrder);
        //If the new player does not have new gene, add BP
        if (newPlayer.hasGene(GENE.POP)) {
            _progress.POPdecide = true;
            return null;
        } else {
            newPlayer.addBP(getPhase4Income());
            return new GameTextMessage(MESSAGE.PHASE4_INCOME, new String[]{newPlayer.getName(), Integer.toString(getPhase4Income())}, COLOR_TYPE.PLAYER, newPlayer.getID());
        }
    }

    public boolean[][] getSpawnableLocations(Player player) {
        boolean[][] avail = new boolean[Soup.SOUP_WIDTH][Soup.SOUP_HEIGHT];

        int cost = getDivisionCost(player);

        if (player.getBP() < cost) {
            return avail;
        }

        if (player.getNumAmoebas() >= this.maximumAmoebaCount()) {
            return avail;
        }

        //With SPO or number of amoebas below 2, you can spawn anywhere without your own amoeba
        if (player.hasGene(GENE.SPO) || player.getNumAmoebas() <= 1) {
            for (int x = 0; x < Soup.SOUP_WIDTH; x++) {
                for (int y = 0; y < Soup.SOUP_HEIGHT; y++) {
                    //Chec
                    if (Soup.isValidLocation(x, y) && !_soup.getCell(x, y).hasAmoebaOfColor(player.getColorID())) {
                        avail[x][y] = true;
                    }
                }
            }
        //In other cases, cells that are neighbors are valid spawn locations
        } else {

            for (Amoeba amoeba : player.getAmoebas()) {
                Point loc = amoeba.getLocation();
                Point north = getDestiation(loc, DIRECTION.NORTH);
                Point south = getDestiation(loc, DIRECTION.SOUTH);
                Point east = getDestiation(loc, DIRECTION.EAST);
                Point west = getDestiation(loc, DIRECTION.WEST);

                if (!_soup.getCell(north).hasAmoebaOfColor(player.getColorID())) avail[north.x][north.y] = true;
                if (!_soup.getCell(south).hasAmoebaOfColor(player.getColorID())) avail[south.x][south.y] = true;
                if (!_soup.getCell(east).hasAmoebaOfColor(player.getColorID())) avail[east.x][east.y] = true;
                if (!_soup.getCell(west).hasAmoebaOfColor(player.getColorID())) avail[west.x][west.y] = true;

            }
            
        }

        return avail;
    }

    /***PHASE 5***/
    private List<GameMessage> startPhase5() {
        List<GameMessage> res = new LinkedList<>();
        
        _progress.currentPhase = Progress.PHASE_5_DEATHS;
        _progress.currentOrder = 0;
        _progress.currentAmoeba = 0;

        res.add(new GameTextMessage(MESSAGE.NEW_PHASE, new String[]{"5"}, COLOR_TYPE.EMPHASIZE, 0));

        GameMessage da = nextDeadAmoeba();

         //PHASE 6
        if (da == null) {
            res.add(startPhase6());
        } else {
            res.add(da);
        }

        return res;
    }

    public List<GameMessage> deathAck(int playerID, boolean useBANG) {
        List<GameMessage> res = new LinkedList<>();

        Player player = getCurrentPlayer();

        if (playerID == player.getID() && _progress.currentPhase == Progress.PHASE_5_DEATHS) {
            //death
            naturalDeathHandling(getCurrentAmoeba());
            //BANG used
            if (useBANG && player.hasGene(GENE.BANG)) {
                //add 1 DP to everyone
                for (Amoeba amoeba : _soup.getCell(_progress.BANGlocation).getAmoebas()) {
                    amoeba.addDP(1);
                }
                //reset death phase progress
                _progress.currentOrder = 0;
                _progress.currentAmoeba = 0;

                res.add(new GameTextMessage(MESSAGE.GENE_BANG, new String[]{pointToString(_progress.BANGlocation)}, COLOR_TYPE.PLAYER, player.getID(),
                        new RelevantGene[]{new RelevantGene(player.getID(), GENE.BANG)}));
            }

            _progress.clearBANGProgress();
            GameMessage da = nextDeadAmoeba();

             //PHASE 6
            if (da == null) {
                res.add(startPhase6());
            } else {
                res.add(da);
            }
        }
        return res;
    }

    public boolean isAmoebaDead(Player player, Amoeba amoeba) {
        int HP = 2;
        
        if (player.hasGene(GENE.LIF)) HP++;
        if (player.hasGene(GENE.SLS)) HP--;
        
        return amoeba.getDP() >= HP;

    }

    private void naturalDeathHandling(Amoeba amoeba) {
        int[] foods = new int[_soup.getFoodColors().length];

        for (int i = 0; i < foods.length; i++) {
            foods[i] = 2;
        }

        Player owner = getPlayerByID(amoeba.playerID);
        Point loc = amoeba.getLocation();
        //remove
        _soup.getCell(loc).removeAmoeba(amoeba);
        owner.removeAmoeba(amoeba);
        //leave food
        _soup.getCell(loc).addFood(foods);
        //Wait a little
        sleep(DEATH_WAIT);
    }

    private void SFSdeathHandling(Amoeba amoeba) {
        int[] foods = new int[_soup.getFoodColors().length];

        //no food is left when DEF was used
        if (!_progress.DEFnoFood) {
            for (int i = 0; i < foods.length; i++) {
                foods[i] = 1;
            }
        }

        Player owner = getPlayerByID(amoeba.playerID);
        Point loc = amoeba.getLocation();
        //remove
        _soup.getCell(loc).removeAmoeba(amoeba);
        owner.removeAmoeba(amoeba);
        //leave food
        _soup.getCell(loc).addFood(foods);
    }

    private GameMessage nextDeadAmoeba() {
         do {
            Player nextPlayer = getPlayerByOrder(_progress.currentOrder);
            //Find dead amoeba in this player
            for (int i = _progress.currentAmoeba; i < nextPlayer.getNumAmoebas(); i++) {
                Amoeba amoeba = nextPlayer.getAmoebas().get(i);
                if (isAmoebaDead(nextPlayer, amoeba)) {
                    _progress.currentAmoeba = i;
                    //check for bang
                    if (nextPlayer.hasGene(GENE.BANG)) {
                        _progress.BANGactive = true;
                        _progress.BANGPlayerID = nextPlayer.getID();
                        _progress.BANGlocation = amoeba.getLocation();
                    }
                    return new GameTextMessage(MESSAGE.DEAD_AMOEBA, new String[]{nextPlayer.getName(), Integer.toString(amoeba.number)}, COLOR_TYPE.PLAYER, nextPlayer.getID());
                }
            }
            _progress.currentOrder++;
            _progress.currentAmoeba = 0;
            //If not one has dead amoeba, done
            if (_progress.currentOrder >= _players.size()) break;
         } while (true);

         return null;
    }

    /***PHASE 6***/
    private GameMessage startPhase6() {
        //Before starting phase 6, check if ARG can be used
        if (!_progress.AGRused) {
            for (Player player : _players) {
                //Some one can use AGR
                if (player.hasGene(GENE.AGR) && player.getBP() > 0) {
                    _progress.AGRactive = true;
                    _progress.currentOrder = _players.indexOf(player);
                    _progress.currentAmoeba = 0;
                    return null;
                }
             }
            //No one can use AGR
            _progress.AGRused = true;
            return startPhase6();
        } else {
            _progress.currentPhase = Progress.PHASE_6_SCORING;
            _progress.currentOrder = _progress.currentAmoeba = 0;
            _progress.ackedScore = 0;
            return new GameTextMessage(MESSAGE.NEW_PHASE, new String[]{"6"}, COLOR_TYPE.EMPHASIZE, 0);
        }
    }

    public synchronized List<GameMessage> scoreAck(int playerID, int ack) {
        List<GameMessage> res = new LinkedList<>();
        
        Player player = getCurrentPlayer();

        if (player == null) return res;

        if (_progress.currentPhase != Progress.PHASE_6_SCORING) return res;
        if (ack != _progress.ackedScore) return res;

        int score = computeScore(player);

        if (_progress.ackedScore < score) {
            player.advance(1);
            _progress.ackedScore++;

            //hop on check
            boolean advanced = false;
            do {
                advanced = false;
                for (Player other : _players) {
                    //skip yourself
                    if (other.getID() == player.getID()) continue;
                    //check if on the same space
                    if (other.getAdvance() == player.getAdvance()) {
                        player.advance(1);
                        advanced = true;
                        break;
                    }

                }
            } while (advanced);
            sleep(SCORING_WAIT);
            res.add(new GameEventMessage(EVENT.SCORE_STEP, new String[]{}));
          //End of this player
        } else {
            _progress.ackedScore = 0;
            _progress.currentOrder++;
            res.add(new GameTextMessage(MESSAGE.SCORE, new String[]{player.getName(), Integer.toString(score)}, COLOR_TYPE.PLAYER, player.getID()));
            //New round
            if (_progress.currentOrder >= _players.size()) {
                //check for end game
                for (Player p : _players) {
                    //Ending condition
                    if (p.getAdvance() >= ENDING_SCORE) {
                        _progress.ended = true;
                        Collections.sort(_players);
                        res.add(new GameTextMessage(MESSAGE.GAME_END, new String[]{}, COLOR_TYPE.EMPHASIZE, 0));
                        return res;
                    }
                }
                //check environment
                if (_environment.peek(0) == null) {
                        _progress.ended = true;
                        Collections.sort(_players);
                        res.add(new GameTextMessage(MESSAGE.GAME_END, new String[]{}, COLOR_TYPE.EMPHASIZE, 0));
                        return res;
                }
                //handle SOC
                res.addAll(useSOC());
                //start new round
                res.addAll(startNewRound());
            }
        }

        return res;
    }

    public int computeScore(Player player) {
        int numAmoebas = player.getNumAmoebas();
        int numCards = player.getNumGenesForScore();

        //card points
        int points = Math.min(Math.max(numCards - 2, 0), 4);

        //add amoeba points
        //5+ player scoring
        if (_players.size() > 4) {
            points += Math.max(numAmoebas-1, 0);
            if (numAmoebas > 3) points++;
        //3,4 player scoring
        } else {
            points += Math.max(numAmoebas-2, 0);
            if (numAmoebas > 4) points++;
        }
        return points;
    }

    /***GENES***/
    public boolean isTENusable(Player player, Amoeba amoeba, DIRECTION dir) {
        //player has TEN, movement is not staying on the same spot, and there is some food to carry
        return (player.hasGene(GENE.TEN) && !doesHitWall(amoeba.getLocation(), dir) && dir != DIRECTION.NONE && _soup.getCell(amoeba.getLocation()).getTotalFoodCount() > 0);
    }

    private GameMessage useTEN(Player player, int[] foods, Point loc, DIRECTION direction) {

        //Check sum
        int sum = 0;
        for (int f : foods) sum+=f;
        if (sum == 0) return null;

        Point newLoc = getDestiation(loc, direction);
        _soup.getCell(loc).takeFood(foods);
        _soup.getCell(newLoc).addFood(foods);

        //int count = 0;
        //for (int i = 0; i < foods.length; i++) count += foods[i];

        return new GameTextMessage(MESSAGE.GENE_TEN, new String[]{player.getName(), foodToString(foods)},
                COLOR_TYPE.PLAYER, player.getID(), new RelevantGene[]{new RelevantGene(player.getID(), GENE.TEN)});
    }

    public boolean canPurchaseSOC(Player player) {
        if (player.hasGene(GENE.SOC)) return false;

        int min = 99;
        for (Player p : _players) {
            min = Math.min(p.getAdvance(), min);
        }

        //you can buy when at the last place
        return (player.getAdvance() == min && _genePool.getGene(GENE.SOC).max > 0);
    }

    public int getSOCcost() {
        //purchase from the player
        if (_genePool.getGene(GENE.SOC).getAvailableNumber() == 0) return 2;

        return _genePool.getGene(GENE.SOC).getBPCost();
    }

    private List<GameMessage> useSOC() {
        List<GameMessage> res = new LinkedList<>();

        Player withSOC = null;
        for (Player player : _players) {
            if (player.hasGene(GENE.SOC)) {
                withSOC = player;
                break;
            }
        }

        //if no one has SOC
        if (withSOC == null) return res;

        //Have every player 6 or more ahead pay 1BP
        for (Player player : _players) {
            if (player.getAdvance() >= withSOC.getAdvance() + 6 && player.getBP() > 0) {
                player.addBP(-1);
                withSOC.addBP(1);
                res.add(new GameTextMessage(MESSAGE.GENE_SOC, new String[]{withSOC.getName(), player.getName()}, COLOR_TYPE.PLAYER, withSOC.getID(),
                        new RelevantGene[]{new RelevantGene(withSOC.getID(), GENE.SOC)}));

            }
        }

        return res;
    }

    public GameMessage POPdecision(int playerID, boolean usePOP) {
        Player player = getCurrentPlayer();

        if (playerID != player.getID()) return null;
        if (_progress.currentPhase != Progress.PHASE_4_DIVISION) return null;
        if (!_progress.POPdecide) return null;

        _progress.POPdecide = false;

        if (usePOP) {
            _progress.POPactive = true;
            player.addBP(-player.getBP());

            return new GameTextMessage(MESSAGE.GENE_POP, new String[]{player.getName()}, COLOR_TYPE.PLAYER, player.getID(),
                    new RelevantGene[]{new RelevantGene(player.getID(),GENE.POP)});
        } else {
            player.addBP(getPhase4Income());
            return new GameTextMessage(MESSAGE.PHASE4_INCOME, new String[]{player.getName(), Integer.toString(getPhase4Income())}, COLOR_TYPE.PLAYER, player.getID());

        }
    }

    public boolean canHeal(Player player, Amoeba amoeba) {
        //you need to have heal to do make any effect
        if (player.hasGene(GENE.HEA1) || player.hasGene(GENE.HEA2)) {
            //If its player's own amoeba, and has DP and player has enough moeny, heal is possible
            if (player.getID() == amoeba.playerID && amoeba.getDP() > 0 && player.getBP() >= 3)  {
                return true;
            }
        }

        return false;
    }

    public GameMessage heal(int playerID, Amoeba heal, Amoeba transfer) {
        Player player = getCurrentPlayer();
        heal = getPlayerByID(heal.playerID).getAmoebaByNumber(heal.number);
        //ID check
        if (player.getID() != playerID) return null;
        //phase check
        if (_progress.currentPhase != Progress.PHASE_4_DIVISION) return null;
        //healed amoeba must be your own
        if (heal.playerID != player.getID()) return null;
        //GENE check
        if (!player.hasGene(GENE.HEA1) && !player.hasGene(GENE.HEA2)) return null;

        //Transfer
        if (transfer != null) {
            transfer = getPlayerByID(transfer.playerID).getAmoebaByNumber(transfer.number);
            //location check
            if  (!transfer.getLocation().equals(heal.getLocation())) return null;
            //BP check
            if (player.getBP() < 4) return null;
            player.addBP(-4);
            heal.addDP(-1);
            transfer.addDP(1);
            return new GameTextMessage(MESSAGE.GENE_HEA2, new String[]{player.getName(), Integer.toString(heal.number), getPlayerByID(transfer.playerID).getName(), Integer.toString(transfer.number)},
                    COLOR_TYPE.PLAYER, player.getID(), new RelevantGene[]{new RelevantGene(player.getID(), GENE.HEA2)});
        //Heal only
        } else {
            if (player.getBP() < 3) return null;
            //heal
            heal.addDP(-1);
            player.addBP(-3);
            return new GameTextMessage(MESSAGE.GENE_HEA1, new String[]{player.getName(), Integer.toString(heal.number)}, COLOR_TYPE.PLAYER, player.getID(),
                    new RelevantGene[]{new RelevantGene(player.getID(), GENE.HEA1)});
        }
    }

    public GameMessage ADAexchange(int playerID, GENE exchange, GENE newGene) {
        Player player = getCurrentPlayer();

        //ID check
        if (player.getID() != playerID) return null;
        //phase check
        if (_progress.currentPhase != Progress.PHASE_3_NEW_GENES) return null;
        //ADA
        if (!player.hasGene(GENE.ADA)) return null;
        //exchange card
        if (!player.hasGene(exchange)) return null;

        if (!isExchangable(player, newGene, exchange)) return null;

        int cost = getGeneCost(player, newGene) - _genePool.getGene(exchange).getBPCost();

        //Without predecessor
        if (_genePool.getGene(newGene).getPredecessors().isEmpty()) {
            //exchange
            player.removeGene(_genePool.getGene(exchange));
            player.addGene(_genePool.getGene(newGene), _progress.currentRound);
        //With predecessor
        } else {
            player.removeGene(_genePool.getGene(exchange));
            player.addGene(_genePool.getGene(newGene), _progress.currentRound);
            for (GENE pred : _genePool.getGene(newGene).getPredecessors()) {
                if (player.hasGene(pred)) {
                    player.removeGene(_genePool.getGene(pred));
                    break;
                }
            }
        }

        if (cost > 0) {
            player.addBP(-cost);
            return new GameTextMessage(MESSAGE.GENE_ADA, new String[]{player.getName(), _genePool.getGene(exchange).name, _genePool.getGene(newGene).name, Integer.toString(cost)},
                    COLOR_TYPE.PLAYER, player.getID(), new RelevantGene[]{new RelevantGene(player.getID(), GENE.ADA)});
        } else {
            return new GameTextMessage(MESSAGE.GENE_ADA_FREE, new String[]{player.getName(), _genePool.getGene(exchange).name, _genePool.getGene(newGene).name},
                    COLOR_TYPE.PLAYER, player.getID(), new RelevantGene[]{new RelevantGene(player.getID(), GENE.ADA)});
        }
    }


    public boolean[][] getSuctionLocation(Point loc) {
        boolean[][] avails = new boolean[Soup.SOUP_WIDTH][Soup.SOUP_HEIGHT];
        
        Point Nloc = getDestiation(loc, DIRECTION.NORTH);
        Point Sloc = getDestiation(loc, DIRECTION.SOUTH);
        Point Eloc = getDestiation(loc, DIRECTION.EAST);
        Point Wloc = getDestiation(loc, DIRECTION.WEST);

        if (!Nloc.equals(loc)) avails[Nloc.x][Nloc.y] = true;
        if (!Sloc.equals(loc)) avails[Sloc.x][Sloc.y] = true;
        if (!Eloc.equals(loc)) avails[Eloc.x][Eloc.y] = true;
        if (!Wloc.equals(loc)) avails[Wloc.x][Wloc.y] = true;

        return avails;
    }

    public List<ATTACK_RESPONSE> getAttackResponse(Player attacker, Amoeba attackerAmoeba, Player defender, Amoeba defenderAmoeba) {
        return this.getAttackResponse(attacker, attackerAmoeba, defender, defenderAmoeba, false);
    }
    
    public List<ATTACK_RESPONSE> getAttackResponse(Player attacker, Amoeba attackerAmoeba, Player defender, Amoeba defenderAmoeba, boolean ignoreLocation) {
        List<ATTACK_RESPONSE> res = new LinkedList<>();

        //null check
        if (attacker == null || attackerAmoeba == null || defender == null || defenderAmoeba == null) return res;
        //Cannot attack if not in saeme location
        if (!attackerAmoeba.getLocation().equals(defenderAmoeba.getLocation()) && !_progress.ESCSPDdecide && !ignoreLocation) return res;
        //Cannot eat yourself
        if (attackerAmoeba.equals(defenderAmoeba)) return res;
        //gene check
        if (!attacker.hasGene(GENE.SFS) && !attacker.hasGene(GENE.AGR)) return res;

        //SFS
        if (_progress.currentPhase == Progress.PHASE_1_MOVE_AND_FEED) {
            //Armor
            if (defender.hasGene(GENE.ARM) && attacker.getID() != defender.getID()) {
                res.add(ATTACK_RESPONSE.BLOCKED);
                return res;
            }
            //ALM
            if (defender.hasGene(GENE.ALM) && !defender.hasGene(GENE.ESC) && !_progress.ALMactive) {
                 res.add(ATTACK_RESPONSE.ALARM);
            }
            //MTM
            if (defender.hasGene(GENE.MOU)) res.add(ATTACK_RESPONSE.MORE_THAN_A_MOUTHFUL);
        //AGR
        } else if (_progress.currentPhase == Progress.PHASE_5_DEATHS) {
            if (!attacker.hasGene(GENE.AGR)) return res;
            //Armor cannot block AGR attacks, only reduces damage
            if (defender.hasGene(GENE.ARM) && attacker.getID() != defender.getID()) {
                res.add(ATTACK_RESPONSE.ARMOR);
                return res;
            }
        }

        //COMMON ones
        if (_progress.currentPhase == Progress.PHASE_1_MOVE_AND_FEED || _progress.currentPhase == Progress.PHASE_5_DEATHS) {
            //Thread display
            if (defender.hasGene(GENE.DIS) && attackerAmoeba.number > defenderAmoeba.number && attacker.getID() != defender.getID()) {
                res.clear();
                res.add(ATTACK_RESPONSE.BLOCKED);
                return res;
            }
            //HC
            if (defender.hasGene(GENE.HARD)) res.add(ATTACK_RESPONSE.HARD_CRUST);

            //ESCusable?
            boolean ESCusable = false;
            int ESCcost = 1;
            if (defender.hasGene(GENE.STR)) ESCcost = 0;
            if (!_progress.ESCused) ESCusable = true;
            if (_progress.ESCused && !_progress.ESCSPDused && (defender.hasGene(GENE.SPD) || defender.hasGene(GENE.PER))) {
                ESCusable = true;
                ESCcost = 0;
            }

            //ALM
            if (ESCusable && !attacker.hasGene(GENE.CAM2) && defender.hasGene(GENE.ALM) && !defender.hasGene(GENE.ESC) && _progress.ALMactive && defender.getBP() >= ESCcost) res.add(ATTACK_RESPONSE.ESCAPE);
            //ESC
            if (ESCusable && !attacker.hasGene(GENE.CAM2) && defender.hasGene(GENE.ESC) && defender.getBP() >= ESCcost) res.add(ATTACK_RESPONSE.ESCAPE);
            //DEF
            if (!_progress.DEFused && !attacker.hasGene(GENE.CAM2) && defender.hasGene(GENE.DEF) && defender.getBP() > 0) res.add(ATTACK_RESPONSE.DEFENSE);
        }

        if (res.isEmpty()) res.add(ATTACK_RESPONSE.NO_DEFENSE);

        return res;
    }

    public List<GameMessage> SFSSelected(int playerID, int sfsTargetPlayerID, int sfsTargetAmoebaNum) {
        List<GameMessage> res = new LinkedList<>();

        Player attacker = getCurrentPlayer();
        Amoeba attackerAmoeba = getCurrentAmoeba();
        Player defender = getPlayerByID(sfsTargetPlayerID);
        Amoeba defenderAmoeba = defender.getAmoebaByNumber(sfsTargetAmoebaNum);

        _progress.attackedLocation = defenderAmoeba.getLocation();
        _progress.attackerID = attacker.getID();
        _progress.attackerAmoebaNum = attackerAmoeba.number;
        _progress.defenderID = defender.getID();
        _progress.defenderAmoebaNum = defenderAmoeba.number;

        //ID check
        if (attacker.getID() != playerID) return res;
        //phase check
        if (_progress.currentPhase != Progress.PHASE_1_MOVE_AND_FEED) return res;
        //BP check
        if (attacker.getBP() == 0) return res;
        //Eat check
        List<ATTACK_RESPONSE> atkRes = getAttackResponse(attacker, attackerAmoeba, defender, defenderAmoeba);
        if (atkRes.isEmpty() || atkRes.contains(ATTACK_RESPONSE.BLOCKED)) return res;

        //pay 1 BP
        attacker.addBP(-1);

        //if no defense is performed
        if (atkRes.contains(ATTACK_RESPONSE.NO_DEFENSE) || (atkRes.size() == 1 && atkRes.contains(ATTACK_RESPONSE.ALARM))) {
           res.addAll(SFSSuccess(attacker, attackerAmoeba, defender, defenderAmoeba));
        //Defense available
        } else {
            //CAM1
            res.addAll(CAM1handling(attacker, attackerAmoeba, atkRes));
            //update defense
            atkRes = getAttackResponse(attacker, attackerAmoeba, defender, defenderAmoeba);
            //if no defense is performed anymore
            if (atkRes.contains(ATTACK_RESPONSE.NO_DEFENSE) || (atkRes.size() == 1 && atkRes.contains(ATTACK_RESPONSE.ALARM))) {
               res.addAll(SFSSuccess(attacker, attackerAmoeba, defender, defenderAmoeba));
               return res;
            }
            //activate defender
            _progress.defenderActive = true;

            //If active defense is not available, no defense
            if (!atkRes.contains(ATTACK_RESPONSE.DEFENSE) && !atkRes.contains(ATTACK_RESPONSE.ESCAPE)) {
                res.addAll(defenderGiveUp(defender.getID()));
            }
        }


        return res;
    }

    private List<GameMessage> CAM1handling(Player attacker, Amoeba attackerAmoeba, List<ATTACK_RESPONSE> atkRes) {
        List<GameMessage> res = new LinkedList<>();

        //If attacker has CAM1 and defender has something that can be disabled by CAM
        if (attacker.hasGene(GENE.CAM1) && (atkRes.contains(ATTACK_RESPONSE.ESCAPE) || atkRes.contains(ATTACK_RESPONSE.DEFENSE))) {
            //Dice
            int dice = roll();
            res.add(new GameTextMessage(MESSAGE.DIE_ROLL, new String[]{attacker.getName(), Integer.toString(dice)},
                    COLOR_TYPE.PLAYER, attacker.getID()));

            //Result of CAM1 message
            if (dice > 3) {
                res.add(new GameTextMessage(MESSAGE.GENE_CAM1_SUCCESS, new String[]{attacker.getName(), Integer.toString(attackerAmoeba.number)},
                        COLOR_TYPE.PLAYER, attacker.getID(), new RelevantGene[]{new RelevantGene(attacker.getID(), GENE.CAM1)}));

                _progress.DEFused = true;
                _progress.ESCused = true;
                _progress.ESCSPDused = true;
            } else {
                res.add(new GameTextMessage(MESSAGE.GENE_CAM1_FAIL, new String[]{attacker.getName(), Integer.toString(attackerAmoeba.number)},
                        COLOR_TYPE.PLAYER, attacker.getID()));
            }
        }

        return res;
    }


    public List<GameMessage> SFSAck(int playerID, boolean useBANG, Point exLoc, boolean useTOX) {
        List<GameMessage> res = new LinkedList<>();

        Player attacker = getPlayerByID(_progress.attackerID);
        Amoeba attackerAmoeba = attacker.getAmoebaByNumber(_progress.attackerAmoebaNum);

        Player defender = getPlayerByID(_progress.defenderID);
        Amoeba defenderAmoeba = defender.getAmoebaByNumber(_progress.defenderAmoebaNum);

        if (playerID != defender.getID()) return res;

        _progress.amoebaFed = true;

        //More Than a Mouthful
        if (defender.hasGene(GENE.MOU)) {
               //Add 1DP
            defenderAmoeba.addDP(1);
            res.add(new GameTextMessage(MESSAGE.GENE_SFS_SUCCESS_MOU, new String[]{attacker.getName(), Integer.toString(attackerAmoeba.number), defender.getName(), Integer.toString(defenderAmoeba.number)},
                    COLOR_TYPE.PLAYER, attacker.getID(), new RelevantGene[]{new RelevantGene(attacker.getID(), GENE.SFS), new RelevantGene(defender.getID(), GENE.MOU)}));

            //Excrete normally
            res.addAll(excrete(attacker, attackerAmoeba, exLoc, useTOX));
        //Normal attack
        } else {
            //remove the amoeba
            SFSdeathHandling(defenderAmoeba);
            res.add(new GameTextMessage(MESSAGE.GENE_SFS_SUCCESS, new String[]{attacker.getName(), Integer.toString(attackerAmoeba.number), defender.getName(), Integer.toString(defenderAmoeba.number)},
                    COLOR_TYPE.PLAYER, attacker.getID(), new RelevantGene[]{new RelevantGene(attacker.getID(), GENE.SFS)}));
        }

        //Activate alarm
        if (defender.hasGene(GENE.ALM) && !_progress.ALMactive) {
            _progress.ALMactive = true;
            res.add(new GameTextMessage(MESSAGE.GENE_ALM, new String[]{}, COLOR_TYPE.PLAYER, defender.getID(),
                    new RelevantGene[]{new RelevantGene(defender.getID(), GENE.ALM)}));
        }

        //BANG used
        if (useBANG && defender.hasGene(GENE.BANG) && _option.canBANGwithSFS) {
            //add 1 DP to everyone
            for (Amoeba amoeba : _soup.getCell(defenderAmoeba.getLocation()).getAmoebas()) {
                amoeba.addDP(1);
            }

            res.add(new GameTextMessage(MESSAGE.GENE_BANG, new String[]{pointToString(_progress.defenderBANGloc)}, COLOR_TYPE.PLAYER, defender.getID(),
                    new RelevantGene[]{new RelevantGene(defender.getID(), GENE.BANG)}));
        }

        //could be end of amoeba
        res.addAll(amoebaFinished());

        return res;
    }

    
    private List<GameMessage> SFSSuccess(Player attacker, Amoeba attackerAmoeba, Player defender, Amoeba defenderAmoeba) {

         if (defender.hasGene(GENE.BANG) && _option.canBANGwithSFS) {
            _progress.defenderBANG = true;
            _progress.defenderBANGloc = defenderAmoeba.getLocation();
            List<GameMessage> res = new LinkedList<>();
            res.add(new GameTextMessage(MESSAGE.GENE_BANG_NOTICE, new String[]{defender.getName(), Integer.toString(defenderAmoeba.number)},
                                    COLOR_TYPE.PLAYER, defender.getID()/*, new RelevantGene[]{new RelevantGene(defender.getID(), GENE.BANG)})*/));
            return res;
        //Else end
        } else {
            return SFSAck(defender.getID(), false, null, false);
        }

        
        /*List<GameMessage> res = new LinkedList<GameMessage>();

        _progress.amoebaFed = true;

        //remove the amoeba
        SFSdeathHandling(defenderAmoeba);
        res.add(new GameTextMessage(MESSAGE.GENE_SFS_SUCCESS, new String[]{attacker.getName(), Integer.toString(attackerAmoeba.number), defender.getName(), Integer.toString(defenderAmoeba.number)},
                COLOR_TYPE.PLAYER, attacker.getID(), new RelevantGene[]{new RelevantGene(attacker.getID(), GENE.SFS)}));

        //Activate alarm
        if (atkRes.contains(ATTACK_RESPONSE.ALARM)) {
            _progress.ALMactive = true;
            res.add(new GameTextMessage(MESSAGE.GENE_ALM, new String[]{}, COLOR_TYPE.PLAYER, defender.getID(),
                    new RelevantGene[]{new RelevantGene(defender.getID(), GENE.ALM)}));
        }

        //If defender has BANG, ask
        if (defender.hasGene(GENE.BANG)) {
            _progress.defenderBANG = true;
            _progress.defenderBANGloc = defenderAmoeba.getLocation();
        //Else end
        } else {
            //could be end of amoeba
            res.add(amoebaFinished());
        }

        return res;*/
    }

    private List<GameMessage> SFSSuccessMOU(Player attacker, Amoeba attackerAmoeba, Player defender, Amoeba defenderAmoeba, Point exLoc, boolean useTox) {

        return SFSAck(defender.getID(), false, exLoc, useTox);
        /*
        List<GameMessage> res = new LinkedList<GameMessage>();
        //Add 1DP
        defenderAmoeba.addDP(1);
        res.add(new GameTextMessage(MESSAGE.GENE_SFS_SUCCESS_MOU, new String[]{attacker.getName(), Integer.toString(attackerAmoeba.number), defender.getName(), Integer.toString(defenderAmoeba.number)},
                COLOR_TYPE.PLAYER, attacker.getID(), new RelevantGene[]{new RelevantGene(attacker.getID(), GENE.SFS), new RelevantGene(defender.getID(), GENE.MOU)}));

        //Excrete normally
        res.addAll(excrete(attacker, attackerAmoeba, exLoc, useTox));
        _progress.amoebaFed = true;

        //Activate alarm
        if (atkRes.contains(ATTACK_RESPONSE.ALARM)) {
            _progress.ALMactive = true;
            res.add(new GameTextMessage(MESSAGE.GENE_ALM, new String[]{}, COLOR_TYPE.PLAYER, defender.getID(),
                    new RelevantGene[]{new RelevantGene(defender.getID(), GENE.ALM)}));
        }

        //could be end of amoeba
        res.add(amoebaFinished());

        return res;*/
    }

    private List<GameMessage> SFSFailure(Player attacker, Player defender, List<ATTACK_RESPONSE> atkRes) {
        List<GameMessage> res = new LinkedList<>();
        _progress.amoebaFed = false;

        res.add(new GameTextMessage(MESSAGE.GENE_SFS_FAILURE, new String[]{}, COLOR_TYPE.PLAYER, attacker.getID()));

        //Activate alarm
        if (atkRes.contains(ATTACK_RESPONSE.ALARM)) {
            _progress.ALMactive = true;
            res.add(new GameTextMessage(MESSAGE.GENE_ALM, new String[]{}, COLOR_TYPE.PLAYER, defender.getID(),
                    new RelevantGene[]{new RelevantGene(defender.getID(), GENE.ALM)}));
        }


        res.addAll(amoebaStarved(attacker.getID()));

        return res;
    }

    public List<Amoeba> getAGRusableAmoebas(Player player) {
        List<Amoeba> usable = new LinkedList<>();

        //GENE check
        if (!player.hasGene(GENE.AGR)) return usable;
        //BP check
        if (player.getBP() == 0) return usable;

        List<Point> locs = new LinkedList<>();
        for (Amoeba attacker : player.getAmoebas()) {
            Point loc = attacker.getLocation();
            //Skip if same location is already covered
            if (locs.contains(loc)) continue;
            //check if there is at least an amoeba to attack
            for (Amoeba defender : _soup.getCell(loc).getAmoebas()) {
                //Skip yourself
                if (attacker.equals(defender)) continue;
                //get attack response
                List<ATTACK_RESPONSE> atkRes = getAttackResponse(player, attacker, getPlayerByID(defender.playerID), defender);
                //if defenser can be attacked
                if (atkRes.size() > 0 && !atkRes.contains(ATTACK_RESPONSE.BLOCKED)) {
                    usable.add(attacker);
                    locs.add(loc);
                    break;
                }
            }
        }

        return usable;
    }

    public GameMessage AGRcanceled(int playerID) {
        Player player = getCurrentPlayer();
        //ID check
        if (player.getID() != playerID) return null;
        //AGR check
        if (!_progress.AGRactive) return null;
        //set AGR as used
        _progress.AGRused = true;
        _progress.AGRactive = false;

        return startPhase6();
    }

    public List<GameMessage> AGRSelected(int playerID, int attackerAmoebaNum, int targetPlayerID, int targetAmoebaNum) {
        List<GameMessage> res = new LinkedList<>();

        Player attacker = getCurrentPlayer();
        Amoeba attackerAmoeba = attacker.getAmoebaByNumber(attackerAmoebaNum);
        Player defender = getPlayerByID(targetPlayerID);
        Amoeba defenderAmoeba = defender.getAmoebaByNumber(targetAmoebaNum);

        _progress.attackedLocation = defenderAmoeba.getLocation();
        _progress.attackerID = attacker.getID();
        _progress.attackerAmoebaNum = attackerAmoeba.number;
        _progress.defenderID = defender.getID();
        _progress.defenderAmoebaNum = defenderAmoeba.number;

        //ID check
        if (attacker.getID() != playerID) return res;
        //phase check
        if (_progress.currentPhase != Progress.PHASE_5_DEATHS || !_progress.AGRactive) return res;
        //BP check
        if (attacker.getBP() == 0) return res;
        //Eat check
        List<ATTACK_RESPONSE> atkRes = getAttackResponse(attacker, attackerAmoeba, defender, defenderAmoeba);
        if (atkRes.isEmpty() || atkRes.contains(ATTACK_RESPONSE.BLOCKED)) return res;

        //pay 1 BP
        attacker.addBP(-1);

        //if no defense is performed
        if (atkRes.contains(ATTACK_RESPONSE.NO_DEFENSE) || (atkRes.contains(ATTACK_RESPONSE.ARMOR) && atkRes.size() == 1)) {
            res.addAll(AGRsuccess(attacker, attackerAmoeba, defender, defenderAmoeba, atkRes));
            return res;
        //defense available
        } else {
            //CAM1
            res.addAll(CAM1handling(attacker, attackerAmoeba, atkRes));
            //update defense
            atkRes = getAttackResponse(attacker, attackerAmoeba, defender, defenderAmoeba);
            //if no defense is performed anymore
            if (atkRes.contains(ATTACK_RESPONSE.NO_DEFENSE) || (atkRes.size() == 1 && atkRes.contains(ATTACK_RESPONSE.ARMOR))) {
               res.addAll(AGRsuccess(attacker, attackerAmoeba, defender, defenderAmoeba, atkRes));
               return res;
            }
            //activate defender
            _progress.defenderActive = true;

            //If active defense is not available, no defense
            if (!atkRes.contains(ATTACK_RESPONSE.DEFENSE) && !atkRes.contains(ATTACK_RESPONSE.ESCAPE)) {
                res.addAll(defenderGiveUp(defender.getID()));
            }
        }


        return res;
    }


    private List<GameMessage> AGRsuccess(Player attacker, Amoeba attackerAmoeba, Player defender, Amoeba defenderAmoeba, List<ATTACK_RESPONSE> atkRes) {
        List<GameMessage> res = new LinkedList<>();

        _progress.AGRused = true;
        _progress.AGRactive = false;
        _progress.clearDefenseProgress();

        RelevantGene[] rgenes;
        //Has armor
        if (atkRes.contains(ATTACK_RESPONSE.ARMOR)) {
            //Armor reduces damage to 1
            defenderAmoeba.addDP(1);
            rgenes = new RelevantGene[]{new RelevantGene(attacker.getID(), GENE.AGR), new RelevantGene(defender.getID(), GENE.ARM)};
        //Else
        } else {
            //Add enough DPs to kill an amoeba
            defenderAmoeba.addDP(5);
            rgenes = new RelevantGene[]{new RelevantGene(attacker.getID(), GENE.AGR)};
        }

        res.add(new GameTextMessage(MESSAGE.GENE_AGR_SUCCESS, new String[]{attacker.getName(), Integer.toString(attackerAmoeba.number), defender.getName(), Integer.toString(defenderAmoeba.number)},
                COLOR_TYPE.PLAYER, attacker.getID(), rgenes));

        //Redo death phase
        _progress.currentOrder = 0;
        _progress.currentAmoeba = 0;
        GameMessage da = nextDeadAmoeba();

        //PHASE 6
        if (da == null) {
            res.add(startPhase6());
        } else {
            res.add(da);
        }

        return res;
    }

    private List<GameMessage> AGRFailure(int attackerID) {
        List<GameMessage> res = new LinkedList<>();

        _progress.AGRused = true;
        _progress.AGRactive = false;
        _progress.clearDefenseProgress();

        res.add(new GameTextMessage(MESSAGE.GENE_AGR_FAILURE, new String[]{}, COLOR_TYPE.PLAYER, attackerID));

        //Redo death phase
        _progress.currentOrder = 0;
        _progress.currentAmoeba = 0;
        GameMessage da = nextDeadAmoeba();

        //PHASE 6
        if (da == null) {
            res.add(startPhase6());
        } else {
            res.add(da);
        }

        return res;
    }



    public  List<GameMessage> defenderGiveUp(int defenderID) {
        List<GameMessage> res = new LinkedList<>();

        Player attacker = getPlayerByID(_progress.attackerID);
        Amoeba attackerAmoeba = attacker.getAmoebaByNumber(_progress.attackerAmoebaNum);
        Player defender = getPlayerByID(_progress.defenderID);
        Amoeba defenderAmoeba = defender.getAmoebaByNumber(_progress.defenderAmoebaNum);

        //check
        if (!_progress.defenderActive) return res;
        //check ID
        if (defender.getID() != defenderID) return res;

        //Mark all as used
        _progress.DEFused = true;
        _progress.ESCused = true;
        _progress.ESCSPDused = true;
        _progress.defenderActive = false;

        //See if there is any defense left
        List<ATTACK_RESPONSE> atkRes = getAttackResponse(attacker, attackerAmoeba, defender, defenderAmoeba);
        //if no defense is possible anymore
        if (atkRes.contains(ATTACK_RESPONSE.NO_DEFENSE) || (atkRes.size() == 1 && atkRes.contains(ATTACK_RESPONSE.ALARM))
                                                        || (atkRes.size() == 1 && atkRes.contains(ATTACK_RESPONSE.ARMOR))) {
            //SFS
            if (_progress.currentPhase == Progress.PHASE_1_MOVE_AND_FEED) {
                res.addAll(SFSSuccess(attacker, attackerAmoeba, defender, defenderAmoeba));
            //AGR
            } else if (_progress.currentPhase == Progress.PHASE_5_DEATHS) {
                res.addAll(AGRsuccess(attacker, attackerAmoeba, defender, defenderAmoeba, atkRes));
            }
           return res;
        //if  HC is left
        } else if (atkRes.contains(ATTACK_RESPONSE.HARD_CRUST)) {
            //check if attacker has enough BP
            if (attacker.getBP() == 0) {
                res.addAll(attackerActionSelected(attacker.getID(), false, null, false));
                return res;
            }
            //check back to the attacker
           _progress.attackerHARD = true;
           if (atkRes.contains(ATTACK_RESPONSE.MORE_THAN_A_MOUTHFUL)) _progress.attackerMOUexcretes = true;
        //MOU
        } else if (atkRes.contains(ATTACK_RESPONSE.MORE_THAN_A_MOUTHFUL)) {
            //check back to the attacker
           _progress.attackerMOUexcretes = true;
        }

        return res;
    }

    public List<GameMessage> attackerActionSelected(int attackerID, boolean payHC, Point atkExLoc, boolean atkUseTOX) {
        List<GameMessage> res = new LinkedList<>();

        Player attacker = getPlayerByID(_progress.attackerID);
        Amoeba attackerAmoeba = attacker.getAmoebaByNumber(_progress.attackerAmoebaNum);
        Player defender = getPlayerByID(_progress.defenderID);
        Amoeba defenderAmoeba = defender.getAmoebaByNumber(_progress.defenderAmoebaNum);

        //check
        if (!_progress.attackerHARD && !_progress.attackerMOUexcretes) return res;
        //check ID
        if (attacker.getID() != attackerID) return res;

        boolean HARD = _progress.attackerHARD;
        boolean MOU =  _progress.attackerMOUexcretes;
        _progress.attackerHARD = false;
        _progress.attackerMOUexcretes = false;

        List<ATTACK_RESPONSE> atkRes = getAttackResponse(attacker, attackerAmoeba, defender, defenderAmoeba);

        //If not paid and in movement phase
        if (HARD && _progress.attackerHARD && !payHC && _progress.currentPhase == Progress.PHASE_1_MOVE_AND_FEED) {
            //SFS failed
            res.add(new GameTextMessage(MESSAGE.GENE_HARD_GIVEUP, new String[]{attacker.getName()}, COLOR_TYPE.PLAYER, attacker.getID(),
                    new RelevantGene[]{new RelevantGene(defender.getID(), GENE.HARD)}));
            res.addAll(SFSFailure(attacker, defender, atkRes));
            return res;
        }
        //If not paid and in agression phase
        if (HARD && !payHC && _progress.currentPhase == Progress.PHASE_5_DEATHS) {
            //AGR failed
            res.add(new GameTextMessage(MESSAGE.GENE_HARD_GIVEUP, new String[]{attacker.getName()}, COLOR_TYPE.PLAYER, attacker.getID(),
                    new RelevantGene[]{new RelevantGene(defender.getID(), GENE.HARD)}));
            res.addAll(AGRFailure(attackerID));
            return res;
        }

        //Else pay 1BP
        if (HARD) {
            attacker.addBP(-1);
            res.add(new GameTextMessage(MESSAGE.GENE_HARD_PAID, new String[]{attacker.getName()}, COLOR_TYPE.PLAYER, attacker.getID(),
                    new RelevantGene[]{new RelevantGene(defender.getID(), GENE.HARD)}));
        }

        //AGR doesn't have excretion
        if (_progress.currentPhase == Progress.PHASE_5_DEATHS) {
             res.addAll(AGRsuccess(attacker, attackerAmoeba, defender, defenderAmoeba, atkRes));
             return res;
        }

        //SFS does
        if (MOU && _progress.currentPhase == Progress.PHASE_1_MOVE_AND_FEED) {
            res.addAll(SFSSuccessMOU(attacker, attackerAmoeba, defender, defenderAmoeba, atkExLoc, atkUseTOX));
            return res;
        } else if (_progress.currentPhase == Progress.PHASE_1_MOVE_AND_FEED) {
            res.addAll(SFSSuccess(attacker, attackerAmoeba, defender, defenderAmoeba));
            return res;
        }

        return res;
    }

    public List<GameMessage>DEFUse(int defenderID) {
        Player defender = getPlayerByID(_progress.defenderID);
        //ID check
        if (defender.getID() != defenderID) return null;
        //active check
        if (!_progress.defenderActive) return null;
        //verify
        if (_progress.DEFused) return null;

        //Pay for DEF
        defender.addBP(-1);
        //When DEF is used, no food will be left even if defending failed
        _progress.DEFnoFood = true;

        return DEFRoll(defenderID);
    }

    public List<GameMessage> DEFRoll(int defenderID) {
        List<GameMessage> res = new LinkedList<>();

        Player attacker = getPlayerByID(_progress.attackerID);
        Amoeba attackerAmoeba = attacker.getAmoebaByNumber(_progress.attackerAmoebaNum);
        Player defender = getPlayerByID(_progress.defenderID);
        Amoeba defenderAmoeba = defender.getAmoebaByNumber(_progress.defenderAmoebaNum);

        //ID check
        if (defender.getID() != defenderID) return res;
        //active check
        if (!_progress.defenderActive) return res;
        //check ATK_RES
        List<ATTACK_RESPONSE> atkRes = getAttackResponse(attacker, attackerAmoeba, defender, defenderAmoeba);
        if (!atkRes.contains(ATTACK_RESPONSE.DEFENSE)) return res;


        _progress.diceState = DICE_STATE.DEF;
        _progress.dice1 = roll();
        _progress.dice2 =  roll();
        res.add(new GameTextMessage(MESSAGE.DICE_DEF, new String[]{Integer.toString(_progress.dice1), Integer.toString(_progress.dice2)}, COLOR_TYPE.DEFAULT, 0,
                new RelevantGene[]{new RelevantGene(defender.getID(), GENE.DEF)}));


        return res;
    }

    public List<GameMessage> DEFAck(int defenderID) {
        List<GameMessage> res = new LinkedList<>();

        Player attacker = getPlayerByID(_progress.attackerID);
        Amoeba attackerAmoeba = attacker.getAmoebaByNumber(_progress.attackerAmoebaNum);
        Player defender = getPlayerByID(_progress.defenderID);
        Amoeba defenderAmoeba = defender.getAmoebaByNumber(_progress.defenderAmoebaNum);

        //ID check
        if (defender.getID() != defenderID) return res;
        //active check
        if (!_progress.defenderActive) return res;
        //check ATK_RES
        List<ATTACK_RESPONSE> atkRes = getAttackResponse(attacker, attackerAmoeba, defender, defenderAmoeba);
        if (!atkRes.contains(ATTACK_RESPONSE.DEFENSE)) return res;
        //Dice check
        if (_progress.diceState != DICE_STATE.DEF) return res;

        int diceATK = _progress.dice1;
        int diceDEF = _progress.dice2;
        _progress.clearDice();

        //wait a little
        sleep(DICE_WAIT);

        //Finished if not a  tie
        //Attacker win
        if (diceATK > diceDEF) {
            res.add(new GameTextMessage(MESSAGE.GENE_DEF_ATTACKER_WIN, new String[]{defender.getName(), Integer.toString(defenderAmoeba.number)}, COLOR_TYPE.PLAYER, defender.getID(),
                    new RelevantGene[]{new RelevantGene(defender.getID(), GENE.DEF)}));
            //Per can be used
            if (!_progress.PERused && defender.hasGene(GENE.PER)) {
                _progress.PERused = true;
                res.add(new GameTextMessage(MESSAGE.GENE_PER, new String[]{defender.getName(), Integer.toString(defenderAmoeba.number)}, COLOR_TYPE.PLAYER, defender.getID(),
                        new RelevantGene[]{new RelevantGene(defender.getID(), GENE.PER)}));
            //DEF failed
            } else {
                _progress.DEFused = true;
                //Update ATK_RES
                atkRes = getAttackResponse(attacker, attackerAmoeba, defender, defenderAmoeba);
                //If there is escape left
                if (atkRes.contains(ATTACK_RESPONSE.ESCAPE)) {
                    return res;
                //Else end of defender action
                } else {
                   res.addAll(defenderGiveUp(defenderID));
                   return res;
                }
            }
        //Defender win
        } else if (diceDEF > diceATK) {
            res.add(new GameTextMessage(MESSAGE.GENE_DEF_DEFENDER_WIN, new String[]{defender.getName(), Integer.toString(defenderAmoeba.number)}, COLOR_TYPE.PLAYER, defender.getID(),
                    new RelevantGene[]{new RelevantGene(defender.getID(), GENE.DEF)}));
            //Per can be used
            if (!_progress.PERused && attacker.hasGene(GENE.PER)) {
                _progress.PERused = true;
                res.add(new GameTextMessage(MESSAGE.GENE_PER, new String[]{attacker.getName(), Integer.toString(attackerAmoeba.number)}, COLOR_TYPE.PLAYER, attacker.getID(),
                        new RelevantGene[]{new RelevantGene(attacker.getID(), GENE.PER)}));
            //During Phase 1
            } else if (_progress.currentPhase == Progress.PHASE_1_MOVE_AND_FEED) {
                //attacker starves
                res.addAll(SFSFailure(attacker, defender, atkRes));
                return res;
            } else if (_progress.currentPhase == Progress.PHASE_5_DEATHS) {
                //AGR fails
                res.addAll(AGRFailure(attacker.getID()));
                return res;
            }
        }

        //Tie or 2nd attempt
        res.addAll(DEFRoll(defenderID));
        
        
        return res;
    }

    public List<GameMessage> ESCUse(int defenderID) {
        List<GameMessage> res = new LinkedList<>();

        Player attacker = getPlayerByID(_progress.attackerID);
        Amoeba attackerAmoeba = attacker.getAmoebaByNumber(_progress.attackerAmoebaNum);
        Player defender = getPlayerByID(_progress.defenderID);
        Amoeba defenderAmoeba = defender.getAmoebaByNumber(_progress.defenderAmoebaNum);

        //ID check
        if (defender.getID() != defenderID) return res;
        //active check
        if (!_progress.defenderActive) return res;
        //check ATK_RES
        List<ATTACK_RESPONSE> atkRes = getAttackResponse(attacker, attackerAmoeba, defender, defenderAmoeba);
        if (!atkRes.contains(ATTACK_RESPONSE.ESCAPE)) return res;

        //Pay for it
        if (!defender.hasGene(GENE.STR) && !_progress.ESCused) defender.addBP(-1);

        //Roll
        res.add(ESCRoll(defender));

        return res;
    }

    private GameMessage ESCRoll(Player defender) {
        //Roll
        if (defender.hasGene(GENE.MV2)) {
            _progress.diceState = DICE_STATE.ESC_MV2;
            return null;
        } else if (defender.hasGene(GENE.MV1)) {
            _progress.diceState = DICE_STATE.ESC_MV1;
            _progress.dice1 = roll();
            _progress.dice2 = roll();
            if (defender.hasGene(GENE.STR)) {
                return new GameTextMessage(MESSAGE.DICE_ROLL, new String[]{defender.getName(), Integer.toString(_progress.dice1), Integer.toString(_progress.dice2)},
                        COLOR_TYPE.PLAYER, defender.getID(), new RelevantGene[] {new RelevantGene(defender.getID(), GENE.MV1), new RelevantGene(defender.getID(), GENE.STR)});
            } else {
                return new GameTextMessage(MESSAGE.DICE_ROLL, new String[]{defender.getName(), Integer.toString(_progress.dice1), Integer.toString(_progress.dice2)},
                        COLOR_TYPE.PLAYER, defender.getID(), new RelevantGene[] {new RelevantGene(defender.getID(), GENE.MV1)});
            }
        } else {
            _progress.diceState = DICE_STATE.ESC;
            _progress.dice1 = roll();
            if (defender.hasGene(GENE.STR)) {
                return new GameTextMessage(MESSAGE.DIE_ROLL, new String[]{defender.getName(), Integer.toString(_progress.dice1)}, COLOR_TYPE.PLAYER, defender.getID(),
                        new RelevantGene[]{new RelevantGene(defender.getID(), GENE.STR)});
            } else {
                return new GameTextMessage(MESSAGE.DIE_ROLL, new String[]{defender.getName(), Integer.toString(_progress.dice1)}, COLOR_TYPE.PLAYER, defender.getID());
            }
        }

    }


    public List<GameMessage> ESCDirectionSelected(int defenderID, DIRECTION dir, int[] TENfoods, boolean useREB) {
        List<GameMessage> res = new LinkedList<>();

        Player attacker = getPlayerByID(_progress.attackerID);
        Amoeba attackerAmoeba = attacker.getAmoebaByNumber(_progress.attackerAmoebaNum);
        Player defender = getPlayerByID(_progress.defenderID);
        Amoeba defenderAmoeba = defender.getAmoebaByNumber(_progress.defenderAmoebaNum);

        //ID check
        if (defender.getID() != defenderID) return res;
        //active check
        if (!_progress.defenderActive) return res;
        //check ATK_RES
        List<ATTACK_RESPONSE> atkRes = getAttackResponse(attacker, attackerAmoeba, defender, defenderAmoeba);
        if (!atkRes.contains(ATTACK_RESPONSE.ESCAPE)) return res;

        //Tentacle
        if (defender.hasGene(GENE.TEN) && TENfoods != null) {
            res.add(useTEN(defender, TENfoods, defenderAmoeba.getLocation(), dir));
        }

       //MOVE message
        if (dir != DIRECTION.NONE) {
            if (useREB) {
                res.add(new GameTextMessage(MESSAGE.GENE_REB_MOVE, new String[]{defender.getName(), Integer.toString(defenderAmoeba.number), getDirectionString(getOppositeDirection(dir))},
                        COLOR_TYPE.PLAYER, defender.getID(), new RelevantGene[]{new RelevantGene(defender.getID(), GENE.REB)}));
            } else {
                res.add(new GameTextMessage(MESSAGE.MOVE, new String[]{defender.getName(), Integer.toString(defenderAmoeba.number), getDirectionString(dir)},
                        COLOR_TYPE.PLAYER, defender.getID()));
            }
        } else {
            res.add(new GameTextMessage(MESSAGE.MOVE_STAY, new String[]{defender.getName(), Integer.toString(defenderAmoeba.number)},
                    COLOR_TYPE.PLAYER, defender.getID()));
        }


        Point oldLoc = (Point)defenderAmoeba.getLocation().clone();

        //Move
        res.addAll(moveAmoeba(defenderAmoeba, dir));

        _progress.clearDice();

        res.addAll(endOfMovementHandling(defender, defenderAmoeba, oldLoc));

        //SPD used
        if (_progress.ESCused) {
            _progress.ESCSPDused = true;
            _progress.ESCSPDdecide = false;
        }
        _progress.ESCused = true;
        _progress.ESCfinished = true;
        //And SPD is available and not used, SPD is possible
        if (!_progress.ESCSPDused && defender.hasGene(GENE.SPD) || defender.hasGene(GENE.PER)) {
            _progress.ESCSPDdecide = true;
        } else {
            _progress.ESCSPDdecide = false;
        }

        return res;
    }


    public List<GameMessage> ESCAck(int defenderID) {
        List<GameMessage> res = new LinkedList<>();

        Player attacker = getPlayerByID(_progress.attackerID);
        Amoeba attackerAmoeba = attacker.getAmoebaByNumber(_progress.attackerAmoebaNum);
        Player defender = getPlayerByID(_progress.defenderID);
        Amoeba defenderAmoeba = defender.getAmoebaByNumber(_progress.defenderAmoebaNum);
        
        //ID check
        if (defender.getID() != defenderID) return res;
        //active check
        if (!_progress.defenderActive) return res;
        //finished check
        if (!_progress.ESCfinished) return res;

        _progress.ESCfinished = false;
        
        boolean inSameLoc = attackerAmoeba.getLocation().equals(defenderAmoeba.getLocation());
        boolean inOrigLoc = defenderAmoeba.getLocation().equals(_progress.attackedLocation);

        //Attack successful
        if ((_option.SFSESCHOLisSuccess && inSameLoc) || (inSameLoc && inOrigLoc)) {
            //update ATK_RES
            List<ATTACK_RESPONSE> atkRes = getAttackResponse(attacker, attackerAmoeba, defender, defenderAmoeba);
            //Failed case
            //If there is escape left
            if (atkRes.contains(ATTACK_RESPONSE.DEFENSE)) {
                return res;
            //Else end of defender action
            } else {
               res.addAll(defenderGiveUp(defenderID));
               return res;
            }
        //Attack failed
        } else {
            //Failed case
            if (_progress.currentPhase == Progress.PHASE_1_MOVE_AND_FEED) {
                //update ATK_RES
                List<ATTACK_RESPONSE> atkRes = getAttackResponse(attacker, attackerAmoeba, defender, defenderAmoeba);
                //attacker starves
                res.addAll(SFSFailure(attacker, defender, atkRes));
                return res;
            } else if (_progress.currentPhase == Progress.PHASE_5_DEATHS) {
                //AGR fails
                res.addAll(AGRFailure(attacker.getID()));
                return res;
            }
        }


        return res;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");

        for (Player player : _players) {
            sb.append(player.getID());
            sb.append(", ");
            sb.append(player.getName());
            sb.append("; ");
        }

        sb.append("]");
        return sb.toString();
    }
}
