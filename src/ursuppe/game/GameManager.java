package ursuppe.game;

import ursuppe.game.message.GameTextMessage;
import ursuppe.game.message.ServerMessage;
import java.awt.Color;
import java.awt.Point;
import java.io.IOException;
import java.net.BindException;
import java.net.SocketException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JOptionPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import network.Message;
import network.MinaServer;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import ursuppe.UrsuppeApp;
import ursuppe.UrsuppeResource;
import ursuppe.ai.AIContainer;
import ursuppe.game.GameState.DIRECTION;
import ursuppe.game.GenePool.GENE;
import ursuppe.game.exception.GameException;
import ursuppe.game.exception.GameInProgressException;
import ursuppe.game.exception.NotEnoughPlayerException;
import ursuppe.game.exception.TooManyPlayersException;
import ursuppe.game.message.GameEventMessage;
import ursuppe.game.message.GameMessage;
import ursuppe.gui.icon.CustomIconSet;

/**
 *
 * @author Personal
 */
public class GameManager extends IoHandlerAdapter {

    private final Map<IoSession, String> _connectedNames;
    private final Map<IoSession, Integer> _activePlayers;
    
    private final MinaServer _server;
    private GameState _game;
    private GameOption _options;
    private final AIContainer _AIcontainer;

    private final int _port;

    private boolean _acceptCommand = true;

    private Map<Integer, CustomIconSet> _customIcons;

    private Replay _replay;
    private Log _log;

    public GameManager(int port, GameOption option) {
        _server = new MinaServer(this, port);
        _port = port;
        _options = option;
        _connectedNames = new HashMap<>();
        _activePlayers = new HashMap<>();
        _AIcontainer = new AIContainer(this);
        resetGame();
    }

    @Override
    public void exceptionCaught( IoSession session, Throwable cause ) throws Exception
    {
        UrsuppeApp.debug(cause);
       if (cause instanceof IOException) {
           disconnected(session);
       } else {
            disconnected(session);
            UrsuppeApp.dump(cause);
            JOptionPane.showMessageDialog(null, UrsuppeResource.getString("error.OtherException") + "\n" + cause.getMessage());
        }
    }

    @Override
    public void messageReceived( IoSession session, Object message ) throws Exception
    {
        if (message instanceof Message) {
            try {
                messageHandling(session, (Message)message);
            } catch (Exception e) {
                UrsuppeApp.debug(e);
                UrsuppeApp.dump(e);
                JOptionPane.showMessageDialog(null, UrsuppeResource.getString("error.OtherException") + "\n" + e.getMessage());
            }
        }
    }

    @Override
    public void messageSent(IoSession session, Object message) {
    }

    @Override
    public void sessionIdle( IoSession session, IdleStatus status ) throws Exception
    {
    }

    public boolean start() {
        try {
            return _server.start();
        } catch (BindException  be) {
            JOptionPane.showMessageDialog(null, UrsuppeResource.getString("error.portAlreadyUsed"));
            return false;
        } catch (SocketException se) {
            JOptionPane.showMessageDialog(null, se.getMessage());
            return false;
        } catch (IOException ioe) {
            JOptionPane.showMessageDialog(null, ioe.getMessage());
            return false;
        }
    }

    public void stop() {
        _server.stop();
    }
//
//    public void run() {
//        try {
//            ServerSocket socket = new ServerSocket(_port, 10);
//            socket.setSoTimeout(2000);
//            _isRunning = true;
//            _isActive = true;
//            while (_isActive) {
//                try {
//                    Socket connectionSocket = socket.accept();
//                    MessageConnection connection = new MessageConnection(this, connectionSocket);
//                    synchronized (this) {
//                        _connections.add(connection);
//                    }
//                } catch (InterruptedIOException iie) {
//                    this.broadcast(new Message(Message.COMMAND_SETUP, Message.CHECKALIVE, 0));
//                }
//            }
//        } catch (BindException be) {
//            _bindException = true;
//            JOptionPane.showMessageDialog(null, UrsuppeResource.getString("error.portAlreadyUsed"));
//        } catch (IOException ex) {
//            JOptionPane.showMessageDialog(null, UrsuppeResource.getString("error.OtherException") + "\n" + ex.getMessage());
//            ex.printStackTrace();
//        } finally {
//            _isRunning = false;
//        }
//    }

    public void setAcceptCommand(boolean accept) {
        _acceptCommand = accept;
    }

    public boolean startGame() {
        if (_game.getProgress().started || _game.getProgress().ended) return false;
        
        try {
            _game.startGame();
            //sendGameMessage(GameMessage.singleArgMessage(ARGC1_MESSAGE.ROUND_START,  Integer.toString(_game.getProgress().round), COLOR_TYPE.DEFAULT, 0));
            broadcastCustomIcons();

            _log = new Log(_options);
            _replay = new Replay(_log.logName, _customIcons);

            sendGameMessage(new GameTextMessage(GameTextMessage.MESSAGE.GAME_START, new String[] {}, GameTextMessage.COLOR_TYPE.EMPHASIZE, 0));
            broadcast(new Message(Message.COMMAND_GAME, Message.ENTIRE_GAME_STATE, _game));

            return true;
        } catch (NotEnoughPlayerException nep) {
//            nep.printStackTrace();
            JOptionPane.showMessageDialog(null, UrsuppeResource.getString("error.NotEnoughPlayer"));
        } catch (GameException ge) {
//            ge.printStackTrace();
            JOptionPane.showMessageDialog(null, UrsuppeResource.getString("error.OtherException") + "\n" + ge.getMessage());
        }
        return false;

    }

    public void disconnected(IoSession session) {
        synchronized (this) {
            try {
                session.close(true);
            } catch (Exception e) {
                UrsuppeApp.debug(e);
            }
            if (_connectedNames.containsKey(session)) {
                sendServerMessage(ServerMessage.singleArgMessage(ServerMessage.ARGC1_MESSAGE.DISCONNECTED, _connectedNames.get(session), ServerMessage.COLOR_TYPE.DEFAULT));
            }
            if (_activePlayers.containsKey(session)) {
                 if (!_game.getProgress().started) {
                    sendServerMessage(_game.removePlayer(_activePlayers.get(session)));
                 }
            }
            _connectedNames.remove(session);
            _activePlayers.remove(session);
        }
    }



    public synchronized void resetGame() {
        _game = new GameState(_options);
        _customIcons = new HashMap<>();

        _activePlayers.clear();

        if (_log != null)
            _log.close();

        if (resetCountDownTimer != null) {
            resetCountDownTimer.cancel();
            resetCountDownTimer = null;
        }

        sendServerMessage(ServerMessage.zeroArgMessage(ServerMessage.ARGC0_MESSAGE.NEWGAME, ServerMessage.COLOR_TYPE.DEFAULT));
        broadcast(new Message(Message.COMMAND_SETUP, Message.NEWGAME, 1));
        broadcast(new Message(Message.COMMAND_GAME, Message.ENTIRE_GAME_STATE, _game));
    }

    public void resetGame(String name) {
        if (_server == null) return;

       {
            sendServerMessage(ServerMessage.singleArgMessage(ServerMessage.ARGC1_MESSAGE.RESET_VOTE, name, ServerMessage.COLOR_TYPE.EMPHASIZE));
            if (resetCountDownTimer == null) {
                resetCountDownTimer = new Timer();
                resetCountDownTimer.schedule(new ResetCountDown(10), 0, 1000);
                System.out.println("scheduled");
            }
        }
    }

    private Timer resetCountDownTimer = null;

    public void cancelReset(String name) {
        if (resetCountDownTimer != null) {
            resetCountDownTimer.cancel();
            resetCountDownTimer = null;
            sendServerMessage(ServerMessage.singleArgMessage(ServerMessage.ARGC1_MESSAGE.RESET_CANCEL, name, ServerMessage.COLOR_TYPE.EMPHASIZE));
        }
    }

    private class ResetCountDown extends TimerTask {

        private int count;

        public ResetCountDown(int count) {
            this.count = count;
        }

        @Override
        public void run() {
            if (count > 0) {
                sendServerMessage(ServerMessage.singleArgMessage(ServerMessage.ARGC1_MESSAGE.RESET_COUNTDOWN, count+"", ServerMessage.COLOR_TYPE.EMPHASIZE));
                count--;
            } else {
                resetGame();
                this.cancel();
            }
        }
    }
    
    public void toggleGameOption(int option) {
        switch (option) {
            case Message.TOGGLE_EXGENE:
                _options = new GameOption(!_options.allowExtras, _options.sfsStrict, _options.SFSESCHOLisSuccess, _options.canBANGwithSFS, _options.par);
                break;
            case Message.TOGGLE_SFS:
                _options = new GameOption(_options.allowExtras, !_options.sfsStrict, _options.SFSESCHOLisSuccess, _options.canBANGwithSFS, _options.par);
                break;
            case Message.TOGGLE_HOL:
                _options = new GameOption(_options.allowExtras, _options.sfsStrict, !_options.SFSESCHOLisSuccess, _options.canBANGwithSFS, _options.par);
                break;
            case Message.TOGGLE_BANG:
                _options = new GameOption(_options.allowExtras, _options.sfsStrict, _options.SFSESCHOLisSuccess, !_options.canBANGwithSFS, _options.par);
                break;
            case Message.TOGGLE_PAR:
                int nextOrd = (_options.par.ordinal() + 1) % 4;
                _options = new GameOption(_options.allowExtras, _options.sfsStrict, _options.SFSESCHOLisSuccess, _options.canBANGwithSFS, GameOption.PARASITISM.values()[nextOrd]);
                break;
            default:
                UrsuppeApp.dump("Invalid toggle game option:" + option);
        }
        sendServerMessage(ServerMessage.zeroArgMessage(ServerMessage.ARGC0_MESSAGE.OPTION_CHANGED, ServerMessage.COLOR_TYPE.EMPHASIZE));
        broadcast(new Message(Message.COMMAND_SETUP, Message.OPTION_CHANGE_NOTIFICATION, _options, option));
       
    }


    public void broadcastCustomIcons(int id) {
        if (_customIcons.containsKey(id)) {
            broadcast(new Message(Message.COMMAND_SETUP, Message.CUSTOM_ICONS, new Object[]{id, _customIcons.get(id), _customIcons}));
        }

    }
    public void broadcastCustomIcons() {
        for (int id : _customIcons.keySet()) {
            broadcastCustomIcons(id);
        }
    }

    public void sendCustomIcons(IoSession session) {
        for (int id : _customIcons.keySet()) {
            _server.sendMessage(session, new Message(Message.COMMAND_SETUP, Message.CUSTOM_ICONS, new Object[]{id, _customIcons.get(id), _customIcons}));
        }
    }

    public boolean isRunning() {
        return _server.isActive();
    }

    public int getPort() {
        return _port;
    }


    public void addAI() {
        if (_game.getProgress().started) return;
        
        if (_AIcontainer != null) {
            if (_game.getPlayers().size() <= 5) {
                _AIcontainer.addAI(getPort());
            }
        }
    }

    public void removeAI() {
        if (_game.getProgress().started) return;
        
        if (_AIcontainer != null) {
            _AIcontainer.removeAI();
        }
    }


    public int getAIcount() {
        if (_AIcontainer != null) {
            return _AIcontainer.getCount();
        }
        return 0;
    }

    public void cleanupConnection(String name) {
        if (_connectedNames.containsValue(name)) {
            IoSession target = null;
            for (IoSession key : _connectedNames.keySet()) {
                if (_connectedNames.get(key).contentEquals(name)) {
                    target = key;
                    break;
                }
            }
            if (target != null) {
                _connectedNames.remove(target);
                _activePlayers.remove(target);
            }
        }
    }

    private void messageHandling(IoSession session, Message msg) {
        if (msg.getMessageType() != Message.CHECKALIVE)  UrsuppeApp.debug("server:"+msg.toString());
        switch (msg.getCommandType()) {
            case Message.COMMAND_SETUP:
                setupMessageHandling(session, msg);
                break;
            case Message.COMMAND_CHAT:
                int colorID = -1;
                if (_activePlayers.get(session)!=null) {
                    colorID = _game.getPlayerByID((Integer) _activePlayers.get(session)).getColorID();
                }
                broadcast(new Message(Message.COMMAND_CHAT, Message.USER_MESSAGE, new Object[]{_connectedNames.get(session) + ":" + msg.getItems()[0], colorID}));
                break;
            case Message.COMMAND_GAME:
                gameMessageHandling(session, msg);
                break;
            case Message.COMMAND_ERROR:
                break;
            case Message.COMMAND_SERVER:
                serverCommandHandling(session, msg);
                break;
            default:
                JOptionPane.showMessageDialog(null, UrsuppeResource.getString("error.UnhandledCommandType") + ":" + msg.getCommandType());
                break;
        }
    }

    private synchronized void setupMessageHandling(IoSession session, Message msg) {
        switch (msg.getMessageType()) {
            //Connection Request
            case Message.CONNECT:
                boolean ack = false;
                String name = (String)msg.getItems()[0];
                //Check if name exists
                synchronized(this) {
                    if (!_connectedNames.containsValue(name)) {
                        ack = true;
                        _connectedNames.put(session, name);

                    }
                }
                //Send the response back
                _server.sendMessage(session, new Message(Message.COMMAND_SETUP, Message.CONNECT, ack));
                //If the connection was accepted
                if (ack) {
                    sendServerMessage(ServerMessage.singleArgMessage(ServerMessage.ARGC1_MESSAGE.CONNECTED, name, ServerMessage.COLOR_TYPE.DEFAULT));
                    //send the current state of the game
                    _server.sendMessage(session, new Message(Message.COMMAND_GAME, Message.ENTIRE_GAME_STATE, _game));
                } else {
                    session.close(false);
                }
                //give the custom icons
                sendCustomIcons(session);

               //Send the new game state
                broadcast(new Message(Message.COMMAND_GAME, Message.ENTIRE_GAME_STATE, _game));

               break;
            case Message.JOIN:
                try {
                    //Attempt to add a player
                    int preferredColor1 = (Integer)msg.getItems()[2];
                    int preferredColor2 = (Integer)msg.getItems()[3];
                    int id = _game.addPlayer(_connectedNames.get(session), preferredColor1, preferredColor2);
                    CustomIconSet customIcon = (CustomIconSet)msg.getItems()[1];
                    
                    //If successful, broad cast the result
                    sendServerMessage(ServerMessage.singleArgMessage(ServerMessage.ARGC1_MESSAGE.JOINED, _connectedNames.get(session), ServerMessage.COLOR_TYPE.DEFAULT));
                    //send the player ID
                    _server.sendMessage(session, new Message(Message.COMMAND_SETUP, Message.JOIN, id));
                    _activePlayers.put(session, id);
                    if (customIcon != null) _customIcons.put(id, customIcon);
                    //Send the new icons
                    if (customIcon != null) {
                        broadcastCustomIcons(id);
                    }
                } catch (GameInProgressException gip) {
                    _server.sendMessage(session, new Message(Message.COMMAND_ERROR, Message.ERROR_DIALOG, UrsuppeResource.getString("error.GameInProgress")));
                } catch (TooManyPlayersException ge) {
                    _server.sendMessage(session, new Message(Message.COMMAND_ERROR, Message.ERROR_DIALOG, UrsuppeResource.getString("error.TooManyPlayers")));
                }
                //Send the new game state
                broadcast(new Message(Message.COMMAND_GAME, Message.ENTIRE_GAME_STATE, _game));
                break;
            case Message.LEAVE:
                 int leaveID = (Integer)msg.getItems()[0];
                 _activePlayers.remove(session);
                sendServerMessage(_game.removePlayer(leaveID));
                broadcast(new Message(Message.COMMAND_GAME, Message.ENTIRE_GAME_STATE, _game));
                break;
            case Message.DISCONNECT:
                 disconnected(session);

                 int id = (Integer)msg.getItems()[0];

                 if (!_game.getProgress().started) {
                    sendServerMessage(_game.removePlayer(id));
                 }
                broadcast(new Message(Message.COMMAND_GAME, Message.ENTIRE_GAME_STATE, _game));
                break;
            case Message.CHECKALIVE:
                break;
            default:
                JOptionPane.showMessageDialog(null, UrsuppeResource.getString("error.UnhandledServerMessageType") + ":" + msg.getMessageType());
                break;
        }
    }

    private synchronized void gameMessageHandling(IoSession session, Message msg) {

        if (!_game.isStarted()) {
            return;
        }

        int id = (Integer)msg.getItems()[0];
        int[] TENfoods;
        boolean useREB;
        
        switch (msg.getMessageType()) {
            case Message.INITIAL_SCORE:
                int iScore = (Integer)msg.getItems()[1];
                sendGameMessage(_game.setInitialScore(id, iScore));
                break;
            case Message.INITIAL_AMOEBA:
                int iaNum = (Integer)msg.getItems()[1];
                Point iaLoc = (Point)msg.getItems()[2];
                sendGameMessage(_game.addInitialAmoeba(id, iaNum, iaLoc));
                break;
            case Message.DRIFT:
                TENfoods = (int[])msg.getItems()[1];
                useREB = (Boolean)msg.getItems()[2];
                sendGameMessage(_game.amoebaDrifted(id, TENfoods, useREB));
                break;
            case Message.MOVE_REQ:
                sendGameMessage(_game.moveRequested(id));
                break;
            case Message.MOVE:
                DIRECTION dir = (DIRECTION)msg.getItems()[1];
                TENfoods = (int[])msg.getItems()[2];
                useREB = (Boolean)msg.getItems()[3];
                sendGameMessage(_game.amoebaMoved(id, dir, TENfoods, useREB));
                break;
            case Message.EAT:
                Point exLoc = (Point)msg.getItems()[1];
                boolean tox = (Boolean)msg.getItems()[2];
                sendGameMessage(_game.eat(id, exLoc, tox));
                break;
            case Message.EAT_WITH_GENES:
                int[] foods = (int[])msg.getItems()[1];
                int[] parPlayerIDs = (int[])msg.getItems()[2];
                Point sucLoc = (Point)msg.getItems()[3];
                int[] sucFoods = (int[])msg.getItems()[4];
                Point exLocG = (Point)msg.getItems()[5];
                boolean toxG = (Boolean)msg.getItems()[6];
                sendGameMessage(_game.eat(id, foods, parPlayerIDs, sucLoc, sucFoods, exLocG, toxG));
                break;
            case Message.STARVE:
                sendGameMessage(_game.amoebaStarved(id));
                break;
            case Message.HOLD_STAY:
                sendGameMessage(_game.HOLDSelected(id));
                break;
            case Message.HOLD:
                boolean useHOL = (Boolean)msg.getItems()[1];
                TENfoods = (int[])msg.getItems()[2];
                sendGameMessage(_game.HOLDSelected(id, useHOL, TENfoods));
                break;
            case Message.CANCEL_SPEED:
                sendGameMessage(_game.cancelSpeed(id));
                break;
            case Message.GENE_DEFECT:
                GENE[] discards = (GENE[])msg.getItems()[1];
                sendGameMessage(_game.geneDefectHandled(id, discards));
                break;
            case Message.GENE_PURCHASE:
                GENE gene = (GENE)msg.getItems()[1];
                sendGameMessage(_game.genePurchase(id, gene));
                break;
            case Message.ADA:
                GENE exchange = (GENE)msg.getItems()[1];
                GENE newGene = (GENE)msg.getItems()[2];
                sendGameMessage(_game.ADAexchange(id, exchange, newGene));
                break;
            case Message.END_GENE_PURCHASE:
                sendGameMessage(_game.endGenePurchase(id));
                break;
            case Message.CELL_DIVISION:
                int cdNum = (Integer)msg.getItems()[1];
                Point cdLoc = (Point)msg.getItems()[2];
                sendGameMessage(_game.cellDivision(id, cdNum, cdLoc));
                break;
            case Message.END_CELL_DIVISION:
                sendGameMessage(_game.endCellDivision(id));
                break;
            case Message.POP_DECISION:
                boolean usePOP = (Boolean)msg.getItems()[1];
                sendGameMessage(_game.POPdecision(id, usePOP));
                break;
            case Message.HEAL:
                Amoeba heal = (Amoeba)msg.getItems()[1];
                Amoeba transfer = (Amoeba)msg.getItems()[2];
                sendGameMessage(_game.heal(id, heal, transfer));
                break;
            case Message.DEATH_ACK:
                boolean useBANG = (Boolean)msg.getItems()[1];
                sendGameMessage(_game.deathAck(id, useBANG));
                break;
            case Message.SCORE_ACK:
                int ackedScore = (Integer)msg.getItems()[1];
                List<GameMessage> res = _game.scoreAck(id, ackedScore);
                if (res.isEmpty()) return;
                sendGameMessage(res);
                break;
            case Message.SFS_ATTACK:
               int sfsTargetPlayerID = (Integer)msg.getItems()[1];
               int sfsTargetAmoebaNum= (Integer)msg.getItems()[2];
            //_client.write(new Message(Message.COMMAND_GAME, Message.SFS_ATTACK, new Object[]{_playerID, targetPlayerID, targetAmoebaNum}));
                sendGameMessage(_game.SFSSelected(id, sfsTargetPlayerID, sfsTargetAmoebaNum));
                break;
            case Message.AGR_CANCEL:
                sendGameMessage(_game.AGRcanceled(id));
                break;
            case Message.AGR_ATTACK:
               int agrAttackerAmoebaNum = (Integer)msg.getItems()[1];
               int agrTargetPlayerID = (Integer)msg.getItems()[2];
               int agrTargetAmoebaNum= (Integer)msg.getItems()[3];
                sendGameMessage(_game.AGRSelected(id, agrAttackerAmoebaNum, agrTargetPlayerID, agrTargetAmoebaNum));
                break;
            case Message.DEFENDER_GIVEUP:
                sendGameMessage(_game.defenderGiveUp(id));
                break;
            case Message.ATTACKER_ACTION:
                boolean payHC = (Boolean)msg.getItems()[1];
                Point atkExLoc = (Point)msg.getItems()[2];
                boolean atkUseTOX = (Boolean)msg.getItems()[3];
                sendGameMessage(_game.attackerActionSelected(id, payHC, atkExLoc, atkUseTOX));
                break;
            case Message.DEF_USE:
                sendGameMessage(_game.DEFUse(id));
                break;
            case Message.DEF_ACK:
                sendGameMessage(_game.DEFAck(id));
                break;
            case Message.ESC_USE:
                sendGameMessage(_game.ESCUse(id));
                break;
            case Message.ESCAPE:
                DIRECTION ESCdir = (DIRECTION)msg.getItems()[1];
                TENfoods = (int[])msg.getItems()[2];
                useREB = (Boolean)msg.getItems()[3];
                sendGameMessage(_game.ESCDirectionSelected(id, ESCdir, TENfoods, useREB));
                break;
            case Message.ESC_ACK:
                sendGameMessage(_game.ESCAck(id));
                break;
            case Message.SFS_ACK:
                boolean useBAN = (Boolean)msg.getItems()[1];
                sendGameMessage(_game.SFSAck(id, useBAN, null, false));
                break;
            default:
                JOptionPane.showMessageDialog(null, UrsuppeResource.getString("error.UnhandledServerMessageType") + ":" + msg.getMessageType());
                break;
        }

        _game.populateGameStatistics();
        broadcast(new Message(Message.COMMAND_GAME, Message.ENTIRE_GAME_STATE, _game));

        if (_game.getProgress().ended) {
            for (Player player : _game.getPlayers()) {
                sendGameMessage(new GameTextMessage(GameTextMessage.MESSAGE.FINAL_SCORE, new String[] {player.getName(), player.getAdvance()+""}, GameTextMessage.COLOR_TYPE.PLAYER, player.getID()));
            }

            if (_replay != null)
                _replay.save();
        }
    }
    private void serverCommandHandling(IoSession session, Message msg) {
        if (!_acceptCommand) return;

        //must be player or end of game
        if (!_activePlayers.containsKey(session) && (_game.getProgress().started && !_game.getProgress().ended)) return;

        if (msg.getMessageType() == Message.FETCH_REPLAY && _game.getProgress().ended && _replay != null) {
            Message rep = new Message(Message.COMMAND_SERVER, Message.FETCHED_REPLAY, _replay);
            session.write(rep);
        } else {
            //get connected name from session and then broadcast
            Message bcast = new Message(msg.getCommandType(), msg.getMessageType(), _connectedNames.get(session));

            // broadcast so that organizer of the server actually issues these commands
            // causes dead lock otherwise
            broadcast(bcast);
        }
    }

    private synchronized void broadcast(Message msg) {
        if (_server != null) {
            for (IoSession session : _server.getSessions().values()) {
                _server.sendMessage(session, msg);
            }
            //keep log for replay
            if (_replay != null && _log != null) {
                if (msg.getMessageType() == Message.ENTIRE_GAME_STATE ||
                        msg.getMessageType() == Message.SERVER_MESSAGE ||
                        msg.getMessageType() == Message.GAME_TEXT_MESSAGE ||
                        msg.getMessageType() == Message.GAME_EVENT_MESSAGE ||
                        msg.getMessageType() == Message.USER_MESSAGE) {
                    _replay.addMessage(msg);
                    _log.add(msg);
                }
            }
        }

    }

    private void sendGameMessage(List<GameMessage> msgs) {
        if (msgs == null) return;
        for (GameMessage msg : msgs) {
            sendGameMessage(msg);
        }
    }

    private void sendGameMessage(GameMessage message) {
        if (message != null) {
            if (message instanceof GameTextMessage) {
                broadcast(new Message(Message.COMMAND_GAME, Message.GAME_TEXT_MESSAGE, message));
            } else if (message instanceof GameEventMessage) {
                broadcast(new Message(Message.COMMAND_GAME, Message.GAME_EVENT_MESSAGE, message));
            }
        }
    }
    
    private void sendServerMessage(ServerMessage message) {
        if (message != null) {
            SimpleAttributeSet attr = null;
            switch (message.colorType) {
                case DEFAULT:
                    broadcast(new Message(Message.COMMAND_CHAT, Message.SERVER_MESSAGE, new Object[]{message.string, null}));
                    break;
                case EMPHASIZE:
                    attr = new SimpleAttributeSet();
                    StyleConstants.setBold(attr, true);
                    broadcast(new Message(Message.COMMAND_CHAT, Message.SERVER_MESSAGE, new Object[]{message.string, attr}));
                    break;
                case ERROR:
                    attr = new SimpleAttributeSet();
                    StyleConstants.setBold(attr, true);
                    StyleConstants.setForeground(attr, Color.RED);
                    broadcast(new Message(Message.COMMAND_CHAT, Message.SERVER_MESSAGE, new Object[]{message.string, attr}));
                    break;
            }
        }
    }
}
