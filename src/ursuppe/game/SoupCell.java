/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.game;

import java.awt.Color;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Personal
 */
public class SoupCell implements Serializable {

    public static final long serialVersionUID = 13481578349L;

    private int[] _foods;
    private List<Amoeba> _amoebas = new LinkedList<>();
    private int _DP;

    public void initialize(int num) {
        _foods = new int[num];

        for (int i = 0; i < _foods.length; i++) {
            _foods[i] = GameState.INITIAL_FOODS;
        }
    }

    public void addFood(int[] add) {
        if (add.length != _foods.length) return;

         for (int i = 0; i < _foods.length; i++) {
             _foods[i] += add[i];
         }
    }

    public void takeFood(int[] sub) {
        if (sub.length != _foods.length) return;

         for (int i = 0; i < _foods.length; i++) {
             _foods[i] -= sub[i];
         }
    }

    public void addDP(int DP) {
        _DP += DP;
    }

    public int getDP() {
        return _DP;
    }

    public int[] getFood() {
        return _foods;
    }

    public int getTotalFoodCount() {
        int sum = 0;
        for (int f : _foods) sum+= f;

        return sum;
    }

    public void addAmoeba(Amoeba amoeba) {
        _amoebas.add(amoeba);
    }

    public void removeAmoeba(Amoeba amoeba) {
        _amoebas.remove(amoeba);
    }

    public List<Amoeba> getAmoebas() {
        return _amoebas;
    }

    public boolean hasAmoebaOfColor(int colorID) {
        for (Amoeba amoeba : _amoebas) {
            if (amoeba.colorID == colorID) return true;
        }

        return false;
    }
}
