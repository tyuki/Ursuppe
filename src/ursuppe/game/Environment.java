package ursuppe.game;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import ursuppe.game.GameState.DIRECTION;

public class Environment implements Serializable {


    public static final long serialVersionUID = 64813971238L;

    private final List<EnvironmentCard> _environment;

    public Environment() {
        _environment = getInitialList();

        Collections.shuffle(_environment);
    }

    public static List<EnvironmentCard> getInitialList() {
        LinkedList<EnvironmentCard> list = new LinkedList<>();
        list.add(new EnvironmentCard(6,  DIRECTION.WEST, 0));
        list.add(new EnvironmentCard(8,  DIRECTION.EAST, 1));
        list.add(new EnvironmentCard(9,  DIRECTION.WEST, 2));
        list.add(new EnvironmentCard(9,  DIRECTION.NORTH, 3));
        list.add(new EnvironmentCard(10, DIRECTION.EAST, 4));
        list.add(new EnvironmentCard(10, DIRECTION.EAST, 5));
        list.add(new EnvironmentCard(10, DIRECTION.SOUTH, 6));
        list.add(new EnvironmentCard(11, DIRECTION.NORTH, 7));
        list.add(new EnvironmentCard(12, DIRECTION.WEST, 8));
        list.add(new EnvironmentCard(13, DIRECTION.SOUTH, 9));
        list.add(new EnvironmentCard(14, DIRECTION.NONE, 10));
        return list;
    }

    public EnvironmentCard pop() {
        EnvironmentCard ret = _environment.get(0);
        _environment.remove(0);

        return ret;
    }

    public EnvironmentCard peek(int i) {
        if (i >= _environment.size()) return null;

        return _environment.get(i);
    }

    public List<EnvironmentCard> getList() {
        return Collections.unmodifiableList(_environment);
    }
}
