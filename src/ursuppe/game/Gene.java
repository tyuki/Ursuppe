/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.game;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import ursuppe.UrsuppeResource;
import ursuppe.game.GenePool.GENE;

/**
 *
 * @author Personal
 */
public class Gene implements Serializable {


    public static final long serialVersionUID = 478137486L;


    public final GENE gene;
    public final String name;
    private int _num;
    public final int max;
    private final int _BPcost;
    private final int _MPcost;
    private final List<GENE> _predecessors;
    public final GENE successor;

    public Gene(GENE gene, GameOption option, int numPlayers) {
        this.gene = gene;
        this.name = GenePool.toName(gene);

        int num = 0;
        
        int extraNum = 0;
        if (numPlayers > 4 || option.allowExtras) {
            extraNum++;
        }

        switch (gene) {
            case INT:
                if (numPlayers == 3) num = 1;
                if (numPlayers == 4) num = 2;
                if (numPlayers >= 5) num = 3;
                max = _num = num;
                _BPcost = 2;
                _MPcost = 3;
                break;
            case MV1:
                if (numPlayers == 3) num = 2;
                if (numPlayers == 4) num = 2;
                if (numPlayers >= 5) num = 3;
                max = _num = num;
                _BPcost = 3;
                _MPcost = 2;
                break;
            case SPO:
                if (numPlayers == 3) num = 1;
                if (numPlayers == 4) num = 1;
                if (numPlayers >= 5) num = 2;
                max = _num = num;
                _BPcost = 3;
                _MPcost = 3;
                break;
            case SPD:
                if (numPlayers == 3) num = 1;
                if (numPlayers == 4) num = 2;
                if (numPlayers >= 5) num = 2;
                max = _num = num;
                _BPcost = 4;
                _MPcost = 3;
                break;
            case DEF:
                if (numPlayers == 3) num = 1;
                if (numPlayers == 4) num = 1;
                if (numPlayers >= 5) num = 2;
                max = _num = num;
                _BPcost = 4;
                _MPcost = 4;
                break;
            case ESC:
                if (numPlayers == 3) num = 1;
                if (numPlayers == 4) num = 2;
                if (numPlayers >= 5) num = 2;
                max = _num = num;
                _BPcost = 4;
                _MPcost = 4;
                break;
            case SUB:
                if (numPlayers == 3) num = 1;
                if (numPlayers == 4) num = 1;
                if (numPlayers >= 5) num = 2;
                max = _num = num;
                _BPcost = 4;
                _MPcost = 4;
                break;
            case RAY:
                if (numPlayers == 3) num = 1;
                if (numPlayers == 4) num = 2;
                if (numPlayers >= 5) num = 2;
                max = _num = num;
                _BPcost = 5;
                _MPcost = -2;
                break;
            case STR:
                if (numPlayers == 3) num = 1;
                if (numPlayers == 4) num = 1;
                if (numPlayers >= 5) num = 2;
                max = _num = num;
                _BPcost = 5;
                if (numPlayers >= 5) {
                    _MPcost = 5;
                } else {
                    _MPcost = 4;
                }
                break;
            case TEN:
                if (numPlayers == 3) num = 1;
                if (numPlayers == 4) num = 2;
                if (numPlayers >= 5) num = 2;
                max = _num = num;
                _BPcost = 5;
                _MPcost = 4;
                break;
            case HOL:
                if (numPlayers == 3) num = 1;
                if (numPlayers == 4) num = 1;
                if (numPlayers >= 5) num = 1;
                max = _num = num;
                _BPcost = 5;
                _MPcost = 4;
                break;
            case LIF:
                if (numPlayers == 3) num = 1;
                if (numPlayers == 4) num = 2;
                if (numPlayers >= 5) num = 2;
                max = _num = num;
                _BPcost = 5;
                _MPcost = 5;
                break;
            case FRU:
                if (numPlayers == 3) num = 0;
                if (numPlayers == 4) num = 1;
                if (numPlayers >= 5) num = 2;
                max = _num = num;
                _MPcost = 5;
                if (numPlayers >= 5) {
                    _BPcost = 5;
                } else {
                    _BPcost = 6;
                }
                break;
            case SFS:
                if (numPlayers == 3) num = 2;
                if (numPlayers == 4) num = 2;
                if (numPlayers >= 5) num = 3;
                max = _num = num;
                _BPcost = 6;
                if (numPlayers >= 5) {
                    _MPcost = 5;
                } else {
                    _MPcost = 4;
                }
                break;
            case PAR:
                if (numPlayers == 3) num = 0;
                if (numPlayers == 4) num = 1;
                if (numPlayers >= 5) num = 2;
                max = _num = num;
                _BPcost = 6;
                _MPcost = 5;
                break;
            case DIV:
                if (numPlayers == 3) num = 1;
                if (numPlayers == 4) num = 2;
                if (numPlayers >= 5) num = 2;
                max = _num = num;
                _BPcost = 6;
                _MPcost = 5;
                break;
            case PER:
                if (numPlayers == 3) num = 1;
                if (numPlayers == 4) num = 1;
                if (numPlayers >= 5) num = 1;
                max = _num = num;
                _BPcost = 4;
                _MPcost = 4;
                break;
            case MV2:
                if (numPlayers == 3) num = 1;
                if (numPlayers == 4) num = 2;
                if (numPlayers >= 5) num = 2;
                max = _num = num;
                _BPcost = 5;
                _MPcost = 5;
                break;
            case AGR:
                if (numPlayers == 3) num = 1;
                if (numPlayers == 4) num = 1;
                if (numPlayers >= 5) num = 1;
                max = _num = num;
                _BPcost = 5;
                _MPcost = 5;
                break;
            case ARM:
                if (numPlayers == 3) num = 1;
                if (numPlayers == 4) num = 1;
                if (numPlayers >= 5) num = 1;
                max = _num = num;
                _BPcost = 6;
                _MPcost = 6;
                break;
            case DandF:
                max = _num = extraNum;
                _BPcost = 2;
                _MPcost = 3;
                break;
            case GLU:
                max = _num = extraNum;
                _BPcost = 3;
                _MPcost = -2;
                break;
            case MIG:
                max = _num = extraNum;
                _BPcost = 3;
                _MPcost = 3;
                break;
            case HARD:
                max = _num = extraNum;
                _BPcost = 3;
                _MPcost = 3;
                break;
            case ALM:
                max = _num = extraNum;
                _BPcost = 3;
                _MPcost = 3;
                break;
            case CLE:
                max = _num = extraNum;
                _BPcost = 3;
                _MPcost = 3;
                break;
            case SOC:
                max = _num = extraNum;
                _BPcost = 4;
                _MPcost = 3;
                break;
            case FAST:
                max = _num = extraNum;
                _BPcost = 4;
                _MPcost = 3;
                break;
            case MOU:
                max = _num = extraNum;
                _BPcost = 4;
                _MPcost = 3;
                break;
            case ADA:
                max = _num = extraNum;
                _BPcost = 4;
                _MPcost = 3;
                break;
            case SUC1:
                max = _num = extraNum;
                _BPcost = 4;
                _MPcost = 4;
                break;
            case DIS:
                max = _num = extraNum;
                _BPcost = 4;
                _MPcost = 4;
                break;
            case CAM1:
                max = _num = extraNum;
                _BPcost = 4;
                _MPcost = 4;
                break;
            case HEA1:
                max = _num = extraNum;
                _BPcost = 5;
                _MPcost = 4;
                break;
            case FLEX:
                max = _num = extraNum;
                _BPcost = 5;
                _MPcost = 4;
                break;
            case SLS:
                max = _num = extraNum;
                _BPcost = 5;
                _MPcost = 5;
                break;
            case TOX:
                max = _num = extraNum;
                _BPcost = 5;
                _MPcost = 5;
                break;
            case BANG:
                max = _num = extraNum;
                _BPcost = 5;
                _MPcost = 5;
                break;
            case REB:
                max = _num = extraNum;
                _BPcost = 5;
                _MPcost = 4;
                break;
            case POP:
                max = _num = extraNum;
                _BPcost = 6;
                _MPcost = 5;
                break;
            case ENE:
                max = _num = extraNum;
                _BPcost = 6;
                _MPcost = 6;
                break;
            case PHD:
                max = _num = extraNum;
                _BPcost = 3;
                _MPcost = 4;
                break;
            case ENV:
                max = _num = extraNum;
                _BPcost = 4;
                _MPcost = 3;
                break;
            case CAM2:
                max = _num = extraNum;
                _BPcost = 5;
                _MPcost = 4;
                break;
            case HEA2:
                max = _num = extraNum;
                _BPcost = 5;
                _MPcost = 5;
                break;
            case SUC2:
                max = _num = extraNum;
                _BPcost = 6;
                _MPcost = 6;
                break;
            default:
                max = _num = 0;
                _BPcost = 0;
                _MPcost = 0;
                break;
        }

        //predecessor
        switch (gene) {
            case PER:
                _predecessors = new LinkedList<GENE>(){{add(GENE.SPD);}};
                break;
            case MV2:
                _predecessors = new LinkedList<GENE>(){{add(GENE.MV1);}};
                break;
            case AGR:
                _predecessors = new LinkedList<GENE>(){{add(GENE.SFS);}};
                break;
            case ARM:
                _predecessors = new LinkedList<GENE>(){{add(GENE.ESC); add(GENE.DEF);}};
                break;
            case PHD:
                _predecessors = new LinkedList<GENE>(){{add(GENE.INT);}};
                break;
            case ENV:
                _predecessors = new LinkedList<GENE>(){{add(GENE.MIG);}};
                break;
            case CAM2:
                _predecessors = new LinkedList<GENE>(){{add(GENE.CAM1);}};
                break;
            case HEA2:
                _predecessors = new LinkedList<GENE>(){{add(GENE.HEA1);}};
                break;
            case SUC2:
                _predecessors = new LinkedList<GENE>(){{add(GENE.SUC1);}};
                break;
            default:
                _predecessors = new LinkedList<>();
                break;
        }

        //successor
        switch (gene) {
            case INT:
                successor = GENE.PHD;
                break;
            case MV1:
                successor = GENE.MV2;
                break;
            case SPD:
                successor = GENE.PER;
                break;
            case DEF:
                successor = GENE.ARM;
                break;
            case ESC:
                successor = GENE.ARM;
                break;
            case SFS:
                successor = GENE.AGR;
                break;
            case MIG:
                successor = GENE.ENV;
                break;
            case CAM1:
                successor = GENE.CAM2;
                break;
            case SUC1:
                successor = GENE.SUC2;
                break;
            case HEA1:
                successor = GENE.HEA2;
                break;

            default:
                successor = null;
                break;
        }
    }

    public int getAvailableNumber() {
        return _num;
    }

    public int getBPCost() {
        return _BPcost;
    }

    public int getMPCost() {
        return _MPcost;
    }

    public int getMPReleaseValue() {

        if (gene == GENE.RAY) return 4;
        if (gene == GENE.SLS) return 4;

        return _MPcost;
    }

    public int getCardScoreCount() {

        if (gene == GENE.RAY) return 0;
        if (_predecessors.size() > 0) return 2;

        return 1;
    }

    public List<GENE> getPredecessors() {
        return _predecessors;
    }

    public void geneBought() {
        _num--;
    }

    public void geneReleased() {
        _num++;
    }

    public String toDesciptionString() {
         StringBuffer desc = new StringBuffer("<B>" + UrsuppeResource.getString("gene."+name+".name") + "</B>");
        desc.append("<BR>");
        desc.append(UrsuppeResource.getString("term.BP") + ":" + getBPCost() + " " + UrsuppeResource.getString("term.MP") + ":"  + getMPCost());
        desc.append("<BR>");
        desc.append(UrsuppeResource.getString("term.gene.availability") + ":" + getAvailableNumber() + " (" + max + ")");
        desc.append("<BR>");
        desc.append("<BR>");
        desc.append("<div style='font-size: "+UrsuppeResource.getString("description.fontSize")+"pt; font-family: "+UrsuppeResource.getString("description.fontFamily")+"'>");
        desc.append(UrsuppeResource.getString("gene."+name+".description") + "</div>");

        return desc.toString();
    }
}
