/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.game;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Calendar;
import network.Message;
import ursuppe.UrsuppeResource;

/**
 *
 * @author Personal
 */
public class Log {

    public static final String LOG_PATH = "./log/";
    public static final String LOG_EXT = ".txt";

    public final String logName;
    private PrintStream printer;

    public Log(GameOption options) {
       Calendar cal = Calendar.getInstance();
       String tempName = String.format("ursuppe-%d-%02d-%02d-%02d%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));

        {
        String postFix = "";
            int attempt = 0;
            while (true && attempt < 10) {
                File file = new File(LOG_PATH + tempName + postFix + LOG_EXT);
                if (!file.exists()) {
                    if (!file.getParentFile().exists()) {
                        file.getParentFile().mkdir();
                    }
                    try {
                        file.createNewFile();
                    } catch (IOException ex) {
                        ursuppe.UrsuppeApp.debug(ex);
                    }
                } else {
                    attempt++;
                    postFix = "-" + attempt;
                    continue;
                }
                break;
            }
            logName = tempName + postFix;
        }
        File file = new File(LOG_PATH + logName + LOG_EXT);
        try {
            printer = new PrintStream(file);
        } catch (FileNotFoundException ex) {
            ursuppe.UrsuppeApp.debug(ex);
            printer = null;
        }

        //version
        print(UrsuppeResource.getString("title") + " " + UrsuppeResource.getString("gameVersion"), false);
        //option
        print(options, false);

    }

    public void add(Message message) {
        if (printer != null) {
            if (message.getMessageType() == Message.SERVER_MESSAGE ||
                message.getMessageType() == Message.GAME_TEXT_MESSAGE ||
                message.getMessageType() == Message.USER_MESSAGE) {
                    try {
                        print(message.getItems()[0], true);
                    } catch (Exception e) {}
            }
        }
    }

    private void print(Object obj, boolean addTimeStamp) {
        if (printer != null && obj != null) {
            try {
                if (addTimeStamp) {
                    Calendar cal = Calendar.getInstance();
                    String timeStamp = String.format("[%02d:%02d] ", cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));

                    printer.println(timeStamp + obj.toString());
                } else {
                    printer.println(obj.toString());
                }
                printer.flush();
            } catch (Exception e) {}
        }
    }

    public void close() {
        printer.close();
        printer = null;
    }

}
