package ursuppe.game;

import java.io.Serializable;
import network.Message;
import ursuppe.UrsuppeResource;

/**
 *
 * @author Personal
 */
public class GameOption implements Serializable {


    public static final long serialVersionUID = 79185186L;

    public static enum PARASITISM {
        ONLY_ONCE, DIFFERENT_AMOEBA, DIFFERENT_PLAYER, SAME_AMOEBA_TWICE
    }
    
    public final PARASITISM par;
    public final boolean allowExtras;
    public final boolean sfsStrict;
    public final boolean SFSESCHOLisSuccess;
    public final boolean canBANGwithSFS;

    public GameOption(boolean extras, boolean strictSFS, boolean SFSESCHOL, boolean BANGSFS, PARASITISM par) {
        this.par = par;
        allowExtras = extras;
        sfsStrict = strictSFS;
        SFSESCHOLisSuccess = SFSESCHOL;
        canBANGwithSFS = BANGSFS;
    }
    
    public String toShortString(int option) {
        StringBuilder sb = new StringBuilder();
        
        if (option == Message.TOGGLE_EXGENE) {
            sb.append(UrsuppeResource.getString("options.short.exGene"));
            sb.append(": ");
            if (allowExtras) {
                sb.append(UrsuppeResource.getString("extraGenesYesRB.text"));
            } else {
                sb.append( UrsuppeResource.getString("extraGenesNoRB.text"));
            }
        }
        
        if (option == Message.TOGGLE_SFS) {
            sb.append(UrsuppeResource.getString("options.short.SFS"));
            sb.append(": ");
            if (sfsStrict) {
                sb.append(UrsuppeResource.getString("sfsStrictRB.text"));
            } else {
                sb.append(UrsuppeResource.getString("sfsLooseRB.text"));
            }
        }
        
        if (option == Message.TOGGLE_HOL) {
            sb.append(UrsuppeResource.getString("options.short.HOL"));
            sb.append(": ");
            if (SFSESCHOLisSuccess) {
                sb.append(UrsuppeResource.getString("sfsHOLsuccessRButton.text"));
            } else {
                sb.append(UrsuppeResource.getString("sfsHOLfailRButton.text"));
            }
        }
        
        if (option == Message.TOGGLE_BANG) {
            sb.append(UrsuppeResource.getString("options.short.BANG"));
            sb.append(": ");
            if (canBANGwithSFS) {
                sb.append(UrsuppeResource.getString("sfsBANGyesRButton.text"));
            } else {
                sb.append(UrsuppeResource.getString("sfsBANGnoRButton.text"));
            }
        }
        
        if (option == Message.TOGGLE_PAR) {
            sb.append(UrsuppeResource.getString("options.short.PAR"));
            sb.append(": ");
            switch (par) {
                case ONLY_ONCE:
                    sb.append(UrsuppeResource.getString("parRadioButtonOnlyOne.text"));
                    break;
                case DIFFERENT_AMOEBA:
                    sb.append(UrsuppeResource.getString("parRadioButtonTwoAmoebas.text"));
                    break;
                case DIFFERENT_PLAYER:
                    sb.append(UrsuppeResource.getString("parRadioButtonTwoPlayers.text"));
                    break;
                case SAME_AMOEBA_TWICE:
                    sb.append(UrsuppeResource.getString("parRadioButtonTwice.text"));
                    break;
            }
        }
        
        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("["+UrsuppeResource.getString("gameSettingLabel.text") + "]\n");
        //Extra Gene
        sb.append(UrsuppeResource.getString("extraGenesLabel.text")).append(": ");
        if (allowExtras) {
            sb.append(UrsuppeResource.getString("extraGenesYesRB.text"));
        } else {
            sb.append( UrsuppeResource.getString("extraGenesNoRB.text"));
        }
        sb.append("\n");

        //SFS strict
        sb.append(UrsuppeResource.getString("sfsLabel.text")).append(" ");
        if (sfsStrict) {
            sb.append(UrsuppeResource.getString("sfsStrictRB.text"));
        } else {
            sb.append(UrsuppeResource.getString("sfsLooseRB.text"));
        }
        sb.append("\n");

        //SFS+HOL
        sb.append(UrsuppeResource.getString("sfsHOLLabel.text")).append(" ");
        if (SFSESCHOLisSuccess) {
            sb.append(UrsuppeResource.getString("sfsHOLsuccessRButton.text"));
        } else {
            sb.append(UrsuppeResource.getString("sfsHOLfailRButton.text"));
        }
        sb.append("\n");

        //SFS+HOL
        sb.append(UrsuppeResource.getString("sfsBANGLabel.text")).append(" ");
        if (canBANGwithSFS) {
            sb.append(UrsuppeResource.getString("sfsBANGyesRButton.text"));
        } else {
            sb.append(UrsuppeResource.getString("sfsBANGnoRButton.text"));
        }
        sb.append("\n");

        //PAR
        sb.append(UrsuppeResource.getString("parLabel.text")).append(": ");
        switch (par) {
            case ONLY_ONCE:
                sb.append(UrsuppeResource.getString("parRadioButtonOnlyOne.text"));
                break;
            case DIFFERENT_AMOEBA:
                sb.append(UrsuppeResource.getString("parRadioButtonTwoAmoebas.text"));
                break;
            case DIFFERENT_PLAYER:
                sb.append(UrsuppeResource.getString("parRadioButtonTwoPlayers.text"));
                break;
            case SAME_AMOEBA_TWICE:
                sb.append(UrsuppeResource.getString("parRadioButtonTwice.text"));
                break;
        }
        sb.append("\n");

        return sb.toString();
    }

}
