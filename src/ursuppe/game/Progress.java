/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.game;

import java.awt.Point;
import java.io.Serializable;
import ursuppe.game.GameState.DIRECTION;

/**
 *
 * @author Personal
 */
public class Progress implements Serializable {


    public static final long serialVersionUID = 84348793443L;

    public static final int PHASE_INITIAL_SCORE = -2;
    public static final int PHASE_FIRST_AMOEBA = -1;
    public static final int PHASE_SECOND_AMOEBA = 0;
    public static final int PHASE_1_MOVE_AND_FEED = 1;
    public static final int PHASE_2_OZONE_CHANGE = 2;
    public static final int PHASE_3_NEW_GENES = 3;
    public static final int PHASE_4_DIVISION = 4;
    public static final int PHASE_5_DEATHS = 5;
    public static final int PHASE_6_SCORING = 6;

    public static enum DICE_STATE {
        NONE,
        MOVE, MOVE_MV1, MOVE_MV2,
        DEF,
        ESC, ESC_MV1, ESC_MV2
    }


    public boolean started;
    public boolean ended;

    public int currentRound;
    public int currentPhase;
    public int currentOrder;
    public int currentAmoeba;

    public boolean amoebaMoved;
    public boolean amoebaFed;
    public boolean amoebaExcreted;

    //SPD
    public boolean SPDused;
    
    //ALM
    public boolean ALMactive;

    //AGR vars
    public boolean AGRused;
    public boolean AGRactive;


    //Dice
    public DICE_STATE diceState;
    public int dice1;
    public int dice2;

    //HOL vars
    public boolean HOLactive;
    public DIRECTION HOLdirection;
    public Point HOLlocation;
    public int HOLPlayerID;
    public int HOLAmoebaIndex;

    //BANG vars
    public boolean BANGactive;
    public Point BANGlocation;
    public int BANGPlayerID;

    //POP vars
    public boolean POPactive;
    public boolean POPdecide;

    //Defense
    public Point attackedLocation;
    public boolean defenderActive;
    public int defenderID;
    public int defenderAmoebaNum;
    public int attackerID;
    public int attackerAmoebaNum;
    public boolean DEFused;
    public boolean DEFnoFood;
    public boolean ESCused;
    public boolean ESCSPDused;
    public boolean ESCSPDdecide;
    public boolean ESCfinished;
    public boolean attackerHARD;
    public boolean attackerMOUexcretes;
    public boolean PERused;
    public boolean defenderBANG;
    public Point defenderBANGloc;

    //scoring ack
    public int ackedScore;

    public Progress() {
        started = false;
        clearRoundProgress();
        clearAmoebaProgress();
        clearDice();
        clearHOLProgress();
        clearBANGProgress();
        clearPOPProgress();
    }

    public void startGame() {
        currentRound = 0;
        currentPhase = PHASE_INITIAL_SCORE;
        currentOrder = 0;
        currentAmoeba = -1;

        started = true;

        clearRoundProgress();
        clearAmoebaProgress();
        clearDice();
        clearHOLProgress();
        clearBANGProgress();
        clearPOPProgress();
        clearDefenseProgress();
    }

    public void clearRoundProgress() {
        ALMactive = false;
        AGRused = false;
        AGRactive = false;
        clearAmoebaProgress();
        clearDice();
        clearHOLProgress();
        clearBANGProgress();
        clearPOPProgress();
    }

    public void clearAmoebaProgress() {
        amoebaMoved = false;
        amoebaFed = false;
        amoebaExcreted = false;
        SPDused = false;
        clearDefenseProgress();
    }

    public void clearDice() {
        diceState = DICE_STATE.NONE;
        dice1 = 0;
        dice2 = 0;
    }

    public void clearHOLProgress() {
        HOLactive = false;
        HOLdirection = DIRECTION.NONE;
        HOLlocation = null;
        HOLPlayerID = -1;
        HOLAmoebaIndex = -1;
    }

    public void clearBANGProgress() {
        BANGactive = false;
        BANGlocation = null;
        BANGPlayerID = -1;
    }

    public void clearPOPProgress() {
        POPactive = false;
        POPdecide = false;
    }

    public void clearDefenseProgress() {
        attackedLocation = null;
        defenderActive = false;
        defenderID = -1;
        defenderAmoebaNum = -1;
        attackerID = -1;
        attackerAmoebaNum = -1;
        DEFused = false;
        DEFnoFood = false;
        ESCused = false;
        ESCSPDused = false;
        ESCSPDdecide = false;
        ESCfinished = false;
        attackerHARD = false;
        attackerMOUexcretes = false;
        PERused = false;
        defenderBANG = false;
        defenderBANGloc = null;
    }

    public String dumpProgress() {
        StringBuffer res = new StringBuffer("[");

        if (started) res.append("started, ");
        if (ended) res.append("started, ");
        res.append("round " + currentRound + ", ");
        res.append("phase " + currentPhase + ", ");
        res.append("order " + currentOrder + ", ");
        res.append("amoeba " + currentAmoeba + ", ");

        if (amoebaMoved) res.append("amoebaMoved, ");
        if (amoebaFed) res.append("amoebaFed, ");
        if (amoebaExcreted) res.append("amoebaExcreted, ");

        if (SPDused) res.append("SPDused, ");

        if (ALMactive) res.append("ALMactive, ");

        if (AGRused) res.append("AGRused, ");
        if (AGRactive) res.append("AGRactive, ");

        if (diceState != DICE_STATE.NONE) res.append(diceState + ", (");
        if (diceState != DICE_STATE.NONE) res.append(dice1 + "|");
        if (diceState != DICE_STATE.NONE) res.append(dice2 + "), ");

        if (HOLactive) res.append("HOLactive, ");
        if (HOLactive) res.append(HOLdirection + "e, ");
        if (HOLactive) res.append("HOLPlayerID " + HOLPlayerID + " , ");
        if (HOLactive) res.append("HOLAmoebaIndex " + HOLAmoebaIndex + " , ");
        if (HOLactive) res.append("HOLactive, ");

        if (BANGactive) res.append("BANGactive, ");
        if (BANGactive) res.append("BANGlocation " + BANGlocation + " , ");
        if (BANGactive) res.append("BANGPlayerID " + BANGPlayerID + " , ");

        if (POPactive) res.append("POPactive, ");
        if (POPdecide) res.append("POPdecide, ");

        if (attackedLocation != null) res.append("attackedLocation" + attackedLocation + ", ");
        if (attackedLocation != null) res.append("defenderID" + defenderID + ", ");
        if (attackedLocation != null) res.append("defenderAmoebaNum" + defenderAmoebaNum + ", ");
        if (attackedLocation != null) res.append("attackerID" + attackerID + ", ");
        if (attackedLocation != null) res.append("attackerAmoebaNum" + attackerAmoebaNum + ", ");
        if (defenderActive) res.append("defenderActive, ");
        if (DEFused) res.append("DEFused, ");
        if (DEFnoFood) res.append("DEFnoFood, ");
        if (ESCused) res.append("ESCused, ");
        if (ESCSPDused) res.append("ESCSPDused, ");
        if (ESCSPDdecide) res.append("ESCSPDdecide, ");
        if (ESCfinished) res.append("ESCfinished, ");
        if (attackerHARD) res.append("attackerHARD, ");
        if (attackerMOUexcretes) res.append("attackerMOUexcretes, ");
        if (PERused) res.append("PERused, ");
        if (defenderBANG) res.append("defenderBANG, ");
        if (defenderBANG) res.append("defenderBANGloc" + defenderBANGloc + ", ");

        res.append("ackedScore" + ackedScore + "]");

        return res.toString();
    }
}
