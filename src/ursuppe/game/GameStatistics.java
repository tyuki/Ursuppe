/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.game;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import ursuppe.game.GenePool.GENE;

/**
 *
 * @author Personal
 */
public class GameStatistics implements Serializable {
    
    
    public static final long serialVersionUID = 7841348644156L;

    public final int[] envrionmentHistory = new int[] {-1,-2,-3,-4,-5,-6,-7,-8,-9,-10,-11,-12};
    public final Map<Player, PlayerHistory> playerHistory = new HashMap<>();

    public GameStatistics(List<Player> players) {
        for (Player player : players) {
            playerHistory.put(player, new PlayerHistory(player.getID()));
        }
    }

    public void populateStatistics(GameState state) {
        final Progress progress = state.getProgress();
        final EnvironmentCard currentEnvironment = state.getCurrentEnvironment();

        if (currentEnvironment != null)
            envrionmentHistory[progress.currentRound] = currentEnvironment.ID;

        for (Player player : state.getPlayers()) {
            playerHistory.get(player).populateStatistics( progress, player);
        }
    }

//    public void registerGenePurchase(Player player, final GENE gene, Progress progress) {
//        if (!playerHistory.get(player).purchasedGenes.containsKey(progress.currentRound)) {
//            playerHistory.get(player).purchasedGenes.put(progress.currentRound, new ArrayList<GENE>() { {add(gene);} });
//        } else {
//            playerHistory.get(player).purchasedGenes.get(progress.currentRound).add(gene);
//        }
//    }
//    public void registerGeneRelease(Player player, final GENE gene, Progress progress) {
//        if (!playerHistory.get(player).releasedGenes.containsKey(progress.currentRound)) {
//            playerHistory.get(player).releasedGenes.put(progress.currentRound, new ArrayList<GENE>() { {add(gene);} });
//        } else {
//            playerHistory.get(player).releasedGenes.get(progress.currentRound).add(gene);
//        }
//    }

    public class PlayerHistory implements Serializable {

        public static final long serialVersionUID = 7080401030L;

        public final int playerID;
        
        public final int[] pointHistory = new int[12];
        public final int[] geneHistory = new int[12];
        public final int[] amoebaHistory = new int[12];
        public final int[] BPhistory = new int[12];
        public final int[] MPhistory = new int[12];
//        public final Map<Integer, List<GenePool.GENE>> purchasedGenes = new TreeMap<Integer, List<GenePool.GENE>>();
//        public final Map<Integer, List<GenePool.GENE>> releasedGenes = new TreeMap<Integer, List<GenePool.GENE>>();

        public PlayerHistory(int playerID) {
            this.playerID = playerID;
        }

        private void populateStatistics(Progress progress, Player player) {
                pointHistory[progress.currentRound] = player.getAdvance();
                geneHistory[progress.currentRound] = player.getNumGenesForScore();
                amoebaHistory[progress.currentRound] = player.getNumAmoebas();
                BPhistory[progress.currentRound] = player.getBP();
                MPhistory[progress.currentRound] = player.getMP();
        }

    }
}
