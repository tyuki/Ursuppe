package ursuppe.game;

import java.awt.Point;
import java.io.Serializable;

public class Amoeba implements Serializable, Comparable<Amoeba> {


    public static final long serialVersionUID = 59713748791L;

    public final int playerID;
    public final int number;
    public final int colorID;

    private Point _location;

    private int _DP;
    //private

    public Amoeba(int playerID, int num, int colorID, Point location) {
        this.playerID = playerID;
        number =num;
        this.colorID = colorID;
        _location = location;

        _DP = 0;
    }

    public Point getLocation() {
        return _location;
    }

    public void setLocation(Point location) {
        _location = location;
    }

    public int getDP() {
        return _DP;
    }

    public void addDP(int dmg) {
        _DP += dmg;
    }

    @Override
    public int compareTo(Amoeba o) {
       if (this.playerID > o.playerID) {
           return 1;
       } else if (this.playerID < o.playerID) {
           return -1;
       } else {
           if (this.number > o.number) {
               return 1;
           } else {
               return -1;
           }
       }


    }
}
