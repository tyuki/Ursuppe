/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.game;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import network.Message;
import ursuppe.UrsuppeApp;
import ursuppe.UrsuppeResource;
import ursuppe.gui.icon.CustomIconSet;
import ursuppe.gui.icon.IconContainer;

/**
 *
 * @author Personal
 */
public class Replay implements Serializable {

    public static final long serialVersionUID = 100000000L;

    public static final String REPLAY_PATH = "./replay/";
    public static final String REPLAY_EXT = ".rep";

    protected final List<Message> messages = new ArrayList<>();
    protected final List<Integer> gameStateMap = new ArrayList<>();
    protected final Map<Integer, CustomIconSet> iconSet;

    public final String replayName;

    public final int[] roundKeyFrames = new int[12];
    public final int[][] phaseKeyFrames = new int[12][9];

    public Replay(String replayName, Map<Integer, CustomIconSet> iconSet) {
        this.replayName = replayName;
        this.iconSet = iconSet;

        for (int i = 0; i < roundKeyFrames.length; i++) {
            roundKeyFrames[i] = -1;
            for (int j = 0; j < phaseKeyFrames[i].length; j++) {
                phaseKeyFrames[i][j] = -1;
            }

        }

    }

    public void addMessage(Message msg) {
        if (msg != null) {
            try {
                //Colne the message by serializing and deserializing back
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream oos  = new ObjectOutputStream(baos);
                oos.writeObject(msg);
                byte[] array = baos.toByteArray();
                ByteArrayInputStream bais = new ByteArrayInputStream(array);
                ObjectInputStream ois = new ObjectInputStream(bais);
                Message msgClone = (Message)ois.readObject();

                messages.add(msgClone);

                if (msgClone.getMessageType() == Message.ENTIRE_GAME_STATE) {
                    gameStateMap.add(messages.size()-1);
                    int frameNum = gameStateMap.size()-1;

                     Progress progress = ((GameState)msg.getItems()[0]).getProgress();
                     if (roundKeyFrames[progress.currentRound] == -1) roundKeyFrames[progress.currentRound] = frameNum;
                     if (phaseKeyFrames[progress.currentRound][progress.currentPhase+2] == -1) phaseKeyFrames[progress.currentRound][progress.currentPhase+2] = frameNum;

                }


            } catch (IOException ex) {
                Logger.getLogger(Replay.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException cce) {
                
            }
        }
    }

    public int getGameStateLength() {
        return gameStateMap.size();
    }

    public GameState getGameState(int i) {
        if (0 <= i && i < gameStateMap.size()) {
            if (0 <= gameStateMap.get(i) && gameStateMap.get(i) < messages.size()) {
                return (GameState)messages.get(gameStateMap.get(i)).getItems()[0];
            }
        }

        return null;
    }

    public List<Message> getInitialMessages() {
        return messages.subList(0, gameStateMap.get(0));
    }
    
    public List<Message> getMessagesAfter(int i) {
        if (i + 1 < gameStateMap.size()) {
            return messages.subList(gameStateMap.get(i), gameStateMap.get(i+1));
        } else {
            return messages.subList(gameStateMap.get(i), messages.size());
        }
    }
    
    public int getNextRoundFrame(final int currentFrame) {
        
        try {
            GameState current = getGameState(currentFrame);
            if (current != null) {
                int nextRound = current.getProgress().currentRound + 1;
                if (nextRound > 11 || nextRound < 0) return currentFrame;
                if (roundKeyFrames[nextRound] == -1) return getGameStateLength()-1;
                
                return roundKeyFrames[nextRound];
            }
        } catch (Exception e) {
            UrsuppeApp.debug(e);
        }
        return currentFrame;
    }
    
    public int getPrevRoundFrame(final int currentFrame) {
        
        try {
            GameState current = getGameState(currentFrame);
            if (current != null) {
                if (currentFrame == getGameStateLength() -1) {
                    return roundKeyFrames[current.getProgress().currentRound];
                }
                
                int prevRound = current.getProgress().currentRound - 1;
                if (prevRound < 0) return 0;
                if (prevRound > 11) return currentFrame;
                if (roundKeyFrames[prevRound] == -1) throw new RuntimeException("Unexpected case.");
                
                return roundKeyFrames[prevRound];
            }
        } catch (Exception e) {
            UrsuppeApp.debug(e);
        }
        return currentFrame;
    }
    
    
    public int getNextPhaseFrame(final int currentFrame) {
        try {
            GameState current = getGameState(currentFrame);
            if (current != null) {
                int currentRound = current.getProgress().currentRound;
                int nextPhase = current.getProgress().currentPhase + 1 + 2;
                if (current.getProgress().currentPhase == -1) nextPhase++;
                while (true) {
                    if (nextPhase < 0) return currentFrame;
                    if (nextPhase > 8) return getNextRoundFrame(currentFrame);
                    if (phaseKeyFrames[currentRound][nextPhase] == -1) {
                        nextPhase++;
                        continue;
                    }
                    
                    break;
                }
                return phaseKeyFrames[currentRound][nextPhase];
                
            }
        } catch (Exception e) {
            UrsuppeApp.debug(e);
        }
        
        return currentFrame;
    }
    
    public int getPrevPhaseFrame(final int currentFrame) {
        
        try {
            GameState current = getGameState(currentFrame);
            if (current != null) {
                int currentRound = current.getProgress().currentRound ;
                int prevPhase = current.getProgress().currentPhase - 1 + 2;
                while (true) {
                    if (prevPhase > 8) return currentFrame;
                    if (prevPhase < 0) return getPrevRoundFrame(currentFrame);
                    if (phaseKeyFrames[currentRound][prevPhase] == -1) {
                        prevPhase--;
                        continue;
                    }
                    
                    break;
                }
                return phaseKeyFrames[currentRound][prevPhase];
            }
        } catch (Exception e) {
            UrsuppeApp.debug(e);
        }
        return currentFrame;
    }

    public void save() {
        File file = new File(REPLAY_PATH+replayName+REPLAY_EXT);
        if (!file.exists()) {
            try {
                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                }
                file.createNewFile();
            } catch (Exception ex) {
                ursuppe.UrsuppeApp.debug(ex);
                return;
            }
        }
        
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
            oos.writeObject(this);
            oos.flush();
            oos.close();
        } catch (IOException ex) {
                ursuppe.UrsuppeApp.debug(ex);
        }
    }

    public static Replay load(File file) {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            return (Replay)ois.readObject();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, UrsuppeResource.getString("error.IncompatibleReplay"));
            ursuppe.UrsuppeApp.debug(ex);
            return null;
        }
    }

    public void populateIconContainer() {
        IconContainer.clearCustomIconCache();
        IconContainer.clearExternalCustomIcons();

        for (int id : iconSet.keySet()) {
            CustomIconSet customIcons = iconSet.get(id);
            IconContainer.addCustomIcon(IconContainer.CUSTOM_ICON_PREFIX+id, customIcons.iconBase);
            IconContainer.addCustomNumberBase(IconContainer.CUSTOM_ICON_PREFIX+id, customIcons.numBase);
            IconContainer.addCustomScoreBase(IconContainer.CUSTOM_SCORE_ICON_NAME+id, customIcons.scoreBase);
            IconContainer.addCustomPlayerBackgroundBase(IconContainer.CUSTOM_BACKGROUND_PREFIX+id, customIcons.BGbase);
        }
    }

}
