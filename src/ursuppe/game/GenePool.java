/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.game;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import ursuppe.gui.icon.IconContainer;

/**
 *
 * @author Personal
 */
public class GenePool implements Serializable {


    public static final long serialVersionUID = 3791848961L;

    public static enum GENE {
        INT, MV1, SPO, SPD, DEF, ESC, SUB, RAY, STR, TEN, HOL, LIF, FRU, SFS, PAR, DIV, PER, MV2, AGR, ARM,
        DandF, GLU, MIG, HARD, ALM, CLE, SOC, FAST, MOU, ADA, SUC1, DIS, CAM1, HEA1, FLEX, SLS, TOX, BANG, REB, POP, ENE, PHD, ENV, CAM2, HEA2, SUC2
    }

    private static Map<String, GENE> _StringToGENE;
    private static Map<GENE, String> _GENEToString;

    private Map<GENE,Gene> _genes;

    public GenePool(GameOption option) {
        _genes = new HashMap<>();
        initialize(option, 4);
    }

    public Gene getGene(GENE gene) {
        return _genes.get(gene);
    }

    public static GENE[] getRegularGenes() {
        return new GENE[] {GENE.INT, GENE.MV1, GENE.SPO, GENE.SPD,
                            GENE.DEF, GENE.ESC, GENE.SUB, GENE.RAY,
                            GENE.STR, GENE.TEN, GENE.HOL, GENE.LIF,
                            GENE.FRU, GENE.SFS, GENE.PAR, GENE.DIV,
                            GENE.PER, GENE.MV2, GENE.AGR, GENE.ARM};
    }

    public static GENE[] getExtraGenes() {
        return new GENE[] {GENE.DandF, GENE.GLU, GENE.MIG, GENE.HARD,
                            GENE.ALM, GENE.CLE, GENE.SOC, GENE.FAST,
                            GENE.MOU, GENE.ADA, GENE.SUC1, GENE.DIS,
                            GENE.CAM1, GENE.HEA1, GENE.FLEX, GENE.SLS,
                            GENE.TOX, GENE.BANG, GENE.REB, GENE.POP,
                            GENE.ENE, GENE.PHD, GENE.ENV, GENE.CAM2,
                            GENE.HEA2, GENE.SUC2};
    }




    static {
        _StringToGENE = new HashMap<>();
        _GENEToString = new HashMap<>();
        
         _GENEToString.put(GENE.INT, "INT");
         _GENEToString.put(GENE.MV1, "MV1");
         _GENEToString.put(GENE.SPO, "SPO");
         _GENEToString.put(GENE.SPD, "SPD");
         _GENEToString.put(GENE.DEF, "DEF");
         _GENEToString.put(GENE.ESC, "ESC");
         _GENEToString.put(GENE.SUB, "SUB");
         _GENEToString.put(GENE.RAY, "RAY");
         _GENEToString.put(GENE.STR, "STR");
         _GENEToString.put(GENE.TEN, "TEN");
         _GENEToString.put(GENE.HOL, "HOL");
         _GENEToString.put(GENE.LIF, "LIF");
         _GENEToString.put(GENE.FRU, "FRU");
         _GENEToString.put(GENE.SFS, "SFS");
         _GENEToString.put(GENE.PAR, "PAR");
         _GENEToString.put(GENE.DIV, "DIV");
         _GENEToString.put(GENE.PER, "PER");
         _GENEToString.put(GENE.MV2, "MV2");
         _GENEToString.put(GENE.AGR, "AGR");
         _GENEToString.put(GENE.ARM, "ARM");
         _GENEToString.put(GENE.DandF, "D&F");
         _GENEToString.put(GENE.GLU, "GLU");
         _GENEToString.put(GENE.MIG, "MIG");
         _GENEToString.put(GENE.HARD, "HARD");
         _GENEToString.put(GENE.ALM, "ALM");
         _GENEToString.put(GENE.CLE, "CLE");
         _GENEToString.put(GENE.SOC, "SOC");
         _GENEToString.put(GENE.FAST, "FAST");
         _GENEToString.put(GENE.MOU, "MOU");
         _GENEToString.put(GENE.ADA, "ADA");
         _GENEToString.put(GENE.SUC1, "SUC1");
         _GENEToString.put(GENE.DIS, "DIS");
         _GENEToString.put(GENE.CAM1, "CAM1");
         _GENEToString.put(GENE.HEA1, "HEA1");
         _GENEToString.put(GENE.FLEX, "FLEX");
         _GENEToString.put(GENE.SLS, "SLS");
         _GENEToString.put(GENE.TOX, "TOX");
         _GENEToString.put(GENE.BANG, "BANG");
         _GENEToString.put(GENE.REB, "REB");
         _GENEToString.put(GENE.POP, "POP");
         _GENEToString.put(GENE.ENE, "ENE");
         _GENEToString.put(GENE.PHD, "PHD");
         _GENEToString.put(GENE.ENV, "ENV");
         _GENEToString.put(GENE.CAM2, "CAM2");
         _GENEToString.put(GENE.HEA2, "HEA2");
         _GENEToString.put(GENE.SUC2, "SUC2");

         for (GENE g : _GENEToString.keySet()) {
             String str = _GENEToString.get(g);
             _StringToGENE.put(str, g);
         }
    }

    public void initialize(GameOption option, int numPlayers) {
        _genes.put(GENE.INT, new Gene(GENE.INT, option, numPlayers));
        _genes.put(GENE.MV1, new Gene(GENE.MV1, option, numPlayers));
        _genes.put(GENE.SPO, new Gene(GENE.SPO, option, numPlayers));
        _genes.put(GENE.SPD, new Gene(GENE.SPD, option, numPlayers));
        _genes.put(GENE.DEF, new Gene(GENE.DEF, option, numPlayers));
        _genes.put(GENE.ESC, new Gene(GENE.ESC, option, numPlayers));
        _genes.put(GENE.SUB, new Gene(GENE.SUB, option, numPlayers));
        _genes.put(GENE.RAY, new Gene(GENE.RAY, option, numPlayers));
        _genes.put(GENE.STR, new Gene(GENE.STR, option, numPlayers));
        _genes.put(GENE.TEN, new Gene(GENE.TEN, option, numPlayers));
        _genes.put(GENE.HOL, new Gene(GENE.HOL, option, numPlayers));
        _genes.put(GENE.LIF, new Gene(GENE.LIF, option, numPlayers));
        _genes.put(GENE.FRU, new Gene(GENE.FRU, option, numPlayers));
        _genes.put(GENE.SFS, new Gene(GENE.SFS, option, numPlayers));
        _genes.put(GENE.PAR, new Gene(GENE.PAR, option, numPlayers));
        _genes.put(GENE.DIV, new Gene(GENE.DIV, option, numPlayers));
        _genes.put(GENE.PER, new Gene(GENE.PER, option, numPlayers));
        _genes.put(GENE.MV2, new Gene(GENE.MV2, option, numPlayers));
        _genes.put(GENE.AGR, new Gene(GENE.AGR, option, numPlayers));
        _genes.put(GENE.ARM, new Gene(GENE.ARM, option, numPlayers));
        _genes.put(GENE.DandF, new Gene(GENE.DandF, option, numPlayers));
        _genes.put(GENE.GLU,   new Gene(GENE.GLU,   option, numPlayers));
        _genes.put(GENE.MIG,   new Gene(GENE.MIG,   option, numPlayers));
        _genes.put(GENE.HARD,   new Gene(GENE.HARD,   option, numPlayers));
        _genes.put(GENE.ALM,   new Gene(GENE.ALM,   option, numPlayers));
        _genes.put(GENE.CLE,   new Gene(GENE.CLE,   option, numPlayers));
        _genes.put(GENE.SOC,   new Gene(GENE.SOC,   option, numPlayers));
        _genes.put(GENE.FAST,   new Gene(GENE.FAST,   option, numPlayers));
        _genes.put(GENE.MOU,   new Gene(GENE.MOU,   option, numPlayers));
        _genes.put(GENE.ADA,   new Gene(GENE.ADA,   option, numPlayers));
        _genes.put(GENE.SUC1,  new Gene(GENE.SUC1,  option, numPlayers));
        _genes.put(GENE.DIS,   new Gene(GENE.DIS,   option, numPlayers));
        _genes.put(GENE.CAM1,  new Gene(GENE.CAM1,  option, numPlayers));
        _genes.put(GENE.HEA1,  new Gene(GENE.HEA1,  option, numPlayers));
        _genes.put(GENE.FLEX,   new Gene(GENE.FLEX,   option, numPlayers));
        _genes.put(GENE.SLS,   new Gene(GENE.SLS,   option, numPlayers));
        _genes.put(GENE.TOX,   new Gene(GENE.TOX,   option, numPlayers));
        _genes.put(GENE.BANG,   new Gene(GENE.BANG,   option, numPlayers));
        _genes.put(GENE.REB,   new Gene(GENE.REB,   option, numPlayers));
        _genes.put(GENE.POP,   new Gene(GENE.POP,   option, numPlayers));
        _genes.put(GENE.ENE,   new Gene(GENE.ENE,   option, numPlayers));
        _genes.put(GENE.PHD,   new Gene(GENE.PHD,   option, numPlayers));
        _genes.put(GENE.ENV,   new Gene(GENE.ENV,   option, numPlayers));
        _genes.put(GENE.CAM2,  new Gene(GENE.CAM2,  option, numPlayers));
        _genes.put(GENE.HEA2,  new Gene(GENE.HEA2,  option, numPlayers));
        _genes.put(GENE.SUC2,  new Gene(GENE.SUC2,  option, numPlayers));
    }

    public static String toName(GENE gene) {
        return _GENEToString.get(gene);
    }

    public static GENE toGENE(String name) {
        return _StringToGENE.get(name);
    }

    public static String getIconFilename(GENE gene) {
        if (IconContainer.getIcon(GenePool.toName(gene)) != null) {
            return GenePool.toName(gene);
        }
        return "gene";
    }

}
