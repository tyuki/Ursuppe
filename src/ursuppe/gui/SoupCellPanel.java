/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * SoupBoxPanel.java
 *
 * Created on 2010/01/13, 14:58:24
 */

package ursuppe.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import ursuppe.game.Amoeba;
import ursuppe.game.GameState.ATTACK_RESPONSE;
import ursuppe.game.Soup;
import ursuppe.game.SoupCell;
import ursuppe.gui.effect.EffectListener;

/**
 *
 * @author Personal
 */
public class SoupCellPanel extends javax.swing.JPanel implements EffectListener {

    public static enum CELL_TYPE {
        SOUP, ROCK
    }

    private UrsuppeGUI _gui;

    //public static Color SOUP_COLOR = Color.decode("#9296e6");

//    public static Color SOUP_COLOR = new Color(0, 0, 0, 0);
//    public static Color ROCK_COLOR = new Color(0, 0, 0, 0);
    public static Color DISABLED_COLOR = new Color(53, 53, 53, 150);

    private Color colorForIconTest = Color.decode("#f0f0f0");

    private Point _coordinate;
    private CELL_TYPE _type;
    private int _activePlayerIDinDisabledState;

    private List<AmoebaLabel> _amoebaLabels;

    /** Creates new form SoupBoxPanel */
    public SoupCellPanel() {
        initComponents();


        _amoebaLabels = new ArrayList<>();
        _amoebaLabels.add(amoebaLabel00);
        _amoebaLabels.add(amoebaLabel01);
        _amoebaLabels.add(amoebaLabel02);
        _amoebaLabels.add(amoebaLabel03);

        _amoebaLabels.add(amoebaLabel10);
        _amoebaLabels.add(amoebaLabel11);
        _amoebaLabels.add(amoebaLabel12);
        _amoebaLabels.add(amoebaLabel13);

        _amoebaLabels.add(amoebaLabel20);
        _amoebaLabels.add(amoebaLabel21);
        _amoebaLabels.add(amoebaLabel22);
        _amoebaLabels.add(amoebaLabel23);
    }
    
    @Override
    protected void paintComponent(Graphics g) {
//        super.paintComponent(g);
//        setOpaque(false);
//        if (!isEnabled()) {
//                g.setColor(DISABLED_COLOR);
//                g.fillRect(0, 0, 120, 120);
//        }
        if (_gui == null) {
            g.setColor(colorForIconTest);
            g.fillRect(0, 0, 120, 120);
        }

    }

    @Override
    protected void paintChildren(Graphics g) {
        super.paintChildren(g);
        if (!isEnabled()) {
                g.setColor(DISABLED_COLOR);
                g.fillRect(0, 0, 120, 120);
                int xloc = 0;
                int yloc = 30;
                for (AmoebaLabel al : _amoebaLabels) {
                    if (al.getAmoeba() != null && al.getAmoeba().playerID == _activePlayerIDinDisabledState) {
                        g.translate(xloc, yloc);
                        al.paint(g);
                        g.translate(-xloc, -yloc);
                    }
                    xloc = xloc + 30;
                    if (xloc >= 120) {
                        xloc = 0;
                        yloc = yloc + 30;
                    }
                }
        }
    }
    
    

    public void setColorForIconTest(Color colorForIconTest) {
        this.colorForIconTest = colorForIconTest;
    }

    public void setGUI(UrsuppeGUI gui) {
        _gui = gui;
        for (AmoebaLabel amoebaLabel : _amoebaLabels) {
            amoebaLabel.setGUI(_gui);
        }
        foodLabel1.setGUI(_gui);
    }

    public void setCoordinate(int x, int y) {
        _coordinate = new Point(x,y);
    }

    public void setType(CELL_TYPE type) {
        _type = type;
    }

    public void setFoodColors(int[] colors) {
        foodLabel1.setFoodColors(colors);
    }

    public void setSoupCell(SoupCell cell) {
        foodLabel1.setSoupCell(cell);

        List<Amoeba> amoebas = cell.getAmoebas();

        for (int i = 0; i < Math.min(amoebas.size(), _amoebaLabels.size()); i++) {
            _amoebaLabels.get(i).setAmoeba(amoebas.get(i));
            _amoebaLabels.get(i).stopAnimation();
        }

        for (int i = amoebas.size(); i < _amoebaLabels.size(); i++) {
            _amoebaLabels.get(i).setAmoeba(null);
            _amoebaLabels.get(i).stopAnimation();
        }
    }

    public Point getCoordinate() {
        return _coordinate;
    }

    @Override
    public void setEnabled(boolean enabled) {
        setEnabled(enabled, -1);
    }
    
    public void setEnabled(boolean enabled, int id) {
        super.setEnabled(enabled);
        _activePlayerIDinDisabledState = id;
    }

    public void animateAmoeba(Amoeba amoeba) {
        for (AmoebaLabel amoebaLabel : _amoebaLabels) {
            if (amoebaLabel.getAmoeba() != null && amoebaLabel.getAmoeba().equals(amoeba)) {
                amoebaLabel.startAnimation();
            }
        }
    }

    public void stopAllAnimation() {
        for (AmoebaLabel amoebaLabel : _amoebaLabels) {
            amoebaLabel.stopAnimation();
        }
    }

    public void emphasize(boolean emp) {
        //this.getBorder().
        if (emp) {
            setBorder(javax.swing.BorderFactory.createEtchedBorder(Color.red, Color.red));
        } else {
            setBorder(null);
        }
    }

    public void setDefense(Amoeba amoeba, List<ATTACK_RESPONSE> res) {
        for (AmoebaLabel amoebaLabel : _amoebaLabels) {
            if (amoebaLabel.getAmoeba() != null && amoebaLabel.getAmoeba().equals(amoeba)) {
                amoebaLabel.setDefense(res);
            }
        }
    }

    public void resetDefense(Amoeba amoeba) {
        for (AmoebaLabel amoebaLabel : _amoebaLabels) {
            amoebaLabel.resetDefense();
        }
    }

    public void emphasize(Amoeba amoeba) {
        for (AmoebaLabel amoebaLabel : _amoebaLabels) {
            if (amoebaLabel.getAmoeba() != null && amoebaLabel.getAmoeba().equals(amoeba)) {
                amoebaLabel.setEmphasis(true);
            }
        }
    }

    public void clearEmphasis() {
        for (AmoebaLabel amoebaLabel : _amoebaLabels) {
            amoebaLabel.setEmphasis(false);
        }
    }

    public AmoebaLabel getAmoebaLabel(int index) {
        return _amoebaLabels.get(index);
    }

    @Override
    public void effectStarted() {
        foodLabel1.setSuppressed(true);
    }

    @Override
    public void effectFinished() {
        foodLabel1.setSuppressed(false);
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        foodLabel1 = new ursuppe.gui.FoodLabel();
        amoebaLabel00 = new ursuppe.gui.AmoebaLabel();
        amoebaLabel01 = new ursuppe.gui.AmoebaLabel();
        amoebaLabel02 = new ursuppe.gui.AmoebaLabel();
        amoebaLabel03 = new ursuppe.gui.AmoebaLabel();
        amoebaLabel10 = new ursuppe.gui.AmoebaLabel();
        amoebaLabel11 = new ursuppe.gui.AmoebaLabel();
        amoebaLabel12 = new ursuppe.gui.AmoebaLabel();
        amoebaLabel13 = new ursuppe.gui.AmoebaLabel();
        amoebaLabel20 = new ursuppe.gui.AmoebaLabel();
        amoebaLabel21 = new ursuppe.gui.AmoebaLabel();
        amoebaLabel22 = new ursuppe.gui.AmoebaLabel();
        amoebaLabel23 = new ursuppe.gui.AmoebaLabel();

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(ursuppe.UrsuppeApp.class).getContext().getResourceMap(SoupCellPanel.class);
        setBackground(resourceMap.getColor("Form.background")); // NOI18N
        setName("Form"); // NOI18N
        setPreferredSize(new java.awt.Dimension(120, 120));
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                formMousePressed(evt);
            }
        });
        setLayout(new java.awt.GridBagLayout());

        foodLabel1.setText(resourceMap.getString("foodLabel1.text")); // NOI18N
        foodLabel1.setMaximumSize(new java.awt.Dimension(120, 30));
        foodLabel1.setMinimumSize(new java.awt.Dimension(120, 30));
        foodLabel1.setName("foodLabel1"); // NOI18N
        foodLabel1.setPreferredSize(new java.awt.Dimension(120, 30));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 4;
        add(foodLabel1, gridBagConstraints);

        amoebaLabel00.setText(resourceMap.getString("amoebaLabel00.text")); // NOI18N
        amoebaLabel00.setMaximumSize(new java.awt.Dimension(30, 30));
        amoebaLabel00.setMinimumSize(new java.awt.Dimension(30, 30));
        amoebaLabel00.setName("amoebaLabel00"); // NOI18N
        amoebaLabel00.setPreferredSize(new java.awt.Dimension(30, 30));
        amoebaLabel00.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                amoebaLabelMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 1;
        add(amoebaLabel00, gridBagConstraints);

        amoebaLabel01.setMaximumSize(new java.awt.Dimension(30, 30));
        amoebaLabel01.setMinimumSize(new java.awt.Dimension(30, 30));
        amoebaLabel01.setName("amoebaLabel01"); // NOI18N
        amoebaLabel01.setPreferredSize(new java.awt.Dimension(30, 30));
        amoebaLabel01.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                amoebaLabelMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 1;
        add(amoebaLabel01, gridBagConstraints);

        amoebaLabel02.setMaximumSize(new java.awt.Dimension(30, 30));
        amoebaLabel02.setMinimumSize(new java.awt.Dimension(30, 30));
        amoebaLabel02.setName("amoebaLabel02"); // NOI18N
        amoebaLabel02.setPreferredSize(new java.awt.Dimension(30, 30));
        amoebaLabel02.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                amoebaLabelMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 1;
        add(amoebaLabel02, gridBagConstraints);

        amoebaLabel03.setMaximumSize(new java.awt.Dimension(30, 30));
        amoebaLabel03.setMinimumSize(new java.awt.Dimension(30, 30));
        amoebaLabel03.setName("amoebaLabel03"); // NOI18N
        amoebaLabel03.setPreferredSize(new java.awt.Dimension(30, 30));
        amoebaLabel03.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                amoebaLabelMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 1;
        add(amoebaLabel03, gridBagConstraints);

        amoebaLabel10.setMaximumSize(new java.awt.Dimension(30, 30));
        amoebaLabel10.setMinimumSize(new java.awt.Dimension(30, 30));
        amoebaLabel10.setName("amoebaLabel10"); // NOI18N
        amoebaLabel10.setPreferredSize(new java.awt.Dimension(30, 30));
        amoebaLabel10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                amoebaLabelMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 2;
        add(amoebaLabel10, gridBagConstraints);

        amoebaLabel11.setMaximumSize(new java.awt.Dimension(30, 30));
        amoebaLabel11.setMinimumSize(new java.awt.Dimension(30, 30));
        amoebaLabel11.setName("amoebaLabel11"); // NOI18N
        amoebaLabel11.setPreferredSize(new java.awt.Dimension(30, 30));
        amoebaLabel11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                amoebaLabelMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 2;
        add(amoebaLabel11, gridBagConstraints);

        amoebaLabel12.setMaximumSize(new java.awt.Dimension(30, 30));
        amoebaLabel12.setMinimumSize(new java.awt.Dimension(30, 30));
        amoebaLabel12.setName("amoebaLabel12"); // NOI18N
        amoebaLabel12.setPreferredSize(new java.awt.Dimension(30, 30));
        amoebaLabel12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                amoebaLabelMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 2;
        add(amoebaLabel12, gridBagConstraints);

        amoebaLabel13.setMaximumSize(new java.awt.Dimension(30, 30));
        amoebaLabel13.setMinimumSize(new java.awt.Dimension(30, 30));
        amoebaLabel13.setName("amoebaLabel13"); // NOI18N
        amoebaLabel13.setPreferredSize(new java.awt.Dimension(30, 30));
        amoebaLabel13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                amoebaLabelMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 2;
        add(amoebaLabel13, gridBagConstraints);

        amoebaLabel20.setMaximumSize(new java.awt.Dimension(30, 30));
        amoebaLabel20.setMinimumSize(new java.awt.Dimension(30, 30));
        amoebaLabel20.setName("amoebaLabel20"); // NOI18N
        amoebaLabel20.setPreferredSize(new java.awt.Dimension(30, 30));
        amoebaLabel20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                amoebaLabelMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 3;
        add(amoebaLabel20, gridBagConstraints);

        amoebaLabel21.setMaximumSize(new java.awt.Dimension(30, 30));
        amoebaLabel21.setMinimumSize(new java.awt.Dimension(30, 30));
        amoebaLabel21.setName("amoebaLabel21"); // NOI18N
        amoebaLabel21.setPreferredSize(new java.awt.Dimension(30, 30));
        amoebaLabel21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                amoebaLabelMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 3;
        add(amoebaLabel21, gridBagConstraints);

        amoebaLabel22.setMaximumSize(new java.awt.Dimension(30, 30));
        amoebaLabel22.setMinimumSize(new java.awt.Dimension(30, 30));
        amoebaLabel22.setName("amoebaLabel22"); // NOI18N
        amoebaLabel22.setPreferredSize(new java.awt.Dimension(30, 30));
        amoebaLabel22.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                amoebaLabelMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 3;
        add(amoebaLabel22, gridBagConstraints);

        amoebaLabel23.setMaximumSize(new java.awt.Dimension(30, 30));
        amoebaLabel23.setMinimumSize(new java.awt.Dimension(30, 30));
        amoebaLabel23.setName("amoebaLabel23"); // NOI18N
        amoebaLabel23.setPreferredSize(new java.awt.Dimension(30, 30));
        amoebaLabel23.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                amoebaLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                amoebaLabelMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 3;
        add(amoebaLabel23, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void formMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMousePressed
       if (_gui != null && Soup.isValidLocation(_coordinate) && isEnabled()) {
             _gui.soupCellSelected(_coordinate);
       }
    }//GEN-LAST:event_formMousePressed

    private void amoebaLabelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_amoebaLabelMousePressed
        if (((AmoebaLabel)evt.getSource()).getIcon() == null) {
             this.formMousePressed(evt);
        } else if (_gui != null) {
            Amoeba amoeba = ((AmoebaLabel)evt.getSource()).getAmoeba();
            _gui.amoebaSelected(amoeba.playerID, amoeba.number);
        }
    }//GEN-LAST:event_amoebaLabelMousePressed

    private void amoebaLabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_amoebaLabelMouseEntered
         if (_gui != null && ((AmoebaLabel)evt.getSource()).getIcon() != null) {
            Amoeba amoeba = ((AmoebaLabel)evt.getSource()).getAmoeba();
            _gui.amoebaMouseEntered(amoeba.playerID, amoeba.number);
        }
    }//GEN-LAST:event_amoebaLabelMouseEntered

    private void amoebaLabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_amoebaLabelMouseExited
        if (_gui != null && ((AmoebaLabel)evt.getSource()).getIcon() != null) {
            Amoeba amoeba = ((AmoebaLabel)evt.getSource()).getAmoeba();
            _gui.amoebaMouseExited(amoeba.playerID, amoeba.number);
        }
    }//GEN-LAST:event_amoebaLabelMouseExited


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ursuppe.gui.AmoebaLabel amoebaLabel00;
    private ursuppe.gui.AmoebaLabel amoebaLabel01;
    private ursuppe.gui.AmoebaLabel amoebaLabel02;
    private ursuppe.gui.AmoebaLabel amoebaLabel03;
    private ursuppe.gui.AmoebaLabel amoebaLabel10;
    private ursuppe.gui.AmoebaLabel amoebaLabel11;
    private ursuppe.gui.AmoebaLabel amoebaLabel12;
    private ursuppe.gui.AmoebaLabel amoebaLabel13;
    private ursuppe.gui.AmoebaLabel amoebaLabel20;
    private ursuppe.gui.AmoebaLabel amoebaLabel21;
    private ursuppe.gui.AmoebaLabel amoebaLabel22;
    private ursuppe.gui.AmoebaLabel amoebaLabel23;
    private ursuppe.gui.FoodLabel foodLabel1;
    // End of variables declaration//GEN-END:variables

}
