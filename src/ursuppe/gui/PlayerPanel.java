/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * PlayerPanel.java
 *
 * Created on 2010/01/13, 18:11:54
 */

package ursuppe.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JLabel;
import ursuppe.UrsuppeResource;
import ursuppe.game.Amoeba;
import ursuppe.game.Gene;
import ursuppe.game.GenePool;
import ursuppe.game.GenePool.GENE;
import ursuppe.game.Player;
import ursuppe.gui.UrsuppeColor.TYPE;
import ursuppe.gui.effect.AnimationEffectInstance;
import ursuppe.gui.effect.Effect;
import ursuppe.gui.icon.IconContainer;

/**
 *
 * @author Personal
 */
public class PlayerPanel extends javax.swing.JPanel {

    private UrsuppeGUI _gui;
    private List<JLabel> _geneLabels;
    private Player _player;
    private List<Effect> _effects;

    private static final Color DEFAULT_COLOR = new Color(204,204,255);

    private boolean _RmousePressed;

    /** Creates new form PlayerPanel */
    public PlayerPanel() {
        initComponents();

        _effects = Collections.synchronizedList(new LinkedList<>());
        //geneLabel1.setIcon(IconContainer.getMiniIcon("gene"));
        //geneLabel1.setHorizontalAlignment(SwingConstants.CENTER);
        //geneLabel1.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, Color.red, null));
        
    }

    public void setGUI(UrsuppeGUI gui) {
        _gui = gui;

        amoebaLabel.setRepaintTarget(this);

        _geneLabels = new LinkedList<>();
        _geneLabels.add(geneLabel1);
        _geneLabels.add(geneLabel2);
        _geneLabels.add(geneLabel3);
        _geneLabels.add(geneLabel4);
        _geneLabels.add(geneLabel5);
        _geneLabels.add(geneLabel6);
        _geneLabels.add(geneLabel7);

    }

    public void setPlayer(Player player) {
        _player = player;
        if (_player == null) {
            //Clear everything
            nameLabel.setText("");
            BPLabel.setText("");
            MPLabel.setText("");
            amoebasLabel.setText("");
            genesLabel.setText("");
            amoebaLabel.setAmoeba(null);
            for (JLabel geneLabel : _geneLabels) {
                geneLabel.setIcon(null);
                geneLabel.setToolTipText("");
            }
            this.setBackground(DEFAULT_COLOR);
            this.repaint();
            return;
        }
        nameLabel.setText(_player.getName());
        BPLabel.setText(UrsuppeResource.getString("term.BP")+":"+_player.getBP());
        MPLabel.setText(UrsuppeResource.getString("term.MP")+":"+_player.getMP());
        amoebasLabel.setText(UrsuppeResource.getString("term.amoeba")+":"+_player.getNumAmoebas());
        genesLabel.setText(UrsuppeResource.getString("term.genes")+":"+_player.getNumGenesForScore());
        this.setBackground(UrsuppeColor.get(player.getColorID()).toColor(TYPE.PLAYER));

        for (int i = 0; i < Math.min(player.getNumGenes(), _geneLabels.size()); i++) {
            _geneLabels.get(i).setIcon(IconContainer.getMiniIcon(GenePool.getIconFilename(_player.getGenes().get(i).gene)));
            _geneLabels.get(i).setToolTipText(UrsuppeResource.getString("gene."+_player.getGenes().get(i).name+".name"));
        }

        for (int i = player.getNumGenes(); i < _geneLabels.size(); i++) {
            _geneLabels.get(i).setIcon(null);
            _geneLabels.get(i).setToolTipText("");
        }

        amoebaLabel.setAmoeba(new Amoeba(_player.getID(), -1, _player.getColorID(), new Point(-1,-1)));
    }


   public void showGeneUsed(int playerID, GENE gene) {
       if (_player == null) return;

       if (_player.getID() != playerID || !_player.hasGene(gene)) return;

       for (int i = 0; i < Math.min(_geneLabels.size(),_player.getNumGenes()); i++) {
           if (_player.getGenes().get(i).gene == gene || _player.getGenes().get(i).getPredecessors().contains(gene)) {
               Point loc = new Point(_geneLabels.get(i).getLocation().x-5, _geneLabels.get(i).getLocation().y-5);
               AnimationEffectInstance effect = IconContainer.getEffect("cardused", 33, loc);
               effect.register(this, _effects);
               //GeneActiveThread gat = new GeneActiveThread(_geneLabels.get(i));
               //Thread thread = new Thread(gat);
               //thread.start();
               return;
           }
       }
   }

   public void animateAmoeba() {
       amoebaLabel.startAnimation();
   }

   public void stopAnimation() {
       amoebaLabel.stopAnimation();
   }

    @Override
   public void paintComponent(Graphics g) {
       if (_player != null) {
            g.drawImage(IconContainer.getCustomPlayerBackground(IconContainer.CUSTOM_BACKGROUND_PREFIX+_player.getID(), UrsuppeColor.get(_player.getColorID()).toColor(TYPE.PLAYER)), 0, 0, null);
            draw(BPLabel, g);
            draw(MPLabel, g);
            draw(amoebaLabel, g);
            draw(amoebasLabel, g);
            draw(geneLabel1, g);
            draw(geneLabel2, g);
            draw(geneLabel3, g);
            draw(geneLabel4, g);
            draw(geneLabel5, g);
            draw(geneLabel6, g);
            draw(geneLabel7, g);
            draw(genesLabel, g);
            draw(nameLabel, g);
       } else {
           super.paintComponent(g);
       }
       try {
           for (Effect effect : _effects) {
               effect.paint(g);
           }
       } catch (java.util.ConcurrentModificationException cme) {
           cme.printStackTrace();
           this.repaint();
//            UrsuppeApp.getApplication().getMainFrame().repaint();
//            _gui.repaint();
       }

   }

    private void draw(JLabel label, Graphics g) {
        g.translate(label.getLocation().x, label.getLocation().y);
        try {
            label.paint(g);
        } catch (Exception e) {}
        g.translate(-label.getLocation().x, -label.getLocation().y);
    }

    public void soupBackgroundChanged() {
        amoebaLabel.setAmoeba(amoebaLabel.getAmoeba());
        this.repaint();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        amoebaLabel = new ursuppe.gui.AmoebaLabel();
        nameLabel = new javax.swing.JLabel();
        BPLabel = new javax.swing.JLabel();
        MPLabel = new javax.swing.JLabel();
        amoebasLabel = new javax.swing.JLabel();
        genesLabel = new javax.swing.JLabel();
        geneLabel1 = new javax.swing.JLabel();
        geneLabel2 = new javax.swing.JLabel();
        geneLabel3 = new javax.swing.JLabel();
        geneLabel4 = new javax.swing.JLabel();
        geneLabel5 = new javax.swing.JLabel();
        geneLabel6 = new javax.swing.JLabel();
        geneLabel7 = new javax.swing.JLabel();

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(ursuppe.UrsuppeApp.class).getContext().getResourceMap(PlayerPanel.class);
        setBackground(resourceMap.getColor("Form.background")); // NOI18N
        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        setMaximumSize(new java.awt.Dimension(280, 133));
        setMinimumSize(new java.awt.Dimension(280, 133));
        setName("Form"); // NOI18N
        setPreferredSize(new java.awt.Dimension(280, 133));

        amoebaLabel.setText(resourceMap.getString("amoebaLabel.text")); // NOI18N
        amoebaLabel.setMaximumSize(new java.awt.Dimension(30, 30));
        amoebaLabel.setMinimumSize(new java.awt.Dimension(30, 30));
        amoebaLabel.setName("amoebaLabel"); // NOI18N
        amoebaLabel.setPreferredSize(new java.awt.Dimension(30, 30));

        nameLabel.setFont(resourceMap.getFont("playerName.font")); // NOI18N
        nameLabel.setText(resourceMap.getString("nameLabel.text")); // NOI18N
        nameLabel.setName("nameLabel"); // NOI18N
        nameLabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                nameLabelMouseEntered(evt);
            }
        });

        BPLabel.setFont(resourceMap.getFont("playerPanel.font")); // NOI18N
        BPLabel.setText(resourceMap.getString("BPLabel.text")); // NOI18N
        BPLabel.setMaximumSize(new java.awt.Dimension(46, 24));
        BPLabel.setMinimumSize(new java.awt.Dimension(46, 24));
        BPLabel.setName("BPLabel"); // NOI18N
        BPLabel.setPreferredSize(new java.awt.Dimension(46, 24));

        MPLabel.setFont(resourceMap.getFont("MPLabel.font")); // NOI18N
        MPLabel.setText(resourceMap.getString("MPLabel.text")); // NOI18N
        MPLabel.setMaximumSize(new java.awt.Dimension(46, 24));
        MPLabel.setMinimumSize(new java.awt.Dimension(46, 24));
        MPLabel.setName("MPLabel"); // NOI18N
        MPLabel.setPreferredSize(new java.awt.Dimension(46, 24));

        amoebasLabel.setFont(resourceMap.getFont("amoebasLabel.font")); // NOI18N
        amoebasLabel.setText(resourceMap.getString("amoebasLabel.text")); // NOI18N
        amoebasLabel.setMaximumSize(new java.awt.Dimension(50, 24));
        amoebasLabel.setMinimumSize(new java.awt.Dimension(50, 24));
        amoebasLabel.setName("amoebasLabel"); // NOI18N
        amoebasLabel.setPreferredSize(new java.awt.Dimension(70, 24));

        genesLabel.setFont(resourceMap.getFont("genesLabel.font")); // NOI18N
        genesLabel.setText(resourceMap.getString("genesLabel.text")); // NOI18N
        genesLabel.setMaximumSize(new java.awt.Dimension(50, 24));
        genesLabel.setMinimumSize(new java.awt.Dimension(50, 24));
        genesLabel.setName("genesLabel"); // NOI18N
        genesLabel.setPreferredSize(new java.awt.Dimension(70, 24));

        geneLabel1.setText(resourceMap.getString("geneLabel1.text")); // NOI18N
        geneLabel1.setMaximumSize(new java.awt.Dimension(30, 40));
        geneLabel1.setMinimumSize(new java.awt.Dimension(30, 40));
        geneLabel1.setName("geneLabel1"); // NOI18N
        geneLabel1.setPreferredSize(new java.awt.Dimension(30, 40));
        geneLabel1.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                geneLabelMouseWheelMoved(evt);
            }
        });
        geneLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                geneLabelMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                geneLabelMousePressed(evt);
            }
        });

        geneLabel2.setMaximumSize(new java.awt.Dimension(30, 40));
        geneLabel2.setMinimumSize(new java.awt.Dimension(30, 40));
        geneLabel2.setName("geneLabel2"); // NOI18N
        geneLabel2.setPreferredSize(new java.awt.Dimension(30, 40));
        geneLabel2.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                geneLabelMouseWheelMoved(evt);
            }
        });
        geneLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                geneLabelMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                geneLabelMousePressed(evt);
            }
        });

        geneLabel3.setMaximumSize(new java.awt.Dimension(30, 40));
        geneLabel3.setMinimumSize(new java.awt.Dimension(30, 40));
        geneLabel3.setName("geneLabel3"); // NOI18N
        geneLabel3.setPreferredSize(new java.awt.Dimension(30, 40));
        geneLabel3.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                geneLabelMouseWheelMoved(evt);
            }
        });
        geneLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                geneLabelMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                geneLabelMousePressed(evt);
            }
        });

        geneLabel4.setMaximumSize(new java.awt.Dimension(30, 40));
        geneLabel4.setMinimumSize(new java.awt.Dimension(30, 40));
        geneLabel4.setName("geneLabel4"); // NOI18N
        geneLabel4.setPreferredSize(new java.awt.Dimension(30, 40));
        geneLabel4.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                geneLabelMouseWheelMoved(evt);
            }
        });
        geneLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                geneLabelMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                geneLabelMousePressed(evt);
            }
        });

        geneLabel5.setMaximumSize(new java.awt.Dimension(30, 40));
        geneLabel5.setMinimumSize(new java.awt.Dimension(30, 40));
        geneLabel5.setName("geneLabel5"); // NOI18N
        geneLabel5.setPreferredSize(new java.awt.Dimension(30, 40));
        geneLabel5.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                geneLabelMouseWheelMoved(evt);
            }
        });
        geneLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                geneLabelMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                geneLabelMousePressed(evt);
            }
        });

        geneLabel6.setMaximumSize(new java.awt.Dimension(30, 40));
        geneLabel6.setMinimumSize(new java.awt.Dimension(30, 40));
        geneLabel6.setName("geneLabel6"); // NOI18N
        geneLabel6.setPreferredSize(new java.awt.Dimension(30, 40));
        geneLabel6.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                geneLabelMouseWheelMoved(evt);
            }
        });
        geneLabel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                geneLabelMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                geneLabelMousePressed(evt);
            }
        });

        geneLabel7.setMaximumSize(new java.awt.Dimension(30, 40));
        geneLabel7.setMinimumSize(new java.awt.Dimension(30, 40));
        geneLabel7.setName("geneLabel7"); // NOI18N
        geneLabel7.setPreferredSize(new java.awt.Dimension(30, 40));
        geneLabel7.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                geneLabelMouseWheelMoved(evt);
            }
        });
        geneLabel7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                geneLabelMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                geneLabelMousePressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(nameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(11, 11, 11)
                        .addComponent(amoebaLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(geneLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(geneLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(geneLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(geneLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(geneLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(geneLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(geneLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(BPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(MPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(amoebasLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(genesLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(nameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(amoebaLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(MPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(amoebasLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(genesLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(geneLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(geneLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(geneLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(geneLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(geneLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(geneLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(geneLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void geneLabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_geneLabelMouseEntered
        int i = _geneLabels.indexOf(evt.getSource());

        if (_player != null && i >= 0 && i < _player.getNumGenes()) {
            Gene gene = _player.getGenes().get(i);
            _gui.setControlDescription(gene.toDesciptionString());
        }

    }//GEN-LAST:event_geneLabelMouseEntered

    private void nameLabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nameLabelMouseEntered
        if (_player != null) {
            StringBuffer desc = new StringBuffer("<B>"+_player.getName()+"</B>");
            desc.append("<BR>");
            desc.append(UrsuppeResource.getString("term.genes")+":");
            desc.append("<BR>");
            for (Gene gene : _player.getGenes()) {
                desc.append(UrsuppeResource.getString("gene."+gene.name+".name")+"<BR>");
            }
            _gui.setControlDescription(desc.toString());
            _RmousePressed = false;
        }
    }//GEN-LAST:event_nameLabelMouseEntered

    private void geneLabelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_geneLabelMousePressed
        try {
            //Left click
            if (evt.getButton() == MouseEvent.BUTTON2 || evt.getButton() == MouseEvent.BUTTON3) {
                if (_RmousePressed) {
                    _gui.setGeneDescriptionCarretToHead();
                } else {
                    _gui.setGeneDescriptionCarretToTail();
                }
                _RmousePressed = !_RmousePressed;
            }
        } catch (Exception e) {e.printStackTrace();}
    }//GEN-LAST:event_geneLabelMousePressed

    private void geneLabelMouseWheelMoved(java.awt.event.MouseWheelEvent evt) {//GEN-FIRST:event_geneLabelMouseWheelMoved
       try {
            if (evt.getWheelRotation() > 0) {
                _gui.setGeneDescriptionCarretToTail();
                _RmousePressed = true;
            } else {
                _gui.setGeneDescriptionCarretToHead();
                _RmousePressed = false;
            }
        } catch (Exception e) {e.printStackTrace();}
    }//GEN-LAST:event_geneLabelMouseWheelMoved


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel BPLabel;
    private javax.swing.JLabel MPLabel;
    private ursuppe.gui.AmoebaLabel amoebaLabel;
    private javax.swing.JLabel amoebasLabel;
    private javax.swing.JLabel geneLabel1;
    private javax.swing.JLabel geneLabel2;
    private javax.swing.JLabel geneLabel3;
    private javax.swing.JLabel geneLabel4;
    private javax.swing.JLabel geneLabel5;
    private javax.swing.JLabel geneLabel6;
    private javax.swing.JLabel geneLabel7;
    private javax.swing.JLabel genesLabel;
    private javax.swing.JLabel nameLabel;
    // End of variables declaration//GEN-END:variables



}
