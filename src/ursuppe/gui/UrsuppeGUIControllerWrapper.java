/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.gui;

import java.util.ArrayList;
import java.util.List;
import ursuppe.ai.AIPlayer;
import ursuppe.control.UrsuppeController;
import ursuppe.game.EnvironmentCard;
import ursuppe.game.GameState;
import ursuppe.game.GameState.DIRECTION;

/**
 * Wrapper for selecting between multiple controllers.
 * Used to toggle between AI and player
 */
public class UrsuppeGUIControllerWrapper implements UrsuppeController {

    private List<UrsuppeController> controllers;
    private int currentController;

    public UrsuppeGUIControllerWrapper(UrsuppeController controller) {
        if (controller == null) {
            throw new RuntimeException("Wrapper must have at least one controller.");
        }
        this.controllers = new ArrayList<>(2);
        this.controllers.add(controller);
        this.currentController = 0;
    }

    public UrsuppeGUIControllerWrapper(List<UrsuppeController> controllers) {
        if (controllers == null || controllers.isEmpty()) {
            throw new RuntimeException("Wrapper must have at least one controller.");
        }
        this.controllers = controllers;
        this.currentController = 0;
    }

    public int numControllers() {
        return controllers.size();
    }

    public void addController(UrsuppeController controller) {
        if (controller != null)
            controllers.add(controller);
    }

    public int setCurrentController(int id) {
        if (0 <= id && id < controllers.size()) {
            currentController = id;
        }

        return currentController;
    }

    public void setID(int id) {
        for (UrsuppeController controller : controllers) {
            controller.setID(id);
        }
    }

    public void gameReset() {
        for (UrsuppeController controller : controllers) {
            controller.gameReset();
        }
    }
    
    public void selectInitialScore(int playerID, GameState game) {
        controllers.get(currentController).selectInitialScore(playerID, game);
    }

    public void selectInitialAmoeba(int playerID, GameState game) {
        controllers.get(currentController).selectInitialAmoeba(playerID, game);
    }

    public void moveResponse(int playerID, int amoebaIndex, List<DIRECTION> dir, GameState game) {
        controllers.get(currentController).moveResponse(playerID, amoebaIndex, dir, game);
    }

    public void selectPhase1Action(int playerID, int amoebaIndex, GameState game) {
       controllers.get(currentController).selectPhase1Action(playerID, amoebaIndex, game);
    }

    public void handleGeneDefect(int playerID, GameState game) {
        controllers.get(currentController).handleGeneDefect(playerID, game);
    }

    public void selectPhase3Action(int playerID, GameState game) {
        controllers.get(currentController).selectPhase3Action(playerID, game);
    }

    public void selectPhase4Action(int playerID, GameState game) {
        controllers.get(currentController).selectPhase4Action(playerID, game);
    }

    public void death(int playerID, int amoebaID, GameState game) {
        controllers.get(currentController).death(playerID, amoebaID, game);
    }

    public void scoring(int playerID, GameState game) {
        controllers.get(currentController).scoring(playerID, game);
    }

    public void decideHOL(int playerID, int amoebaIndex, DIRECTION dir, GameState game) {
        controllers.get(currentController).decideHOL(playerID, amoebaIndex, dir, game);
    }

    public void decidePOP(int playerID, GameState game) {
        controllers.get(currentController).decidePOP(playerID, game);
    }

    public void showNextEnvironment(EnvironmentCard env) {
        controllers.get(currentController).showNextEnvironment(env);
    }

    public void decideAGR(int playerID, GameState game) {
        controllers.get(currentController).decideAGR(playerID, game);
    }

    public void defenderAction(int attackerID, int attackerAmoebaNum, int defenderID, int defenderAmoebaNum, GameState game) {
        controllers.get(currentController).defenderAction(attackerID, attackerAmoebaNum, defenderID, defenderAmoebaNum, game);
    }

    public void attackerAction(int attackerID, int attackerAmoebaNum, int defenderID, int defenderAmoebaNum, boolean HARD, boolean MOUexcretes, GameState game) {
        controllers.get(currentController).attackerAction(attackerID, attackerAmoebaNum, defenderID, defenderAmoebaNum, HARD, MOUexcretes, game);
    }

    public void selectEscapeDirection(int defenderID, int defenderAmoebaNum, List<DIRECTION> dir, GameState game) {
        controllers.get(currentController).selectEscapeDirection(defenderID, defenderAmoebaNum, dir, game);
    }

    public void decideESCSPD(int defenderID, GameState game) {
        controllers.get(currentController).decideESCSPD(defenderID, game);
    }

    public void decideSFSBANG(int defenderID, int defenderAmoebaNum, GameState game) {
        controllers.get(currentController).decideSFSBANG(defenderID, defenderAmoebaNum, game);
    }

}
