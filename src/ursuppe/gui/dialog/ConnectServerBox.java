/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ConnectServerBox.java
 *
 * Created on 2010/01/15, 17:02:13
 */

package ursuppe.gui.dialog;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import ursuppe.PropertyContainer;
import ursuppe.UrsuppeApp;
import ursuppe.UrsuppeResource;
import ursuppe.gui.UrsuppeColor;
import ursuppe.gui.UrsuppeColor.TYPE;
import ursuppe.gui.UrsuppeGUI;
import ursuppe.gui.icon.IconContainer;

/**
 *
 * @author Personal
 */
public class ConnectServerBox extends javax.swing.JDialog {

    private final UrsuppeGUI _gui;
    private boolean _ctrlPressed;
    private boolean _vPressed;
    
    private JPanel colorSelection1[];
    private JPanel colorSelection2[];
    
    private int preferredColor1 = -1;
    private int preferredColor2 = -1;

    private final boolean _localViewMode;

    /** Creates new form ConnectServerBox */
    public ConnectServerBox(java.awt.Frame parent, UrsuppeGUI gui, boolean localViewMode) {
        super(parent, true);
        _gui = gui;
        _localViewMode = localViewMode;
        initComponents();
        initializeAmoebaIconCBox();
        initializePreferredColorSelection();

        String name = PropertyContainer.getProperty(PropertyContainer.HISTORY_FILE, "connect.name");
        String host = PropertyContainer.getProperty(PropertyContainer.HISTORY_FILE, "connect.host");
        String port = PropertyContainer.getProperty(PropertyContainer.HISTORY_FILE, "connect.port");
        String amoeba = PropertyContainer.getProperty(PropertyContainer.HISTORY_FILE, "connect.amoeba");
        String bg = PropertyContainer.getProperty(PropertyContainer.HISTORY_FILE, "connect.playerBG");

        initializeFields(name, host, port, amoeba, bg, localViewMode);
    }

    public ConnectServerBox(java.awt.Frame parent, UrsuppeGUI gui, int port, boolean localViewMode) {
        super(parent, true);
        _gui = gui;
        _localViewMode = localViewMode;
        initComponents();
        initializeAmoebaIconCBox();
        initializePreferredColorSelection();

        initializeFields(null, "localhost", Integer.toString(port), null, null, localViewMode);

    }

    private void initializeFields(String name, String host, String port, String amoeba, String bg, boolean localViewMode) {

        if (name != null) {
            playerNameTextField.setText(name);
        }
        if (host != null) {
            hostNameTextField.setText(host);
        }
        if (port != null) {
            portTextField.setText(port);
        }
        if (amoeba != null) {
            amoebaIconCBox.setSelectedItem(amoeba);
        }
        if (bg != null) {
            playerBGCBox.setSelectedItem(bg);
        }


        if (localViewMode) {
            hostNameTextField.setEnabled(false);
            portTextField.setEnabled(false);
            watchButton.setEnabled(false);
        }
    }

    private void initializeAmoebaIconCBox() {
        for (String str : IconContainer.getCustomIconNames()) {
            if (str.compareTo(IconContainer.DEFAULT_CUSTOM_ICON) != 0) {
                amoebaIconCBox.addItem(str);
            }
        }
        for (String str : IconContainer.getCustomPlayerBackgroundNames()) {
            if (str.compareTo(IconContainer.DEFAULT_BACKGROUND) != 0) {
                playerBGCBox.addItem(str);
            }
        }
    }
    
    private void initializePreferredColorSelection() {
            colorSelection1 = new JPanel[] { preferredColor1a, preferredColor1b, preferredColor1c, preferredColor1d, preferredColor1e, preferredColor1f };
            colorSelection2 = new JPanel[] { preferredColor2a, preferredColor2b, preferredColor2c, preferredColor2d, preferredColor2e, preferredColor2f };

            List<UrsuppeColor> colors = UrsuppeColor.getPlayerColors();
            
            if (colors.size() != colorSelection1.length) throw new RuntimeException("Assert failed");
            
            for (int i = 0; i < 6; i++) {
                colorSelection1[i].setBackground(colors.get(i).toColor(TYPE.PLAYER));
                colorSelection2[i].setBackground(colors.get(i).toColor(TYPE.PLAYER));
                colorSelection1[i].setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
                colorSelection2[i].setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
                
                final int colID = i;
                colorSelection1[i].addMouseListener(new MouseListener() {
                    @Override
                    public void mouseClicked(MouseEvent e) {}
                    @Override
                    public void mousePressed(MouseEvent e) {}
                    @Override
                    public void mouseReleased(MouseEvent e) {
                        preferredColor1Selected(colID);
                    }
                    @Override
                    public void mouseEntered(MouseEvent e) {}
                    @Override
                    public void mouseExited(MouseEvent e) {}
                });
                colorSelection2[i].addMouseListener(new MouseListener() {
                    @Override
                    public void mouseClicked(MouseEvent e) {}
                    @Override
                    public void mousePressed(MouseEvent e) {}
                    @Override
                    public void mouseReleased(MouseEvent e) {
                        preferredColor2Selected(colID);
                    }
                    @Override
                    public void mouseEntered(MouseEvent e) {}
                    @Override
                    public void mouseExited(MouseEvent e) {}
                });
            }
            
            
            // load stored setting
            final String prefCol1 = PropertyContainer.getProperty(PropertyContainer.HISTORY_FILE, "connect.preferredColor1");
            final String prefCol2 = PropertyContainer.getProperty(PropertyContainer.HISTORY_FILE, "connect.preferredColor2");
            
            if (prefCol1 != null) {
                try {
                    preferredColor1Selected(Integer.parseInt(prefCol1));
                } catch (Exception e) {
                    UrsuppeApp.debug(e);
                }
            }
            if (prefCol2 != null) {
                try {
                    preferredColor2Selected(Integer.parseInt(prefCol2));
                } catch (Exception e) {
                    UrsuppeApp.debug(e);
                }
            }
            
            
    }
    
    private void preferredColor1Selected(int colID) {
        
            for (int i = 0; i < 6; i++) {
                if (colID == i) {
                    colorSelection1[i].setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED, Color.BLACK, Color.BLACK));
                    preferredColor1 = colID;
                } else {
                    colorSelection1[i].setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
                }
            }
        
    }
    private void preferredColor2Selected(int colID) {
        
            for (int i = 0; i < 6; i++) {
                if (colID == i) {
                    colorSelection2[i].setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED, Color.BLACK, Color.BLACK));
                    preferredColor2 = colID;
                } else {
                    colorSelection2[i].setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
                }
            }
    }
    

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        modeGroup = new javax.swing.ButtonGroup();
        connectServerLabel = new javax.swing.JLabel();
        playerNameLabel = new javax.swing.JLabel();
        playerNameTextField = new javax.swing.JTextField();
        hostNameLabel = new javax.swing.JLabel();
        hostNameTextField = new javax.swing.JTextField();
        portLabel = new javax.swing.JLabel();
        portTextField = new javax.swing.JTextField();
        okButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        joinButton = new javax.swing.JRadioButton();
        watchButton = new javax.swing.JRadioButton();
        amoebaIconLabel = new javax.swing.JLabel();
        amoebaIconCBox = new javax.swing.JComboBox();
        playerBGLabel = new javax.swing.JLabel();
        playerBGCBox = new javax.swing.JComboBox();
        preferredColorLabel1 = new javax.swing.JLabel();
        preferredColor1a = new javax.swing.JPanel();
        preferredColor1b = new javax.swing.JPanel();
        preferredColor1c = new javax.swing.JPanel();
        preferredColor1d = new javax.swing.JPanel();
        preferredColor1e = new javax.swing.JPanel();
        preferredColor1f = new javax.swing.JPanel();
        preferredColorLabel2 = new javax.swing.JLabel();
        preferredColor2a = new javax.swing.JPanel();
        preferredColor2b = new javax.swing.JPanel();
        preferredColor2c = new javax.swing.JPanel();
        preferredColor2d = new javax.swing.JPanel();
        preferredColor2e = new javax.swing.JPanel();
        preferredColor2f = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(ursuppe.UrsuppeApp.class).getContext().getResourceMap(ConnectServerBox.class);
        setTitle(resourceMap.getString("connectServerBox.title")); // NOI18N
        setName("Form"); // NOI18N
        setResizable(false);

        connectServerLabel.setFont(resourceMap.getFont("dialogEmphasized.font")); // NOI18N
        connectServerLabel.setText(resourceMap.getString("connectServerLabel.text")); // NOI18N
        connectServerLabel.setName("connectServerLabel"); // NOI18N
        connectServerLabel.setPreferredSize(new java.awt.Dimension(100, 24));

        playerNameLabel.setFont(resourceMap.getFont("dialog.font")); // NOI18N
        playerNameLabel.setText(resourceMap.getString("playerNameLabel.text")); // NOI18N
        playerNameLabel.setName("playerNameLabel"); // NOI18N

        playerNameTextField.setText(resourceMap.getString("playerNameTextField.text")); // NOI18N
        playerNameTextField.setName("playerNameTextField"); // NOI18N

        hostNameLabel.setFont(resourceMap.getFont("dialog.font")); // NOI18N
        hostNameLabel.setText(resourceMap.getString("hostNameLabel.text")); // NOI18N
        hostNameLabel.setName("hostNameLabel"); // NOI18N

        hostNameTextField.setText(resourceMap.getString("hostNameTextField.text")); // NOI18N
        hostNameTextField.setName("hostNameTextField"); // NOI18N
        hostNameTextField.setPreferredSize(new java.awt.Dimension(100, 19));
        hostNameTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                hostNameTextFieldKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                hostNameTextFieldKeyReleased(evt);
            }
        });

        portLabel.setFont(resourceMap.getFont("dialog.font")); // NOI18N
        portLabel.setText(resourceMap.getString("portLabel.text")); // NOI18N
        portLabel.setName("portLabel"); // NOI18N

        portTextField.setText(resourceMap.getString("portTextField.text")); // NOI18N
        portTextField.setName("portTextField"); // NOI18N
        portTextField.setPreferredSize(new java.awt.Dimension(60, 19));

        okButton.setText(resourceMap.getString("term.ok")); // NOI18N
        okButton.setName("okButton"); // NOI18N
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        cancelButton.setText(resourceMap.getString("term.cancel")); // NOI18N
        cancelButton.setName("cancelButton"); // NOI18N
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        modeGroup.add(joinButton);
        joinButton.setFont(resourceMap.getFont("dialog.font")); // NOI18N
        joinButton.setSelected(true);
        joinButton.setText(resourceMap.getString("joinButton.text")); // NOI18N
        joinButton.setName("joinButton"); // NOI18N

        modeGroup.add(watchButton);
        watchButton.setFont(resourceMap.getFont("dialog.font")); // NOI18N
        watchButton.setText(resourceMap.getString("watchButton.text")); // NOI18N
        watchButton.setName("watchButton"); // NOI18N

        amoebaIconLabel.setFont(resourceMap.getFont("dialog.font")); // NOI18N
        amoebaIconLabel.setText(resourceMap.getString("amoebaIconLabel.text")); // NOI18N
        amoebaIconLabel.setName("amoebaIconLabel"); // NOI18N

        amoebaIconCBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "default" }));
        amoebaIconCBox.setName("amoebaIconCBox"); // NOI18N

        playerBGLabel.setFont(resourceMap.getFont("dialog.font")); // NOI18N
        playerBGLabel.setText(resourceMap.getString("playerBGLabel.text")); // NOI18N
        playerBGLabel.setName("playerBGLabel"); // NOI18N

        playerBGCBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "default" }));
        playerBGCBox.setName("playerBGCBox"); // NOI18N

        preferredColorLabel1.setFont(resourceMap.getFont("dialog.font")); // NOI18N
        preferredColorLabel1.setText(resourceMap.getString("dialog.connect.preferredColor1")); // NOI18N
        preferredColorLabel1.setName("preferredColorLabel1"); // NOI18N

        preferredColor1a.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        preferredColor1a.setName("preferredColor1a"); // NOI18N

        preferredColor1b.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        preferredColor1b.setName("preferredColor1b"); // NOI18N

        preferredColor1c.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        preferredColor1c.setName("preferredColor1c"); // NOI18N

        preferredColor1d.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        preferredColor1d.setName("preferredColor1d"); // NOI18N

        preferredColor1e.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        preferredColor1e.setName("preferredColor1e"); // NOI18N

        preferredColor1f.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        preferredColor1f.setName("preferredColor1f"); // NOI18N

        preferredColorLabel2.setFont(resourceMap.getFont("dialog.font")); // NOI18N
        preferredColorLabel2.setText(resourceMap.getString("dialog.connect.preferredColor2")); // NOI18N
        preferredColorLabel2.setName("preferredColorLabel2"); // NOI18N

        preferredColor2a.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        preferredColor2a.setName("preferredColor2a"); // NOI18N

        preferredColor2b.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        preferredColor2b.setName("preferredColor2b"); // NOI18N

        preferredColor2c.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        preferredColor2c.setName("preferredColor2c"); // NOI18N

        preferredColor2d.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        preferredColor2d.setName("preferredColor2d"); // NOI18N

        preferredColor2e.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        preferredColor2e.setName("preferredColor2e"); // NOI18N

        preferredColor2f.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        preferredColor2f.setName("preferredColor2f"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(preferredColorLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(preferredColorLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(connectServerLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(hostNameLabel)
                                    .addComponent(portLabel)
                                    .addComponent(playerNameLabel)
                                    .addComponent(amoebaIconLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(portTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(hostNameTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
                                    .addComponent(playerNameTextField)
                                    .addComponent(amoebaIconCBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(playerBGCBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(playerBGLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 84, Short.MAX_VALUE)
                        .addGap(168, 168, 168))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(preferredColor1a, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(preferredColor1b, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(preferredColor1c, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(preferredColor1d, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(preferredColor1e, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(preferredColor1f, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(okButton)
                                    .addComponent(joinButton))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cancelButton)
                                    .addComponent(watchButton)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(preferredColor2a, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(preferredColor2b, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(preferredColor2c, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(preferredColor2d, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(preferredColor2e, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(preferredColor2f, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(connectServerLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(playerNameLabel)
                    .addComponent(playerNameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hostNameLabel)
                    .addComponent(hostNameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(portLabel)
                    .addComponent(portTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(amoebaIconCBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(amoebaIconLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(playerBGLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(playerBGCBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(preferredColorLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(preferredColor1a, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(preferredColor1b, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(preferredColor1c, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(preferredColor1d, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(preferredColor1e, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(preferredColor1f, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addComponent(preferredColorLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(preferredColor2a, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(preferredColor2b, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(preferredColor2c, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(preferredColor2d, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(preferredColor2e, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(preferredColor2f, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(joinButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(okButton))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(watchButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cancelButton)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        
        try {
            String name = playerNameTextField.getText();
            if (name.length() == 0 || name.matches("^\\s*$")) {
                JOptionPane.showMessageDialog(this, UrsuppeResource.getString("error.invalidName"));
                return;
            }

            if (name.length() > 20) {
                JOptionPane.showMessageDialog(this, UrsuppeResource.getString("error.nameTooLong"));
                return;
            }

            String host = hostNameTextField.getText();

            int port = Integer.parseInt(portTextField.getText());
            if (port < 0 || port > 65535) {
                JOptionPane.showMessageDialog(this, UrsuppeResource.getString("error.portOutOfRange"));
                return;
            }
            String icon = null;
            if (amoebaIconCBox.getSelectedIndex() > 0) icon = (String)amoebaIconCBox.getSelectedItem();
            String bg = null;
            if (playerBGCBox.getSelectedIndex() > 0) bg = (String)playerBGCBox.getSelectedItem();

            if (_localViewMode) {
                _gui.addPlayer(name, host, port, icon, bg, preferredColor1, preferredColor2);
            } else {

                PropertyContainer.setProperty(PropertyContainer.HISTORY_FILE, "connect.name", name);
                PropertyContainer.setProperty(PropertyContainer.HISTORY_FILE, "connect.host", host);
                PropertyContainer.setProperty(PropertyContainer.HISTORY_FILE, "connect.port", port+"");
                if (icon != null) {
                    PropertyContainer.setProperty(PropertyContainer.HISTORY_FILE, "connect.amoeba", icon);
                } else {
                    PropertyContainer.removeProperty(PropertyContainer.HISTORY_FILE, "connect.amoeba");
                }
                if (bg != null) {
                    PropertyContainer.setProperty(PropertyContainer.HISTORY_FILE, "connect.playerBG", bg);
                } else {
                    PropertyContainer.removeProperty(PropertyContainer.HISTORY_FILE, "connect.playerBG");
                }

                //preferred color
                PropertyContainer.setProperty(PropertyContainer.HISTORY_FILE, "connect.preferredColor1", preferredColor1+"");
                PropertyContainer.setProperty(PropertyContainer.HISTORY_FILE, "connect.preferredColor2", preferredColor2+"");

                PropertyContainer.saveProperties();


                _gui.connectServer(name, host, port, joinButton.isSelected(), icon, bg, preferredColor1, preferredColor2);
            }

        } catch (NumberFormatException nfe) {
                JOptionPane.showMessageDialog(this, UrsuppeResource.getString("error.invalidPort"));
        }
    }//GEN-LAST:event_okButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void hostNameTextFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_hostNameTextFieldKeyPressed
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_CONTROL) _ctrlPressed = true;
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_V) _vPressed = true;
    }//GEN-LAST:event_hostNameTextFieldKeyPressed

    private void hostNameTextFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_hostNameTextFieldKeyReleased
        boolean check = false;
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_CONTROL) {
            if (_ctrlPressed && _vPressed) check = true;
            _ctrlPressed = false;
        }
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_V) {
            if (_ctrlPressed && _vPressed) check = true;
            _vPressed = false;
        }

        if (check) {
            String str = hostNameTextField.getText();
            if (str.matches(".+:\\d{1,5}")) {
                String[] split = str.split(":");
                if (split.length == 2) {
                    hostNameTextField.setText(split[0]);
                    portTextField.setText(split[1]);
                }
            }
        }
    }//GEN-LAST:event_hostNameTextFieldKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox amoebaIconCBox;
    private javax.swing.JLabel amoebaIconLabel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JLabel connectServerLabel;
    private javax.swing.JLabel hostNameLabel;
    private javax.swing.JTextField hostNameTextField;
    private javax.swing.JRadioButton joinButton;
    private javax.swing.ButtonGroup modeGroup;
    private javax.swing.JButton okButton;
    private javax.swing.JComboBox playerBGCBox;
    private javax.swing.JLabel playerBGLabel;
    private javax.swing.JLabel playerNameLabel;
    private javax.swing.JTextField playerNameTextField;
    private javax.swing.JLabel portLabel;
    private javax.swing.JTextField portTextField;
    private javax.swing.JPanel preferredColor1a;
    private javax.swing.JPanel preferredColor1b;
    private javax.swing.JPanel preferredColor1c;
    private javax.swing.JPanel preferredColor1d;
    private javax.swing.JPanel preferredColor1e;
    private javax.swing.JPanel preferredColor1f;
    private javax.swing.JPanel preferredColor2a;
    private javax.swing.JPanel preferredColor2b;
    private javax.swing.JPanel preferredColor2c;
    private javax.swing.JPanel preferredColor2d;
    private javax.swing.JPanel preferredColor2e;
    private javax.swing.JPanel preferredColor2f;
    private javax.swing.JLabel preferredColorLabel1;
    private javax.swing.JLabel preferredColorLabel2;
    private javax.swing.JRadioButton watchButton;
    // End of variables declaration//GEN-END:variables

}
