/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * FoodSelectionPanel.java
 *
 * Created on 2010/01/21, 19:36:12
 */

package ursuppe.gui.dialog;

import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import ursuppe.gui.UrsuppeColor;
import ursuppe.gui.UrsuppeColor.TYPE;

/**
 *
 * @author Personal
 */
public class FoodSelectionPanel extends javax.swing.JPanel {

    private int _colorID;
    private int _size;
    private int _max;
    private JPanel[] _foodPanels;
    private int _selected;

    private final Color _defaultColor;

    /** Creates new form FoodSelectionPanel */
    public FoodSelectionPanel() {
        initComponents();

        _defaultColor = foodNumPanel.getBackground();
        //this.plusButton.ass
    }

    public void initialize(int colorID, int size, int avail) {
        _colorID = colorID;
        _size = size;
        _max = Math.min(size, avail);

        setLayout(new java.awt.GridLayout(4+_size, 0));
        
        _foodPanels = new JPanel[_size];
        for (int i = 0; i < _size; i++) {
            _foodPanels[i] = new JPanel();
            if (i < _max) _foodPanels[i].setBorder(BorderFactory.createLineBorder(Color.black));
            this.add(_foodPanels[i], 1);
        }

        colorPanel.setBackground(UrsuppeColor.get(_colorID).toColor(TYPE.FOOD));
       // this.getLayout().
    }

    public void setMax(int max) {
        _max = max;
        for (int i = 0; i < _size; i++) {
            if (i < _max) {
                _foodPanels[i].setBorder(BorderFactory.createLineBorder(Color.black));
            } else {
                _foodPanels[i].setBorder(null);
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        foodNumPanel = new javax.swing.JPanel();
        foodNumLabel = new javax.swing.JLabel();
        plusButton = new javax.swing.JButton();
        minusButton = new javax.swing.JButton();
        colorPanel = new javax.swing.JPanel();

        setName("Form"); // NOI18N
        setLayout(new java.awt.GridLayout(7, 0));

        foodNumPanel.setMaximumSize(new java.awt.Dimension(20, 20));
        foodNumPanel.setMinimumSize(new java.awt.Dimension(20, 20));
        foodNumPanel.setName("foodNumPanel"); // NOI18N
        foodNumPanel.setPreferredSize(new java.awt.Dimension(20, 20));

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(ursuppe.UrsuppeApp.class).getContext().getResourceMap(FoodSelectionPanel.class);
        foodNumLabel.setFont(resourceMap.getFont("foodNumLabel.font")); // NOI18N
        foodNumLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        foodNumLabel.setText(resourceMap.getString("foodNumLabel.text")); // NOI18N
        foodNumLabel.setName("foodNumLabel"); // NOI18N

        javax.swing.GroupLayout foodNumPanelLayout = new javax.swing.GroupLayout(foodNumPanel);
        foodNumPanel.setLayout(foodNumPanelLayout);
        foodNumPanelLayout.setHorizontalGroup(
            foodNumPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(foodNumLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 21, Short.MAX_VALUE)
        );
        foodNumPanelLayout.setVerticalGroup(
            foodNumPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(foodNumLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
        );

        add(foodNumPanel);

        plusButton.setText(resourceMap.getString("plusButton.text")); // NOI18N
        plusButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        plusButton.setMaximumSize(new java.awt.Dimension(20, 20));
        plusButton.setMinimumSize(new java.awt.Dimension(20, 20));
        plusButton.setName("plusButton"); // NOI18N
        plusButton.setPreferredSize(new java.awt.Dimension(20, 20));
        add(plusButton);

        minusButton.setText(resourceMap.getString("minusButton.text")); // NOI18N
        minusButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        minusButton.setMaximumSize(new java.awt.Dimension(20, 20));
        minusButton.setMinimumSize(new java.awt.Dimension(20, 20));
        minusButton.setName("minusButton"); // NOI18N
        minusButton.setPreferredSize(new java.awt.Dimension(20, 20));
        add(minusButton);

        colorPanel.setMaximumSize(new java.awt.Dimension(20, 20));
        colorPanel.setMinimumSize(new java.awt.Dimension(20, 20));
        colorPanel.setName("colorPanel"); // NOI18N
        colorPanel.setPreferredSize(new java.awt.Dimension(20, 20));

        javax.swing.GroupLayout colorPanelLayout = new javax.swing.GroupLayout(colorPanel);
        colorPanel.setLayout(colorPanelLayout);
        colorPanelLayout.setHorizontalGroup(
            colorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 21, Short.MAX_VALUE)
        );
        colorPanelLayout.setVerticalGroup(
            colorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 25, Short.MAX_VALUE)
        );

        add(colorPanel);
    }// </editor-fold>//GEN-END:initComponents

    public void update(int num, boolean capped) {
        if (_foodPanels == null) return;

        _selected = num;

        //minus
        if (_selected == 0) {
            minusButton.setEnabled(false);
        } else {
            minusButton.setEnabled(true);
        }
        //plus
        if (_selected >= _max) {
            plusButton.setEnabled(false);
        } else {
            plusButton.setEnabled(!capped);
        }


        //food colors
        for (int i = 0; i < _selected; i++) {
            _foodPanels[i].setBackground(UrsuppeColor.get(_colorID).toColor(TYPE.FOOD));
        }
        for (int i = _selected; i < _foodPanels.length; i++) {
            _foodPanels[i].setBackground(_defaultColor);
        }

        foodNumLabel.setText(Integer.toString(_selected));
    }

    public JButton getPlusButton() {
        return plusButton;
    }

    public JButton getMinusButton() {
        return minusButton;
    }

    public int getSelectedNumbser() {
        return _selected;
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel colorPanel;
    private javax.swing.JLabel foodNumLabel;
    private javax.swing.JPanel foodNumPanel;
    private javax.swing.JButton minusButton;
    private javax.swing.JButton plusButton;
    // End of variables declaration//GEN-END:variables

}
