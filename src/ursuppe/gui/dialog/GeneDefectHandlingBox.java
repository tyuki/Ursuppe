/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * GeneDeffectHandlingBox.java
 *
 * Created on 2010/01/21, 10:26:25
 */

package ursuppe.gui.dialog;

import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import ursuppe.UrsuppeResource;
import ursuppe.game.GameState;
import ursuppe.game.Gene;
import ursuppe.game.GenePool;
import ursuppe.game.GenePool.GENE;
import ursuppe.game.Player;
import ursuppe.gui.UrsuppeGUI;
import ursuppe.gui.icon.IconContainer;

/**
 *
 * @author Personal
 */
public class GeneDefectHandlingBox extends javax.swing.JDialog {

    private UrsuppeGUI _gui;
    boolean[] _selected;
    private Player _player;
    private GameState _game;

    /** Creates new form GeneDeffectHandlingBox */
    public GeneDefectHandlingBox(java.awt.Frame parent, UrsuppeGUI gui, Player player, GameState game) {
        super(parent, false);
        initComponents();

        _gui = gui;
        _player = player;
        _game = game;

        //Genes
        _selected = new boolean[_player.getNumGenes()];
        for (int i = 0; i < _player.getNumGenes(); i++) {
            Gene gene = player.getGenes().get(i);
            JLabel geneLabel = new JLabel();
            geneLabel.setPreferredSize(new Dimension(60,120));
            geneLabel.setVerticalAlignment(SwingConstants.BOTTOM);
            geneLabel.setIcon(IconContainer.getIcon(GenePool.getIconFilename(gene.gene)));
            geneLabel.setText(Integer.toString(i));
            geneLabel.setToolTipText(UrsuppeResource.getString("gene."+gene.name+".name"));
            geneLabel.addMouseListener(new MouseListener() {

                public void mouseClicked(MouseEvent e) {
                    //throw new UnsupportedOperationException("Not supported yet.");
                }

                public void mousePressed(MouseEvent e) {
                    genePressed(e);
                }

                public void mouseReleased(MouseEvent e) {
                    //throw new UnsupportedOperationException("Not supported yet.");
                }

                public void mouseEntered(MouseEvent e) {
                    geneEntered(e);
                }

                public void mouseExited(MouseEvent e) {
                    //throw new UnsupportedOperationException("Not supported yet.");
                }
            });
            genesPanel.add(geneLabel);
        }


        updateState();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        geneDefectHandlingLabel = new javax.swing.JLabel();
        geneDefectDescription1 = new javax.swing.JLabel();
        geneDefectDescription2 = new javax.swing.JLabel();
        genesPanel = new javax.swing.JPanel();
        exceedingMPLabel = new javax.swing.JLabel();
        discardValueLabel = new javax.swing.JLabel();
        requiredBPLabel = new javax.swing.JLabel();
        okButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(ursuppe.UrsuppeApp.class).getContext().getResourceMap(GeneDefectHandlingBox.class);
        setTitle(resourceMap.getString("dialog.gdh.title")); // NOI18N
        setAlwaysOnTop(true);
        setName("Form"); // NOI18N
        setResizable(false);

        geneDefectHandlingLabel.setFont(resourceMap.getFont("dialogEmphasized.font")); // NOI18N
        geneDefectHandlingLabel.setText(resourceMap.getString("dialog.gdh.title")); // NOI18N
        geneDefectHandlingLabel.setName("geneDefectHandlingLabel"); // NOI18N

        geneDefectDescription1.setFont(resourceMap.getFont("dialog.font")); // NOI18N
        geneDefectDescription1.setText(resourceMap.getString("dialog.gdh.description1")); // NOI18N
        geneDefectDescription1.setName("geneDefectDescription1"); // NOI18N

        geneDefectDescription2.setFont(resourceMap.getFont("dialog.font")); // NOI18N
        geneDefectDescription2.setText(resourceMap.getString("dialog.gdh.description2")); // NOI18N
        geneDefectDescription2.setName("geneDefectDescription2"); // NOI18N

        genesPanel.setName("genesPanel"); // NOI18N
        genesPanel.setPreferredSize(new java.awt.Dimension(376, 130));
        genesPanel.setRequestFocusEnabled(false);

        exceedingMPLabel.setFont(resourceMap.getFont("exceedingMPLabel.font")); // NOI18N
        exceedingMPLabel.setText(resourceMap.getString("dialog.gdh.exceedingMP")); // NOI18N
        exceedingMPLabel.setName("exceedingMPLabel"); // NOI18N

        discardValueLabel.setFont(resourceMap.getFont("discardValueLabel.font")); // NOI18N
        discardValueLabel.setText(resourceMap.getString("dialog.gdh.discardValue")); // NOI18N
        discardValueLabel.setName("discardValueLabel"); // NOI18N

        requiredBPLabel.setText(resourceMap.getString("dialog.gdh.requiredBP")); // NOI18N
        requiredBPLabel.setName("requiredBPLabel"); // NOI18N

        okButton.setText(resourceMap.getString("term.ok")); // NOI18N
        okButton.setMargin(new java.awt.Insets(7, 14, 7, 14));
        okButton.setName("okButton"); // NOI18N
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(genesPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE)
                    .addComponent(geneDefectDescription2, javax.swing.GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE)
                    .addComponent(geneDefectHandlingLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE)
                    .addComponent(geneDefectDescription1, javax.swing.GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(exceedingMPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(discardValueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(okButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(requiredBPLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 101, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(geneDefectHandlingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(geneDefectDescription1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(geneDefectDescription2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(genesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(exceedingMPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(discardValueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(requiredBPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(okButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void genePressed(java.awt.event.MouseEvent evt) {
        try {
            int i = Integer.parseInt(((JLabel)evt.getSource()).getText());
            _selected[i] = !_selected[i];
            if (_selected[i]) {
                ((JLabel)evt.getSource()).setVerticalAlignment(SwingConstants.TOP);
            } else {
                ((JLabel)evt.getSource()).setVerticalAlignment(SwingConstants.BOTTOM);
            }
            updateState();
        } catch (Exception e) {}
        
    }

    private void updateState() {
        
        int excess = _player.getMP() - _game.getCurrentEnvironment().maxMP;
        exceedingMPLabel.setText(UrsuppeResource.getString("dialog.gdh.exceedingMP")+Integer.toString(excess));

        int discardValue = 0;
        for (int i = 0; i < _player.getNumGenes(); i++) {
            if (_selected[i]) {
                discardValue += _player.getGenes().get(i).getMPReleaseValue();
            }
        }
        discardValueLabel.setText(UrsuppeResource.getString("dialog.gdh.discardValue")+Integer.toString(discardValue));

        int remaining  = excess - discardValue;
        requiredBPLabel.setText(UrsuppeResource.getString("dialog.gdh.requiredBP")+Integer.toString(remaining));

        if (remaining > _player.getBP()) {
            okButton.setEnabled(false);
        } else {
            okButton.setEnabled(true);
        }
    }

    private void geneEntered(java.awt.event.MouseEvent evt) {
        try {
            int i = Integer.parseInt(((JLabel)evt.getSource()).getText());
            Gene gene = _player.getGenes().get(i);
            _gui.setControlDescription(gene.toDesciptionString());
        } catch (Exception e) {}
    }

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed

        List<GENE> genes = new LinkedList<>();
        for (int i = 0; i < _selected.length; i++) {
            if (_selected[i]) genes.add(_player.getGenes().get(i).gene);
        }

        GENE[] discards = new GENE[genes.size()];

        for (int i = 0; i < discards.length; i++) {
            discards[i] = genes.get(i);
        }

        _gui.geneDefectHandled(discards);
        this.dispose();
    }//GEN-LAST:event_okButtonActionPerformed

    


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel discardValueLabel;
    private javax.swing.JLabel exceedingMPLabel;
    private javax.swing.JLabel geneDefectDescription1;
    private javax.swing.JLabel geneDefectDescription2;
    private javax.swing.JLabel geneDefectHandlingLabel;
    private javax.swing.JPanel genesPanel;
    private javax.swing.JButton okButton;
    private javax.swing.JLabel requiredBPLabel;
    // End of variables declaration//GEN-END:variables

}
