/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ursuppe.gui.dialog;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JComboBox;
import javax.swing.border.BevelBorder;

/**
 *
 * @author Personal
 */
public class ColorSelectionCBox extends JComboBox {

    public ColorSelectionCBox() {
        this.setBorder(new BevelBorder(BevelBorder.RAISED, Color.lightGray, Color.white));
    }
    
    

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
    }
    
    
    
}
