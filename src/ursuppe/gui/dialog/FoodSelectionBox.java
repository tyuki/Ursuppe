/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * FoodSelectionBox.java
 *
 * Created on 2010/01/23, 20:15:31
 */

package ursuppe.gui.dialog;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.swing.JButton;
import ursuppe.game.Amoeba;
import ursuppe.game.GameState;
import ursuppe.game.Player;
import ursuppe.gui.UrsuppeColor;
import ursuppe.gui.UrsuppeGUI;

/**
 *
 * @author Personal
 */
public class FoodSelectionBox extends javax.swing.JDialog {

    private UrsuppeGUI _gui;
    private GameState _game;
    private Player _player;
    private Amoeba _amoeba;
    private final int _maxFoods;

    private List<FoodSelectionPanel> _foodPanels;
    private int[] _selected;
    private int[] _foods;
    private int[] _foodColors;

    private ParasitismTargetSelectionPanel _parPanel;
    private int _activePars;

    private boolean _starveOK;
    private boolean _autoOK;

    /** Creates new form FoodSelectionBox */
    public FoodSelectionBox(java.awt.Frame parent, UrsuppeGUI gui, GameState game, Player player, Amoeba amoeba, int[] sucFoods) {
        super(parent, false);

        initComponents();

        _gui = gui;
        _game = game;
        _player = player;
        _amoeba = amoeba;

        _maxFoods = _game.getMaximumRequiredFoodNumber(player);
        int size = _game.getMaximumRequiredFoodNumberPerColor(player);

        //AutoEat and Starve Buttons
        boolean canEatNormally = _game.canFeedAfterSUC(player, amoeba.getLocation(), sucFoods, false, false);
        boolean canEatWithGene = _game.canFeedAfterSUC(player, amoeba.getLocation(), sucFoods, true, false) || 
                                _game.canFeedAfterSUC(player, amoeba.getLocation(), sucFoods, true, true) ||
                                _game.canFeedAfterSUC(player, amoeba.getLocation(), sucFoods, false, true);

        _starveOK = !canEatNormally;
        _autoOK  = (canEatNormally || canEatWithGene);

        if (_starveOK) starveButton.setEnabled(false);
        if (_autoOK) autoEatButton.setEnabled(true);

        //Food and food colors
        _foodColors = _game.getSoup().getFoodColors();
        int[] cellFoods = _game.getSoup().getCell(_amoeba.getLocation()).getFood();
        //If SUC was not used, use the food in the cell as is
        if (sucFoods == null) {
            _foods = cellFoods;
        //else add the SUC food to the cell food
        } else {
            _foods = new int[_foodColors.length];
            for (int i = 0; i < _foods.length; i++) {
                _foods[i] = cellFoods[i] + sucFoods[i];
            }
        }

        //Food selection
        _foodPanels = new LinkedList<>();
        _selected = new int[_foods.length];
        for (int i = 0; i < _foods.length; i++) {
            _foodPanels.add(new FoodSelectionPanel());
            //skip you own waste
            if (player.getColorID() == (_foodColors[i])) continue;
            //initialize with the available food, including SUC food
            _foodPanels.get(i).initialize(_foodColors[i], size, _foods[i]);
            _foodPanels.get(i).getPlusButton().addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    plusPressed(e);
                }

            });
            _foodPanels.get(i).getMinusButton().addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    minusPressed(e);
                }

            });
            _foodPanels.get(i).update(0, false);
            foodsPanel.add(_foodPanels.get(i));
        }

        //PAR target selection
        Map<Player, Integer> PARtargets = _game.getPARtargets(_player, _amoeba.getLocation());
        int PARnum = _game.getPARusableNum(_player, _amoeba.getLocation());
        if (PARtargets.size() > 0) {
            _parPanel = new ParasitismTargetSelectionPanel(this, PARnum, PARtargets);
            parPanel.add(_parPanel);
        }
        pack();
        autoEatButton.requestFocus();
    }


    private void plusPressed(ActionEvent e) {
        try {
            FoodSelectionPanel parent = (FoodSelectionPanel)((JButton)e.getSource()).getParent();
            int i = _foodPanels.indexOf(parent);

            _selected[i]++;
            update();
        } catch (Exception ex) {ex.printStackTrace();}
    }

    private void minusPressed(ActionEvent e) {
        try {
            FoodSelectionPanel parent = (FoodSelectionPanel)((JButton)e.getSource()).getParent();
            int i = _foodPanels.indexOf(parent);

            _selected[i]--;
            update();
        } catch (Exception ex) {ex.printStackTrace();}
    }

    private void update() {
        int sum = 0;
        for (int n : _selected) sum+=n;

        boolean cap;
        if (sum == _maxFoods) {
            cap = true;
        } else {
            cap = false;
        }

        for (int i = 0; i < _foodPanels.size(); i++) {
            _foodPanels.get(i).update(_selected[i], cap);
        }

        if (_game.canEat(_player, _selected, _activePars)) {
            eatButton.setEnabled(true);
            autoEatButton.setEnabled(false);
        } else {
            eatButton.setEnabled(false);
            if (_autoOK) autoEatButton.setEnabled(true);
        }
    }

    public void parSelectionChanged(int pars) {
        _activePars = pars;
        update();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        foodSelectionLabel = new javax.swing.JLabel();
        buttonPanel = new javax.swing.JPanel();
        eatButton = new javax.swing.JButton();
        autoEatButton = new javax.swing.JButton();
        starveButton = new javax.swing.JButton();
        parPanel = new javax.swing.JPanel();
        foodsPanel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(ursuppe.UrsuppeApp.class).getContext().getResourceMap(FoodSelectionBox.class);
        setTitle(resourceMap.getString("dialog.food.title")); // NOI18N
        setAlwaysOnTop(true);
        setName("Form"); // NOI18N
        setResizable(false);

        foodSelectionLabel.setFont(resourceMap.getFont("dialogEmphasized.font")); // NOI18N
        foodSelectionLabel.setText(resourceMap.getString("dialog.food.label")); // NOI18N
        foodSelectionLabel.setName("foodSelectionLabel"); // NOI18N

        buttonPanel.setName("buttonPanel"); // NOI18N
        buttonPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 30, 5));

        eatButton.setText(resourceMap.getString("dialog.food.eat")); // NOI18N
        eatButton.setEnabled(false);
        eatButton.setName("eatButton"); // NOI18N
        eatButton.setPreferredSize(new java.awt.Dimension(70, 21));
        eatButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eatButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(eatButton);

        autoEatButton.setText(resourceMap.getString("dialog.food.autoEat")); // NOI18N
        autoEatButton.setEnabled(false);
        autoEatButton.setName("autoEatButton"); // NOI18N
        autoEatButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                autoEatButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(autoEatButton);

        starveButton.setText(resourceMap.getString("dialog.food.starve")); // NOI18N
        starveButton.setEnabled(false);
        starveButton.setName("starveButton"); // NOI18N
        starveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                starveButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(starveButton);

        parPanel.setName("parPanel"); // NOI18N

        foodsPanel.setName("foodsPanel"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(foodsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 311, Short.MAX_VALUE)
                    .addComponent(foodSelectionLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 311, Short.MAX_VALUE)
                    .addComponent(parPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 311, Short.MAX_VALUE)
                    .addComponent(buttonPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 311, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(foodSelectionLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(foodsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 15, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(parPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 15, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void eatButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eatButtonActionPerformed
        if (_parPanel != null) {
            _gui.foodsSelected(_selected, _parPanel.getSelectedPARtargets());
        } else {
            _gui.foodsSelected(_selected, null);
        }
        this.dispose();
    }//GEN-LAST:event_eatButtonActionPerformed

    private void autoEatButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_autoEatButtonActionPerformed
        _gui.autoEatSelected();
        this.dispose();
    }//GEN-LAST:event_autoEatButtonActionPerformed

    private void starveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_starveButtonActionPerformed
        _gui.starveSelected();
        this.dispose();
    }//GEN-LAST:event_starveButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton autoEatButton;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton eatButton;
    private javax.swing.JLabel foodSelectionLabel;
    private javax.swing.JPanel foodsPanel;
    private javax.swing.JPanel parPanel;
    private javax.swing.JButton starveButton;
    // End of variables declaration//GEN-END:variables

}
