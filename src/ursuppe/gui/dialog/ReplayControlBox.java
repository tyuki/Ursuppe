/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ReplayControlBox.java
 *
 * Created on 2012/01/05, 16:13:02
 */

package ursuppe.gui.dialog;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.filechooser.FileFilter;
import network.Message;
import ursuppe.PropertyContainer;
import ursuppe.UrsuppeResource;
import ursuppe.control.Organizer;
import ursuppe.game.GameState;
import ursuppe.game.Progress;
import ursuppe.game.Replay;
import ursuppe.game.message.GameEventMessage;
import ursuppe.game.message.GameTextMessage;
import ursuppe.gui.UrsuppeGUIController;

/**
 *
 * @author Personal
 */
public class ReplayControlBox extends javax.swing.JDialog {

    private Replay replay;
    private final UrsuppeGUIController viewer;

    private final int BASE_RATE = 1000;

    private Player player = new Player();
    private Timer timer;
    private int stepSize = 1;
    private int speed = 1;
    private int skipFactor = 1;
    

    /** Creates new form ReplayControlBox */
    public ReplayControlBox(java.awt.Frame parent, UrsuppeGUIController viewer) {
        super(parent, false);
        initComponents();
        initOptionMenuItems();
        
        this.viewer = viewer;
        this.titleLabel.setText("");
    }

    private void stepSizeSelected(int stepSize) {
        this.stepSize = stepSize;
        PropertyContainer.setProperty(PropertyContainer.HISTORY_FILE, "replay.step", stepSize+"");
        PropertyContainer.saveProperties();
    }
    private void speedSelected(int speed) {
        this.speed = speed;

        startPlayer();
        
        PropertyContainer.setProperty(PropertyContainer.HISTORY_FILE, "replay.speed", speed+"");
        PropertyContainer.saveProperties();
        
    }

    private void startPlayer() {
        if (timer != null) {
            timer.cancel();
        }
        timer = new Timer();
        boolean playing = player.playing;
        player = new Player();
        player.playing = playing;
        timer.schedule(player, 0, Math.round(BASE_RATE / speed*1.0));
    }

    private void skipFactorForward() {
        if (!player.playing) {
            player.setPlaying(true);
        }
        if (skipFactor < 0) {
            skipFactor = 1;
        } else {
            skipFactor *= 2;
        }
    }

    private void skipFactorBackward() {
        if (!player.playing) {
            player.setPlaying(true);
        }
        if (skipFactor > 0) {
            skipFactor = -1;
        } else {
            skipFactor *= 2;
        }
    }

    private void initOptionMenuItems() {

        String prevStepSize = PropertyContainer.getProperty(PropertyContainer.HISTORY_FILE, "replay.step");
        int[] stepSizes = new int[] {1, 2, 5, 10, 20, 30, 40, 50};

        for (int i = 0; i < stepSizes.length; i++) {
            final int id = stepSizes[i];
            JCheckBoxMenuItem item = new JCheckBoxMenuItem(id + "");
            stepSizeMenu.add(item);
            stepSizeGroup.add(item);
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    stepSizeSelected(id);
                }
            });
            if (prevStepSize != null && prevStepSize.contentEquals(id+"")) {
                item.setSelected(true);
                stepSizeSelected(stepSizes[i]);
            }
            if (prevStepSize == null && stepSizes[i] == 1) {
                item.setSelected(true);
                stepSizeSelected(stepSizes[i]);
            }
        }

        String prevSpeed = PropertyContainer.getProperty(PropertyContainer.HISTORY_FILE, "replay.speed");
        int[] speeds = new int [] {1,2,4,8,16,32,64,128};

        for (int i = 0; i < speeds.length; i++) {
            final int id = speeds[i];
            JCheckBoxMenuItem item = new JCheckBoxMenuItem(id + "");
            replaySpeedMenu.add(item);
            speedGroup.add(item);
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    speedSelected(id);
                }
            });
            if (prevSpeed != null && prevSpeed.contentEquals(id+"")) {
                item.setSelected(true);
                speedSelected(speeds[i]);
            }
            if (prevSpeed == null && speeds[i] == 16) {
                item.setSelected(true);
                speedSelected(speeds[i]);
            }
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        
        replay = null;
        if (timer != null) {
            timer.cancel();
        }
    }


    private void replayLoaded(Replay replay) {
        this.replay = replay;
        if (replay != null) {
            replayProgress.setMaximum(replay.getGameStateLength()-1);
            replayProgress.setValue(0);
            replay.populateIconContainer();
            viewer.replayMode(replay.getGameState(0));
            viewer.setGameState(replay.getGameState(0));

            startPlayer();
        }
    }

//    private void 

    private class Player extends TimerTask {

        private boolean playing = false;

        private void setPlaying(boolean val) {
            player.playing = val;
            skipFactor = 1;
            if (player.playing) {
                playButton.setText("||");
            } else {
                playButton.setText(">");
            }
        }
        @Override
        public void run() {
            if (replay == null) this.cancel();

            if (playing) {
                int current = replayProgress.getValue();
                int next = Math.max(0, Math.min(current+1*skipFactor, replay.getGameStateLength()-1));
                if ((current == replay.getGameStateLength() - 1 && next == replay.getGameStateLength() - 1) || (current == next && next == 0)) {
                    setPlaying(false);
                } else {
                    if (skipFactor > 0) {
                        showMessagesBetween(current, next);
                    }

                    replayProgress.setValue(next);
                }
            }
        }
        
    }

    private void showMessagesBetween(int current, int next) {
        if (replay == null) return;

        GameState state = replay.getGameState(current);
        if (state == null) return;
        
        for (int i = current; i < next; i++) {
            List<Message> messages = replay.getMessagesAfter(i);
            for (Message msg : messages) {
                if (msg.getMessageType() == Message.GAME_TEXT_MESSAGE) {
                    Organizer.gameTextMessageHandling(viewer, viewer, -1, state, (GameTextMessage)msg.getItems()[0]);
                } else if (msg.getMessageType() == Message.GAME_EVENT_MESSAGE) {
                    Organizer.gameEventMessageHandling(viewer, (GameEventMessage)msg.getItems()[0]);
                }
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        stepSizeGroup = new javax.swing.ButtonGroup();
        speedGroup = new javax.swing.ButtonGroup();
        replayProgress = new javax.swing.JSlider();
        fastForwardButton = new javax.swing.JButton();
        playButton = new javax.swing.JButton();
        fastRewindButton = new javax.swing.JButton();
        titleLabel = new javax.swing.JLabel();
        roundControlPanel = new javax.swing.JPanel();
        roundCountLabel = new javax.swing.JLabel();
        prevRoundButton = new javax.swing.JButton();
        nextRoundButton = new javax.swing.JButton();
        roundLabel = new javax.swing.JLabel();
        phaseControlPanel = new javax.swing.JPanel();
        phaseCountLabel = new javax.swing.JLabel();
        prevPhaseButton = new javax.swing.JButton();
        nextPhaseButton = new javax.swing.JButton();
        phaseLbael = new javax.swing.JLabel();
        frameControlPanel = new javax.swing.JPanel();
        frameCountLabel = new javax.swing.JLabel();
        prevFrameButton = new javax.swing.JButton();
        nextFrameButton = new javax.swing.JButton();
        frameLabel = new javax.swing.JLabel();
        replayMenuBar = new javax.swing.JMenuBar();
        loadMenu = new javax.swing.JMenu();
        loadFileMenu = new javax.swing.JMenuItem();
        optionMenu = new javax.swing.JMenu();
        replaySpeedMenu = new javax.swing.JMenu();
        stepSizeMenu = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(ursuppe.UrsuppeApp.class).getContext().getResourceMap(ReplayControlBox.class);
        setTitle(resourceMap.getString("dialog.replay.title")); // NOI18N
        setAlwaysOnTop(true);
        setName("Form"); // NOI18N
        setResizable(false);

        replayProgress.setPaintTicks(true);
        replayProgress.setSnapToTicks(true);
        replayProgress.setFocusable(false);
        replayProgress.setName("replayProgress"); // NOI18N
        replayProgress.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                replayProgressStateChanged(evt);
            }
        });

        fastForwardButton.setText(resourceMap.getString("fastForwardButton.text")); // NOI18N
        fastForwardButton.setMaximumSize(new java.awt.Dimension(60, 30));
        fastForwardButton.setMinimumSize(new java.awt.Dimension(60, 30));
        fastForwardButton.setName("fastForwardButton"); // NOI18N
        fastForwardButton.setPreferredSize(new java.awt.Dimension(60, 30));
        fastForwardButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fastForwardButtonActionPerformed(evt);
            }
        });

        playButton.setText(resourceMap.getString("playButton.text")); // NOI18N
        playButton.setMaximumSize(new java.awt.Dimension(60, 30));
        playButton.setMinimumSize(new java.awt.Dimension(60, 30));
        playButton.setName("playButton"); // NOI18N
        playButton.setPreferredSize(new java.awt.Dimension(60, 30));
        playButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                playButtonActionPerformed(evt);
            }
        });

        fastRewindButton.setText(resourceMap.getString("fastRewindButton.text")); // NOI18N
        fastRewindButton.setMaximumSize(new java.awt.Dimension(60, 30));
        fastRewindButton.setMinimumSize(new java.awt.Dimension(60, 30));
        fastRewindButton.setName("fastRewindButton"); // NOI18N
        fastRewindButton.setPreferredSize(new java.awt.Dimension(60, 30));
        fastRewindButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fastRewindButtonActionPerformed(evt);
            }
        });

        titleLabel.setFont(resourceMap.getFont("dialogEmphasized.font")); // NOI18N
        titleLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleLabel.setText(resourceMap.getString("dialog.replay.loading")); // NOI18N
        titleLabel.setName("titleLabel"); // NOI18N

        roundControlPanel.setName("roundControlPanel"); // NOI18N

        roundCountLabel.setFont(resourceMap.getFont("dialog.font")); // NOI18N
        roundCountLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        roundCountLabel.setText(resourceMap.getString("roundCountLabel.text")); // NOI18N
        roundCountLabel.setName("roundCountLabel"); // NOI18N

        prevRoundButton.setText(resourceMap.getString("prevRoundButton.text")); // NOI18N
        prevRoundButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        prevRoundButton.setMaximumSize(new java.awt.Dimension(40, 30));
        prevRoundButton.setMinimumSize(new java.awt.Dimension(20, 20));
        prevRoundButton.setName("prevRoundButton"); // NOI18N
        prevRoundButton.setPreferredSize(new java.awt.Dimension(20, 20));
        prevRoundButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prevRoundButtonActionPerformed(evt);
            }
        });

        nextRoundButton.setText(resourceMap.getString("nextRoundButton.text")); // NOI18N
        nextRoundButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        nextRoundButton.setMaximumSize(new java.awt.Dimension(40, 30));
        nextRoundButton.setMinimumSize(new java.awt.Dimension(20, 20));
        nextRoundButton.setName("nextRoundButton"); // NOI18N
        nextRoundButton.setPreferredSize(new java.awt.Dimension(20, 20));
        nextRoundButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextRoundButtonActionPerformed(evt);
            }
        });

        roundLabel.setFont(resourceMap.getFont("dialog.font")); // NOI18N
        roundLabel.setText(resourceMap.getString("dialog.replay.label.round")); // NOI18N
        roundLabel.setName("roundLabel"); // NOI18N

        javax.swing.GroupLayout roundControlPanelLayout = new javax.swing.GroupLayout(roundControlPanel);
        roundControlPanel.setLayout(roundControlPanelLayout);
        roundControlPanelLayout.setHorizontalGroup(
            roundControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(roundControlPanelLayout.createSequentialGroup()
                .addGroup(roundControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(roundControlPanelLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(roundLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, roundControlPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(prevRoundButton, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(roundCountLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(nextRoundButton, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        roundControlPanelLayout.setVerticalGroup(
            roundControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(roundControlPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(roundLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(roundControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(roundCountLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(prevRoundButton, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nextRoundButton, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        phaseControlPanel.setName("phaseControlPanel"); // NOI18N

        phaseCountLabel.setFont(resourceMap.getFont("dialog.font")); // NOI18N
        phaseCountLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        phaseCountLabel.setText(resourceMap.getString("phaseCountLabel.text")); // NOI18N
        phaseCountLabel.setName("phaseCountLabel"); // NOI18N

        prevPhaseButton.setText(resourceMap.getString("prevPhaseButton.text")); // NOI18N
        prevPhaseButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        prevPhaseButton.setMaximumSize(new java.awt.Dimension(40, 30));
        prevPhaseButton.setMinimumSize(new java.awt.Dimension(20, 20));
        prevPhaseButton.setName("prevPhaseButton"); // NOI18N
        prevPhaseButton.setPreferredSize(new java.awt.Dimension(20, 20));
        prevPhaseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prevPhaseButtonActionPerformed(evt);
            }
        });

        nextPhaseButton.setText(resourceMap.getString("nextPhaseButton.text")); // NOI18N
        nextPhaseButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        nextPhaseButton.setMaximumSize(new java.awt.Dimension(40, 30));
        nextPhaseButton.setMinimumSize(new java.awt.Dimension(20, 20));
        nextPhaseButton.setName("nextPhaseButton"); // NOI18N
        nextPhaseButton.setPreferredSize(new java.awt.Dimension(20, 20));
        nextPhaseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextPhaseButtonActionPerformed(evt);
            }
        });

        phaseLbael.setFont(resourceMap.getFont("dialog.font")); // NOI18N
        phaseLbael.setText(resourceMap.getString("dialog.replay.label.phase")); // NOI18N
        phaseLbael.setName("phaseLbael"); // NOI18N

        javax.swing.GroupLayout phaseControlPanelLayout = new javax.swing.GroupLayout(phaseControlPanel);
        phaseControlPanel.setLayout(phaseControlPanelLayout);
        phaseControlPanelLayout.setHorizontalGroup(
            phaseControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(phaseControlPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(phaseControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(phaseLbael, javax.swing.GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE)
                    .addGroup(phaseControlPanelLayout.createSequentialGroup()
                        .addComponent(prevPhaseButton, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(phaseCountLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 17, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(nextPhaseButton, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        phaseControlPanelLayout.setVerticalGroup(
            phaseControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(phaseControlPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(phaseLbael)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(phaseControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(phaseCountLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(prevPhaseButton, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nextPhaseButton, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        frameControlPanel.setName("frameControlPanel"); // NOI18N

        frameCountLabel.setFont(resourceMap.getFont("dialog.font")); // NOI18N
        frameCountLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        frameCountLabel.setText(resourceMap.getString("frameCountLabel.text")); // NOI18N
        frameCountLabel.setName("frameCountLabel"); // NOI18N

        prevFrameButton.setText(resourceMap.getString("prevFrameButton.text")); // NOI18N
        prevFrameButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        prevFrameButton.setMaximumSize(new java.awt.Dimension(40, 30));
        prevFrameButton.setMinimumSize(new java.awt.Dimension(20, 20));
        prevFrameButton.setName("prevFrameButton"); // NOI18N
        prevFrameButton.setPreferredSize(new java.awt.Dimension(20, 20));
        prevFrameButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prevFrameButtonActionPerformed(evt);
            }
        });

        nextFrameButton.setText(resourceMap.getString("nextFrameButton.text")); // NOI18N
        nextFrameButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        nextFrameButton.setMaximumSize(new java.awt.Dimension(40, 30));
        nextFrameButton.setMinimumSize(new java.awt.Dimension(20, 20));
        nextFrameButton.setName("nextFrameButton"); // NOI18N
        nextFrameButton.setPreferredSize(new java.awt.Dimension(20, 20));
        nextFrameButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextFrameButtonActionPerformed(evt);
            }
        });

        frameLabel.setFont(resourceMap.getFont("dialog.font")); // NOI18N
        frameLabel.setText(resourceMap.getString("dialog.replay.label.frame")); // NOI18N
        frameLabel.setName("frameLabel"); // NOI18N

        javax.swing.GroupLayout frameControlPanelLayout = new javax.swing.GroupLayout(frameControlPanel);
        frameControlPanel.setLayout(frameControlPanelLayout);
        frameControlPanelLayout.setHorizontalGroup(
            frameControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(frameControlPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(frameControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, frameControlPanelLayout.createSequentialGroup()
                        .addComponent(prevFrameButton, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(frameCountLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(nextFrameButton, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(frameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE))
                .addContainerGap())
        );
        frameControlPanelLayout.setVerticalGroup(
            frameControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(frameControlPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(frameLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(frameControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(frameCountLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(prevFrameButton, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nextFrameButton, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        replayMenuBar.setName("replayMenuBar"); // NOI18N

        loadMenu.setText(resourceMap.getString("dialog.replay.menu.load")); // NOI18N
        loadMenu.setName("loadMenu"); // NOI18N

        loadFileMenu.setText(resourceMap.getString("dialog.replay.menu.load.file")); // NOI18N
        loadFileMenu.setName("loadFileMenu"); // NOI18N
        loadFileMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadFileMenuActionPerformed(evt);
            }
        });
        loadMenu.add(loadFileMenu);

        replayMenuBar.add(loadMenu);

        optionMenu.setText(resourceMap.getString("dialog.replay.menu.options")); // NOI18N
        optionMenu.setName("optionMenu"); // NOI18N

        replaySpeedMenu.setText(resourceMap.getString("dialog.replay.menu.options.speed")); // NOI18N
        replaySpeedMenu.setName("replaySpeedMenu"); // NOI18N
        optionMenu.add(replaySpeedMenu);

        stepSizeMenu.setText(resourceMap.getString("dialog.replay.menu.options.step")); // NOI18N
        stepSizeMenu.setName("stepSizeMenu"); // NOI18N
        optionMenu.add(stepSizeMenu);

        replayMenuBar.add(optionMenu);

        setJMenuBar(replayMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(replayProgress, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 343, Short.MAX_VALUE)
                            .addComponent(titleLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 343, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(roundControlPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(phaseControlPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(frameControlPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(72, 72, 72)
                        .addComponent(fastRewindButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(playButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(fastForwardButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {fastForwardButton, fastRewindButton, playButton});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titleLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(phaseControlPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(roundControlPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(frameControlPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(replayProgress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(playButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fastForwardButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fastRewindButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {fastForwardButton, fastRewindButton, playButton});

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void loadFileMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadFileMenuActionPerformed
        final JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(new File("./"));
        fc.setMultiSelectionEnabled(false);
        fc.setFileFilter(new FileFilter() {

            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) return true;
                return f.getName().matches(".+\\.rep$");
            }

            @Override
            public String getDescription() {
                return ".rep (Ursuppe replays)";
            }
        });

        this.titleLabel.setText(UrsuppeResource.getString("dialog.replay.loading"));
        this.titleLabel.repaint();
        //In response to a button click:
        int returnVal = fc.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            replayLoaded(Replay.load(file));
            this.titleLabel.setText(file.getName());
        } else {
            this.titleLabel.setText("");
        }
        this.titleLabel.repaint();
    }//GEN-LAST:event_loadFileMenuActionPerformed

    private void replayProgressStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_replayProgressStateChanged
        if (replay != null) {
            frameCountLabel.setText(replayProgress.getValue()+"/"+(replay.getGameStateLength()-1));
            GameState state = replay.getGameState(replayProgress.getValue());
            roundCountLabel.setText(state.getProgress().currentRound+"");
            final int currentPhase = state.getProgress().currentPhase;
            
            if (currentPhase == -2) {
                phaseCountLabel.setText("P");
            } else if (currentPhase == -1 || currentPhase == 0) {
                phaseCountLabel.setText("A");
            } else {
                phaseCountLabel.setText(state.getProgress().currentPhase+"");
            }
            
            
            viewer.setGameState(replay.getGameState(replayProgress.getValue()));
        }
    }//GEN-LAST:event_replayProgressStateChanged

    private void playButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_playButtonActionPerformed
        player.setPlaying(!player.playing);

    }//GEN-LAST:event_playButtonActionPerformed

    private void nextFrameButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextFrameButtonActionPerformed
        if (!player.playing) {
            int current = replayProgress.getValue();
            int next = Math.min(current + stepSize, replay.getGameStateLength());
            showMessagesBetween(current, next);
            replayProgress.setValue(next);
        }
    }//GEN-LAST:event_nextFrameButtonActionPerformed

    private void prevFrameButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prevFrameButtonActionPerformed
        if (!player.playing) {
            int current = replayProgress.getValue();
            int next = Math.min(current - stepSize, replay.getGameStateLength());
            showMessagesBetween(current, next);
            replayProgress.setValue(next);
        }
    }//GEN-LAST:event_prevFrameButtonActionPerformed

    private void fastForwardButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fastForwardButtonActionPerformed
        skipFactorForward();
    }//GEN-LAST:event_fastForwardButtonActionPerformed

    private void fastRewindButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fastRewindButtonActionPerformed
        skipFactorBackward();
    }//GEN-LAST:event_fastRewindButtonActionPerformed

    private void prevRoundButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prevRoundButtonActionPerformed
        if (!player.playing) {
            int current = replayProgress.getValue();
            int prev = replay.getPrevRoundFrame(current);
            replayProgress.setValue(prev);
        }
    }//GEN-LAST:event_prevRoundButtonActionPerformed

    private void nextRoundButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextRoundButtonActionPerformed
        if (!player.playing) {
            int current = replayProgress.getValue();
            int next = replay.getNextRoundFrame(current);
            viewer.getSoundController().mute();
            showMessagesBetween(current, next);
            viewer.getSoundController().unmute();
            replayProgress.setValue(next);
        }
    }//GEN-LAST:event_nextRoundButtonActionPerformed

    private void prevPhaseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prevPhaseButtonActionPerformed
        if (!player.playing) {
            int current = replayProgress.getValue();
            int prev = replay.getPrevPhaseFrame(current);
            replayProgress.setValue(prev);
        }
    }//GEN-LAST:event_prevPhaseButtonActionPerformed

    private void nextPhaseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextPhaseButtonActionPerformed
        if (!player.playing) {
            int current = replayProgress.getValue();
            int next = replay.getNextPhaseFrame(current);
            viewer.getSoundController().mute();
            showMessagesBetween(current, next);
            viewer.getSoundController().unmute();
            replayProgress.setValue(next);
        }
    }//GEN-LAST:event_nextPhaseButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel endLabel2;
    private javax.swing.JButton fastForwardButton;
    private javax.swing.JButton fastRewindButton;
    private javax.swing.JButton forwardStepButton2;
    private javax.swing.JPanel frameControlPanel;
    private javax.swing.JLabel frameCountLabel;
    private javax.swing.JLabel frameLabel;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JMenuItem loadFileMenu;
    private javax.swing.JMenu loadMenu;
    private javax.swing.JButton nextFrameButton;
    private javax.swing.JButton nextPhaseButton;
    private javax.swing.JButton nextRoundButton;
    private javax.swing.JMenu optionMenu;
    private javax.swing.JPanel phaseControlPanel;
    private javax.swing.JLabel phaseCountLabel;
    private javax.swing.JLabel phaseLbael;
    private javax.swing.JButton playButton;
    private javax.swing.JButton prevFrameButton;
    private javax.swing.JButton prevPhaseButton;
    private javax.swing.JButton prevRoundButton;
    private javax.swing.JMenuBar replayMenuBar;
    private javax.swing.JSlider replayProgress;
    private javax.swing.JMenu replaySpeedMenu;
    private javax.swing.JButton rewindStepButton2;
    private javax.swing.JPanel roundControlPanel;
    private javax.swing.JLabel roundCountLabel;
    private javax.swing.JLabel roundLabel;
    private javax.swing.ButtonGroup speedGroup;
    private javax.swing.ButtonGroup stepSizeGroup;
    private javax.swing.JMenu stepSizeMenu;
    private javax.swing.JLabel titleLabel;
    // End of variables declaration//GEN-END:variables

}
