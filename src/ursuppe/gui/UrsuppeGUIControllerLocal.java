/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ursuppe.gui;

import java.util.List;
import ursuppe.UrsuppeApp;
import ursuppe.control.DummyController;
import ursuppe.control.Organizer;
import ursuppe.game.GameState;
import ursuppe.game.GameState.DIRECTION;
import ursuppe.gui.dialog.ConnectServerBox;

/**
 *
 * @author Personal
 */
public class UrsuppeGUIControllerLocal extends UrsuppeGUIController {

    protected int _port;

    public UrsuppeGUIControllerLocal(GUIControllerComponents components) {
        super(components);
    }

    @Override
    public void connectServer(String playerName, String hostName, int port, boolean join, String iconName, String bgName, int preferredColor1, int preferredColor2) {
        _port = port;
        super.connectServer(playerName, hostName, port, join, iconName, bgName, preferredColor1, preferredColor2);
    }

    @Override
    protected void addPlayerMenuActionPerformed(java.awt.event.ActionEvent evt) {
        _testIconMenu.setEnabled(false);
        _csBox = new ConnectServerBox(UrsuppeApp.getApplication().getMainFrame(), this, _port, true);
        _csBox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
        UrsuppeApp.getApplication().show(_csBox);
    }



    @Override
    protected Organizer getOrganizer() {
        for (DummyController dummy : dummies) {
            if (dummy.getPlayerID() == _playerID) {
                return dummy.getOrganizer();
            }
        }
        return super.getOrganizer();
    }

    protected void setPlayer(int playerID) {
        for (DummyController dummy : dummies) {
            if (dummy.getPlayerID() == playerID) {
                setID(playerID);
                break;
            }
        }
    }


    @Override
    public void selectInitialScore(int playerID, GameState game) {
        setPlayer(playerID);
        super.selectInitialScore(playerID, game);
    }

    @Override
    public void selectInitialAmoeba(int playerID, GameState game) {
        setPlayer(playerID);
        super.selectInitialAmoeba(playerID, game);
    }

    @Override
    public void moveResponse(int playerID, int amoebaIndex, List<DIRECTION> dirs, GameState game) {
        setPlayer(playerID);
        super.moveResponse(playerID, amoebaIndex, dirs, game);
    }

    @Override
    public void selectPhase1Action(int playerID, int amoebaIndex, GameState game) {
        setPlayer(playerID);
        super.selectPhase1Action(playerID, amoebaIndex, game);

    }

    @Override
    public void handleGeneDefect(int playerID, GameState game) {
        setPlayer(playerID);
        super.handleGeneDefect(playerID, game);
    }

    @Override
    public void selectPhase3Action(int playerID, GameState game) {
        setPlayer(playerID);
        super.selectPhase3Action(playerID, game);
    }

    @Override
    public void selectPhase4Action(int playerID, GameState game) {
        setPlayer(playerID);
        super.selectPhase4Action(playerID, game);
    }

    @Override
    public void death(int playerID, int amoebaID, GameState game) {
        setPlayer(playerID);
        super.death(playerID, amoebaID, game);
    }

    @Override
    public void scoring(int playerID, GameState game) {
        setPlayer(playerID);
        super.scoring(playerID, game);
    }

    @Override
    public void decideHOL(int playerID, int amoebaIndex, DIRECTION dir, GameState game) {
        setPlayer(playerID);
        super.decideHOL(playerID, amoebaIndex, dir, game);
    }

    @Override
    public void decidePOP(int playerID, GameState game) {
        setPlayer(playerID);
        super.decidePOP(playerID, game);
    }

    @Override
    public void decideAGR(int playerID, GameState game) {
        setPlayer(playerID);
        super.decideAGR(playerID, game);
    }

    @Override
    public void defenderAction(int attackerID, int attackerAmoebaNum, int defenderID, int defenderAmoebaNum, GameState game) {
        setID(defenderID);
        super.defenderAction(attackerID, attackerAmoebaNum, defenderID, defenderAmoebaNum, game);
    }

    @Override
    public void attackerAction(int attackerID, int attackerAmoebaNum, int defenderID, int defenderAmoebaNum, boolean HARD, boolean MOUexcretes, GameState game) {
        setID(attackerID);
        super.attackerAction(attackerID, attackerAmoebaNum, defenderID, defenderAmoebaNum, HARD, MOUexcretes, game);
    }

    @Override
    public void selectEscapeDirection(int defenderID, int defenderAmoebaNum, List<DIRECTION> dir, GameState game) {
        setID(defenderID);
        super.selectEscapeDirection(defenderID, defenderAmoebaNum, dir, game);
    }

    @Override
    public void decideESCSPD(int defenderID, GameState game) {
        setID(defenderID);
        super.decideESCSPD(defenderID, game);
    }

    @Override
    public void decideSFSBANG(int defenderID, int defenderAmoebaNum, GameState game) {
        setID(defenderID);
        super.decideSFSBANG(defenderID, defenderAmoebaNum, game);
    }

}
