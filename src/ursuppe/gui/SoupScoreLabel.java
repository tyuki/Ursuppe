/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JLabel;

/**
 *
 * @author Personal
 */
public class SoupScoreLabel extends JLabel {

    private UrsuppeGUI _gui;
    private final int number;
    protected Image image = null;

    public SoupScoreLabel(int number) {
        super();
        this.number = number;
    }

    public void setImage(Image icon) {
        this.setBackground(null);
        this.image = icon;
    }

    public void setGUI(UrsuppeGUI gui) {
        this._gui = gui;
    }


    @Override
    public void paintComponent(Graphics g) {
        //Draw Icon
        
        if (image != null) {
            g.drawImage(image, 0, 0, null);
        }

        //Draw text
        if ((number >= 1 && number <=6) || number == 10 || number == 20 || number == 30 || number == 40 || number == 42) {
            if (_gui != null) {
                g.setColor(_gui.getSoupBackground().scoreTrackColor);
            }
            g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 12));
            if (number < 10) {
                g.drawString(number+"", 8, 14);
            } else if (number > 20) {
                g.drawString(number+"", 3, 14);
            } else {
                g.drawString(number+"", 4, 14);
            }
        }
    }
}
