/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ControlPanel.java
 *
 * Created on 2010/01/13, 21:40:33
 */

package ursuppe.gui;

import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.ToolTipManager;
import ursuppe.UrsuppeResource;
import ursuppe.gui.icon.IconContainer;

/**
 *
 * @author Personal
 */
public class ControlPanel extends javax.swing.JPanel {

    private UrsuppeGUI _gui;

    private JLabel[] _controls;

    private boolean _RmousePressed;
    
    private static final int TOOL_TIP_INITIAL_DELAY = 200;
    private static final int TOOL_TIP_DISMISS_DELAY = 5000;

    private static final String CMD_INITIAL_AMOEBA = "initialAmoeba";
    private static final String CMD_MOVE = "move";
    private static final String CMD_DRIFT = "drift";
    private static final String CMD_EAT = "eat";
    private static final String CMD_STARVE = "starve";
    private static final String CMD_SFS = "sfsAttack";
    private static final String CMD_HOLD = "holding";
    private static final String CMD_SPEED = "useSPD";
    private static final String CMD_CANCEL_SPEED = "cancelSPD";

    private static final String CMD_NEW_GENE = "newGene";
    private static final String CMD_ADA = "highlyAdaptable";
    private static final String CMD_PHASE3_END = "phase3End";

    private static final String CMD_CELL_DIVISION = "cellDivision";
    private static final String CMD_PHASE4_END = "phase4End";
    private static final String CMD_HEAL = "healing";

    private static final String CMD_AGR = "agrAttack";
    private static final String CMD_AGR_CANCEL = "agrCancel";

    private static final String GAME_SETTING = "gameSetting";



    /** Creates new form ControlPanel */
    public ControlPanel() {
        initComponents();

        _controls = new JLabel[7];

        _controls[0] = control0;
        _controls[1] = control1;
        _controls[2] = control2;
        _controls[3] = control3;
        _controls[4] = control4;
        _controls[5] = control5;
        _controls[6] = control6;

        ToolTipManager.sharedInstance().setInitialDelay(TOOL_TIP_INITIAL_DELAY);
        ToolTipManager.sharedInstance().setDismissDelay(TOOL_TIP_DISMISS_DELAY);

        //control1.setIcon(IconContainer.getIcon("control"));
        clearControl();
    }

    public void setGUI(UrsuppeGUI gui) {
        _gui = gui;
    }

    public void enableGameSettingDisplay() {
        //Game Settings
        ImageIcon icon = IconContainer.getIcon(GAME_SETTING);
        if (icon == null) icon = IconContainer.getIcon("control");
        gameSetting.setIcon(icon);
        //gameSetting.setToolTipText(UrsuppeResource.getString("control."+GAME_SETTING+".name"));
        gameSetting.setEnabled(true);
    }
    
    public void setControlPhase0(boolean myTurn) {
        clearControl();
        
        if (myTurn) setControl(0, CMD_INITIAL_AMOEBA);
    }

    public void setControlPhase1(boolean drift, boolean move, boolean hold, boolean speed, boolean cancelSPD, boolean eat, boolean sfs, boolean starve) {
        clearControl();

        //0 : drift
        //1 : move
        //2 : hold, speed
        //3
        //4 : east/fastfood
        //5 : sfs

        //Drift can be always done
        if (drift) setControl(0, CMD_DRIFT);

        //move
        if (move) setControl(1, CMD_MOVE);

        //hold, speed
        if (hold) {
            setControl(2, CMD_HOLD);
        } else if (speed) {
            setControl(2, CMD_SPEED);
        }
        
        //eat, fastfood
        if (eat) {
            setControl(4, CMD_EAT);
        }
        
        //SFS
        if (sfs) setControl(5, CMD_SFS);

        //starve
        if (starve) {
            setControl(6, CMD_STARVE);
        } else if (cancelSPD) {
            setControl(6, CMD_CANCEL_SPEED);
        }

    }

    public void setControlPhase3(boolean ADA) {
        setControl(0, CMD_NEW_GENE);
        if (ADA) setControl(1, CMD_ADA);

        setControl(3, CMD_PHASE3_END);
    }

    public void setControlPhase4(boolean heal1, boolean heal2) {
        setControl(0, CMD_CELL_DIVISION);
        setControl(3, CMD_PHASE4_END);

        if (heal1 || heal2) setControl(1, CMD_HEAL);
    }

    public void setControlAGR() {
        setControl(0, CMD_AGR);
        setControl(3, CMD_AGR_CANCEL);
    }

    public void clearControl() {
        for (int i = 0; i < _controls.length; i++) {
            _controls[i].setIcon(null);
            _controls[i].setText("");
            _controls[i].setToolTipText(null);
            _controls[i].setEnabled(false);
        }
    }

    private void setControl(int index, String name) {
        ImageIcon icon = IconContainer.getIcon(name);
        if (icon == null) icon = IconContainer.getIcon("control");
        _controls[index].setIcon(icon);
        _controls[index].setToolTipText(UrsuppeResource.getString("control."+name+".name"));
        _controls[index].setText(name);
        _controls[index].setEnabled(true);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        control0 = new javax.swing.JLabel();
        control1 = new javax.swing.JLabel();
        control2 = new javax.swing.JLabel();
        control3 = new javax.swing.JLabel();
        control4 = new javax.swing.JLabel();
        control5 = new javax.swing.JLabel();
        control6 = new javax.swing.JLabel();
        gameSetting = new javax.swing.JLabel();

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(ursuppe.UrsuppeApp.class).getContext().getResourceMap(ControlPanel.class);
        setBackground(resourceMap.getColor("Form.background")); // NOI18N
        setMaximumSize(new java.awt.Dimension(330, 50));
        setMinimumSize(new java.awt.Dimension(330, 50));
        setName("Form"); // NOI18N
        setPreferredSize(new java.awt.Dimension(330, 50));

        control0.setText(resourceMap.getString("control0.text")); // NOI18N
        control0.setMaximumSize(new java.awt.Dimension(36, 36));
        control0.setMinimumSize(new java.awt.Dimension(32, 32));
        control0.setName("control0"); // NOI18N
        control0.setPreferredSize(new java.awt.Dimension(32, 32));
        control0.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                controlMouseWheelMoved(evt);
            }
        });
        control0.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                controlMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                controlMousePressed(evt);
            }
        });

        control1.setMaximumSize(new java.awt.Dimension(36, 36));
        control1.setMinimumSize(new java.awt.Dimension(32, 32));
        control1.setName("control1"); // NOI18N
        control1.setPreferredSize(new java.awt.Dimension(32, 32));
        control1.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                controlMouseWheelMoved(evt);
            }
        });
        control1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                controlMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                controlMousePressed(evt);
            }
        });

        control2.setMaximumSize(new java.awt.Dimension(36, 36));
        control2.setMinimumSize(new java.awt.Dimension(32, 32));
        control2.setName("control2"); // NOI18N
        control2.setPreferredSize(new java.awt.Dimension(32, 32));
        control2.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                controlMouseWheelMoved(evt);
            }
        });
        control2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                controlMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                controlMousePressed(evt);
            }
        });

        control3.setMaximumSize(new java.awt.Dimension(36, 36));
        control3.setMinimumSize(new java.awt.Dimension(32, 32));
        control3.setName("control3"); // NOI18N
        control3.setPreferredSize(new java.awt.Dimension(32, 32));
        control3.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                controlMouseWheelMoved(evt);
            }
        });
        control3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                controlMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                controlMousePressed(evt);
            }
        });

        control4.setMaximumSize(new java.awt.Dimension(36, 36));
        control4.setMinimumSize(new java.awt.Dimension(32, 32));
        control4.setName("control4"); // NOI18N
        control4.setPreferredSize(new java.awt.Dimension(32, 32));
        control4.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                controlMouseWheelMoved(evt);
            }
        });
        control4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                controlMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                controlMousePressed(evt);
            }
        });

        control5.setMaximumSize(new java.awt.Dimension(36, 36));
        control5.setMinimumSize(new java.awt.Dimension(32, 32));
        control5.setName("control5"); // NOI18N
        control5.setPreferredSize(new java.awt.Dimension(32, 32));
        control5.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                controlMouseWheelMoved(evt);
            }
        });
        control5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                controlMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                controlMousePressed(evt);
            }
        });

        control6.setMaximumSize(new java.awt.Dimension(36, 36));
        control6.setMinimumSize(new java.awt.Dimension(32, 32));
        control6.setName("control6"); // NOI18N
        control6.setPreferredSize(new java.awt.Dimension(32, 32));
        control6.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                controlMouseWheelMoved(evt);
            }
        });
        control6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                controlMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                controlMousePressed(evt);
            }
        });

        gameSetting.setEnabled(false);
        gameSetting.setMaximumSize(new java.awt.Dimension(36, 36));
        gameSetting.setMinimumSize(new java.awt.Dimension(32, 32));
        gameSetting.setName("gameSetting"); // NOI18N
        gameSetting.setPreferredSize(new java.awt.Dimension(32, 32));
        gameSetting.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                gameSettingMouseWheelMoved(evt);
            }
        });
        gameSetting.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                gameSettingMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                gameSettingMousePressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(234, 234, 234)
                        .addComponent(control6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(control0, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(control1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(control2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(control3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(control4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(control5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(gameSetting, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(gameSetting, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(control6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(control5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(control4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(control3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(control2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(control1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(control0, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void controlMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_controlMouseEntered
        try {
            if (evt.getComponent().isEnabled()) {
                String text = ((JLabel)evt.getSource()).getText();
                _gui.setControlDescription(UrsuppeResource.getString("control."+text+".description"));
                _RmousePressed = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_controlMouseEntered

    private void controlMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_controlMousePressed
        try {
            if (evt.getComponent().isEnabled()) {

                //Left click
                if (evt.getButton() == MouseEvent.BUTTON1) {
                    String cmd = ((JLabel)evt.getSource()).getText();

                    //Drift
                    if (cmd.compareTo(CMD_DRIFT) == 0) {
                        _gui.driftSelected();
                    } else if (cmd.compareTo(CMD_MOVE) == 0) {
                        _gui.moveSelected();
                    } else if (cmd.compareTo(CMD_EAT) == 0) {
                        _gui.eatSelected();
                    } else if (cmd.compareTo(CMD_STARVE) == 0) {
                        _gui.starveSelected();
                    } else if (cmd.compareTo(CMD_SPEED) == 0) {
                        _gui.SPDSelected();
                    } else if (cmd.compareTo(CMD_CANCEL_SPEED) == 0) {
                        _gui.cancelSPDSelected();
                    } else if (cmd.compareTo(CMD_HOLD) == 0) {
                        _gui.HOLSelected();
                    } else if (cmd.compareTo(CMD_ADA) == 0) {
                        _gui.ADASelected();
                    } else if (cmd.compareTo(CMD_PHASE3_END) == 0)  {
                        _gui.endPhase3Selected();
                    } else if (cmd.compareTo(CMD_PHASE4_END) == 0) {
                        _gui.endPhase4Selected();
                    } else if (cmd.compareTo(CMD_AGR_CANCEL) == 0) {
                        _gui.AGRCanceled();
                    }
                } else if (evt.getButton() == MouseEvent.BUTTON2 || evt.getButton() == MouseEvent.BUTTON3) {
                    if (_RmousePressed) {
                        _gui.setControlDescriptionCarretToHead();
                    } else {
                        _gui.setControlDescriptionCarretToTail();
                    }
                    _RmousePressed = !_RmousePressed;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_controlMousePressed

    private void gameSettingMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_gameSettingMouseEntered
        try {
            _gui.showGameSetting();
            _RmousePressed = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_gameSettingMouseEntered

    private void gameSettingMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_gameSettingMousePressed
        try {
            //Right click
            if (evt.getButton() == MouseEvent.BUTTON2 || evt.getButton() == MouseEvent.BUTTON3) {
                if (_RmousePressed) {
                    _gui.setControlDescriptionCarretToHead();
                } else {
                    _gui.setControlDescriptionCarretToTail();
                }
                _RmousePressed = !_RmousePressed;
            }
        } catch (Exception e) {e.printStackTrace();}
    }//GEN-LAST:event_gameSettingMousePressed

    private void controlMouseWheelMoved(java.awt.event.MouseWheelEvent evt) {//GEN-FIRST:event_controlMouseWheelMoved
        try {
            if (evt.getWheelRotation() > 0) {
                _gui.setControlDescriptionCarretToTail();
                _RmousePressed = true;
            } else {
                _gui.setControlDescriptionCarretToHead();
                _RmousePressed = false;
            }
        } catch (Exception e) {e.printStackTrace();}
    }//GEN-LAST:event_controlMouseWheelMoved

    private void gameSettingMouseWheelMoved(java.awt.event.MouseWheelEvent evt) {//GEN-FIRST:event_gameSettingMouseWheelMoved
        try {
            if (evt.getWheelRotation() > 0) {
                _gui.setControlDescriptionCarretToTail();
                _RmousePressed = true;
            } else {
                _gui.setControlDescriptionCarretToHead();
                _RmousePressed = false;
            }
        } catch (Exception e) {e.printStackTrace();}
    }//GEN-LAST:event_gameSettingMouseWheelMoved


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel control0;
    private javax.swing.JLabel control1;
    private javax.swing.JLabel control2;
    private javax.swing.JLabel control3;
    private javax.swing.JLabel control4;
    private javax.swing.JLabel control5;
    private javax.swing.JLabel control6;
    private javax.swing.JLabel gameSetting;
    // End of variables declaration//GEN-END:variables

}
