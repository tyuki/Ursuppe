/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ursuppe.gui;

import java.util.Map;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSeparator;
import sound.SoundContainer;
import ursuppe.PropertyContainer;
import ursuppe.UrsuppeApp;
import ursuppe.UrsuppeResource;
import sound.SOUND_EVENT;

/**
 *
 * @author Personal
 */
public class SoundController {
    
    private boolean sound = true;
    private boolean[] soundType = new boolean[SOUND_EVENT.TYPE.values().length];
    private int volume = 10;
    private JMenu soundMenu;
    
    private boolean muted;

    public SoundController() {
    }
    
    public SoundController(JMenu soundMenu) {
        this.soundMenu = soundMenu;
        initializeMenuControl();
    }
    
    /**
     * Mutes the sound overriding other configurations specified as user settings.
     * This if for internal control of sound on/off
     * 
     */
    public void mute() {
        muted = true;
    }
    
    public void unmute() {
        muted = false;
    }
    
    public void soundEvent(SOUND_EVENT event) {
        if (muted) return;
        if (!sound) {
            return;
        }
        try {
            if (soundType[event.type.ordinal()]) {
                SoundContainer.INSTANCE.play(event, volume);
            }
        } catch (Exception e) {
            UrsuppeApp.debug(e);
        }
    }
    
    public void setSound(final boolean ON) {
        this.sound = ON;
        PropertyContainer.setProperty(PropertyContainer.HISTORY_FILE, "option.sound", ON+"");
        PropertyContainer.saveProperties();
    }
    
    public void setSound(final SOUND_EVENT.TYPE type, final boolean ON) {
        try {
            soundType[type.ordinal()] = ON;
            PropertyContainer.setProperty(PropertyContainer.HISTORY_FILE, "option.sound."+type.name().toLowerCase(), ON+"");
            PropertyContainer.saveProperties();
        } catch (Exception e) {
            UrsuppeApp.debug(e);
        }
    }
    
    public void setVolume(final int vol) {
        if (vol < 1 || vol > 10) {
            volume = 10;
        } else {
            volume = vol;
        }
        PropertyContainer.setProperty(PropertyContainer.HISTORY_FILE, "option.volume", volume+"");
        PropertyContainer.saveProperties();
    }
    
    private void initializeMenuControl() {
        // for all sounds
        {
            ButtonGroup volGroup = new ButtonGroup();
            JMenu volMenu = new JMenu(UrsuppeResource.getString("menu.sound.volume"));
            soundMenu.add(volMenu);

            try {
                final String volumeStr = PropertyContainer.getProperty(PropertyContainer.HISTORY_FILE, "option.volume");
                final int vol = Integer.parseInt(volumeStr);
                setVolume(vol);
            } catch (Exception e) {
                setVolume(10);
            }
            //volume is only for the full thing
            for (int i = 10; i >= 1; i--) {
                final String volStr = (i>0)?(i*10)+"%":UrsuppeResource.getString("menu.sound.off");
                JCheckBoxMenuItem volMenuItem = new JCheckBoxMenuItem(volStr);
                volMenu.add(volMenuItem);
                volGroup.add(volMenuItem);
                if (volume == i) volMenuItem.setSelected(true);
                
                final int vol = i;
                volMenuItem.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    setVolume(vol);
                }
            });
            }
            
            ButtonGroup onOffGroup = new ButtonGroup();
            JRadioButtonMenuItem onMenu = new JRadioButtonMenuItem(UrsuppeResource.getString("menu.sound.on"));
            JRadioButtonMenuItem offMenu = new JRadioButtonMenuItem(UrsuppeResource.getString("menu.sound.off"));
            
            final String soundStr = PropertyContainer.getProperty(PropertyContainer.HISTORY_FILE, "option.sound");
            final boolean soundOn = (soundStr == null)?true:((soundStr.contentEquals("true"))?true:false);
            setSound(soundOn);
            onMenu.setSelected(soundOn);
            offMenu.setSelected(!soundOn);
            
            soundMenu.add(onMenu);
            soundMenu.add(offMenu);
            onOffGroup.add(onMenu);
            onOffGroup.add(offMenu);
            
            onMenu.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    setSound(true);
                }
            });
            offMenu.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    setSound(false);
                }
            });
        }
        soundMenu.add(new JSeparator());
        
        for (final SOUND_EVENT.TYPE type : SOUND_EVENT.TYPE.values()) {
            JMenu menu = new JMenu(UrsuppeResource.getString("menu.sound."+type.name().toLowerCase()));
            soundMenu.add(menu);
            
            ButtonGroup onOffGroup = new ButtonGroup();
            JRadioButtonMenuItem onMenu = new JRadioButtonMenuItem(UrsuppeResource.getString("menu.sound.on"));
            JRadioButtonMenuItem offMenu = new JRadioButtonMenuItem(UrsuppeResource.getString("menu.sound.off"));
            
            menu.add(onMenu);
            menu.add(offMenu);
            onOffGroup.add(onMenu);
            onOffGroup.add(offMenu);
            
            final String soundStr = PropertyContainer.getProperty(PropertyContainer.HISTORY_FILE, "option.sound."+type.name().toLowerCase());
            final boolean soundOn = (soundStr == null)?true:((soundStr.contentEquals("true"))?true:false);
            setSound(type, soundOn);
            onMenu.setSelected(soundOn);
            offMenu.setSelected(!soundOn);
            
            onMenu.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    setSound(type, true);
                }
            });
            offMenu.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    setSound(type, false);
                }
            });
        }
    }
}
