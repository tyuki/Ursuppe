/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.gui;

import ursuppe.game.GameOption;

/**
 *
 * @author Personal
 */
public interface UrsuppeServerGUI {
    public void startServer(int port, GameOption gameOption);
    public void startServerCanceled();
}
