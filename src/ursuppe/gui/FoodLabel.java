/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JLabel;
import ursuppe.game.SoupCell;
import ursuppe.gui.UrsuppeColor.TYPE;

/**
 *
 * @author Personal
 */
public class FoodLabel extends JLabel {

    private static int WIDTH = 120;
    private static int HEIGHT = 30;

    private static int INTERVAL = 16;
    private static int NUMBER_OFFSET_X = 8;
    private static int NUMBER_OFFSET_Y = 15;
    private static int RECT_OFFSET_X = 6;
    private static int RECT_OFFSET_Y = 18;
    private static int RECT_SIZE_X = 10;
    private static int RECT_SIZE_Y = 10;

    private int[] _foodColors;
    private int [] _foods;
    private int _DP;

    private boolean _suppressed;

    private UrsuppeGUI _gui;

    private int[] _foodsSuppressed;
    private int _DPsuppressed;
    

    public void setGUI(UrsuppeGUI gui) {
        _gui = gui;
    }
    
    public void setFoodColors(int[] color) {
        _foodColors = color;
        _gui.repaint();
    }

    public void setSoupCell(SoupCell cell) {
        _foods = cell.getFood();
        _DP = cell.getDP();

        if (_gui != null) {
            _gui.repaint();
        } else {
            this.repaint();
        }
    }

    public void setSuppressed(boolean suppressed) {
        _suppressed = suppressed;
        _foodsSuppressed = _foods;
        _DPsuppressed = _DP;
    }

    @Override
    public void paintComponent(Graphics g) {
        if (_suppressed) {
            paintComponent(g, _foodsSuppressed, _DPsuppressed);
        } else {
            paintComponent(g, _foods, _DP);
        }

    }

    private void paintComponent(Graphics g, int[] foods, int DP) {

        if (_foodColors == null || foods == null) return;

        Graphics2D g2d = (Graphics2D)g;
        for (int i = 0; i < foods.length; i++) {
            if (foods[i] > 0) {
                int extraXoffset = (foods[i] >= 10)?-4:0;
                g2d.setColor(_gui.getSoupBackground().foodCountColor);
                g2d.drawString(Integer.toString(foods[i]), NUMBER_OFFSET_X+INTERVAL*i+extraXoffset, NUMBER_OFFSET_Y);
                g2d.setColor(UrsuppeColor.get(_foodColors[i]).toColor(TYPE.FOOD));
                g2d.fillRect(RECT_OFFSET_X+INTERVAL*i, RECT_OFFSET_Y, RECT_SIZE_X, RECT_SIZE_Y);
            }

        }

        if (DP > 0) {
            g2d.setColor(_gui.getSoupBackground().foodCountColor);
            g2d.drawString(Integer.toString(DP), NUMBER_OFFSET_X+INTERVAL*6, NUMBER_OFFSET_Y);
            g2d.setColor(Color.white);
            g2d.fillOval(RECT_OFFSET_X+INTERVAL*6, RECT_OFFSET_Y, RECT_SIZE_X, RECT_SIZE_Y);
        }
    }




}
