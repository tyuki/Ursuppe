/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.gui;

import java.awt.Point;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import sound.SOUND_EVENT;
import ursuppe.UrsuppeApp;
import ursuppe.control.Organizer;
import ursuppe.control.UrsuppeServerController;
import ursuppe.control.UrsuppeViewer;
import ursuppe.game.Amoeba;
import ursuppe.game.GameOption;
import ursuppe.game.GameState;
import ursuppe.game.GenePool.GENE;
import ursuppe.gui.dialog.StartServerBox;

/**
 *
 * @author Personal
 */
public class UrsuppeServerGUIController implements UrsuppeViewer, UrsuppeServerController, UrsuppeServerGUI {

    protected StartServerBox startServerBox;
    protected Organizer organizer;
    protected UrsuppeServerGUIController controller;

    private JTextPane messagePane;
    private JMenuItem startGameMenu;
    private JMenuItem resetGameMenu;
    private JMenuItem addAIMenu;
    private JMenuItem removeAIMenu;
    private JCheckBoxMenuItem acceptCommandMenu;

    @Override
    public void setGameState(GameState game) {
        startGameMenu.setEnabled(false);
        resetGameMenu.setEnabled(false);
        addAIMenu.setEnabled(false);
        removeAIMenu.setEnabled(false);

        if (game.isStarted()) {
            resetGameMenu.setEnabled(true);
        } else {
            if (game.getPlayers().size() >= 3) {
                startGameMenu.setEnabled(true);
            }
            if (game.getPlayers().size() <= 5) {
                addAIMenu.setEnabled(true);
            }
            if (organizer.getAIcount() > 0) {
                removeAIMenu.setEnabled(true);
            }
        }

    }

    @Override
    public void errorReport(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    @Override
    public void addUserMessage(String text, SimpleAttributeSet attr) {
        addMessage(text, attr);
    }

    @Override
    public void addSystemMessage(String text, SimpleAttributeSet attr) {
        addMessage(text, attr);
    }

    @Override
    public void geneUsed(int playerID, GENE gene) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void soundEvent(SOUND_EVENT event) {
       // throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void moveEvent(Amoeba amoeba, Point src, int srcIndex, Point dst, int dstIndex) {
       // throw new UnsupportedOperationException("Not supported yet.");
    }

    private void addMessage(String text, SimpleAttributeSet attr) {
        try {
            messagePane.getDocument().insertString(messagePane.getDocument().getEndPosition().getOffset(), text+"\n", attr);
            messagePane.setCaretPosition(messagePane.getDocument().getEndPosition().getOffset()-1);
        } catch (BadLocationException ble) {
            ble.printStackTrace();
        }
    }


    public static class ServerGUIControllerComponents {
        public Organizer organizer;
        public JTextPane messagePane;

        public JMenuItem startGameMenu;
        public JMenuItem resetGameMenu;
        public JMenuItem addAIMenu;
        public JMenuItem removeAIMenu;
        public JCheckBoxMenuItem acceptCommandMenu;
    }

    public UrsuppeServerGUIController(ServerGUIControllerComponents components) {
        this.organizer = components.organizer;
        this.messagePane = components.messagePane;
        
        this.startGameMenu = components.startGameMenu;
        this.resetGameMenu = components.resetGameMenu;
        this.addAIMenu = components.addAIMenu;
        this.removeAIMenu = components.removeAIMenu;
        this.acceptCommandMenu = components.acceptCommandMenu;

        organizer.setViewer(this);


        startGameMenu.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startGameMenuActionPerformed(evt);
            }
        });

        resetGameMenu.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetGameMenuActionPerformed(evt);
            }
        });

        addAIMenu.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addAIMenuActionPerformed(evt);
            }
        });

        removeAIMenu.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeAIMenuActionPerformed(evt);
            }
        });

        acceptCommandMenu.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                organizer.setCommandAccept(acceptCommandMenu.isSelected());
            }
        });
    }

    @Override
    public void start() {
        //start server dialog
        startServerBox = new StartServerBox(UrsuppeApp.getApplication().getMainFrame(), this);
        startServerBox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
        UrsuppeApp.getApplication().show(startServerBox);
    }

    @Override
    public void startServer(int port, GameOption gameOption) {
        startServerBox.setVisible(false);

        UrsuppeApp.trace("[UrsuppeServerGUIContoller] startServer");

         if (organizer.startServer(port, gameOption)) {
            UrsuppeApp.trace("[UrsuppeServerGUIContoller] ServerStarted");
            addSystemMessage("ServerStarted", null);
            startServerBox.dispose();
//            _startServerMenu.setEnabled(false);
            startGameMenu.setEnabled(true);
            organizer.connectServer("SERVER", "127.0.0.1", port, false, null, null, -1, -1);
        } else {
            startServerBox.setVisible(true);
        }
    }

    @Override
    public void startServerCanceled() {
        UrsuppeApp.getApplication().quit(null);
    }


    private void startGameMenuActionPerformed(java.awt.event.ActionEvent evt) {
        if (organizer.startGame()) {
            startGameMenu.setEnabled(false);
            addAIMenu.setEnabled(false);
            removeAIMenu.setEnabled(false);
        }
    }
    
    private void resetGameMenuActionPerformed(java.awt.event.ActionEvent evt) {
        organizer.resetGame();
    }
    
    private void addAIMenuActionPerformed(java.awt.event.ActionEvent evt) {
        addAIMenu.setEnabled(false);
        removeAIMenu.setEnabled(false);
        organizer.addAI();
    }

    private void removeAIMenuActionPerformed(java.awt.event.ActionEvent evt) {
        addAIMenu.setEnabled(false);
        removeAIMenu.setEnabled(false);
        organizer.removeAI();
    }

}
