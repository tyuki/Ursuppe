/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.gui;

import java.awt.Point;
import ursuppe.game.Amoeba;
import ursuppe.game.GameOption;
import ursuppe.game.GenePool.GENE;
import ursuppe.gui.icon.SoupBackground;

/**
 *
 * @author Personal
 */
public interface UrsuppeGUI {

    public static enum BOARD_SELECTION_STATE {
        NONE,
        INITIAL_AMOEBA,
        PHASE1_MOVE,
        PHASE1_SFS,
        PHASE4_DIVISION,
        PHASE4_HEAL,
        PHASE5_AGR,

        CLEANLINESS,
        CLEANLINESS_SFS,
        SUCTION,

        ESCAPE
    }

    public static enum GENE_SELECTION_STATE {
        NONE,
        NORMAL,
        ADA
    }

    public static enum AMOEBA_HOVER_STATE {
        NONE,
        SFS,
        AGR,
        HEAL
    }

    public void setControlDescription(String text);
    public void setGeneDescription(String text);
    public void setControlDescriptionCarretToHead();
    public void setControlDescriptionCarretToTail();
    public void setGeneDescriptionCarretToHead();
    public void setGeneDescriptionCarretToTail();
    public void showGameSetting();
    public void sendChatMessage(String text);
    public void initialScoreSelected(int score);
    public void initialAmoebaCanceled();
    public void initialAmoebaSelected(Point loc, int amoebaNum);
    
    public void driftSelected();
    public void moveSelected();
    public void driftTENSelected(int[] TENfoods, boolean useREB);
    public void moveTENSelected(Point loc, int[] TENfoods);
    public void eatSelected();
    public void starveSelected();
    public void SPDSelected();
    public void cancelSPDSelected();
    public void HOLSelected();
    public void HOLTENSelected(int[] TENfoods);
    public void SUCSelected(Point SUCloc, int[] SUCfoods);
    public void foodsSelected(int[] foods, int[] parPlayers);
    public void autoEatSelected();

    public void geneDefectHandled(GENE[] discards);
    public void geneSelected(GENE gene);
    public void ADASelected();
    public void ADAexchangeGeneSelected(GENE exchange);
    public void ADAexchangeSelected(GENE exchange, GENE newGene);
    public void ADACanceled();
    public void endPhase3Selected();
    
    public void divisionAmoebaSelected(Point loc, int amoebaNum);
    public void cellDivisionCanceled();
    public void heal2Selected(Amoeba healed, Amoeba transferred);
    public void heal2Canceled();
    public void endPhase4Selected();

    public void AGRCanceled();

    public void repaint();

    public void ESCSelected();
    public void DEFSelected();
    public void defenderGiveUpSelected();
    public void ESCTENSelected(Point loc, int[] TENfoods);
    
    public void soupCellSelected(Point loc);
    public void amoebaSelected(int playerID, int amoebaNum);
    public void amoebaMouseEntered(int playerID, int amoebaNum);
    public void amoebaMouseExited(int playerID, int amoebaNum);

    public void connectServer(String playerName, String hostName, int port, boolean join, String iconName, String bgName, int preferredColor1, int preferredColor2);
    //for local view mode
    public void addPlayer(String playerName, String hostName, int port, String iconName, String bgName, int preferredColor1, int preferredColor2);

    public SoupBackground getSoupBackground();
}
