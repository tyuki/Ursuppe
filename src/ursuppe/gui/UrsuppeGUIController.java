/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ursuppe.gui;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import sound.SoundContainer;
import ursuppe.PropertyContainer;
import ursuppe.UrsuppeApp;
import ursuppe.UrsuppeResource;
import ursuppe.ai.AIPlayer;
import ursuppe.control.Organizer;
import ursuppe.control.UrsuppeController;
import ursuppe.control.UrsuppeViewer;
import sound.SOUND_EVENT;
import ursuppe.control.DummyController;
import ursuppe.game.Amoeba;
import ursuppe.game.EnvironmentCard;
import ursuppe.game.GameOption;
import ursuppe.game.GameState;
import ursuppe.game.GameState.ATTACK_RESPONSE;
import ursuppe.game.GameState.DIRECTION;
import ursuppe.game.GameState.PHASE1_ACTION;
import ursuppe.game.GenePool.GENE;
import ursuppe.game.Player;
import ursuppe.game.Soup;
import ursuppe.gui.dialog.AmoebaNumberSelectionBox;
import ursuppe.gui.dialog.CleanlinessNotificationBox;
import ursuppe.gui.dialog.ConnectServerBox;
import ursuppe.gui.dialog.DefensiveActionsBox;
import ursuppe.gui.dialog.FoodSelectionBox;
import ursuppe.gui.dialog.GeneDefectHandlingBox;
import ursuppe.gui.dialog.Healing2SelectionBox;
import ursuppe.gui.dialog.HighlyAdaptableBox;
import ursuppe.gui.dialog.ReplayControlBox;
import ursuppe.gui.dialog.SelectInitialScoreBox;
import ursuppe.gui.dialog.SensingDisplayBox;
import ursuppe.gui.dialog.StartServerBox;
import ursuppe.gui.dialog.SuctionSelectionBox;
import ursuppe.gui.dialog.TentacleBox;
import ursuppe.gui.dialog.TestCustomIconBox;
import ursuppe.gui.icon.IconContainer;
import ursuppe.gui.icon.SoupBackground;
import ursuppe.gui.icon.SoupBackgroundContainer;

/**
 *
 * @author Personal
 */
public class UrsuppeGUIController implements UrsuppeViewer, UrsuppeController, UrsuppeGUI, UrsuppeServerGUI {

    private StartServerBox _ssBox;
    protected ConnectServerBox _csBox;
    private SelectInitialScoreBox _sisBox;
    private AmoebaNumberSelectionBox _ansBox;
    private SensingDisplayBox _sdBox;
    private TentacleBox _TENbox;
    private Healing2SelectionBox _HEA2box;
    private HighlyAdaptableBox _ADAbox;
    private SuctionSelectionBox _SUCbox;
    private FoodSelectionBox _foodSelectionBox;
    private CleanlinessNotificationBox _CLEbox;
    private DefensiveActionsBox _DEFbox;

    private ReplayControlBox _replayBox;
    private GameState replayInitialState;

    private final UrsuppeGUIControllerWrapper controllerWrapper;
    protected int _playerID = -1;
    private String playerName;
    private String iconName;
    private String bgName;
    protected Organizer _organizer;
    private BOARD_SELECTION_STATE _boardState = BOARD_SELECTION_STATE.NONE;
    private GENE_SELECTION_STATE _geneState = GENE_SELECTION_STATE.NONE;
    private AMOEBA_HOVER_STATE _hoverState = AMOEBA_HOVER_STATE.NONE;
    
    private SoundController soundController;
    
    private EatingOption _eatingOption = new EatingOption();
    private AttackingOption _attackingOption = new AttackingOption();
    private SoupPanel _soupPanel;
    private PlayerPanel[] _playerPanels;
    private RegularGenePanel _regularGenePanel;
    private ExtraGenePanel _extraGenePanel;
    private ControlPanel _controlPanel;
    private ChatPanel _chatPanel;
    protected JTextPane _descriptionPane;
    protected JTextPane _systemMessagePane;
//    protected JMenuItem _startServerMenu;
    protected JMenuItem _connectServerMenu;
    protected JMenuItem _joinGameMenu;
    protected JMenuItem _leaveGameMenu;
    protected JMenuItem _replayMenu;
//    protected JMenuItem _addAIMenu;
//    protected JMenuItem _removeAIMenu;
//    protected JMenuItem _startGameMenu;
    protected JMenu _soundMenu;
    protected JMenuItem _testIconMenu;
    protected JMenu _soupBGmenu;
    protected JRadioButtonMenuItem _soupBGdefault;
    protected JMenu _soundSetMenu;
    protected JRadioButtonMenuItem _soundSetDefault;
    protected ButtonGroup _soundSetGroup;

    protected String _soupBGname = "default";
    
    protected int preferredColor1 = -1;
    protected int preferredColor2 = -1;

    //menu specific for local viewer
    protected JMenuItem _addPlayerMenu;
    protected List<DummyController> dummies = new ArrayList<>();
    
    public SoundController getSoundController() {
         return soundController;
    }

    @Override
    public SoupBackground getSoupBackground() {
        return SoupBackgroundContainer.getSoupBackGround(_soupBGname);
    }

    @Override
    public void repaint() {
        _soupPanel.repaint();
        for (PlayerPanel pp : _playerPanels) {
            pp.repaint();
        }
        _regularGenePanel.repaint();
        _extraGenePanel.repaint();
        _controlPanel.repaint();
    }

    public static class GUIControllerComponents {

        public Organizer organizer;
        public SoupPanel soupPanel;
        public PlayerPanel[] playerPanels;
        public RegularGenePanel regularGenePanel;
        public ExtraGenePanel extraGenePanel;
        public ControlPanel controlPanel;
        public ChatPanel chatPanel;
        public JTextPane descriptionPane;
        public JTextPane systemMessagePane;
//        public JMenuItem startServerMenu;
        public JMenuItem connectServerMenu;
        public JMenuItem joinGameMenu;
        public JMenuItem leaveGameMenu;
        public JMenuItem replayMenu;
//        public JMenuItem startGameMenu;
//        public JMenuItem addAIMenu;
//        public JMenuItem removeAIMenu;
        public JMenu soundMenu;
        public JMenuItem testIconMenu;
        public JMenu soupBGmenu;
        public JRadioButtonMenuItem soupBGdefault;
        public ButtonGroup soupGroup;
        public JMenu soundSetMenu;
        public JRadioButtonMenuItem soundSetDefault;
        public ButtonGroup soundSetGroup;

        public JMenuItem addPlayerMenu;
    }

    @SuppressWarnings("LeakingThisInConstructor")
    public UrsuppeGUIController(GUIControllerComponents components) {
        _organizer = components.organizer;
        _soupPanel = components.soupPanel;
        _playerPanels = components.playerPanels;
        _regularGenePanel = components.regularGenePanel;
        _extraGenePanel = components.extraGenePanel;
        _controlPanel = components.controlPanel;
        _chatPanel = components.chatPanel;
        _descriptionPane = components.descriptionPane;
        _systemMessagePane = components.systemMessagePane;

//        AIPlayer AI = new AIPlayer(null, bgName, _playerID);
//        UrsuppeGUIControllerWrapper wrapper = new UrsuppeGUIControllerWrapper(new UrsuppeController[] {this, AI} );
        controllerWrapper = new UrsuppeGUIControllerWrapper(this);

        _organizer.setController(controllerWrapper);

        _organizer.setViewer(this);
        _chatPanel.setGUI(this);
        _soupPanel.setGUI(this);
        _regularGenePanel.setGUI(this);
        _extraGenePanel.setGUI(this);
        _controlPanel.setGUI(this);

        for (int i = 0; i < _playerPanels.length; i++) {
            _playerPanels[i].setGUI(this);
        }

        javax.swing.text.Style style = _descriptionPane.addStyle("10pts", null);
        StyleConstants.setFontSize(style, 30);

//         _startServerMenu = components.startServerMenu;
        _connectServerMenu = components.connectServerMenu;
        _joinGameMenu = components.joinGameMenu;
        _leaveGameMenu = components.leaveGameMenu;
        _addPlayerMenu = components.addPlayerMenu;
//        _startGameMenu = components.startGameMenu;
//        _addAIMenu = components.addAIMenu;
//        _removeAIMenu = components.removeAIMenu;
        _replayMenu = components.replayMenu;

        _soundMenu = components.soundMenu;
        _testIconMenu = components.testIconMenu;

        _soupBGmenu = components.soupBGmenu;
        _soupBGdefault = components.soupBGdefault;
        
        _soundSetMenu = components.soundSetMenu;
        _soundSetDefault = components.soundSetDefault;
        _soundSetGroup = components.soundSetGroup;


        //avoid null pointer when used by different types of viewers
        if (_joinGameMenu == null) {
            _joinGameMenu = new JMenuItem();
        }
        if (_leaveGameMenu == null) {
            _leaveGameMenu = new JMenuItem();
        }
        if (_addPlayerMenu == null) {
            _addPlayerMenu = new JMenuItem();
        }

//        _startServerMenu.setEnabled(true);
        _connectServerMenu.setEnabled(true);
        _joinGameMenu.setEnabled(false);
        _leaveGameMenu.setEnabled(false);
        _addPlayerMenu.setEnabled(false);
//        _addAIMenu.setEnabled(false);
//        _removeAIMenu.setEnabled(false);


        //add custom backgrounds
        {
            final String prevBG = PropertyContainer.getProperty(PropertyContainer.HISTORY_FILE, "option.soupBG");
            if (prevBG != null) {
                setSoupBackground(prevBG);
            }
            for (final String soupBGname : SoupBackgroundContainer.getCustomSoupBackgrounds()) {
                JRadioButtonMenuItem rb = new JRadioButtonMenuItem(soupBGname);
                _soupBGmenu.add(rb);
                components.soupGroup.add(rb);
                if (prevBG != null && soupBGname.contentEquals(prevBG)) {
                    rb.setSelected(true);
                }
                rb.addActionListener(new java.awt.event.ActionListener() {

                    @Override
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        if (((JRadioButtonMenuItem) evt.getSource()).isSelected()) {
                            setSoupBackground(soupBGname);
                        }
                    }
                });
            }

            components.soupGroup.add(_soupBGdefault);
            _soupBGdefault.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    if (((JRadioButtonMenuItem) evt.getSource()).isSelected()) {
                        setSoupBackground("default");
                    }
                }
            });
        }
        
        
        // add custom sound sets
        {
            final String prevSS = PropertyContainer.getProperty(PropertyContainer.HISTORY_FILE, "option.soundSet");
            if (prevSS != null) {
                setSoundSet(prevSS);
            }
            for (final String soundSetName : SoundContainer.INSTANCE.getSoundSets()) {
                if (soundSetName.contentEquals("default")) continue;
                
                JRadioButtonMenuItem rb = new JRadioButtonMenuItem(soundSetName);
                _soundSetMenu.add(rb);
                _soundSetGroup.add(rb);
                if (prevSS != null && soundSetName.contentEquals(prevSS)) {
                    rb.setSelected(true);
                }
                rb.addActionListener(new java.awt.event.ActionListener() {

                    @Override
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        if (((JRadioButtonMenuItem) evt.getSource()).isSelected()) {
                            setSoundSet(soundSetName);
                        }
                    }
                });
            }
            _soundSetGroup.add(_soundSetDefault);
            _soundSetDefault.addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    if (((JRadioButtonMenuItem) evt.getSource()).isSelected()) {
                        setSoundSet("default");
                    }
                }
            });
        }
        
         soundController = new SoundController(_soundMenu);



        // Bind actions to menus
//        _startServerMenu.addActionListener(new java.awt.event.ActionListener() {
//            public void actionPerformed(ActionEvent e) {
//                startServerMenuActionPerformed(e);
//            }
//        });

        _connectServerMenu.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                connectServerMenuActionPerformed(e);
            }
        });

        _joinGameMenu.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                joinGameMenuActionPerformed(evt);
            }
        });

        _leaveGameMenu.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                leaveGameMenuActionPerformed(evt);
            }
        });
        
        _addPlayerMenu.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addPlayerMenuActionPerformed(evt);
            }
        });



//        _startGameMenu.addActionListener(new java.awt.event.ActionListener() {
//            public void actionPerformed(java.awt.event.ActionEvent evt) {
//                startGameMenuActionPerformed(evt);
//            }
//        });
//
//        _addAIMenu.addActionListener(new java.awt.event.ActionListener() {
//            public void actionPerformed(java.awt.event.ActionEvent evt) {
//                addAIMenuActionPerformed(evt);
//            }
//        });
//
//        _removeAIMenu.addActionListener(new java.awt.event.ActionListener() {
//            public void actionPerformed(java.awt.event.ActionEvent evt) {
//                removeAIMenuActionPerformed(evt);
//            }
//        });

        _replayMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                replayMenuActionPerformed(evt);
            }
        });


        _testIconMenu.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                testIconMenu(evt);
            }
        });

    }

    protected Organizer getOrganizer() {
        return _organizer;
    }

    public void replayMode(GameState state) {
        replayInitialState = state;
        _controlPanel.enableGameSettingDisplay();
        _playerID = -100;

    }

    private void setSoupBackground(String name) {
        _soupBGname = name;
        PropertyContainer.setProperty(PropertyContainer.HISTORY_FILE, "option.soupBG", name);
        PropertyContainer.saveProperties();

        SoupBackgroundContainer.getSoupBackGround(_soupBGname).applyColors();
        IconContainer.clearCustomIconCache();
        _soupPanel.soupBackgroundChanged();
        for (PlayerPanel pp : _playerPanels) {
            pp.soupBackgroundChanged();
        }
    }
    
    private void setSoundSet(String name) {
        SoundContainer.INSTANCE.setSoundSet(name);
        PropertyContainer.setProperty(PropertyContainer.HISTORY_FILE, "option.soundSet", name);
        PropertyContainer.saveProperties();
    }

    @Override
    public void errorReport(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    @Override
    public void gameReset() {
        //nothing
    }

    public void setMenuState() {

//        _startServerMenu.setEnabled(false);
        _connectServerMenu.setEnabled(false);
        _joinGameMenu.setEnabled(false);
        _leaveGameMenu.setEnabled(false);
        _addPlayerMenu.setEnabled(false);
//        _addAIMenu.setEnabled(false);
//        _removeAIMenu.setEnabled(false);
        _replayMenu.setEnabled(false);
        _testIconMenu.setEnabled(false);

        if (!getOrganizer().isServerRunning() && !getOrganizer().isConnected()) {
//            _startServerMenu.setEnabled(true);
            _connectServerMenu.setEnabled(true);
        }

        if (getOrganizer().isConnected() && !getOrganizer().getCurrentGameState().isStarted()) {
            boolean inGame = false;
            for (Player player : getOrganizer().getCurrentGameState().getPlayers()) {
                if (player.getID() == this._playerID) {
                    inGame = true;
                    break;
                }
            }
            if (inGame) {
                _leaveGameMenu.setEnabled(true);
            } else {
                _joinGameMenu.setEnabled(true);
            }
        }
        if (getOrganizer().isConnected()) {
            _addPlayerMenu.setEnabled(true);
        }


        if (!getOrganizer().isConnected()) {
            _replayMenu.setEnabled(true);
            _testIconMenu.setEnabled(true);
        }

//        if (getOrganizer().isServerRunning() && getOrganizer().isConnected()) {
//            if (!getOrganizer().getCurrentGameState().isStarted()) {
//                if (getOrganizer().getCurrentGameState().getPlayers().size() >= 3) {
//                    _startGameMenu.setEnabled(true);
//                }
//                if (getOrganizer().getCurrentGameState().getPlayers().size() <= 5) {
//                    _addAIMenu.setEnabled(true);
//                }
//                if (getOrganizer().getAIcount() > 0) {
//                    _removeAIMenu.setEnabled(true);
//                }
//            }
//        }
    }

    /****UrsuppeViewer****/
    @Override
    public void setGameState(GameState game) {
        setMenuState();

        if (game.getPlayers().isEmpty()) {
            _playerID = -1;
        }

        //Players
        List<Player> players = game.getPlayers();
        for (int i = 0; i < _playerPanels.length; i++) {
            if (i < players.size()) {
                _playerPanels[i].setPlayer(players.get(i));
            } else {
                _playerPanels[i].setPlayer(null);
            }
        }

        //Soup (Food)
        _soupPanel.setSoup(game.getSoup());
        //Soup (Amoebas)
        //Soup (ENV)
        _soupPanel.setEnvironment(game.getCurrentEnvironment(), game.getEnvironment());
        //Score
        _soupPanel.setScore(players);
        //Dice reset
        _soupPanel.setDice(game.getProgress().dice1, game.getProgress().dice2);
        //Progress
        _soupPanel.setProgress(game.getProgress());
        //Genes
        _regularGenePanel.setGene(game.getGenePool());
        _extraGenePanel.setGene(game.getGenePool());
        _regularGenePanel.clearAvailability();
        if (game.isExtraGeneUsed()) {
            _extraGenePanel.clearAvailability();
        } else {
            _extraGenePanel.setAvailability(null);
        }
        //Player amoeba
        if (game.getProgress().started) {
            for (int i = 0; i < game.getPlayers().size(); i++) {
                if (i == game.getProgress().currentOrder) {
                    _playerPanels[i].animateAmoeba();
                } else {
                    _playerPanels[i].stopAnimation();
                }
            }
        } else if (game.getProgress().ended) {
            for (int i = 0; i < game.getPlayers().size(); i++) {
                _playerPanels[i].stopAnimation();
            }
        }

        repaint();
//            UrsuppeApp.getApplication().getMainFrame().repaint();
//            _gui.repaint();
    }

    @Override
    public void addUserMessage(String text, SimpleAttributeSet attr) {
        _chatPanel.addText(text, attr);
    }

    @Override
    public void addSystemMessage(String text, SimpleAttributeSet attr) {
        try {
            _systemMessagePane.getDocument().insertString(_systemMessagePane.getDocument().getEndPosition().getOffset(), text + "\n", attr);
            _systemMessagePane.setCaretPosition(_systemMessagePane.getDocument().getEndPosition().getOffset() - 1);
        } catch (BadLocationException ex) {
        }
    }

    @Override
    public void geneUsed(int playerID, GENE gene) {
        for (int i = 0; i < _playerPanels.length; i++) {
            _playerPanels[i].showGeneUsed(playerID, gene);
        }
    }

    @Override
    public void soundEvent(SOUND_EVENT evt) {
        if (soundController != null)
            soundController.soundEvent(evt);
    }

    @Override
    public void moveEvent(Amoeba amoeba, Point src, int srcIndex, Point dst, int dstIndex) {
        _soupPanel.moveEvent(amoeba, src, srcIndex, dst, dstIndex);
    }

    /****Ursuppe Controllerr****/
    @Override
    public void setID(int id) {
        _playerID = id;
        _controlPanel.enableGameSettingDisplay();
    }

    @Override
    public void selectInitialScore(int playerID, GameState game) {
        if (!game.isExtraGeneUsed()) {
            _extraGenePanel.setAvailability(null);
        }
        _controlPanel.clearControl();

        if (playerID != _playerID) {
            return;

        }
        soundEvent(SOUND_EVENT.TURN);

        _sisBox = new SelectInitialScoreBox(UrsuppeApp.getApplication().getMainFrame(), this, game.getAvailableInitialScores());
        _sisBox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
        UrsuppeApp.getApplication().show(_sisBox);
    }

    @Override
    public void selectInitialAmoeba(int playerID, GameState game) {
        if (playerID != _playerID) {
            _controlPanel.clearControl();
            _soupPanel.resetAvailability();
            return;
        }
        soundEvent(SOUND_EVENT.TURN);

        _controlPanel.setControlPhase0(true);

        boolean[][] availability = game.getInitialPlacementAvailability();
        _soupPanel.setAvailability(availability, playerID);

        _boardState = BOARD_SELECTION_STATE.INITIAL_AMOEBA;
    }

    @Override
    public void moveResponse(int playerID, int amoebaIndex, List<DIRECTION> dirs, GameState game) {
        _controlPanel.clearControl();

        if (playerID != _playerID) {
            return;
        }
        Player player = game.getPlayerByID(playerID);
        Amoeba amoeba = player.getAmoebas().get(amoebaIndex);
        boolean[][] avail = game.getAvailableDestination(player, amoeba.getLocation(), dirs);
        _soupPanel.setAvailability(avail, -1);
        _soupPanel.emphasize(amoeba.getLocation());

        _boardState = BOARD_SELECTION_STATE.PHASE1_MOVE;

    }

    @Override
    public void selectPhase1Action(int playerID, int amoebaIndex, GameState game) {
        _controlPanel.clearControl();
        _hoverState = AMOEBA_HOVER_STATE.NONE;
        _boardState = BOARD_SELECTION_STATE.NONE;
        _soupPanel.resetAvailability();
        _soupPanel.clearAmoebaEmphasis();

        if (playerID < 0) {
            return;
            //animate the amoeba in question

        }
        Player player = game.getPlayerByID(playerID);
        Amoeba amoeba = player.getAmoebas().get(amoebaIndex);

        _soupPanel.animateAmoeba(amoeba);
        //soupPanel.emphasizeAmoeba(amoeba);
        if (playerID != _playerID) {
            return;
        }
        _soupPanel.emphasize(amoeba.getLocation());

        List<PHASE1_ACTION> actions = game.getAvailablePhase1Actions(playerID, amoebaIndex);

        //Normal eat, but has CLE or TOX
        if (actions.size() == 1 && actions.contains(PHASE1_ACTION.EAT) && (player.hasGene(GENE.CLE) || player.hasGene(GENE.TOX))) {
            eatSelected();
            //You have to choose to do normal eat
        } else if (actions.size() == 1 && actions.contains(PHASE1_ACTION.EAT_WITH_CHOICE)) {
            eatSelected();
            //Eat with genese possible
        } else if (actions.size() == 2 && actions.contains(PHASE1_ACTION.EAT_WITH_GENES) && actions.contains(PHASE1_ACTION.STARVE)) {
            eatSelected();
            //Eating normally or with genes are both possible
        } else if (actions.size() == 2 && (actions.contains(PHASE1_ACTION.EAT) || actions.contains(PHASE1_ACTION.EAT_WITH_CHOICE)) && actions.contains(PHASE1_ACTION.EAT_WITH_GENES)) {
            eatSelected();
            //Has other options than eating
        } else {
            //SFS ON
            if (actions.contains(PHASE1_ACTION.SFS)) {
                _hoverState = AMOEBA_HOVER_STATE.SFS;
                _boardState = BOARD_SELECTION_STATE.PHASE1_SFS;
            }
            //If SFS on with no moving options
            if (actions.contains(PHASE1_ACTION.SFS) && !actions.contains(PHASE1_ACTION.DRIFT) && !actions.contains(PHASE1_ACTION.MOVE)) {
                boolean[][] emphasis = new boolean[Soup.SOUP_WIDTH][Soup.SOUP_HEIGHT];
                emphasis[amoeba.getLocation().x][amoeba.getLocation().y] = true;
                _soupPanel.setAvailability(emphasis, -1);
            }

            _controlPanel.setControlPhase1(actions.contains(PHASE1_ACTION.DRIFT), actions.contains(PHASE1_ACTION.MOVE), actions.contains(PHASE1_ACTION.HOLD),
                    actions.contains(PHASE1_ACTION.SPEED), (actions.contains(PHASE1_ACTION.SPEED) && actions.size() == 1),
                    actions.contains(PHASE1_ACTION.EAT) || actions.contains(PHASE1_ACTION.EAT_WITH_CHOICE),
                    actions.contains(PHASE1_ACTION.SFS), actions.contains(PHASE1_ACTION.STARVE));
        }

    }

    @Override
    public void handleGeneDefect(int playerID, GameState game) {
        _controlPanel.clearControl();
        _soupPanel.stopAllAnimation();
        _soupPanel.clearAmoebaEmphasis();
        _hoverState = AMOEBA_HOVER_STATE.NONE;
        _boardState = BOARD_SELECTION_STATE.NONE;

        if (playerID != _playerID) {
            return;
        }
        
        soundEvent(SOUND_EVENT.TURN);
        
        GeneDefectHandlingBox gdhBox = new GeneDefectHandlingBox(UrsuppeApp.getApplication().getMainFrame(), this, game.getPlayerByID(playerID), game);
        gdhBox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
        UrsuppeApp.getApplication().show(gdhBox);
    }

    @Override
    public void selectPhase3Action(int playerID, GameState game) {
        _hoverState = AMOEBA_HOVER_STATE.NONE;
        _boardState = BOARD_SELECTION_STATE.NONE;

        if (playerID == _playerID) {
            _controlPanel.setControlPhase3(game.getPlayerByID(playerID).hasGene(GENE.ADA));
            _regularGenePanel.setAvailability(game.getPurchasableGenes(game.getPlayerByID(playerID)));

            if (game.isExtraGeneUsed()) {
                _extraGenePanel.setAvailability(game.getPurchasableGenes(game.getPlayerByID(playerID)));
            } else {
                _extraGenePanel.setAvailability(null);
            }

            _geneState = GENE_SELECTION_STATE.NORMAL;
        } else {
            _controlPanel.clearControl();
            _regularGenePanel.clearAvailability();
            if (game.isExtraGeneUsed()) {
                _extraGenePanel.clearAvailability();
            }
        }
    }

    @Override
    public void selectPhase4Action(int playerID, GameState game) {
        _controlPanel.clearControl();
        _soupPanel.resetAvailability();
        _hoverState = AMOEBA_HOVER_STATE.NONE;
        _boardState = BOARD_SELECTION_STATE.NONE;

        if (playerID != _playerID) {
            return;
        }
        Player player = game.getPlayerByID(playerID);
        boolean[][] avail = game.getSpawnableLocations(player);
        _soupPanel.setAvailability(avail, playerID);

        _controlPanel.setControlPhase4(player.hasGene(GENE.HEA1), player.hasGene(GENE.HEA2));
        if (player.hasGene(GENE.HEA1) || player.hasGene(GENE.HEA2)) {
            _hoverState = AMOEBA_HOVER_STATE.HEAL;


        }
        _boardState = BOARD_SELECTION_STATE.PHASE4_DIVISION;
    }

    @Override
    public void death(int playerID, int amoebaID, GameState game) {
        _controlPanel.clearControl();
        _soupPanel.resetAvailability();
        _hoverState = AMOEBA_HOVER_STATE.NONE;
        _boardState = BOARD_SELECTION_STATE.NONE;

        if (playerID != _playerID) {
            return;
        }
        boolean useBANG = false;
        if (game.getPlayerByID(playerID).hasGene(GENE.BANG) && game.getProgress().BANGactive) {
            useBANG = checkUseBAN(game.getProgress().BANGlocation);
        }

        getOrganizer().deathAck(useBANG);
    }

    @Override
    public void scoring(int playerID, GameState game) {
        _controlPanel.clearControl();
        _soupPanel.resetAvailability();
        _hoverState = AMOEBA_HOVER_STATE.NONE;
        _boardState = BOARD_SELECTION_STATE.NONE;

        if (playerID != _playerID) {
            return;
        }
    }

    @Override
    public void decideHOL(int playerID, int amoebaIndex, DIRECTION dir, GameState game) {
        _controlPanel.clearControl();
        if (playerID != _playerID) {
            return;
        }
        soundEvent(SOUND_EVENT.NOTICE);

        Player player = game.getPlayerByID(playerID);
        Amoeba amoeba = player.getAmoebas().get(amoebaIndex);

        boolean[][] emphasis = new boolean[Soup.SOUP_WIDTH][Soup.SOUP_HEIGHT];
        Point loc = amoeba.getLocation();
        Point dst = game.getDestiation(loc, dir);
        emphasis[loc.x][loc.y] = true;
        emphasis[dst.x][dst.y] = true;

        _soupPanel.setAvailability(emphasis, -1);
        _soupPanel.emphasize(loc);

        boolean useHOL = checkUseHOL(amoeba.number, dir);

        //TEN box only opens when at least one food is in the cell TODO
        if (useHOL && game.isTENusable(player, amoeba, dir)) {
            if (_TENbox != null) {
                _TENbox.dispose();

            }
            _TENbox = new TentacleBox(UrsuppeApp.getApplication().getMainFrame(), this, game.getSoup().getFoodColors(), game.getSoup().getCell(amoeba.getLocation()).getFood(), TentacleBox.MODE.HOLD, useHOL);
            _TENbox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
            UrsuppeApp.getApplication().show(_TENbox);
        } else {
            _soupPanel.clearEmphasis();
            _soupPanel.resetAvailability();
            getOrganizer().HOLSelected(useHOL, null);
        }
    }

    @Override
    public void decidePOP(int playerID, GameState game) {
        _controlPanel.clearControl();
        if (playerID != _playerID) {
            return;
        }
        int topNum = game.getPlayerByOrder(0).getNumAmoebas();

        getOrganizer().POPdecided(checkUsePOP(topNum));
    }

    @Override
    public void decideAGR(int playerID, GameState game) {
        if (playerID != _playerID) {
            return;
        }
        Player player = game.getCurrentPlayer();

        List<Amoeba> usable = game.getAGRusableAmoebas(player);

        if (usable.isEmpty()) {
            getOrganizer().AGRCanceled();
        } else {
            _boardState = BOARD_SELECTION_STATE.PHASE5_AGR;
            _hoverState = AMOEBA_HOVER_STATE.AGR;

            //beep
            soundEvent(SOUND_EVENT.TURN);

            boolean[][] avail = new boolean[Soup.SOUP_WIDTH][Soup.SOUP_HEIGHT];
            for (Amoeba amoeba : usable) {
                avail[amoeba.getLocation().x][amoeba.getLocation().y] = true;
            }
            _soupPanel.setAvailability(avail, -1);
            _controlPanel.setControlAGR();
        }
    }

    @Override
    public void defenderAction(int attackerID, int attackerAmoebaNum, int defenderID, int defenderAmoebaNum, GameState game) {
        if (defenderID != _playerID) {
            return;
        }
        Player attacker = game.getPlayerByID(attackerID);
        Amoeba attackerAmoeba = attacker.getAmoebaByNumber(attackerAmoebaNum);

        Player defender = game.getPlayerByID(defenderID);
        Amoeba defenderAmoeba = defender.getAmoebaByNumber(defenderAmoebaNum);

        //Get list of defense
        List<ATTACK_RESPONSE> atkRes = game.getAttackResponse(attacker, attackerAmoeba, defender, defenderAmoeba);

        _DEFbox = new DefensiveActionsBox(UrsuppeApp.getApplication().getMainFrame(), this, attackerAmoeba, defenderAmoeba, atkRes.contains(ATTACK_RESPONSE.DEFENSE), atkRes.contains(ATTACK_RESPONSE.ESCAPE));
        _DEFbox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
        UrsuppeApp.getApplication().show(_DEFbox);
    }

    @Override
    public void attackerAction(int attackerID, int attackerAmoebaNum, int defenderID, int defenderAmoebaNum, boolean HARD, boolean MOUexcretes, GameState game) {
        if (attackerID != _playerID) {
            return;
        }
        _attackingOption.clear();

        Player attacker = game.getPlayerByID(attackerID);
        Amoeba attackerAmoeba = attacker.getAmoebaByNumber(attackerAmoebaNum);

        boolean[][] emphasis = new boolean[Soup.SOUP_WIDTH][Soup.SOUP_HEIGHT];
        emphasis[attackerAmoeba.getLocation().x][attackerAmoeba.getLocation().y] = true;
        _soupPanel.setAvailability(emphasis, -1);

        //check if additional cost is paid
        if (HARD) {
            _attackingOption.payHD = checkPayForHARD();
            if (!_attackingOption.payHD) {
                _soupPanel.resetAvailability();
                getOrganizer().attackerActionSelected(false, null, false);
                return;
            }
        }

        //When excretes happen
        if (MOUexcretes) {
            //Check for tox
            if (attacker.hasGene(GENE.TOX)) {
                _attackingOption.useTox = checkUseTOX();
            }

            //Check CLE
            if (attacker.hasGene(GENE.CLE) && checkUseCLE()) {
                askCLESFS(game, attacker, attackerAmoeba);
                return;
            }
        }


        _soupPanel.resetAvailability();
        getOrganizer().attackerActionSelected(_attackingOption.payHD, null, _attackingOption.useTox);
        return;

    }

    @Override
    public void selectEscapeDirection(int defenderID, int defenderAmoebaNum, List<DIRECTION> dir, GameState game) {
        _controlPanel.clearControl();

        if (defenderID != _playerID) {
            return;
        }
        Player defender = game.getPlayerByID(defenderID);
        Amoeba defenderAmoeba = defender.getAmoebaByNumber(defenderAmoebaNum);
        boolean[][] avail = game.getAvailableDestination(defender, defenderAmoeba.getLocation(), dir);
        _soupPanel.setAvailability(avail, -1);
        _soupPanel.emphasize(defenderAmoeba.getLocation());

        _boardState = BOARD_SELECTION_STATE.ESCAPE;
    }

    @Override
    public void decideESCSPD(int defenderID, GameState game) {
        if (defenderID != _playerID) {
            return;
        }
        Player defender = game.getPlayerByID(defenderID);
        Amoeba defenderAmoeba = defender.getAmoebaByNumber(game.getProgress().defenderAmoebaNum);
        _soupPanel.emphasize(defenderAmoeba.getLocation());

        if (checkUseSPD()) {
            _soupPanel.clearEmphasis();
            getOrganizer().ESCSelected();
        } else {
            _soupPanel.clearEmphasis();
            getOrganizer().ESCAck();
        }
    }

    @Override
    public void decideSFSBANG(int defenderID, int defenderAmoebaNum, GameState game) {
        if (defenderID != _playerID) {
            return;
        }
        getOrganizer().SFSAck(checkUseBAN(game.getProgress().defenderBANGloc));
    }

    @Override
    public void showNextEnvironment(EnvironmentCard env) {
        if (_sdBox != null) {
            _sdBox.dispose();
        }
        _sdBox = new SensingDisplayBox(UrsuppeApp.getApplication().getMainFrame(), env);
        _sdBox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
        UrsuppeApp.getApplication().show(_sdBox);

    }

    /*****Ursuppe GUI****/
    @Override
    public void setControlDescription(String text) {
        _descriptionPane.setText(text);
        _descriptionPane.setCaretPosition(0);
    }

    @Override
    public void setGeneDescription(String text) {
        setControlDescription(text);
        //descriptionPane.setText(text);
        //descriptionPane.setCaretPosition(0);
    }

    @Override
    public void setControlDescriptionCarretToHead() {
        _descriptionPane.setCaretPosition(0);
    }

    @Override
    public void setControlDescriptionCarretToTail() {
        try {
            _descriptionPane.setCaretPosition(_descriptionPane.getDocument().getEndPosition().getOffset() - 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setGeneDescriptionCarretToHead() {
        setControlDescriptionCarretToHead();
    }

    @Override
    public void setGeneDescriptionCarretToTail() {
        setControlDescriptionCarretToTail();
    }

    @Override
    public void showGameSetting() {
        if (getOrganizer().getCurrentGameState() != null) {
            setControlDescription(getOrganizer().getCurrentGameState().getRuleDescription());
        } else if (replayInitialState != null) {
            setControlDescription(replayInitialState.getRuleDescription());
        }
    }

    @Override
    public void sendChatMessage(String text) {
        if (text.matches("^/start$")) {
            getOrganizer().sendStartGameCommand();
        } else if (text.matches("^/reset$")) {
            getOrganizer().sendResetGameCommand();
        } else if (text.matches("^/cancel$")) {
            getOrganizer().sendCancelResetCommand();
        } else if (text.matches("^/addAI$")) {
            getOrganizer().sendAddAICommand();
        } else if (text.matches("^/removeAI$")) {
            getOrganizer().sendRemoveAICommand();
        } else if (text.matches("^/setController\\s+\\d+")) {
            Matcher matcher = Pattern.compile("^/setController\\s+(\\d+)").matcher(text);
            if (matcher.matches()) {
                try {
                    int id = Integer.parseInt(matcher.group(1));
                    int res = controllerWrapper.setCurrentController(id);
                    if (res == 0) {
                        addUserMessage("Controller set to Player", null);
                    } else if (res == 1) {
                        addUserMessage("Controller set to AI", null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                getOrganizer().sendChatMessage(text);
            }
        } else if (text.matches("^/fetchReplay$")) {
            getOrganizer().sendFetchReplayCommand();
        } else if (text.matches("^/toggleExGene$")) {
            getOrganizer().sendToggleExtraGeneCommand();
        } else if (text.matches("^/toggleSFS$")) {
            getOrganizer().sendToggleSFSCommand();
        } else if (text.matches("^/toggleHOL$")) {
            getOrganizer().sendToggleHOLCommand();
        } else if (text.matches("^/toggleBANG$")) {
            getOrganizer().sendToggleBANGCommand();
        } else if (text.matches("^/togglePAR$")) {
            getOrganizer().sendTogglePARCommand();
        } else {
            getOrganizer().sendChatMessage(text);
        }
    }

    @Override
    public void initialScoreSelected(int score) {
        _sisBox.dispose();

        getOrganizer().initialScoreSelected(score);
    }

    @Override
    public void initialAmoebaSelected(Point loc, int amoebaNum) {
        _ansBox.dispose();

        _boardState = BOARD_SELECTION_STATE.NONE;
        _soupPanel.resetAvailability();

        getOrganizer().initialAmoebaSelected(amoebaNum, loc);
    }

    @Override
    public void initialAmoebaCanceled() {
        _ansBox.dispose();

        _boardState = BOARD_SELECTION_STATE.NONE;
        _soupPanel.resetAvailability();
        selectInitialAmoeba(_playerID, getOrganizer().getCurrentGameState());
    }

    @Override
    public void driftSelected() {
        _controlPanel.clearControl();

        GameState game = getOrganizer().getCurrentGameState();
        Player player = game.getCurrentPlayer();
        Amoeba amoeba = game.getCurrentAmoeba();

        boolean useREB = false;
        //check if rebound can be used
        if (player.hasGene(GENE.REB) && game.doesHitWall(amoeba.getLocation(), game.getCurrentEnvironment().direction)
                && !game.doesHitWall(amoeba.getLocation(), game.getOppositeDirection(game.getCurrentEnvironment().direction))) {
            useREB = checkUseREB();
        }

        DIRECTION dir = game.getCurrentEnvironment().direction;
        if (useREB) {
            dir = game.getOppositeDirection(dir);

            //check if TEN can be used

        }
        if (game.isTENusable(player, amoeba, dir)) {
            boolean[][] emphasis = new boolean[Soup.SOUP_WIDTH][Soup.SOUP_HEIGHT];
            Point loc = amoeba.getLocation();
            Point dst = game.getDestiation(loc, dir);
            emphasis[loc.x][loc.y] = true;
            emphasis[dst.x][dst.y] = true;
            _soupPanel.setAvailability(emphasis, -1);
            _soupPanel.emphasize(loc);

            if (_TENbox != null) {
                _TENbox.dispose();

            }
            _TENbox = new TentacleBox(UrsuppeApp.getApplication().getMainFrame(), this, game.getSoup().getFoodColors(), game.getSoup().getCell(amoeba.getLocation()).getFood(), TentacleBox.MODE.DRIFT, useREB);
            _TENbox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
            UrsuppeApp.getApplication().show(_TENbox);
        } else {
            getOrganizer().amoebaDriftSelected(null, useREB);
        }
    }

    @Override
    public void driftTENSelected(int[] TENfoods, boolean useREB) {
        _soupPanel.resetAvailability();
        _soupPanel.clearEmphasis();
        getOrganizer().amoebaDriftSelected(TENfoods, useREB);
    }

    @Override
    public void moveTENSelected(Point loc, int[] TENfoods) {
        _soupPanel.resetAvailability();
        _soupPanel.clearEmphasis();
        getOrganizer().amoebaMoveSelected(loc, TENfoods);
    }

    @Override
    public void moveSelected() {

        GameState game = getOrganizer().getCurrentGameState();
        int cost = game.getMovementCost(_playerID);
        int dice = game.getMovementDiceNum(_playerID);

        if (cost == 0) {
            _controlPanel.clearControl();
            getOrganizer().amoebaMoveSelected();
        } else {
            int res = JOptionPane.showConfirmDialog(null, UrsuppeResource.getString("confirm.move" + cost + "" + dice + ".text"), UrsuppeResource.getString("confirm.move.title"),
                    JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
            if (res == JOptionPane.YES_OPTION) {
                _controlPanel.clearControl();
                getOrganizer().amoebaMoveSelected();
            }
        }
    }

    @Override
    public void starveSelected() {
        _controlPanel.clearControl();

        getOrganizer().starveSelected();
    }

    @Override
    public void eatSelected() {
        _controlPanel.clearControl();
        _eatingOption.clear();

        //getOrganizer().starveSelected();
        GameState game = getOrganizer().getCurrentGameState();
        Player player = game.getCurrentPlayer();
        Amoeba amoeba = game.getCurrentAmoeba();
        List<PHASE1_ACTION> options = game.getFeedingOptions(player, amoeba);

        if (options.contains(PHASE1_ACTION.EAT_WITH_CHOICE) || options.contains(PHASE1_ACTION.EAT_WITH_GENES)) {
            //ask
            //Suc first
            if (player.hasGene(GENE.SUC1) || player.hasGene(GENE.SUC2)) {
                askSUC(game);
                //Then foods
            } else {
                askFoodsToEat(game, player, amoeba, null);
            }
        } else {
            //TOX
            if (player.hasGene(GENE.TOX)) {
                _eatingOption.useTOX = checkUseTOX();
            }
            //CLE
            if (player.hasGene(GENE.CLE) && checkUseCLE()) {
                askCLE(game, player, amoeba);
                return;
            }

            getOrganizer().eatSelected(null, _eatingOption.useTOX);
        }
    }

    private void askSUC(GameState game) {

        Point loc = game.getCurrentAmoeba().getLocation();
        _soupPanel.setAvailability(game.getSuctionLocation(loc), -1);
        _soupPanel.emphasize(loc);
        _boardState = BOARD_SELECTION_STATE.SUCTION;

        _SUCbox = new SuctionSelectionBox(UrsuppeApp.getApplication().getMainFrame(), this, game.getCurrentPlayer(), loc, game);
        _SUCbox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
        UrsuppeApp.getApplication().show(_SUCbox);
    }

    private void askFoodsToEat(GameState game, Player player, Amoeba amoeba, int[] sucFoods) {
        _foodSelectionBox = new FoodSelectionBox(UrsuppeApp.getApplication().getMainFrame(), this, game, player, amoeba, sucFoods);
        _foodSelectionBox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
        UrsuppeApp.getApplication().show(_foodSelectionBox);
    }

    public void CLESelected(Point exLoc) {
        _boardState = BOARD_SELECTION_STATE.NONE;
        if (_CLEbox != null) {
            _CLEbox.dispose();

        }
        _eatingOption.exLoc = exLoc;

        _soupPanel.resetAvailability();
        _soupPanel.clearEmphasis();

        //Auto Eat
        if (_eatingOption.auto) {
            GameState game = getOrganizer().getCurrentGameState();
            Player player = game.getCurrentPlayer();
            Amoeba amoeba = game.getCurrentAmoeba();

            getOrganizer().autoEat(player, amoeba, _eatingOption.sucLoc, _eatingOption.sucFoods, _eatingOption.exLoc, _eatingOption.useTOX);
            //No food or SUC selected
        } else if (_eatingOption.foods == null && _eatingOption.sucFoods == null) {
            getOrganizer().eatSelected(_eatingOption.exLoc, _eatingOption.useTOX);
        } else {
            getOrganizer().eatSelected(_eatingOption.foods, _eatingOption.pars, _eatingOption.sucLoc, _eatingOption.sucFoods, _eatingOption.exLoc, _eatingOption.useTOX);
        }
    }

    public void CLESFSSelected(Point exLoc) {
        _boardState = BOARD_SELECTION_STATE.NONE;
        if (_CLEbox != null) {
            _CLEbox.dispose();

        }
        _attackingOption.exLoc = exLoc;

        _soupPanel.resetAvailability();
        _soupPanel.clearEmphasis();

        getOrganizer().attackerActionSelected(_attackingOption.payHD, exLoc, _attackingOption.useTox);
    }

    @Override
    public void SPDSelected() {
        _controlPanel.clearControl();

        getOrganizer().amoebaMoveSelected();
    }

    @Override
    public void cancelSPDSelected() {
        _controlPanel.clearControl();

        getOrganizer().cancelSPDSelected();
    }

    @Override
    public void HOLSelected() {
        _controlPanel.clearControl();

        getOrganizer().HOLSelected();
    }

    @Override
    public void HOLTENSelected(int[] TENfoods) {
        _controlPanel.clearControl();
        _soupPanel.clearEmphasis();
        _soupPanel.resetAvailability();

        getOrganizer().HOLSelected(true, TENfoods);
    }

    @Override
    public void SUCSelected(Point SUCloc, int[] SUCfoods) {
        _controlPanel.clearControl();
        _soupPanel.clearEmphasis();
        _soupPanel.resetAvailability();
        _boardState = BOARD_SELECTION_STATE.NONE;
        _SUCbox.dispose();

        GameState game = getOrganizer().getCurrentGameState();
        Player player = game.getCurrentPlayer();
        Amoeba amoeba = game.getCurrentAmoeba();

        boolean eatNormal = game.canFeedAfterSUC(player, amoeba.getLocation(), SUCfoods, false, false) && game.getPlayers().size() > 3 && game.getPlayers().size() < 6;
        boolean eatGene = (game.canFeedAfterSUC(player, amoeba.getLocation(), SUCfoods, false, false) && (game.getPlayers().size() == 3 || game.getPlayers().size() == 6))
                || game.canFeedAfterSUC(player, amoeba.getLocation(), SUCfoods, true, false) || game.canFeedAfterSUC(player, amoeba.getLocation(), SUCfoods, true, true);

        int sum = 0;
        for (int n : SUCfoods) {
            sum += n;


        }
        if (sum == 0) {
            _eatingOption.sucLoc = null;
            _eatingOption.sucFoods = null;
        } else {
            _eatingOption.sucLoc = SUCloc;
            _eatingOption.sucFoods = SUCfoods;
        }

        //When Gene can be used, ask
        if (eatGene) {
            askFoodsToEat(game, player, amoeba, SUCfoods);
            //When now possible eatnormally,
        } else if (eatNormal) {
            //TOX
            if (player.hasGene(GENE.TOX)) {
                _eatingOption.useTOX = checkUseTOX();
            }
            //CLE
            if (player.hasGene(GENE.CLE) && checkUseCLE()) {
                askCLE(game, player, amoeba);
                return;
            }

            if (sum == 0) {
                getOrganizer().eatSelected(null, _eatingOption.useTOX);
            } else {
                getOrganizer().eatSelected(null, null, _eatingOption.sucLoc, _eatingOption.sucFoods, null, _eatingOption.useTOX);
            }
            //else starve
        } else {
            getOrganizer().starveSelected();
        }
    }

    @Override
    public void foodsSelected(int[] foods, int[] parPlayers) {
        _eatingOption.foods = foods;
        _eatingOption.pars = parPlayers;

        GameState game = getOrganizer().getCurrentGameState();
        Player player = game.getCurrentPlayer();
        Amoeba amoeba = game.getCurrentAmoeba();

        //TOX
        if (player.hasGene(GENE.TOX)) {
            _eatingOption.useTOX = checkUseTOX();
        }
        //CLE
        if (player.hasGene(GENE.CLE) && checkUseCLE()) {
            askCLE(game, player, amoeba);
            return;
        }

        getOrganizer().eatSelected(_eatingOption.foods, _eatingOption.pars, _eatingOption.sucLoc, _eatingOption.sucFoods, _eatingOption.exLoc, _eatingOption.useTOX);

    }

    @Override
    public void autoEatSelected() {
        _eatingOption.auto = true;

        GameState game = getOrganizer().getCurrentGameState();
        Player player = game.getCurrentPlayer();
        Amoeba amoeba = game.getCurrentAmoeba();

        //TOX
        if (player.hasGene(GENE.TOX)) {
            _eatingOption.useTOX = checkUseTOX();
        }
        //CLE
        if (player.hasGene(GENE.CLE) && checkUseCLE()) {
            askCLE(game, player, amoeba);
            return;
        }

        getOrganizer().autoEat(player, amoeba, _eatingOption.sucLoc, _eatingOption.sucFoods, null, _eatingOption.useTOX);

    }

    @Override
    public void geneDefectHandled(GENE[] discards) {
        getOrganizer().geneDefectsHandled(discards);
    }

    @Override
    public void geneSelected(GENE gene) {

        if (_geneState == GENE_SELECTION_STATE.NORMAL) {
            _controlPanel.clearControl();
            _geneState = GENE_SELECTION_STATE.NONE;
            _controlPanel.clearControl();
            _regularGenePanel.clearAvailability();
            if (getOrganizer().getCurrentGameState().isExtraGeneUsed()) {
                _extraGenePanel.clearAvailability();
            }
            getOrganizer().genePurchase(gene);
        } else if (_geneState == GENE_SELECTION_STATE.ADA) {
            _ADAbox.newGeneSelected(gene);
        }
    }

    @Override
    public void ADASelected() {
        GameState game = getOrganizer().getCurrentGameState();
        _ADAbox = new HighlyAdaptableBox(UrsuppeApp.getApplication().getMainFrame(), this, game, game.getPlayerByID(_playerID));
        _ADAbox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
        UrsuppeApp.getApplication().show(_ADAbox);
        _geneState = GENE_SELECTION_STATE.ADA;
        _regularGenePanel.setAvailability(game.getExchangableGenes(game.getCurrentPlayer(), null));
        _extraGenePanel.setAvailability(game.getExchangableGenes(game.getCurrentPlayer(), null));

    }

    @Override
    public void ADAexchangeGeneSelected(GENE exchange) {
        GameState game = getOrganizer().getCurrentGameState();
        _regularGenePanel.setAvailability(game.getExchangableGenes(game.getCurrentPlayer(), exchange));
        _extraGenePanel.setAvailability(game.getExchangableGenes(game.getCurrentPlayer(), exchange));
    }

    @Override
    public void ADAexchangeSelected(GENE exchange, GENE newGene) {
        _ADAbox.dispose();
        _geneState = GENE_SELECTION_STATE.NONE;
        _controlPanel.clearControl();

        getOrganizer().ADASelected(exchange, newGene);
    }

    @Override
    public void ADACanceled() {
        _ADAbox.dispose();
        _geneState = GENE_SELECTION_STATE.NONE;
        selectPhase3Action(_playerID, getOrganizer().getCurrentGameState());
    }

    @Override
    public void endPhase3Selected() {
        _geneState = GENE_SELECTION_STATE.NONE;
        _controlPanel.clearControl();
        _regularGenePanel.clearAvailability();
        _extraGenePanel.clearAvailability();
        getOrganizer().endGenePurchase();
    }

    @Override
    public void divisionAmoebaSelected(Point loc, int amoebaNum) {
        _ansBox.dispose();

        _boardState = BOARD_SELECTION_STATE.NONE;
        _soupPanel.resetAvailability();
        _controlPanel.clearControl();

        getOrganizer().cellDivision(amoebaNum, loc);
    }

    @Override
    public void cellDivisionCanceled() {
        _ansBox.dispose();

        _boardState = BOARD_SELECTION_STATE.NONE;
        _soupPanel.resetAvailability();

        GameState game = getOrganizer().getCurrentGameState();
        selectPhase4Action(game.getCurrentPlayer().getID(), game);
    }

    @Override
    public void heal2Selected(Amoeba healed, Amoeba transferred) {
        _HEA2box.dispose();
        _HEA2box = null;
        _controlPanel.clearControl();

        _soupPanel.resetAvailability();
        getOrganizer().HEALselected(healed, transferred);
    }

    @Override
    public void heal2Canceled() {
        _HEA2box.dispose();
        _HEA2box = null;
        selectPhase4Action(_playerID, getOrganizer().getCurrentGameState());
    }

    @Override
    public void endPhase4Selected() {
        if (_ansBox != null) {
            _ansBox.dispose();
        }

        _boardState = BOARD_SELECTION_STATE.NONE;
        _soupPanel.resetAvailability();
        _controlPanel.clearControl();

        getOrganizer().endCellDivision();
    }

    @Override
    public void AGRCanceled() {
        _boardState = BOARD_SELECTION_STATE.NONE;
        _hoverState = AMOEBA_HOVER_STATE.NONE;
        _soupPanel.resetAvailability();
        _controlPanel.clearControl();

        getOrganizer().AGRCanceled();
    }

    @Override
    public void ESCSelected() {
        getOrganizer().ESCSelected();
    }

    @Override
    public void DEFSelected() {
        getOrganizer().DEFSelected();
    }

    @Override
    public void defenderGiveUpSelected() {
        getOrganizer().defenderGiveup();
    }

    @Override
    public void ESCTENSelected(Point loc, int[] TENfoods) {
        _soupPanel.resetAvailability();
        _soupPanel.clearEmphasis();

        getOrganizer().ESCDirectionSelected(loc, TENfoods);
    }

    /***Ursuppe GUI Generic Board Selection ***/
    @Override
    public void soupCellSelected(Point loc) {
        if (!getOrganizer().isConnected()) return;

        if (_boardState == BOARD_SELECTION_STATE.NONE) {
            return;


        }
        GameState game = getOrganizer().getCurrentGameState();
        Player player = game.getCurrentPlayer();
        Amoeba amoeba;

        switch (_boardState) {
            case INITIAL_AMOEBA:
                List<Integer> nums = getOrganizer().getCurrentGameState().getRemainingAmoebaNumbers(_playerID);
                if (_ansBox != null) {
                    _ansBox.dispose();

                }
                _ansBox = new AmoebaNumberSelectionBox(UrsuppeApp.getApplication().getMainFrame(), this, nums, AmoebaNumberSelectionBox.MODE.INITIAL_AMOEBA, loc);
                _ansBox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
                UrsuppeApp.getApplication().show(_ansBox);
                _controlPanel.clearControl();
                break;
            case PHASE1_MOVE:
                amoeba = game.getCurrentAmoeba();
                _controlPanel.clearControl();
                if (game.isTENusable(player, amoeba, game.getDirectionToReachNewLocation(amoeba.getLocation(), loc))) {
                    if (_TENbox != null) {
                        _TENbox.dispose();

                    }
                    _TENbox = new TentacleBox(UrsuppeApp.getApplication().getMainFrame(), this, game.getSoup().getFoodColors(), game.getSoup().getCell(amoeba.getLocation()).getFood(), TentacleBox.MODE.MOVE, loc);
                    _TENbox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
                    UrsuppeApp.getApplication().show(_TENbox);
                } else {
                    _soupPanel.resetAvailability();
                    _soupPanel.clearEmphasis();
                    getOrganizer().amoebaMoveSelected(loc, null);
                }
                _boardState = BOARD_SELECTION_STATE.NONE;
                break;
            case PHASE4_DIVISION:
                List<Integer> numsD = getOrganizer().getCurrentGameState().getRemainingAmoebaNumbers(_playerID);
                if (_ansBox != null) {
                    _ansBox.dispose();

                }
                _ansBox = new AmoebaNumberSelectionBox(UrsuppeApp.getApplication().getMainFrame(), this, numsD, AmoebaNumberSelectionBox.MODE.PHASE4_AMOEBA, loc);
                _ansBox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
                UrsuppeApp.getApplication().show(_ansBox);
                _controlPanel.clearControl();
                break;
            case CLEANLINESS:
                CLESelected(loc);
                _controlPanel.clearControl();
                break;
            case CLEANLINESS_SFS:
                CLESFSSelected(loc);
                _controlPanel.clearControl();
                break;
            case SUCTION:
                _soupPanel.clearEmphasis();
                _soupPanel.emphasize(loc);
                _SUCbox.newLocationSelected(loc);
                _controlPanel.clearControl();
                break;
            case ESCAPE:
                Player defender = game.getPlayerByID(game.getProgress().defenderID);
                Amoeba defenderAmoeba = defender.getAmoebaByNumber(game.getProgress().defenderAmoebaNum);
                _controlPanel.clearControl();
                //TEN
                if (game.isTENusable(defender, defenderAmoeba, game.getDirectionToReachNewLocation(defenderAmoeba.getLocation(), loc))) {
                    if (_TENbox != null) {
                        _TENbox.dispose();

                    }
                    _TENbox = new TentacleBox(UrsuppeApp.getApplication().getMainFrame(), this, game.getSoup().getFoodColors(), game.getSoup().getCell(defenderAmoeba.getLocation()).getFood(), TentacleBox.MODE.ESC, loc);
                    _TENbox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
                    UrsuppeApp.getApplication().show(_TENbox);
                } else {
                    _soupPanel.resetAvailability();
                    _soupPanel.clearEmphasis();
                    getOrganizer().ESCDirectionSelected(loc, null);
                }
                _boardState = BOARD_SELECTION_STATE.NONE;
                break;
        }
    }

    @Override
    public void amoebaSelected(int playerID, int amoebaNum) {
        if (!getOrganizer().isConnected()) return;

        GameState game = getOrganizer().getCurrentGameState();
        Player player = game.getCurrentPlayer();
        Player selectedPlayer = game.getPlayerByID(playerID);
        Amoeba amoeba = selectedPlayer.getAmoebaByNumber(amoebaNum);

        switch (_boardState) {
            case PHASE1_SFS:
                Amoeba attackerAmoeba = game.getCurrentAmoeba();
                List<ATTACK_RESPONSE> res = game.getAttackResponse(player, attackerAmoeba, selectedPlayer, amoeba);
                if (res.size() > 0 && !res.contains(ATTACK_RESPONSE.BLOCKED) && checkUseSFS()) {
                    _boardState = BOARD_SELECTION_STATE.NONE;
                    _hoverState = AMOEBA_HOVER_STATE.NONE;
                    _controlPanel.clearControl();
                    _soupPanel.resetAvailability();
                    getOrganizer().SFSSelected(playerID, amoebaNum);
                }
                break;
            case PHASE5_AGR:
                List<Amoeba> usable = game.getAGRusableAmoebas(player);
                attackerAmoeba = null;
                for (Amoeba aa : usable) {
                    if (aa.getLocation().equals(amoeba.getLocation())) {
                        attackerAmoeba = aa;

                    }
                }
                if (attackerAmoeba == null) {
                    return;

                }
                List<ATTACK_RESPONSE> resAGR = game.getAttackResponse(player, attackerAmoeba, selectedPlayer, amoeba);
                if (resAGR.size() > 0 && !resAGR.contains(ATTACK_RESPONSE.BLOCKED) && checkUseAGR()) {
                    _boardState = BOARD_SELECTION_STATE.NONE;
                    _hoverState = AMOEBA_HOVER_STATE.NONE;
                    _controlPanel.clearControl();
                    _soupPanel.resetAvailability();
                    getOrganizer().AGRSelected(attackerAmoeba.number, playerID, amoebaNum);
                }
                break;
            case PHASE4_DIVISION:

                //healing
                if (player.hasGene(GENE.HEA1) || player.hasGene(GENE.HEA2)) {
                    //heal1 only and healing is possible, or has heal 2, but has exactly 3 BP
                    if (((player.hasGene(GENE.HEA1) && !player.hasGene(GENE.HEA2)) || (player.hasGene(GENE.HEA2) && player.getBP() == 3)) && game.canHeal(player, amoeba)) {
                        if (checkHEA1used()) {
                            _controlPanel.clearControl();
                            _soupPanel.resetAvailability();
                            getOrganizer().HEALselected(amoeba, null);
                        }
                        //heal2 needs a separate dialog
                    } else {
                        //Healing target choice
                        if (_HEA2box == null) {
                            //not a valid healing target
                            if (player.getID() != selectedPlayer.getID()) {
                                return;

                            }
                            if (amoeba.getDP() == 0) {
                                return;


                            }
                            _HEA2box = new Healing2SelectionBox(UrsuppeApp.getApplication().getMainFrame(), this, game, player);
                            _HEA2box.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
                            UrsuppeApp.getApplication().show(_HEA2box);

                            _soupPanel.resetAvailability();
                            _controlPanel.clearControl();
                            boolean[][] emphasis = new boolean[Soup.SOUP_WIDTH][Soup.SOUP_HEIGHT];
                            emphasis[amoeba.getLocation().x][amoeba.getLocation().y] = true;
                            _soupPanel.setAvailability(emphasis, playerID);
                        }

                        _HEA2box.setAmoeba(amoeba);
                    }
                }
                break;
        }
    }

    @Override
    public void amoebaMouseEntered(int playerID, int amoebaNum) {
        if (!getOrganizer().isConnected()) return;
        
        if (_hoverState == AMOEBA_HOVER_STATE.NONE) {
            return;


        }
        GameState game = getOrganizer().getCurrentGameState();
        Player player = game.getCurrentPlayer();
        Amoeba attackerAmoeba;
        Player targetPlayer = game.getPlayerByID(playerID);
        Amoeba amoeba = targetPlayer.getAmoebaByNumber(amoebaNum);


        if (player.getID() != _playerID) {
            return;
        }
        switch (_hoverState) {
            //SFS
            case SFS:
                attackerAmoeba = game.getCurrentAmoeba();
                _soupPanel.setDefense(amoeba, game.getAttackResponse(player, attackerAmoeba, targetPlayer, amoeba));
                break;
            //AGR
            case AGR:
                List<Amoeba> usable = game.getAGRusableAmoebas(player);
                attackerAmoeba = null;
                for (Amoeba aa : usable) {
                    if (aa.getLocation().equals(amoeba.getLocation())) {
                        attackerAmoeba = aa;

                    }
                }
                if (attackerAmoeba == null) {
                    return;

                }
                _soupPanel.setDefense(amoeba, game.getAttackResponse(player, attackerAmoeba, targetPlayer, amoeba));
                break;
            //Healing
            case HEAL:
                if ((player.hasGene(GENE.HEA1) || player.hasGene(GENE.HEA2)) && amoeba.playerID == _playerID && amoeba.getDP() > 0) {
                    _soupPanel.emphasizeAmoeba(amoeba);
                }
                break;
        }
    }

    @Override
    public void amoebaMouseExited(int playerID, int amoebaNum) {
        if (!getOrganizer().isConnected()) return;
        
        GameState game = getOrganizer().getCurrentGameState();
        Player player = game.getPlayerByID(playerID);
        Amoeba amoeba = player.getAmoebaByNumber(amoebaNum);

        _soupPanel.resetDefense(amoeba);
    }

    private boolean checkUseBAN(Point BANGloc) {
        boolean[][] location = new boolean[Soup.SOUP_WIDTH][Soup.SOUP_WIDTH];
        location[BANGloc.x][BANGloc.y] = true;
        _soupPanel.setAvailability(location, -1);

        int BANGres = JOptionPane.showConfirmDialog(null, UrsuppeResource.getString("confirm.BANG.text") + "\n"
                + UrsuppeResource.getString("term.location") + ":" + GameState.pointToString(BANGloc),
                UrsuppeResource.getString("confirm.BANG.title"), JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);

        _soupPanel.resetAvailability();

        return BANGres == JOptionPane.YES_OPTION;
    }

    /**confirm dialogs**/
    private boolean checkUseHOL(int amoebaNum, DIRECTION dir) {
        int HOLres = JOptionPane.showConfirmDialog(null, UrsuppeResource.getString("confirm.HOL.text") + "\n"
                + UrsuppeResource.getString("term.amoeba") + " #" + amoebaNum + "\n"
                + UrsuppeResource.getString("term.direction") + ":" + UrsuppeResource.getString("term." + GameState.getDirectionString(dir)),
                UrsuppeResource.getString("confirm.HOL.title"),
                JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);

        return HOLres == JOptionPane.YES_OPTION;
    }

    private boolean checkUsePOP(int topNum) {

        int POPres = JOptionPane.showConfirmDialog(null, UrsuppeResource.getString("confirm.POP.text") + "\n"
                + UrsuppeResource.getString("confirm.POP.note") + " : " + Integer.toString(topNum),
                UrsuppeResource.getString("confirm.POP.title"),
                JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);

        return POPres == JOptionPane.YES_OPTION;
    }

    private boolean checkUseREB() {
        int rebRes = JOptionPane.showConfirmDialog(null, UrsuppeResource.getString("confirm.REB.text"), UrsuppeResource.getString("confirm.REB.title"),
                JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);

        return rebRes == JOptionPane.YES_OPTION;
    }

    private boolean checkUseTOX() {
        int TOXres = JOptionPane.showConfirmDialog(null, UrsuppeResource.getString("confirm.TOX.text"), UrsuppeResource.getString("confirm.TOX.title"),
                JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
        return TOXres == JOptionPane.YES_OPTION;
    }

    private boolean checkUseCLE() {
        _CLEbox = new CleanlinessNotificationBox();
        _CLEbox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
        UrsuppeApp.getApplication().show(_CLEbox);

        return true;
        /*int CLEres = JOptionPane.showConfirmDialog(null, UrsuppeResource.getString("confirm.CLE.text"), UrsuppeResource.getString("confirm.CLE.title"),
        JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
        return CLEres == JOptionPane.YES_OPTION;*/
    }

    private void askCLE(GameState game, Player player, Amoeba amoeba) {
        _boardState = BOARD_SELECTION_STATE.CLEANLINESS;
        boolean[][] avail = game.getAvailableDestination(player, amoeba.getLocation(),
                new ArrayList<DIRECTION>() {

                    {
                        add(DIRECTION.NONE);
                        add(DIRECTION.NORTH);
                        add(DIRECTION.SOUTH);
                        add(DIRECTION.EAST);
                        add(DIRECTION.WEST);
                    }
                });
        _soupPanel.setAvailability(avail, -1);
        _soupPanel.emphasize(amoeba.getLocation());
    }

    private void askCLESFS(GameState game, Player player, Amoeba amoeba) {
        _boardState = BOARD_SELECTION_STATE.CLEANLINESS_SFS;
        _soupPanel.resetAvailability();
        boolean[][] avail = game.getAvailableDestination(player, amoeba.getLocation(),
                new ArrayList<DIRECTION>() {

                    {
                        add(DIRECTION.NONE);
                        add(DIRECTION.NORTH);
                        add(DIRECTION.SOUTH);
                        add(DIRECTION.EAST);
                        add(DIRECTION.WEST);
                    }
                });
        _soupPanel.setAvailability(avail, -1);
        _soupPanel.emphasize(amoeba.getLocation());
    }

    private boolean checkHEA1used() {
        int HEA1res = JOptionPane.showConfirmDialog(null, UrsuppeResource.getString("confirm.HEA1.text"), UrsuppeResource.getString("confirm.HEA1.title"),
                JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);

        return HEA1res == JOptionPane.YES_OPTION;
    }

    private boolean checkUseSFS() {
        int SFSres = JOptionPane.showConfirmDialog(null, UrsuppeResource.getString("confirm.SFS.text"), UrsuppeResource.getString("confirm.SFS.title"),
                JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);

        return SFSres == JOptionPane.YES_OPTION;
    }

    private boolean checkUseAGR() {
        int AGRres = JOptionPane.showConfirmDialog(null, UrsuppeResource.getString("confirm.AGR.text"), UrsuppeResource.getString("confirm.AGR.title"),
                JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);

        return AGRres == JOptionPane.YES_OPTION;
    }

    private boolean checkPayForHARD() {
        int HARDres = JOptionPane.showConfirmDialog(null, UrsuppeResource.getString("confirm.HARD.text"), UrsuppeResource.getString("confirm.HARD.title"),
                JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);

        return HARDres == JOptionPane.YES_OPTION;
    }

    private boolean checkUseSPD() {
        int SPDres = JOptionPane.showConfirmDialog(null, UrsuppeResource.getString("confirm.SPD.text"), UrsuppeResource.getString("confirm.SPD.title"),
                JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);

        return SPDres == JOptionPane.YES_OPTION;
    }

    /**MENU ACTIONS**/
    private void startServerMenuActionPerformed(java.awt.event.ActionEvent evt) {
        _testIconMenu.setEnabled(false);
        _ssBox = new StartServerBox(UrsuppeApp.getApplication().getMainFrame(), this);
        _ssBox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
        UrsuppeApp.getApplication().show(_ssBox);
    }
    public void startServerCanceled() {}

    @Override
    public void startServer(int port, GameOption gameOption) {
        if (getOrganizer().startServer(port, gameOption)) {
            addSystemMessage("ServerStarted", null);
            _ssBox.dispose();
            //Open CSBox to localhost
            _csBox = new ConnectServerBox(UrsuppeApp.getApplication().getMainFrame(), this, port, false);
            _csBox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
            UrsuppeApp.getApplication().show(_csBox);
        }


    }

    private void connectServerMenuActionPerformed(java.awt.event.ActionEvent evt) {
        _testIconMenu.setEnabled(false);
        _csBox = new ConnectServerBox(UrsuppeApp.getApplication().getMainFrame(), this, false);
        _csBox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
        UrsuppeApp.getApplication().show(_csBox);
    }

    public void connectServer(String hostName, int port) {
        getOrganizer().connectServer(hostName, port);
    }

    @Override
    public void connectServer(String playerName, String hostName, int port, boolean join, String iconName, String bgName, int preferredColor1, int preferredColor2) {
        this.playerName = playerName;
        this.iconName = iconName;
        this.bgName = bgName;
        this.preferredColor1 = preferredColor1;
        this.preferredColor2 = preferredColor2;

        if (controllerWrapper.numControllers() < 2) {
            controllerWrapper.addController(AIPlayer.getPlayerSubstitute(getOrganizer(), playerName, -1));
        }

//        LoadingDialog loading = new LoadingDialog(UrsuppeApp.getApplication().getMainFrame(), false);
//        UrsuppeApp.getApplication().show(loading);
//        loading.sh

        _csBox.setVisible(false);
        if (getOrganizer().connectServer(playerName, hostName, port, join, iconName, bgName, preferredColor1, preferredColor2)) {
            addSystemMessage("Connected", null);
            _csBox.dispose();
            _connectServerMenu.setEnabled(false);
        } else {
            _csBox.setVisible(true);
        }
//        loading.dispose();

//        JOptionPane.showL
//        JOptionPane.
    }

    @Override
    public void addPlayer(String playerName, String hostName, int port, String iconName, String bgName, int preferredColor1, int preferredColor2) {

        _csBox.dispose();
        _csBox.setVisible(false);
        dummies.add(new DummyController(playerName, port, iconName, bgName, preferredColor1, preferredColor2));
    }

//    private void startGameMenuActionPerformed(java.awt.event.ActionEvent evt) {
//        if (getOrganizer().startGame()) {
//            setMenuState();
//        }
//    }

    private void joinGameMenuActionPerformed(java.awt.event.ActionEvent evt) {
        getOrganizer().joinGame(playerName, iconName, bgName, preferredColor1, preferredColor2);
    }

    private void leaveGameMenuActionPerformed(java.awt.event.ActionEvent evt) {
        getOrganizer().leaveGame();
    }

    protected void addPlayerMenuActionPerformed(java.awt.event.ActionEvent evt) {
        throw new UnsupportedOperationException("Should be implemented by LocalController");
    }

//    private void addAIMenuActionPerformed(java.awt.event.ActionEvent evt) {
//        getOrganizer().addAI();
//    }
//
//    private void removeAIMenuActionPerformed(java.awt.event.ActionEvent evt) {
//        getOrganizer().removeAI();
//    }

    private void replayMenuActionPerformed(java.awt.event.ActionEvent evt) {
        if (_replayBox == null || !_replayBox.isDisplayable()) {
            _replayBox = new ReplayControlBox(UrsuppeApp.getApplication().getMainFrame(), this);
            _replayBox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
            UrsuppeApp.getApplication().show(_replayBox);
        } else {
            _replayBox.toFront();
        }
    }

    private void testIconMenu(java.awt.event.ActionEvent evt) {
        TestCustomIconBox tiBox = new TestCustomIconBox(UrsuppeApp.getApplication().getMainFrame(), true);
        tiBox.setLocationRelativeTo(UrsuppeApp.getApplication().getMainFrame());
        UrsuppeApp.getApplication().show(tiBox);

    }

    protected class EatingOption {

        public int[] foods;
        public int[] pars;
        public Point sucLoc;
        public int[] sucFoods;
        public Point exLoc;
        public boolean useTOX;
        public boolean auto;

        public void clear() {
            foods = null;
            pars = null;
            sucLoc = null;
            sucFoods = null;
            exLoc = null;
            useTOX = false;

            auto = false;
        }
    }

    protected class AttackingOption {

        public boolean payHD;
        public Point exLoc;
        public boolean useTox;

        public void clear() {
            payHD = false;
            exLoc = null;
            useTox = false;
        }
    }
}
