/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * SoupPanel.java
 *
 * Created on 2010/01/15, 21:12:42
 */

package ursuppe.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.swing.GroupLayout;
import javax.swing.JPanel;
import ursuppe.game.Amoeba;
import ursuppe.game.Environment;
import ursuppe.game.EnvironmentCard;
import ursuppe.game.GameState.ATTACK_RESPONSE;
import ursuppe.game.Player;
import ursuppe.game.Progress;
import ursuppe.game.Soup;
import ursuppe.gui.UrsuppeColor.TYPE;
import ursuppe.gui.effect.Effect;
import ursuppe.gui.icon.IconContainer;

/**
 *
 * @author Personal
 */
public class SoupPanel extends javax.swing.JPanel {

    private UrsuppeGUI _gui;
    private SoupCellPanel[][] _soupCellPanels;
    private JPanel[] _scorePanels;
    private SoupScoreLabel[] _scoreLabels;
    private List<Effect> _effects;

    private static final Color SCORE_TRACK_COLOR_1_19 = new Color(0,0,0,0);
    private static final Color SCORE_TRACK_COLOR_20_29 = new Color(0,0,0,0);
    private static final Color SCORE_TRACK_COLOR_30_41 = new Color(0,0,0,0);
    private static final Color SCORE_TRACK_COLOR_42_52 = new Color(0,0,0,0);
//    private static final Color SCORE_TRACK_COLOR_1_19 = Color.decode("#9F9FFF");
//    private static final Color SCORE_TRACK_COLOR_20_29 = Color.decode("#FFCCFF");
//    private static final Color SCORE_TRACK_COLOR_30_41 = Color.decode("#FF9F9F");
//    private static final Color SCORE_TRACK_COLOR_42_52 = Color.decode("#FF6666");

    /** Creates new form SoupPanel */
    public SoupPanel() {
        initComponents();

        _effects = Collections.synchronizedList(new LinkedList<>());

        //scorePanel01.setBackground(Color.red);

        //Initialize soup array
        _soupCellPanels = new SoupCellPanel[Soup.SOUP_WIDTH][Soup.SOUP_HEIGHT];
        //first row
        _soupCellPanels[1][0] = soupCellPanel10;
        //second row
        _soupCellPanels[0][1] = soupCellPanel01;
        _soupCellPanels[1][1] = soupCellPanel11;
        _soupCellPanels[2][1] = soupCellPanel21;
        _soupCellPanels[3][1] = soupCellPanel31;
        _soupCellPanels[4][1] = soupCellPanel41;
        //third row
        _soupCellPanels[0][2] = soupCellPanel02;
        _soupCellPanels[1][2] = soupCellPanel12;
        _soupCellPanels[3][2] = soupCellPanel32;
        _soupCellPanels[4][2] = soupCellPanel42;
        //fourth row
        _soupCellPanels[0][3] = soupCellPanel03;
        _soupCellPanels[1][3] = soupCellPanel13;
        _soupCellPanels[2][3] = soupCellPanel23;
        _soupCellPanels[3][3] = soupCellPanel33;
        _soupCellPanels[4][3] = soupCellPanel43;
        //fifth row
        _soupCellPanels[0][4] = soupCellPanel04;
        _soupCellPanels[1][4] = soupCellPanel14;
        _soupCellPanels[2][4] = soupCellPanel24;
        _soupCellPanels[3][4] = soupCellPanel34;

        for (int x = 0; x < Soup.SOUP_WIDTH; x++) {
            for (int y = 0; y < Soup.SOUP_HEIGHT; y++) {
                if ((x==2&&y==2) || (x==4&&y==4) || (y==0 && x != 1)) {
                    //skip environment panel and dice panel
                } else if ((y == 0 && x != 1)) {
                    _soupCellPanels[x][y].setCoordinate(x, y);
                    _soupCellPanels[x][y].setType(SoupCellPanel.CELL_TYPE.ROCK);
                } else {
                    _soupCellPanels[x][y].setCoordinate(x, y);
                    _soupCellPanels[x][y].setType(SoupCellPanel.CELL_TYPE.SOUP);
                }
            }
        }

        //initialize score panel
        _scorePanels = new JPanel[52];
        _scorePanels[0] = startPanel;
        _scorePanels[1] = scorePanel01;
        _scorePanels[2] = scorePanel02;
        _scorePanels[3] = scorePanel03;
        _scorePanels[4] = scorePanel04;
        _scorePanels[5] = scorePanel05;
        _scorePanels[6] = scorePanel06;
        _scorePanels[7] = scorePanel07;
        _scorePanels[8] = scorePanel08;
        _scorePanels[9] = scorePanel09;
        _scorePanels[10] = scorePanel10;
        _scorePanels[11] = scorePanel11;
        _scorePanels[12] = scorePanel12;
        _scorePanels[13] = scorePanel13;
        _scorePanels[14] = scorePanel14;
        _scorePanels[15] = scorePanel15;
        _scorePanels[16] = scorePanel16;
        _scorePanels[17] = scorePanel17;
        _scorePanels[18] = scorePanel18;
        _scorePanels[19] = scorePanel19;
        _scorePanels[20] = scorePanel20;
        _scorePanels[21] = scorePanel21;
        _scorePanels[22] = scorePanel22;
        _scorePanels[23] = scorePanel23;
        _scorePanels[24] = scorePanel24;
        _scorePanels[25] = scorePanel25;
        _scorePanels[26] = scorePanel26;
        _scorePanels[27] = scorePanel27;
        _scorePanels[28] = scorePanel28;
        _scorePanels[29] = scorePanel29;
        _scorePanels[30] = scorePanel30;
        _scorePanels[31] = scorePanel31;
        _scorePanels[32] = scorePanel32;
        _scorePanels[33] = scorePanel33;
        _scorePanels[34] = scorePanel34;
        _scorePanels[35] = scorePanel35;
        _scorePanels[36] = scorePanel36;
        _scorePanels[37] = scorePanel37;
        _scorePanels[38] = scorePanel38;
        _scorePanels[39] = scorePanel39;
        _scorePanels[40] = scorePanel40;
        _scorePanels[41] = scorePanel41;
        _scorePanels[42] = scorePanel42;
        _scorePanels[43] = scorePanel43;
        _scorePanels[44] = scorePanel44;
        _scorePanels[45] = scorePanel45;
        _scorePanels[46] = scorePanel46;
        _scorePanels[47] = scorePanel47;
        _scorePanels[48] = scorePanel48;
        _scorePanels[49] = scorePanel49;
        _scorePanels[50] = scorePanel50;
        _scorePanels[51] = scorePanel51;

        _scoreLabels = new SoupScoreLabel[52];
        for (int i = 0; i < 52; i++) {
            _scoreLabels[i] = new SoupScoreLabel(i);
            _scoreLabels[i].setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
            _scorePanels[i].setBorder(null);
            
            GroupLayout layout = (GroupLayout) _scorePanels[i].getLayout();
            layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(_scoreLabels[i], javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(_scoreLabels[i], javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
            );

        }

    }



    public void setGUI(UrsuppeGUI gui) {
        _gui = gui;
        environmentPanel.setGUI(gui);
        progressPanel1.setGUI(gui);
        for (int x = 0; x < Soup.SOUP_WIDTH; x++) {
            for (int y = 0; y < Soup.SOUP_HEIGHT; y++) {
                if ((x==2&&y==2) || (x==4&&y==4) || (y==0 && x != 1)) {
                    //skip environment panel and dice panel
                } else {
                    _soupCellPanels[x][y].setGUI(gui);
                }
            }
        }
        for (int i = 0; i < 52; i++) {
            _scoreLabels[i].setGUI(gui);
        }
    }

    public void setSoup(Soup soup) {
        for (int x = 0; x < Soup.SOUP_WIDTH; x++) {
            for (int y = 0; y < Soup.SOUP_HEIGHT; y++) {
                if (Soup.isValidLocation(x, y)) {
                    _soupCellPanels[x][y].setFoodColors(soup.getFoodColors());
                    _soupCellPanels[x][y].setSoupCell(soup.getCell(x,y));
                    _soupCellPanels[x][y].emphasize(false);
                }
            }
        }
    }

    public void setScore(List<Player> players) {

        //reset color
//        for (int i = 1; i < _scorePanels.length; i++) {
//            if (i > 0 && i < 20) _scorePanels[i].setBackground(SCORE_TRACK_COLOR_1_19);
//            if (i > 19 && i < 30) _scorePanels[i].setBackground(SCORE_TRACK_COLOR_20_29);
//            if (i > 29 && i < 42) _scorePanels[i].setBackground(SCORE_TRACK_COLOR_30_41);
//            if (i > 41 && i < 52) _scorePanels[i].setBackground(SCORE_TRACK_COLOR_42_52);
//        }

//        for (int i = 1; i < 42; i++) _scorePanels[i].setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        for (int i = 1; i < 52; i++) _scorePanels[i].setBorder(null);
        for (int i = 1; i < 52; i++) _scoreLabels[i].setIcon(null);
        for (int i = 1; i < 52; i++) _scoreLabels[i].setImage(null);

        //set player colors
        for (Player player : players) {
            if (player.getAdvance() > 0 && player.getAdvance() < _scorePanels.length) {
                _scoreLabels[player.getAdvance()].setImage(IconContainer.getCustomScoreIcon(IconContainer.CUSTOM_SCORE_ICON_NAME+player.getID(), 
                        UrsuppeColor.get(player.getColorID()).toColor(TYPE.AMOEBA)));
            }
        }
    }

    public void setEnvironment(EnvironmentCard env, Environment environment) {
        environmentPanel.setEnvironment(env);
        environmentForcastPanel1.setEnvironment(environment);
    }

    public void setProgress(Progress progress) {
        progressPanel1.setProgress(progress);
    }

    public void resetAvailability() {
        for (int x=0; x < Soup.SOUP_WIDTH; x++) {
            for (int y=0; y < Soup.SOUP_WIDTH; y++) {
                if (Soup.isValidLocation(x, y)) {
                    _soupCellPanels[x][y].setEnabled(true);
                }
            }
        }
        environmentPanel.setEnabled(true);
    }

    public void setAvailability(boolean[][] availability, int playerID) {
        for (int x=0; x < Soup.SOUP_WIDTH; x++) {
            for (int y=0; y < Soup.SOUP_WIDTH; y++) {
                if (Soup.isValidLocation(x, y)) {
                    _soupCellPanels[x][y].setEnabled(availability[x][y], playerID);
                } else if (x == 2 && y == 2) {
                    environmentPanel.setEnabled(availability[x][y]);
                }
            }
        }
    }

    public void animateAmoeba(Amoeba amoeba) {
        _soupCellPanels[amoeba.getLocation().x][amoeba.getLocation().y].animateAmoeba(amoeba);
    }

    public void stopAllAnimation() {
        for (int x=0; x < Soup.SOUP_WIDTH; x++) {
            for (int y=0; y < Soup.SOUP_WIDTH; y++) {
                if (Soup.isValidLocation(x, y)) {
                    _soupCellPanels[x][y].stopAllAnimation();
                }
            }
        }
    }

    public void emphasize(Point loc) {
        try {
            _soupCellPanels[loc.x][loc.y].emphasize(true);
        } catch (Exception e) {}
    }

    public void clearEmphasis() {
        for (int x=0; x < Soup.SOUP_WIDTH; x++) {
            for (int y=0; y < Soup.SOUP_WIDTH; y++) {
                if (Soup.isValidLocation(x, y)) {
                    _soupCellPanels[x][y].emphasize(false);
                }
            }
        }
    }

    public void setDefense(Amoeba amoeba, List<ATTACK_RESPONSE> res) {
        if (amoeba != null) {
            Point loc = amoeba.getLocation();
            _soupCellPanels[loc.x][loc.y].setDefense(amoeba, res);
        }
    }

    public void resetDefense(Amoeba amoeba) {
        if (amoeba != null) {
            Point loc = amoeba.getLocation();
            _soupCellPanels[loc.x][loc.y].resetDefense(amoeba);
        }
    }

    public void emphasizeAmoeba(Amoeba amoeba) {
        _soupCellPanels[amoeba.getLocation().x][amoeba.getLocation().y].emphasize(amoeba);
    }

    public void clearAmoebaEmphasis() {
        for (int x=0; x < Soup.SOUP_WIDTH; x++) {
            for (int y=0; y < Soup.SOUP_WIDTH; y++) {
                if (Soup.isValidLocation(x, y)) {
                    _soupCellPanels[x][y].clearEmphasis();
                }
            }
        }
    }

    public void setDice(int dice1, int dice2) {
        dicePanel.setDice(dice1, dice2);
    }

    public void moveEvent(Amoeba amoeba, Point src, int srcIndex, Point dst, int dstIndex) {
        Point start = new Point(src.x*120+30*(srcIndex % 4), src.y*120+30+30*(srcIndex / 4));
        Point end = new Point(dst.x*120+30*(dstIndex % 4), dst.y*120+30+30*(dstIndex / 4));
        Effect effect = IconContainer.getCustomIcon(IconContainer.CUSTOM_ICON_PREFIX+amoeba.playerID, UrsuppeColor.get(amoeba.colorID).toColor(TYPE.AMOEBA)).getMovingEffect(
                start, end, 100, 500);
        effect.addEffectListener(_soupCellPanels[dst.x][dst.y].getAmoebaLabel(dstIndex));
        effect.addEffectListener(_soupCellPanels[dst.x][dst.y]);
        effect.register(this, _effects);
    }
    
    @Override
    public void paintComponent(Graphics g) {
        if (_gui != null && _gui.getSoupBackground() != null) {
            g.drawImage(_gui.getSoupBackground().image.getImage(), 0, 0, null);
        }
        if (_effects.size() > 0) {
           try {
               for (Effect effect : _effects) {
                   effect.paint(g);
               }
           } catch (java.util.ConcurrentModificationException cme) {
               //cme.printStackTrace();
                _gui.repaint();
           }
        }
    }

    public void soupBackgroundChanged() {
        progressPanel1.soupBackgroundChanged();
        this.repaint();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        soupCellPanel10 = new ursuppe.gui.SoupCellPanel();
        soupCellPanel01 = new ursuppe.gui.SoupCellPanel();
        soupCellPanel11 = new ursuppe.gui.SoupCellPanel();
        soupCellPanel21 = new ursuppe.gui.SoupCellPanel();
        soupCellPanel31 = new ursuppe.gui.SoupCellPanel();
        soupCellPanel41 = new ursuppe.gui.SoupCellPanel();
        soupCellPanel02 = new ursuppe.gui.SoupCellPanel();
        soupCellPanel12 = new ursuppe.gui.SoupCellPanel();
        environmentPanel = new ursuppe.gui.EnvironmentPanel();
        soupCellPanel32 = new ursuppe.gui.SoupCellPanel();
        soupCellPanel42 = new ursuppe.gui.SoupCellPanel();
        soupCellPanel03 = new ursuppe.gui.SoupCellPanel();
        soupCellPanel13 = new ursuppe.gui.SoupCellPanel();
        soupCellPanel23 = new ursuppe.gui.SoupCellPanel();
        soupCellPanel33 = new ursuppe.gui.SoupCellPanel();
        soupCellPanel43 = new ursuppe.gui.SoupCellPanel();
        soupCellPanel04 = new ursuppe.gui.SoupCellPanel();
        soupCellPanel14 = new ursuppe.gui.SoupCellPanel();
        soupCellPanel24 = new ursuppe.gui.SoupCellPanel();
        soupCellPanel34 = new ursuppe.gui.SoupCellPanel();
        dicePanel = new ursuppe.gui.DicePanel();
        scorePanelA = new ursuppe.gui.TransparentJPanel();
        startPanel = new ursuppe.gui.TransparentJPanel();
        scorePanel01 = new ursuppe.gui.TransparentJPanel();
        scorePanel02 = new ursuppe.gui.TransparentJPanel();
        scorePanel03 = new ursuppe.gui.TransparentJPanel();
        scorePanel04 = new ursuppe.gui.TransparentJPanel();
        scorePanel05 = new ursuppe.gui.TransparentJPanel();
        scorePanel06 = new ursuppe.gui.TransparentJPanel();
        scorePanel07 = new ursuppe.gui.TransparentJPanel();
        scorePanel08 = new ursuppe.gui.TransparentJPanel();
        scorePanel09 = new ursuppe.gui.TransparentJPanel();
        scorePanel10 = new ursuppe.gui.TransparentJPanel();
        scorePanel11 = new ursuppe.gui.TransparentJPanel();
        scorePanel12 = new ursuppe.gui.TransparentJPanel();
        scorePanel13 = new ursuppe.gui.TransparentJPanel();
        scorePanel14 = new ursuppe.gui.TransparentJPanel();
        scorePanel15 = new ursuppe.gui.TransparentJPanel();
        scorePanel16 = new ursuppe.gui.TransparentJPanel();
        scorePanel17 = new ursuppe.gui.TransparentJPanel();
        scorePanel18 = new ursuppe.gui.TransparentJPanel();
        scorePanel19 = new ursuppe.gui.TransparentJPanel();
        scorePanel20 = new ursuppe.gui.TransparentJPanel();
        scorePanel21 = new ursuppe.gui.TransparentJPanel();
        scorePanel22 = new ursuppe.gui.TransparentJPanel();
        scorePanel23 = new ursuppe.gui.TransparentJPanel();
        scorePanel24 = new ursuppe.gui.TransparentJPanel();
        scorePanel25 = new ursuppe.gui.TransparentJPanel();
        scorePanelB = new ursuppe.gui.TransparentJPanel();
        scorePanel26 = new ursuppe.gui.TransparentJPanel();
        scorePanel27 = new ursuppe.gui.TransparentJPanel();
        scorePanel28 = new ursuppe.gui.TransparentJPanel();
        scorePanel29 = new ursuppe.gui.TransparentJPanel();
        scorePanel30 = new ursuppe.gui.TransparentJPanel();
        scorePanel31 = new ursuppe.gui.TransparentJPanel();
        scorePanel32 = new ursuppe.gui.TransparentJPanel();
        scorePanel33 = new ursuppe.gui.TransparentJPanel();
        scorePanel34 = new ursuppe.gui.TransparentJPanel();
        scorePanel35 = new ursuppe.gui.TransparentJPanel();
        scorePanel36 = new ursuppe.gui.TransparentJPanel();
        scorePanel37 = new ursuppe.gui.TransparentJPanel();
        scorePanel38 = new ursuppe.gui.TransparentJPanel();
        scorePanel39 = new ursuppe.gui.TransparentJPanel();
        scorePanel40 = new ursuppe.gui.TransparentJPanel();
        scorePanel41 = new ursuppe.gui.TransparentJPanel();
        scorePanel42 = new ursuppe.gui.TransparentJPanel();
        scorePanel43 = new ursuppe.gui.TransparentJPanel();
        scorePanel44 = new ursuppe.gui.TransparentJPanel();
        scorePanel45 = new ursuppe.gui.TransparentJPanel();
        scorePanel46 = new ursuppe.gui.TransparentJPanel();
        scorePanel47 = new ursuppe.gui.TransparentJPanel();
        scorePanel48 = new ursuppe.gui.TransparentJPanel();
        scorePanel49 = new ursuppe.gui.TransparentJPanel();
        scorePanel50 = new ursuppe.gui.TransparentJPanel();
        scorePanel51 = new ursuppe.gui.TransparentJPanel();
        goalPanel = new ursuppe.gui.TransparentJPanel();
        environmentForcastPanel1 = new ursuppe.gui.EnvironmentForcastPanel();
        progressPanel1 = new ursuppe.gui.ProgressPanel();

        setMaximumSize(new java.awt.Dimension(620, 620));
        setMinimumSize(new java.awt.Dimension(620, 620));
        setName("Form"); // NOI18N
        setPreferredSize(new java.awt.Dimension(620, 620));
        setLayout(new java.awt.GridBagLayout());

        soupCellPanel10.setName("soupCellPanel10"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        add(soupCellPanel10, gridBagConstraints);

        soupCellPanel01.setName("soupCellPanel01"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        add(soupCellPanel01, gridBagConstraints);

        soupCellPanel11.setName("soupCellPanel11"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        add(soupCellPanel11, gridBagConstraints);

        soupCellPanel21.setName("soupCellPanel21"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        add(soupCellPanel21, gridBagConstraints);

        soupCellPanel31.setName("soupCellPanel31"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        add(soupCellPanel31, gridBagConstraints);

        soupCellPanel41.setName("soupCellPanel41"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        add(soupCellPanel41, gridBagConstraints);

        soupCellPanel02.setName("soupCellPanel02"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        add(soupCellPanel02, gridBagConstraints);

        soupCellPanel12.setName("soupCellPanel12"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        add(soupCellPanel12, gridBagConstraints);

        environmentPanel.setName("environmentPanel"); // NOI18N

        javax.swing.GroupLayout environmentPanelLayout = new javax.swing.GroupLayout(environmentPanel);
        environmentPanel.setLayout(environmentPanelLayout);
        environmentPanelLayout.setHorizontalGroup(
            environmentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 120, Short.MAX_VALUE)
        );
        environmentPanelLayout.setVerticalGroup(
            environmentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 120, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        add(environmentPanel, gridBagConstraints);

        soupCellPanel32.setName("soupCellPanel32"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        add(soupCellPanel32, gridBagConstraints);

        soupCellPanel42.setName("soupCellPanel42"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        add(soupCellPanel42, gridBagConstraints);

        soupCellPanel03.setName("soupCellPanel03"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        add(soupCellPanel03, gridBagConstraints);

        soupCellPanel13.setName("soupCellPanel13"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        add(soupCellPanel13, gridBagConstraints);

        soupCellPanel23.setName("soupCellPanel23"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        add(soupCellPanel23, gridBagConstraints);

        soupCellPanel33.setName("soupCellPanel33"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 3;
        add(soupCellPanel33, gridBagConstraints);

        soupCellPanel43.setName("soupCellPanel43"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 3;
        add(soupCellPanel43, gridBagConstraints);

        soupCellPanel04.setName("soupCellPanel04"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        add(soupCellPanel04, gridBagConstraints);

        soupCellPanel14.setName("soupCellPanel14"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        add(soupCellPanel14, gridBagConstraints);

        soupCellPanel24.setName("soupCellPanel24"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        add(soupCellPanel24, gridBagConstraints);

        soupCellPanel34.setName("soupCellPanel34"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 4;
        add(soupCellPanel34, gridBagConstraints);

        dicePanel.setName("dicePanel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        add(dicePanel, gridBagConstraints);

        scorePanelA.setBackground(null);
        scorePanelA.setMaximumSize(new java.awt.Dimension(620, 20));
        scorePanelA.setMinimumSize(new java.awt.Dimension(620, 20));
        scorePanelA.setName("scorePanelA"); // NOI18N
        scorePanelA.setPreferredSize(new java.awt.Dimension(620, 20));
        scorePanelA.setLayout(new java.awt.GridBagLayout());

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(ursuppe.UrsuppeApp.class).getContext().getResourceMap(SoupPanel.class);
        startPanel.setBackground(resourceMap.getColor("startPanel.background")); // NOI18N
        startPanel.setMaximumSize(new java.awt.Dimension(120, 20));
        startPanel.setMinimumSize(new java.awt.Dimension(120, 20));
        startPanel.setName("startPanel"); // NOI18N
        startPanel.setPreferredSize(new java.awt.Dimension(120, 20));

        javax.swing.GroupLayout startPanelLayout = new javax.swing.GroupLayout(startPanel);
        startPanel.setLayout(startPanelLayout);
        startPanelLayout.setHorizontalGroup(
            startPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 120, Short.MAX_VALUE)
        );
        startPanelLayout.setVerticalGroup(
            startPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        scorePanelA.add(startPanel, new java.awt.GridBagConstraints());

        scorePanel01.setBackground(resourceMap.getColor("scorePanel01.background")); // NOI18N
        scorePanel01.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel01.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel01.setName("scorePanel01"); // NOI18N
        scorePanel01.setPreferredSize(new java.awt.Dimension(20, 20));

        javax.swing.GroupLayout scorePanel01Layout = new javax.swing.GroupLayout(scorePanel01);
        scorePanel01.setLayout(scorePanel01Layout);
        scorePanel01Layout.setHorizontalGroup(
            scorePanel01Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel01Layout.setVerticalGroup(
            scorePanel01Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel01, new java.awt.GridBagConstraints());

        scorePanel02.setBackground(resourceMap.getColor("scorePanel01.background")); // NOI18N
        scorePanel02.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel02.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel02.setName("scorePanel02"); // NOI18N
        scorePanel02.setPreferredSize(new java.awt.Dimension(20, 20));

        javax.swing.GroupLayout scorePanel02Layout = new javax.swing.GroupLayout(scorePanel02);
        scorePanel02.setLayout(scorePanel02Layout);
        scorePanel02Layout.setHorizontalGroup(
            scorePanel02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel02Layout.setVerticalGroup(
            scorePanel02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel02, new java.awt.GridBagConstraints());

        scorePanel03.setBackground(resourceMap.getColor("scorePanel01.background")); // NOI18N
        scorePanel03.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel03.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel03.setName("scorePanel03"); // NOI18N

        javax.swing.GroupLayout scorePanel03Layout = new javax.swing.GroupLayout(scorePanel03);
        scorePanel03.setLayout(scorePanel03Layout);
        scorePanel03Layout.setHorizontalGroup(
            scorePanel03Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel03Layout.setVerticalGroup(
            scorePanel03Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel03, new java.awt.GridBagConstraints());

        scorePanel04.setBackground(resourceMap.getColor("scorePanel01.background")); // NOI18N
        scorePanel04.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel04.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel04.setName("scorePanel04"); // NOI18N

        javax.swing.GroupLayout scorePanel04Layout = new javax.swing.GroupLayout(scorePanel04);
        scorePanel04.setLayout(scorePanel04Layout);
        scorePanel04Layout.setHorizontalGroup(
            scorePanel04Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel04Layout.setVerticalGroup(
            scorePanel04Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel04, new java.awt.GridBagConstraints());

        scorePanel05.setBackground(resourceMap.getColor("scorePanel01.background")); // NOI18N
        scorePanel05.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel05.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel05.setName("scorePanel05"); // NOI18N

        javax.swing.GroupLayout scorePanel05Layout = new javax.swing.GroupLayout(scorePanel05);
        scorePanel05.setLayout(scorePanel05Layout);
        scorePanel05Layout.setHorizontalGroup(
            scorePanel05Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel05Layout.setVerticalGroup(
            scorePanel05Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel05, new java.awt.GridBagConstraints());

        scorePanel06.setBackground(resourceMap.getColor("scorePanel01.background")); // NOI18N
        scorePanel06.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel06.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel06.setName("scorePanel06"); // NOI18N

        javax.swing.GroupLayout scorePanel06Layout = new javax.swing.GroupLayout(scorePanel06);
        scorePanel06.setLayout(scorePanel06Layout);
        scorePanel06Layout.setHorizontalGroup(
            scorePanel06Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel06Layout.setVerticalGroup(
            scorePanel06Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel06, new java.awt.GridBagConstraints());

        scorePanel07.setBackground(resourceMap.getColor("scorePanel10.background")); // NOI18N
        scorePanel07.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel07.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel07.setName("scorePanel07"); // NOI18N

        javax.swing.GroupLayout scorePanel07Layout = new javax.swing.GroupLayout(scorePanel07);
        scorePanel07.setLayout(scorePanel07Layout);
        scorePanel07Layout.setHorizontalGroup(
            scorePanel07Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel07Layout.setVerticalGroup(
            scorePanel07Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel07, new java.awt.GridBagConstraints());

        scorePanel08.setBackground(resourceMap.getColor("scorePanel10.background")); // NOI18N
        scorePanel08.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel08.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel08.setName("scorePanel08"); // NOI18N

        javax.swing.GroupLayout scorePanel08Layout = new javax.swing.GroupLayout(scorePanel08);
        scorePanel08.setLayout(scorePanel08Layout);
        scorePanel08Layout.setHorizontalGroup(
            scorePanel08Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel08Layout.setVerticalGroup(
            scorePanel08Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel08, new java.awt.GridBagConstraints());

        scorePanel09.setBackground(resourceMap.getColor("scorePanel10.background")); // NOI18N
        scorePanel09.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel09.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel09.setName("scorePanel09"); // NOI18N

        javax.swing.GroupLayout scorePanel09Layout = new javax.swing.GroupLayout(scorePanel09);
        scorePanel09.setLayout(scorePanel09Layout);
        scorePanel09Layout.setHorizontalGroup(
            scorePanel09Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel09Layout.setVerticalGroup(
            scorePanel09Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel09, new java.awt.GridBagConstraints());

        scorePanel10.setBackground(resourceMap.getColor("scorePanel10.background")); // NOI18N
        scorePanel10.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel10.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel10.setName("scorePanel10"); // NOI18N

        javax.swing.GroupLayout scorePanel10Layout = new javax.swing.GroupLayout(scorePanel10);
        scorePanel10.setLayout(scorePanel10Layout);
        scorePanel10Layout.setHorizontalGroup(
            scorePanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel10Layout.setVerticalGroup(
            scorePanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel10, new java.awt.GridBagConstraints());

        scorePanel11.setBackground(resourceMap.getColor("scorePanel10.background")); // NOI18N
        scorePanel11.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel11.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel11.setName("scorePanel11"); // NOI18N
        scorePanel11.setPreferredSize(new java.awt.Dimension(20, 20));

        javax.swing.GroupLayout scorePanel11Layout = new javax.swing.GroupLayout(scorePanel11);
        scorePanel11.setLayout(scorePanel11Layout);
        scorePanel11Layout.setHorizontalGroup(
            scorePanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel11Layout.setVerticalGroup(
            scorePanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel11, new java.awt.GridBagConstraints());

        scorePanel12.setBackground(resourceMap.getColor("scorePanel10.background")); // NOI18N
        scorePanel12.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel12.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel12.setName("scorePanel12"); // NOI18N

        javax.swing.GroupLayout scorePanel12Layout = new javax.swing.GroupLayout(scorePanel12);
        scorePanel12.setLayout(scorePanel12Layout);
        scorePanel12Layout.setHorizontalGroup(
            scorePanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel12Layout.setVerticalGroup(
            scorePanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel12, new java.awt.GridBagConstraints());

        scorePanel13.setBackground(resourceMap.getColor("scorePanel10.background")); // NOI18N
        scorePanel13.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel13.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel13.setName("scorePanel13"); // NOI18N

        javax.swing.GroupLayout scorePanel13Layout = new javax.swing.GroupLayout(scorePanel13);
        scorePanel13.setLayout(scorePanel13Layout);
        scorePanel13Layout.setHorizontalGroup(
            scorePanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel13Layout.setVerticalGroup(
            scorePanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel13, new java.awt.GridBagConstraints());

        scorePanel14.setBackground(resourceMap.getColor("scorePanel10.background")); // NOI18N
        scorePanel14.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel14.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel14.setName("scorePanel14"); // NOI18N

        javax.swing.GroupLayout scorePanel14Layout = new javax.swing.GroupLayout(scorePanel14);
        scorePanel14.setLayout(scorePanel14Layout);
        scorePanel14Layout.setHorizontalGroup(
            scorePanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel14Layout.setVerticalGroup(
            scorePanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel14, new java.awt.GridBagConstraints());

        scorePanel15.setBackground(resourceMap.getColor("scorePanel10.background")); // NOI18N
        scorePanel15.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel15.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel15.setName("scorePanel15"); // NOI18N

        javax.swing.GroupLayout scorePanel15Layout = new javax.swing.GroupLayout(scorePanel15);
        scorePanel15.setLayout(scorePanel15Layout);
        scorePanel15Layout.setHorizontalGroup(
            scorePanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel15Layout.setVerticalGroup(
            scorePanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel15, new java.awt.GridBagConstraints());

        scorePanel16.setBackground(resourceMap.getColor("scorePanel10.background")); // NOI18N
        scorePanel16.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel16.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel16.setName("scorePanel16"); // NOI18N

        javax.swing.GroupLayout scorePanel16Layout = new javax.swing.GroupLayout(scorePanel16);
        scorePanel16.setLayout(scorePanel16Layout);
        scorePanel16Layout.setHorizontalGroup(
            scorePanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel16Layout.setVerticalGroup(
            scorePanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel16, new java.awt.GridBagConstraints());

        scorePanel17.setBackground(resourceMap.getColor("scorePanel10.background")); // NOI18N
        scorePanel17.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel17.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel17.setName("scorePanel17"); // NOI18N

        javax.swing.GroupLayout scorePanel17Layout = new javax.swing.GroupLayout(scorePanel17);
        scorePanel17.setLayout(scorePanel17Layout);
        scorePanel17Layout.setHorizontalGroup(
            scorePanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel17Layout.setVerticalGroup(
            scorePanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel17, new java.awt.GridBagConstraints());

        scorePanel18.setBackground(resourceMap.getColor("scorePanel10.background")); // NOI18N
        scorePanel18.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel18.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel18.setName("scorePanel18"); // NOI18N

        javax.swing.GroupLayout scorePanel18Layout = new javax.swing.GroupLayout(scorePanel18);
        scorePanel18.setLayout(scorePanel18Layout);
        scorePanel18Layout.setHorizontalGroup(
            scorePanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel18Layout.setVerticalGroup(
            scorePanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel18, new java.awt.GridBagConstraints());

        scorePanel19.setBackground(resourceMap.getColor("scorePanel10.background")); // NOI18N
        scorePanel19.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel19.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel19.setName("scorePanel19"); // NOI18N

        javax.swing.GroupLayout scorePanel19Layout = new javax.swing.GroupLayout(scorePanel19);
        scorePanel19.setLayout(scorePanel19Layout);
        scorePanel19Layout.setHorizontalGroup(
            scorePanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel19Layout.setVerticalGroup(
            scorePanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel19, new java.awt.GridBagConstraints());

        scorePanel20.setBackground(resourceMap.getColor("scorePanel25.background")); // NOI18N
        scorePanel20.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel20.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel20.setName("scorePanel20"); // NOI18N

        javax.swing.GroupLayout scorePanel20Layout = new javax.swing.GroupLayout(scorePanel20);
        scorePanel20.setLayout(scorePanel20Layout);
        scorePanel20Layout.setHorizontalGroup(
            scorePanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel20Layout.setVerticalGroup(
            scorePanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel20, new java.awt.GridBagConstraints());

        scorePanel21.setBackground(resourceMap.getColor("scorePanel25.background")); // NOI18N
        scorePanel21.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel21.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel21.setName("scorePanel21"); // NOI18N

        javax.swing.GroupLayout scorePanel21Layout = new javax.swing.GroupLayout(scorePanel21);
        scorePanel21.setLayout(scorePanel21Layout);
        scorePanel21Layout.setHorizontalGroup(
            scorePanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel21Layout.setVerticalGroup(
            scorePanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel21, new java.awt.GridBagConstraints());

        scorePanel22.setBackground(resourceMap.getColor("scorePanel25.background")); // NOI18N
        scorePanel22.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel22.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel22.setName("scorePanel22"); // NOI18N

        javax.swing.GroupLayout scorePanel22Layout = new javax.swing.GroupLayout(scorePanel22);
        scorePanel22.setLayout(scorePanel22Layout);
        scorePanel22Layout.setHorizontalGroup(
            scorePanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel22Layout.setVerticalGroup(
            scorePanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel22, new java.awt.GridBagConstraints());

        scorePanel23.setBackground(resourceMap.getColor("scorePanel25.background")); // NOI18N
        scorePanel23.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel23.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel23.setName("scorePanel23"); // NOI18N

        javax.swing.GroupLayout scorePanel23Layout = new javax.swing.GroupLayout(scorePanel23);
        scorePanel23.setLayout(scorePanel23Layout);
        scorePanel23Layout.setHorizontalGroup(
            scorePanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel23Layout.setVerticalGroup(
            scorePanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel23, new java.awt.GridBagConstraints());

        scorePanel24.setBackground(resourceMap.getColor("scorePanel25.background")); // NOI18N
        scorePanel24.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel24.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel24.setName("scorePanel24"); // NOI18N

        javax.swing.GroupLayout scorePanel24Layout = new javax.swing.GroupLayout(scorePanel24);
        scorePanel24.setLayout(scorePanel24Layout);
        scorePanel24Layout.setHorizontalGroup(
            scorePanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel24Layout.setVerticalGroup(
            scorePanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel24, new java.awt.GridBagConstraints());

        scorePanel25.setBackground(resourceMap.getColor("scorePanel25.background")); // NOI18N
        scorePanel25.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel25.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel25.setName("scorePanel25"); // NOI18N

        javax.swing.GroupLayout scorePanel25Layout = new javax.swing.GroupLayout(scorePanel25);
        scorePanel25.setLayout(scorePanel25Layout);
        scorePanel25Layout.setHorizontalGroup(
            scorePanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        scorePanel25Layout.setVerticalGroup(
            scorePanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        scorePanelA.add(scorePanel25, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(scorePanelA, gridBagConstraints);

        scorePanelB.setMaximumSize(new java.awt.Dimension(20, 600));
        scorePanelB.setMinimumSize(new java.awt.Dimension(20, 600));
        scorePanelB.setName("scorePanelB"); // NOI18N
        scorePanelB.setPreferredSize(new java.awt.Dimension(20, 600));
        scorePanelB.setLayout(new java.awt.GridBagLayout());

        scorePanel26.setBackground(resourceMap.getColor("scorePanel29.background")); // NOI18N
        scorePanel26.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel26.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel26.setName("scorePanel26"); // NOI18N
        scorePanel26.setPreferredSize(new java.awt.Dimension(20, 20));

        javax.swing.GroupLayout scorePanel26Layout = new javax.swing.GroupLayout(scorePanel26);
        scorePanel26.setLayout(scorePanel26Layout);
        scorePanel26Layout.setHorizontalGroup(
            scorePanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel26Layout.setVerticalGroup(
            scorePanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 26;
        scorePanelB.add(scorePanel26, gridBagConstraints);

        scorePanel27.setBackground(resourceMap.getColor("scorePanel29.background")); // NOI18N
        scorePanel27.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel27.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel27.setName("scorePanel27"); // NOI18N

        javax.swing.GroupLayout scorePanel27Layout = new javax.swing.GroupLayout(scorePanel27);
        scorePanel27.setLayout(scorePanel27Layout);
        scorePanel27Layout.setHorizontalGroup(
            scorePanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel27Layout.setVerticalGroup(
            scorePanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 25;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        scorePanelB.add(scorePanel27, gridBagConstraints);

        scorePanel28.setBackground(resourceMap.getColor("scorePanel29.background")); // NOI18N
        scorePanel28.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel28.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel28.setName("scorePanel28"); // NOI18N

        javax.swing.GroupLayout scorePanel28Layout = new javax.swing.GroupLayout(scorePanel28);
        scorePanel28.setLayout(scorePanel28Layout);
        scorePanel28Layout.setHorizontalGroup(
            scorePanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel28Layout.setVerticalGroup(
            scorePanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 24;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        scorePanelB.add(scorePanel28, gridBagConstraints);

        scorePanel29.setBackground(resourceMap.getColor("scorePanel29.background")); // NOI18N
        scorePanel29.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel29.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel29.setName("scorePanel29"); // NOI18N

        javax.swing.GroupLayout scorePanel29Layout = new javax.swing.GroupLayout(scorePanel29);
        scorePanel29.setLayout(scorePanel29Layout);
        scorePanel29Layout.setHorizontalGroup(
            scorePanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel29Layout.setVerticalGroup(
            scorePanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 23;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        scorePanelB.add(scorePanel29, gridBagConstraints);

        scorePanel30.setBackground(resourceMap.getColor("scorePanel29.background")); // NOI18N
        scorePanel30.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel30.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel30.setName("scorePanel30"); // NOI18N

        javax.swing.GroupLayout scorePanel30Layout = new javax.swing.GroupLayout(scorePanel30);
        scorePanel30.setLayout(scorePanel30Layout);
        scorePanel30Layout.setHorizontalGroup(
            scorePanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel30Layout.setVerticalGroup(
            scorePanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 22;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        scorePanelB.add(scorePanel30, gridBagConstraints);

        scorePanel31.setBackground(resourceMap.getColor("scorePanel36.background")); // NOI18N
        scorePanel31.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel31.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel31.setName("scorePanel31"); // NOI18N

        javax.swing.GroupLayout scorePanel31Layout = new javax.swing.GroupLayout(scorePanel31);
        scorePanel31.setLayout(scorePanel31Layout);
        scorePanel31Layout.setHorizontalGroup(
            scorePanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel31Layout.setVerticalGroup(
            scorePanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 21;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        scorePanelB.add(scorePanel31, gridBagConstraints);

        scorePanel32.setBackground(resourceMap.getColor("scorePanel36.background")); // NOI18N
        scorePanel32.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel32.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel32.setName("scorePanel32"); // NOI18N

        javax.swing.GroupLayout scorePanel32Layout = new javax.swing.GroupLayout(scorePanel32);
        scorePanel32.setLayout(scorePanel32Layout);
        scorePanel32Layout.setHorizontalGroup(
            scorePanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel32Layout.setVerticalGroup(
            scorePanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 20;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        scorePanelB.add(scorePanel32, gridBagConstraints);

        scorePanel33.setBackground(resourceMap.getColor("scorePanel36.background")); // NOI18N
        scorePanel33.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel33.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel33.setName("scorePanel33"); // NOI18N

        javax.swing.GroupLayout scorePanel33Layout = new javax.swing.GroupLayout(scorePanel33);
        scorePanel33.setLayout(scorePanel33Layout);
        scorePanel33Layout.setHorizontalGroup(
            scorePanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel33Layout.setVerticalGroup(
            scorePanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 19;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        scorePanelB.add(scorePanel33, gridBagConstraints);

        scorePanel34.setBackground(resourceMap.getColor("scorePanel36.background")); // NOI18N
        scorePanel34.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel34.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel34.setName("scorePanel34"); // NOI18N

        javax.swing.GroupLayout scorePanel34Layout = new javax.swing.GroupLayout(scorePanel34);
        scorePanel34.setLayout(scorePanel34Layout);
        scorePanel34Layout.setHorizontalGroup(
            scorePanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel34Layout.setVerticalGroup(
            scorePanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 18;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        scorePanelB.add(scorePanel34, gridBagConstraints);

        scorePanel35.setBackground(resourceMap.getColor("scorePanel36.background")); // NOI18N
        scorePanel35.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel35.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel35.setName("scorePanel35"); // NOI18N

        javax.swing.GroupLayout scorePanel35Layout = new javax.swing.GroupLayout(scorePanel35);
        scorePanel35.setLayout(scorePanel35Layout);
        scorePanel35Layout.setHorizontalGroup(
            scorePanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel35Layout.setVerticalGroup(
            scorePanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 17;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        scorePanelB.add(scorePanel35, gridBagConstraints);

        scorePanel36.setBackground(resourceMap.getColor("scorePanel36.background")); // NOI18N
        scorePanel36.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel36.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel36.setName("scorePanel36"); // NOI18N

        javax.swing.GroupLayout scorePanel36Layout = new javax.swing.GroupLayout(scorePanel36);
        scorePanel36.setLayout(scorePanel36Layout);
        scorePanel36Layout.setHorizontalGroup(
            scorePanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel36Layout.setVerticalGroup(
            scorePanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        scorePanelB.add(scorePanel36, gridBagConstraints);

        scorePanel37.setBackground(resourceMap.getColor("scorePanel36.background")); // NOI18N
        scorePanel37.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel37.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel37.setName("scorePanel37"); // NOI18N

        javax.swing.GroupLayout scorePanel37Layout = new javax.swing.GroupLayout(scorePanel37);
        scorePanel37.setLayout(scorePanel37Layout);
        scorePanel37Layout.setHorizontalGroup(
            scorePanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel37Layout.setVerticalGroup(
            scorePanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 15;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        scorePanelB.add(scorePanel37, gridBagConstraints);

        scorePanel38.setBackground(resourceMap.getColor("scorePanel36.background")); // NOI18N
        scorePanel38.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel38.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel38.setName("scorePanel38"); // NOI18N

        javax.swing.GroupLayout scorePanel38Layout = new javax.swing.GroupLayout(scorePanel38);
        scorePanel38.setLayout(scorePanel38Layout);
        scorePanel38Layout.setHorizontalGroup(
            scorePanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel38Layout.setVerticalGroup(
            scorePanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        scorePanelB.add(scorePanel38, gridBagConstraints);

        scorePanel39.setBackground(resourceMap.getColor("scorePanel36.background")); // NOI18N
        scorePanel39.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel39.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel39.setName("scorePanel39"); // NOI18N

        javax.swing.GroupLayout scorePanel39Layout = new javax.swing.GroupLayout(scorePanel39);
        scorePanel39.setLayout(scorePanel39Layout);
        scorePanel39Layout.setHorizontalGroup(
            scorePanel39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel39Layout.setVerticalGroup(
            scorePanel39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        scorePanelB.add(scorePanel39, gridBagConstraints);

        scorePanel40.setBackground(resourceMap.getColor("scorePanel36.background")); // NOI18N
        scorePanel40.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel40.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel40.setName("scorePanel40"); // NOI18N

        javax.swing.GroupLayout scorePanel40Layout = new javax.swing.GroupLayout(scorePanel40);
        scorePanel40.setLayout(scorePanel40Layout);
        scorePanel40Layout.setHorizontalGroup(
            scorePanel40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel40Layout.setVerticalGroup(
            scorePanel40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        scorePanelB.add(scorePanel40, gridBagConstraints);

        scorePanel41.setBackground(resourceMap.getColor("scorePanel36.background")); // NOI18N
        scorePanel41.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel41.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel41.setName("scorePanel41"); // NOI18N

        javax.swing.GroupLayout scorePanel41Layout = new javax.swing.GroupLayout(scorePanel41);
        scorePanel41.setLayout(scorePanel41Layout);
        scorePanel41Layout.setHorizontalGroup(
            scorePanel41Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel41Layout.setVerticalGroup(
            scorePanel41Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        scorePanelB.add(scorePanel41, gridBagConstraints);

        scorePanel42.setBackground(resourceMap.getColor("scorePanel46.background")); // NOI18N
        scorePanel42.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel42.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel42.setName("scorePanel42"); // NOI18N

        javax.swing.GroupLayout scorePanel42Layout = new javax.swing.GroupLayout(scorePanel42);
        scorePanel42.setLayout(scorePanel42Layout);
        scorePanel42Layout.setHorizontalGroup(
            scorePanel42Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel42Layout.setVerticalGroup(
            scorePanel42Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        scorePanelB.add(scorePanel42, gridBagConstraints);

        scorePanel43.setBackground(resourceMap.getColor("scorePanel46.background")); // NOI18N
        scorePanel43.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel43.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel43.setName("scorePanel43"); // NOI18N

        javax.swing.GroupLayout scorePanel43Layout = new javax.swing.GroupLayout(scorePanel43);
        scorePanel43.setLayout(scorePanel43Layout);
        scorePanel43Layout.setHorizontalGroup(
            scorePanel43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel43Layout.setVerticalGroup(
            scorePanel43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        scorePanelB.add(scorePanel43, gridBagConstraints);

        scorePanel44.setBackground(resourceMap.getColor("scorePanel46.background")); // NOI18N
        scorePanel44.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel44.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel44.setName("scorePanel44"); // NOI18N

        javax.swing.GroupLayout scorePanel44Layout = new javax.swing.GroupLayout(scorePanel44);
        scorePanel44.setLayout(scorePanel44Layout);
        scorePanel44Layout.setHorizontalGroup(
            scorePanel44Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel44Layout.setVerticalGroup(
            scorePanel44Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        scorePanelB.add(scorePanel44, gridBagConstraints);

        scorePanel45.setBackground(resourceMap.getColor("scorePanel46.background")); // NOI18N
        scorePanel45.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel45.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel45.setName("scorePanel45"); // NOI18N

        javax.swing.GroupLayout scorePanel45Layout = new javax.swing.GroupLayout(scorePanel45);
        scorePanel45.setLayout(scorePanel45Layout);
        scorePanel45Layout.setHorizontalGroup(
            scorePanel45Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel45Layout.setVerticalGroup(
            scorePanel45Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        scorePanelB.add(scorePanel45, gridBagConstraints);

        scorePanel46.setBackground(resourceMap.getColor("scorePanel46.background")); // NOI18N
        scorePanel46.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel46.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel46.setName("scorePanel46"); // NOI18N

        javax.swing.GroupLayout scorePanel46Layout = new javax.swing.GroupLayout(scorePanel46);
        scorePanel46.setLayout(scorePanel46Layout);
        scorePanel46Layout.setHorizontalGroup(
            scorePanel46Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel46Layout.setVerticalGroup(
            scorePanel46Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        scorePanelB.add(scorePanel46, gridBagConstraints);

        scorePanel47.setBackground(resourceMap.getColor("scorePanel46.background")); // NOI18N
        scorePanel47.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel47.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel47.setName("scorePanel47"); // NOI18N

        javax.swing.GroupLayout scorePanel47Layout = new javax.swing.GroupLayout(scorePanel47);
        scorePanel47.setLayout(scorePanel47Layout);
        scorePanel47Layout.setHorizontalGroup(
            scorePanel47Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel47Layout.setVerticalGroup(
            scorePanel47Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        scorePanelB.add(scorePanel47, gridBagConstraints);

        scorePanel48.setBackground(resourceMap.getColor("scorePanel46.background")); // NOI18N
        scorePanel48.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel48.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel48.setName("scorePanel48"); // NOI18N

        javax.swing.GroupLayout scorePanel48Layout = new javax.swing.GroupLayout(scorePanel48);
        scorePanel48.setLayout(scorePanel48Layout);
        scorePanel48Layout.setHorizontalGroup(
            scorePanel48Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel48Layout.setVerticalGroup(
            scorePanel48Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        scorePanelB.add(scorePanel48, gridBagConstraints);

        scorePanel49.setBackground(resourceMap.getColor("scorePanel46.background")); // NOI18N
        scorePanel49.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel49.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel49.setName("scorePanel49"); // NOI18N

        javax.swing.GroupLayout scorePanel49Layout = new javax.swing.GroupLayout(scorePanel49);
        scorePanel49.setLayout(scorePanel49Layout);
        scorePanel49Layout.setHorizontalGroup(
            scorePanel49Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel49Layout.setVerticalGroup(
            scorePanel49Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        scorePanelB.add(scorePanel49, gridBagConstraints);

        scorePanel50.setBackground(resourceMap.getColor("scorePanel46.background")); // NOI18N
        scorePanel50.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel50.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel50.setName("scorePanel50"); // NOI18N

        javax.swing.GroupLayout scorePanel50Layout = new javax.swing.GroupLayout(scorePanel50);
        scorePanel50.setLayout(scorePanel50Layout);
        scorePanel50Layout.setHorizontalGroup(
            scorePanel50Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel50Layout.setVerticalGroup(
            scorePanel50Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        scorePanelB.add(scorePanel50, gridBagConstraints);

        scorePanel51.setBackground(resourceMap.getColor("scorePanel46.background")); // NOI18N
        scorePanel51.setMaximumSize(new java.awt.Dimension(20, 20));
        scorePanel51.setMinimumSize(new java.awt.Dimension(20, 20));
        scorePanel51.setName("scorePanel51"); // NOI18N

        javax.swing.GroupLayout scorePanel51Layout = new javax.swing.GroupLayout(scorePanel51);
        scorePanel51.setLayout(scorePanel51Layout);
        scorePanel51Layout.setHorizontalGroup(
            scorePanel51Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        scorePanel51Layout.setVerticalGroup(
            scorePanel51Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        scorePanelB.add(scorePanel51, gridBagConstraints);

        goalPanel.setBackground(resourceMap.getColor("scorePanel46.background")); // NOI18N
        goalPanel.setMaximumSize(new java.awt.Dimension(20, 80));
        goalPanel.setMinimumSize(new java.awt.Dimension(20, 80));
        goalPanel.setName("goalPanel"); // NOI18N
        goalPanel.setPreferredSize(new java.awt.Dimension(20, 80));

        javax.swing.GroupLayout goalPanelLayout = new javax.swing.GroupLayout(goalPanel);
        goalPanel.setLayout(goalPanelLayout);
        goalPanelLayout.setHorizontalGroup(
            goalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        goalPanelLayout.setVerticalGroup(
            goalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 80, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        scorePanelB.add(goalPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(scorePanelB, gridBagConstraints);

        environmentForcastPanel1.setName("environmentForcastPanel1"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        add(environmentForcastPanel1, gridBagConstraints);

        progressPanel1.setName("progressPanel1"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        add(progressPanel1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ursuppe.gui.DicePanel dicePanel;
    private ursuppe.gui.EnvironmentForcastPanel environmentForcastPanel1;
    private ursuppe.gui.EnvironmentPanel environmentPanel;
    private ursuppe.gui.TransparentJPanel goalPanel;
    private ursuppe.gui.ProgressPanel progressPanel1;
    private ursuppe.gui.TransparentJPanel scorePanel01;
    private ursuppe.gui.TransparentJPanel scorePanel02;
    private ursuppe.gui.TransparentJPanel scorePanel03;
    private ursuppe.gui.TransparentJPanel scorePanel04;
    private ursuppe.gui.TransparentJPanel scorePanel05;
    private ursuppe.gui.TransparentJPanel scorePanel06;
    private ursuppe.gui.TransparentJPanel scorePanel07;
    private ursuppe.gui.TransparentJPanel scorePanel08;
    private ursuppe.gui.TransparentJPanel scorePanel09;
    private ursuppe.gui.TransparentJPanel scorePanel10;
    private ursuppe.gui.TransparentJPanel scorePanel11;
    private ursuppe.gui.TransparentJPanel scorePanel12;
    private ursuppe.gui.TransparentJPanel scorePanel13;
    private ursuppe.gui.TransparentJPanel scorePanel14;
    private ursuppe.gui.TransparentJPanel scorePanel15;
    private ursuppe.gui.TransparentJPanel scorePanel16;
    private ursuppe.gui.TransparentJPanel scorePanel17;
    private ursuppe.gui.TransparentJPanel scorePanel18;
    private ursuppe.gui.TransparentJPanel scorePanel19;
    private ursuppe.gui.TransparentJPanel scorePanel20;
    private ursuppe.gui.TransparentJPanel scorePanel21;
    private ursuppe.gui.TransparentJPanel scorePanel22;
    private ursuppe.gui.TransparentJPanel scorePanel23;
    private ursuppe.gui.TransparentJPanel scorePanel24;
    private ursuppe.gui.TransparentJPanel scorePanel25;
    private ursuppe.gui.TransparentJPanel scorePanel26;
    private ursuppe.gui.TransparentJPanel scorePanel27;
    private ursuppe.gui.TransparentJPanel scorePanel28;
    private ursuppe.gui.TransparentJPanel scorePanel29;
    private ursuppe.gui.TransparentJPanel scorePanel30;
    private ursuppe.gui.TransparentJPanel scorePanel31;
    private ursuppe.gui.TransparentJPanel scorePanel32;
    private ursuppe.gui.TransparentJPanel scorePanel33;
    private ursuppe.gui.TransparentJPanel scorePanel34;
    private ursuppe.gui.TransparentJPanel scorePanel35;
    private ursuppe.gui.TransparentJPanel scorePanel36;
    private ursuppe.gui.TransparentJPanel scorePanel37;
    private ursuppe.gui.TransparentJPanel scorePanel38;
    private ursuppe.gui.TransparentJPanel scorePanel39;
    private ursuppe.gui.TransparentJPanel scorePanel40;
    private ursuppe.gui.TransparentJPanel scorePanel41;
    private ursuppe.gui.TransparentJPanel scorePanel42;
    private ursuppe.gui.TransparentJPanel scorePanel43;
    private ursuppe.gui.TransparentJPanel scorePanel44;
    private ursuppe.gui.TransparentJPanel scorePanel45;
    private ursuppe.gui.TransparentJPanel scorePanel46;
    private ursuppe.gui.TransparentJPanel scorePanel47;
    private ursuppe.gui.TransparentJPanel scorePanel48;
    private ursuppe.gui.TransparentJPanel scorePanel49;
    private ursuppe.gui.TransparentJPanel scorePanel50;
    private ursuppe.gui.TransparentJPanel scorePanel51;
    private ursuppe.gui.TransparentJPanel scorePanelA;
    private ursuppe.gui.TransparentJPanel scorePanelB;
    private ursuppe.gui.SoupCellPanel soupCellPanel01;
    private ursuppe.gui.SoupCellPanel soupCellPanel02;
    private ursuppe.gui.SoupCellPanel soupCellPanel03;
    private ursuppe.gui.SoupCellPanel soupCellPanel04;
    private ursuppe.gui.SoupCellPanel soupCellPanel10;
    private ursuppe.gui.SoupCellPanel soupCellPanel11;
    private ursuppe.gui.SoupCellPanel soupCellPanel12;
    private ursuppe.gui.SoupCellPanel soupCellPanel13;
    private ursuppe.gui.SoupCellPanel soupCellPanel14;
    private ursuppe.gui.SoupCellPanel soupCellPanel21;
    private ursuppe.gui.SoupCellPanel soupCellPanel23;
    private ursuppe.gui.SoupCellPanel soupCellPanel24;
    private ursuppe.gui.SoupCellPanel soupCellPanel31;
    private ursuppe.gui.SoupCellPanel soupCellPanel32;
    private ursuppe.gui.SoupCellPanel soupCellPanel33;
    private ursuppe.gui.SoupCellPanel soupCellPanel34;
    private ursuppe.gui.SoupCellPanel soupCellPanel41;
    private ursuppe.gui.SoupCellPanel soupCellPanel42;
    private ursuppe.gui.SoupCellPanel soupCellPanel43;
    private ursuppe.gui.TransparentJPanel startPanel;
    // End of variables declaration//GEN-END:variables

}
