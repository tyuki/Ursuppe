/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.gui.icon;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.util.List;
import javax.swing.Icon;

/**
 *
 * @author Personal
 */
public class AnimatedIconInstance implements Icon {

    private List<Image> _images;
    private int _current;

    public AnimatedIconInstance(List<Image> images)  {
        _images = images;
        _current = 0;
    }

    public void reset() {
        _current = 0;
    }

    public void next() {
        _current++;
        _current %= _images.size();
    }

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        g.drawImage(_images.get(_current), x, y, c);
    }

    @Override
    public int getIconWidth() {
        return _images.get(_current).getWidth(null);
    }

    @Override
    public int getIconHeight() {
        return _images.get(_current).getHeight(null);
    }


}
