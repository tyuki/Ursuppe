/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.gui.icon;

import java.awt.Color;
import java.util.EnumMap;
import java.util.Map;
import java.util.Properties;
import javax.swing.ImageIcon;
import ursuppe.UrsuppeApp;
import ursuppe.gui.UrsuppeColor;
import ursuppe.gui.UrsuppeColor.TYPE;

/**
 *
 * @author Personal
 */
public class SoupBackground {
    
    
    public final String name;
    public final ImageIcon image;
    public Color scoreTrackColor;
    public Color roundProgressColor;
    public Color foodCountColor;
    
    private Map<TYPE, float[]> customSVs = new EnumMap<>(TYPE.class);
    
    private static final Color DEFAULT_COLOR = Color.BLACK;
    
    public SoupBackground(String name, ImageIcon image) {
        this.name = name;
        this.image = image;
        this.scoreTrackColor = DEFAULT_COLOR;
        this.foodCountColor = DEFAULT_COLOR;
        this.roundProgressColor = DEFAULT_COLOR;
    }

    public SoupBackground(String name, ImageIcon image, Properties properties) {
        this.name = name;
        this.image = image;
        
        Color bgColor = DEFAULT_COLOR;
        try {
            if (properties != null) {
                final String str = properties.getProperty("background.text.color");
                if (str != null) {
                    bgColor = Color.decode(str);
                }
            }
        } catch (Exception e) { UrsuppeApp.debug(e); }
        
        
        try {
            if (properties != null) {
                final String str = properties.getProperty("scoreTrack.text.color");
                if (str != null) {
                    scoreTrackColor = Color.decode(str);
                }
            }
        } catch (Exception e) { UrsuppeApp.debug(e); }
        if (scoreTrackColor == null) scoreTrackColor = bgColor;
        
        try {
            if (properties != null) {
                final String str = properties.getProperty("foodCount.text.color");
                if (str != null) {
                    foodCountColor = Color.decode(str);
                }
            }
        } catch (Exception e) { UrsuppeApp.debug(e); }
        if (foodCountColor == null) foodCountColor = bgColor;
        
        try {
            if (properties != null) {
                final String str = properties.getProperty("roundProgress.text.color");
                if (str != null) {
                    roundProgressColor = Color.decode(str);
                }
            }
        } catch (Exception e) { UrsuppeApp.debug(e); }
        if (roundProgressColor == null) roundProgressColor = bgColor;
        
        
       if (properties != null) {
            for (TYPE type : TYPE.values()) {
                try {
                        final String Sstr = properties.getProperty(type.name().toLowerCase()+".color.S");
                        final String Vstr = properties.getProperty(type.name().toLowerCase()+".color.V");

                        float S = -1;
                        float V = -1;
                        if (Sstr != null) {
                            S = Float.parseFloat(Sstr);
                        }
                        if (Vstr != null) {
                            V = Float.parseFloat(Vstr);
                        }
                        customSVs.put(type, new float[] {S, V});
                } catch (Exception e) { UrsuppeApp.debug(e); }
            }
        }
    }
    
    public void applyColors() {
        for (TYPE type : TYPE.values()) {
            if (customSVs.containsKey(type)) {
                float[] SV = customSVs.get(type);
                if (SV != null && SV.length == 2) {
                    UrsuppeColor.modifyColor(type, SV[0], SV[1]);
                }
                //if not specified,use default
            } else {
                    UrsuppeColor.modifyColor(type, -1, -1);
            }
        }
    }
}
