/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ursuppe.gui.icon;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import ursuppe.PropertyContainer;
import ursuppe.UrsuppeApp;

/**
 *
 * @author Personal
 */
public class SoupBackgroundContainer {


    private static List<String> _customSoupBackgrounds = new LinkedList<String>();
    private static Map<String, SoupBackground> _soupBackgrounds = new HashMap<String, SoupBackground>();

    public static List<String> getCustomSoupBackgrounds() {
        Collections.sort(_customSoupBackgrounds);
        return Collections.unmodifiableList(_customSoupBackgrounds);
    }

    public static SoupBackground getSoupBackGround(String name) {
        if (_soupBackgrounds.containsKey(name)) {
            return _soupBackgrounds.get(name);
        } else {
            return _soupBackgrounds.get("default");
        }
    }
        
    public static void loadDirectory(final String directory) {
        File dir = new File(directory);
        if (!dir.isDirectory()) return;

        for (File file : dir.listFiles()) {
            if (!file.isFile()) continue;
            
            if (file.getName().matches(".+\\.png")) {
                loadBackground(file);
            }
        }
    }


    public static void loadBackground(File file) {
        InputStream is = null;
        try {
            is = new FileInputStream(file);
            BufferedImage img = ImageIO.read(is);
            final String filename = file.getName().replaceAll("\\.png$", "");
            
            // see if there is a corresponding config file
            Properties properties = null;
            try {
                File cfgFile = null;
                for (File cfgCand : file.getParentFile().listFiles()) {
                    if (cfgCand.getName().matches(filename+"\\.cfg") && cfgCand.isFile() && cfgCand.canRead()) {
                        cfgFile = cfgCand;
                        break;
                    }
                }
                if (cfgFile != null) {
                    properties = PropertyContainer.loadProperties(cfgFile.getPath());
                }
            } catch (Exception e) {
                UrsuppeApp.debug(e);
            }
            if (file != null && img != null) {
                SoupBackground soupBG = new SoupBackground(file.getName(), new ImageIcon(img), properties);
                _soupBackgrounds.put(filename, soupBG);
                if (!filename.matches("^default$")) {
                    _customSoupBackgrounds.add(filename);
                }
            }
            
        } catch (FileNotFoundException fne) {
           JOptionPane.showMessageDialog(null, "Failed to load image.");
        } catch (IOException ex) {
           JOptionPane.showMessageDialog(null, "Failed to load image.");
        } finally {
            try {
                is.close();
            } catch (IOException ex) {
            }
        }
    }
}
