/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.gui.icon;

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.awt.image.PixelGrabber;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Personal
 */
public class AnimatedColorableIconBase implements Serializable {

    public static final long serialVersionUID = 778918348L;

    private List<PixelImage> _base;

    public AnimatedColorableIconBase(List<BufferedImage> images) {
        _base = new LinkedList<>();

        for (BufferedImage image : images) {
            _base.add(new PixelImage(image));
        }
    }

    public AnimatedIcon createAnimatedIcon() {
        List<Image> images = new LinkedList<>();

        for (PixelImage image : _base) {
            images.add(image.getImage());
        }

        return new AnimatedIcon(images);
    }

    public AnimatedIcon createAnimatedIcon(Color color) {
        List<Image> images = new LinkedList<>();

        for (PixelImage image : _base) {
            images.add(image.getImage(color));
        }

        return new AnimatedIcon(images);
    }
    
    public int getAnimationLength() {
        return _base.size();
    }
}
