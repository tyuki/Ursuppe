/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.gui.icon;

import java.awt.Image;
import java.awt.Point;
import java.util.List;
import ursuppe.gui.effect.MovingImageEffect;

/**
 *
 * @author Personal
 */
public class AnimatedIcon {

    private List<Image> _images;

    public AnimatedIcon(List<Image> images)  {
        super();
        _images = images;
    }

    public AnimatedIconInstance getInstance() {
        return new AnimatedIconInstance(_images);
    }

    public MovingImageEffect getMovingEffect(Point start, Point end, long rate, long total) {
        return new MovingImageEffect(_images, start, end, rate, total);
    }

}
