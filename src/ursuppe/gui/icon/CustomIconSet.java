/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.gui.icon;

import java.io.Serializable;

/**
 *
 * @author Personal
 */
public class CustomIconSet implements Serializable {

    public static final long serialVersionUID = 618578934L;

    public final AnimatedColorableIconBase iconBase;
    public final PixelImage[] numBase;
    public final PixelImage scoreBase;
    public final PixelImage BGbase;

    public CustomIconSet(AnimatedColorableIconBase iconBase, PixelImage[] numBase, PixelImage scoreBase, PixelImage BGbase) {
        this.iconBase = iconBase;
        this.numBase = numBase;
        this.scoreBase = scoreBase;
        this.BGbase = BGbase;
    }
}
