/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.gui.icon;

import ursuppe.gui.effect.AnimationEffectInstance;
import ursuppe.gui.effect.AnimationEffect;
import java.awt.Color;
import java.awt.Image;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author Personal
 */
public class IconContainer {

    public static enum IMAGE_TYPE {
        GENE, CONTROL, ENVIRONMENT, OTHER
    }

    private static Map<String, ImageIcon> _images = new HashMap<String, ImageIcon>();
    private static Map<String, ImageIcon> _miniImages = new HashMap<String, ImageIcon>();

    private static Map<String, BufferedImage> _iconBase = new HashMap<String, BufferedImage>();
    private static Map<String, AnimatedIcon> _animatedIcon = new HashMap<String, AnimatedIcon>();
    private static Map<String, AnimatedColorableIconBase> _customIconBase = new HashMap<String, AnimatedColorableIconBase>();

    private static Map<String, PixelImage[]> _customNumberBase = new HashMap<String, PixelImage[]>();
    private static Map<String, Image> _customNumber = new HashMap<String, Image>();

    private static Map<String, PixelImage> _customScoreBase = new HashMap<String, PixelImage>();
    private static Map<String, Image> _customScore = new HashMap<String, Image>();

    private static Map<String, PixelImage> _customBackgroundBase = new HashMap<String, PixelImage>();
    private static Map<String, Image> _customBackground = new HashMap<String, Image>();

    private static Set<String> _externalCustomIcons = new TreeSet<String>();

    private static Map<String, AnimationEffect> _effects = new HashMap<String, AnimationEffect>();

    private static final int MINI_GENE_WIDTH = 30;
    private static final int MINI_GENE_HEIGHT = 40;
    private static final int MINI_ENV_WIDTH = 60;
    private static final int MINI_ENV_HEIGHT = 60;


    public static final String DEFAULT_CUSTOM_ICON = "default";

    public static final String DEFAULT_BACKGROUND = "default";
    public static final Color DEFAULT_BACKGROUND_FONT_COLOR = Color.BLACK;

    public static final String CUSTOM_ICON_PREFIX = "amoeba_";
    public static final String CUSTOM_BACKGROUND_PREFIX = "bg_";
    public static final String CUSTOM_NUM_PREFIX = "num";
    public static final String CUSTOM_SCORE_ICON_NAME = "score";


    private static String convertYensToSlashes(String name) {
        return name.replaceAll("\\\\", "/");
    }

    public static void loadDirectory(String directory, IMAGE_TYPE type) {
        File dir = new File(directory);
        if (!dir.isDirectory()) return;

        for (File file : dir.listFiles()) {
            if (!file.isFile()) continue;
            loadIcon(file.getAbsolutePath(), type);
        }
    }


    public static void loadIcon(String filename, IMAGE_TYPE type) {
        InputStream is = null;
        try {
            is = new FileInputStream(filename);
            BufferedImage img = ImageIO.read(is);
            String converted = convertYensToSlashes(filename);
            Pattern regex = Pattern.compile("(.+\\/)*(.+)\\.png");
            Matcher matcher = regex.matcher(converted);
            if (matcher.matches()) {
                String name = matcher.group(2);
                switch (type) {
                    case GENE:
                       _images.put(name, new ImageIcon(img));
                       _miniImages.put(name, new ImageIcon(img.getScaledInstance(MINI_GENE_WIDTH, MINI_GENE_HEIGHT, Image.SCALE_SMOOTH)));
                        break;
                    case ENVIRONMENT:
                       _images.put(name, new ImageIcon(img));
                       _miniImages.put(name, new ImageIcon(img.getScaledInstance(MINI_ENV_WIDTH, MINI_ENV_HEIGHT, Image.SCALE_SMOOTH)));
                        break;
                    case CONTROL:
                    case OTHER:
                    default:
                       _images.put(name, new ImageIcon(img));
                        break;
                }
            }
        } catch (FileNotFoundException fne) {
           JOptionPane.showMessageDialog(null, "Failed to load image.");
        } catch (IOException ex) {
           JOptionPane.showMessageDialog(null, "Failed to load image.");
        } finally {
            try {
                is.close();
            } catch (IOException ex) {
            }
        }
    }

    public static void loadDirectoryOfColoredIconBase(String directory) {

        File dir = new File(directory);
        if (!dir.isDirectory()) return;

        for (File file : dir.listFiles()) {
            if (!file.isFile()) continue;
            loadColoredIconBase(file.getAbsolutePath());
        }
    }

    public static void loadColoredIconBase(String filename) {
        InputStream is = null;
        try {
            is = new FileInputStream(filename);
            BufferedImage img = ImageIO.read(is);
            String converted = convertYensToSlashes(filename);
            Pattern regex = Pattern.compile("(.+\\/)*(.+)\\.png");
            Matcher matcher = regex.matcher(converted);
            if (matcher.matches()) {
                //Get the name
                String name = matcher.group(2);
                //Add to map
                _iconBase.put(name, img);
            }
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Failed to load image.");
        } catch (IOException ioe) {
            JOptionPane.showMessageDialog(null, "Failed to load image.");
        } finally {
            try {
                is.close();
            } catch (IOException ex) {
                Logger.getLogger(IconContainer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }


    public static void loadCustomIcons(String directory) {
        File dir = new File(directory);
        if (!dir.isDirectory()) return;

        List<String> subdirs = new LinkedList<>();

        //First path
        for (File file : dir.listFiles()) {
            if (!file.isDirectory()) continue;
            subdirs.add(file.getName());
        }

        //
        for (String subdir : subdirs) {
            AnimatedColorableIconBase base = loadAnimatedColorableIconBase(directory+"/"+subdir, CUSTOM_ICON_PREFIX);
            if (base == null || base.getAnimationLength() <= 0) continue;
            
            _customIconBase.put(subdir, base);
            PixelImage[] numBase = loadCustomNumbers(directory+"/"+subdir);
            if (numBase != null) _customNumberBase.put(subdir, numBase);
            PixelImage miniIcon = loadCustomScoreIcon(directory+"/"+subdir);
            if (miniIcon != null) _customScoreBase.put(subdir, miniIcon);
        }
    }

    private static AnimatedColorableIconBase loadAnimatedColorableIconBase(String directory, String prefix) {

        File dir = new File(directory);
        if (!dir.isDirectory()) return null;

        List<String> targets = new LinkedList<>();

        //First path
        for (File file : dir.listFiles()) {
            if (!file.isFile()) continue;

                String converted = convertYensToSlashes(file.getAbsolutePath());
                Pattern regex = Pattern.compile("(.+\\/)*"+prefix+"(.+)\\.png");
                Matcher matcher = regex.matcher(converted);

                if (matcher.matches()) {
                    targets.add(prefix + matcher.group(2));
                }

        }

        Collections.sort(targets);

        List<BufferedImage> base = new LinkedList<>();
        //Second path
        for (String filename : targets) {
            InputStream is = null;
            try {
                is = new FileInputStream(directory+"/"+filename+".png");
                BufferedImage img = ImageIO.read(is);
                base.add(img);

            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(null, "Failed to load image.");
            } catch (IOException ioe) {
                JOptionPane.showMessageDialog(null, "Failed to load image.");
            } finally {
                try {
                    is.close();
                } catch (IOException ex) {
                }
            }
        }

        return new AnimatedColorableIconBase(base);
    }

    private static PixelImage[] loadCustomNumbers(String directory) {
        File dir = new File(directory);
        if (!dir.isDirectory()) return null;

        int count = 0;
        //First path
        for (File file : dir.listFiles()) {
            if (!file.isFile()) continue;
            if (file.getName().matches(CUSTOM_NUM_PREFIX+"(1|2|3|4|5|6|7)\\.png")) count++;
        }

        if (count != 7) return null;

        PixelImage[] nums = new PixelImage[7];

        for (int i = 1; i <= 7; i++) {
            InputStream is = null;
            try {
                is = new FileInputStream(directory+"/"+CUSTOM_NUM_PREFIX+i+".png");
                BufferedImage img = ImageIO.read(is);
                nums[i-1] = new PixelImage(img);

            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(null, "Failed to load image.");
            } catch (IOException ioe) {
                JOptionPane.showMessageDialog(null, "Failed to load image.");
            } finally {
                try {
                    is.close();
                } catch (IOException ex) {
                }
            }
        }

        return nums;
    }


    private static PixelImage loadCustomScoreIcon(String directory) {
        File dir = new File(directory);
        if (!dir.isDirectory()) return null;

        File file = new File(directory+"/"+CUSTOM_SCORE_ICON_NAME+".png");

        if (!file.exists()) {
            return null;
        }

        PixelImage res = null;
        InputStream is = null;
        try {
                is = new FileInputStream(file);
                BufferedImage img = ImageIO.read(is);
                res = new PixelImage(img);

            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(null, "Failed to load image.");
            } catch (IOException ioe) {
                JOptionPane.showMessageDialog(null, "Failed to load image.");
            } finally {
                try {
                    is.close();
                } catch (IOException ex) {
                }
            }

        return res;
    }

    public static void loadCustomPlayerBackground(String directory) {
        File dir = new File(directory);
        if (!dir.isDirectory()) return;

        List<String> files = new LinkedList<>();

        //First path
        for (File file : dir.listFiles()) {
            if (!file.isFile()) continue;
            if (!file.getName().matches(".+\\.png$")) continue;
            files.add(file.getName());
        }

        //
        for (String f : files) {
            String name = f.replaceAll("\\.png$", "");

            InputStream is = null;
            try {
                is = new FileInputStream(directory+"/"+f);
                BufferedImage img = ImageIO.read(is);
                _customBackgroundBase.put(name, new PixelImage(img));
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(null, "Failed to load image.");
            } catch (IOException ioe) {
                ioe.printStackTrace();
                JOptionPane.showMessageDialog(null, "Failed to load image.");
            } finally {
                try {
                    is.close();
                } catch (IOException ex) {
                }
            }
        }
    }

    public static void loadEffects(String directory) {
        File dir = new File(directory);
        if (!dir.isDirectory()) return;

        List<String> subdirs = new LinkedList<>();

        //First path
        for (File file : dir.listFiles()) {
            if (!file.isDirectory()) continue;
            subdirs.add(file.getName());
        }

        //
        for (String subdir : subdirs) {
            _effects.put(subdir, loadEffect(directory+"/"+subdir));
        }

        //_effects = new HashMap<String, Effect>();
    }

    public static AnimationEffect loadEffect(String directory) {

        File dir = new File(directory);
        if (!dir.isDirectory()) return null;

        List<String> targets = new LinkedList<>();

        //First path
        for (File file : dir.listFiles()) {
            if (!file.isFile()) continue;

                String converted = convertYensToSlashes(file.getAbsolutePath());
                Pattern regex = Pattern.compile("(.+\\/)*(.+)\\.png");
                Matcher matcher = regex.matcher(converted);

                if (matcher.matches()) {
                    targets.add(matcher.group(2));
                }

        }

        Collections.sort(targets);

        List<Image> base = new LinkedList<>();
        //Second path
        for (String filename : targets) {
            InputStream is = null;
            try {
                is = new FileInputStream(directory+"/"+filename+".png");
                BufferedImage img = ImageIO.read(is);
                base.add(img);

            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(null, "Failed to load image.");
            } catch (IOException ioe) {
                JOptionPane.showMessageDialog(null, "Failed to load image.");
            } finally {
                try {
                    is.close();
                } catch (IOException ex) {
                }
            }
        }

        return new AnimationEffect(base);
    }

    public static ImageIcon getIcon(String name) {
        return _images.get(name);
    }

    public static ImageIcon getMiniIcon(String name) {
        return _miniImages.get(name);
    }

    public static Collection<String> getCustomIconNames() {
        return _customIconBase.keySet();
    }

    public static void addCustomIcon(String name, AnimatedColorableIconBase base) {
        if (name != null && base != null) {
            _customIconBase.put(name, base);
            _externalCustomIcons.add(name);
        }
    }

    public static AnimatedColorableIconBase getCustomIconBase(String name) {
        return _customIconBase.get(name);
    }

    public static AnimatedIcon getCustomIcon(String name, Color color) {
        if (_animatedIcon.containsKey(getColoredName(name,color))) return _animatedIcon.get(getColoredName(name,color));

        if (_customIconBase.containsKey(name)) {
            AnimatedIcon icon = _customIconBase.get(name).createAnimatedIcon(color);
            _animatedIcon.put(getColoredName(name,color), icon);
            return icon;
        } else if (name.compareTo(DEFAULT_CUSTOM_ICON) != 0) {
            return getCustomIcon(DEFAULT_CUSTOM_ICON, color);
        }

        return null;

    }

    public static PixelImage[] getCustomNumberBase(String name) {
        return _customNumberBase.get(name);
    }

    public static void addCustomNumberBase(String name, PixelImage[] base) {
        if (name != null && base != null) {
            _customNumberBase.put(name, base);
            _externalCustomIcons.add(name);
        }
    }

    public static Image getCustomNumber(String name, Color color, int i) {
        if (i < 0 || i > 7) return null;

        if (_customNumber.containsKey(getColoredName(name,color)+"_num"+i)) return _customNumber.get(getColoredName(name,color)+"_num"+i);

        if (_customNumberBase.containsKey(name)) {
            _customNumber.put(getColoredName(name,color)+"_num"+i, _customNumberBase.get(name)[i-1].getImage(color));
            return _customNumber.get(getColoredName(name,color)+"_num"+i);
        }

        //default
        return _images.get("num"+i).getImage();
    }


    public static PixelImage getCustomScoreBase(String name) {
        return _customScoreBase.get(name);
    }

    public static void addCustomScoreBase(String name, PixelImage base) {
        if (name != null && base != null) {
            _customScoreBase.put(name, base);
            _externalCustomIcons.add(name);
        }
    }

    public static Image getCustomScoreIcon(String name, Color color) {
        if (_customScore.containsKey(getColoredName(name,color))) return _customScore.get(getColoredName(name,color));

        if (_customScoreBase.containsKey(name)) {
            _customScore.put(getColoredName(name,color), _customScoreBase.get(name).getImage(color));
            return _customScore.get(getColoredName(name,color));
        } else if (name.compareTo(DEFAULT_CUSTOM_ICON) != 0) {
            return getCustomScoreIcon(DEFAULT_CUSTOM_ICON, color);
        }
        //default
        throw new RuntimeException("Mini icon for score display not found for :" + name);
    }

    public static PixelImage getCustomPlayerBackgroundBase(String name) {
        return _customBackgroundBase.get(name);
    }

    public static void addCustomPlayerBackgroundBase(String name, PixelImage base) {
        if (name != null && base != null) {
            _customBackgroundBase.put(name, base);
            _externalCustomIcons.add(name);
        }
    }

    public static Collection<String> getCustomPlayerBackgroundNames() {
        return _customBackgroundBase.keySet();
    }

    public static Image getCustomPlayerBackground(String name, Color color) {
        if (_customBackground.containsKey(getColoredName(name,color))) return _customBackground.get(getColoredName(name,color));
        
        if (_customBackgroundBase.containsKey(name)) {
            Image img = _customBackgroundBase.get(name).getImage(color);
            _customBackground.put(getColoredName(name,color), img);
            return img;
        } else if (name.compareTo(DEFAULT_BACKGROUND) != 0) {
            return getCustomPlayerBackground(DEFAULT_BACKGROUND, color);
        }

        return null;
    }

    public static AnimationEffectInstance getEffect(String name, long rate, Point loc) {

        if (_effects.containsKey(name)) return _effects.get(name).createInstance(rate, loc);

        return null;
    }

    public static void clearCustomIconCache() {
        _animatedIcon.clear();
        _customNumber.clear();
        _customScore.clear();
        _customBackground.clear();

    }

    public static void clearExternalCustomIcons() {
        for (String key : _externalCustomIcons) {
            _customIconBase.remove(key);
            _customNumberBase.remove(key);
            _customScoreBase.remove(key);
            _customBackgroundBase.remove(key);
        }

        _externalCustomIcons.clear();
    }

    public static void clearCustomIcons() {
        _iconBase.clear();
        _animatedIcon.clear();
        _customIconBase.clear();

        _customNumberBase.clear();
        _customNumber.clear();
    }

    private static String getColoredName(String name, Color color) {
        return name + "r"+color.getRed() + "g" + color.getGreen() + "b"+color.getBlue() + "a"+color.getAlpha();
    }

}
