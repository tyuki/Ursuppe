/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.gui.icon;

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.awt.image.PixelGrabber;
import java.io.Serializable;

/**
 *
 * @author Personal
 */

public class PixelImage implements Serializable {
    public final int[] pixels;
    public final int width;
    public final int height;
    private static final float BASE_HUE = 0;

    public static final long serialVersionUID = 484168579L;


    public PixelImage(BufferedImage image) {
        width = image.getWidth();
        height = image.getHeight();
        pixels = new int[width*height];
        PixelGrabber pg = new PixelGrabber(image, 0, 0, width, height, pixels, 0, image.getWidth());
        try {
            pg.grabPixels();
        } catch (InterruptedException ex) {
        }
    }

    public Image getImage() {
        MemoryImageSource mis = new MemoryImageSource(width, height, pixels, 0, width);
        Toolkit tk = Toolkit.getDefaultToolkit();
        return tk.createImage(mis);
    }

    public Image getImage(Color color) {
        int[] colored = new int[pixels.length];
        for (int i = 0; i < pixels.length; i++)  {
            int alpha = (pixels[i] >> 24) & 0xff;
            int red   = (pixels[i] >> 16) & 0xff;
            int green = (pixels[i] >>  8) & 0xff;
            int blue  = (pixels[i]      ) & 0xff;

            //Convert to HSB
            float[] origHSB = new float[3];
            Color.RGBtoHSB(red, green, blue, origHSB);

            //Get HSB of the color to replace with
            float[] newColHSB = new float[3];
            Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), newColHSB);

            //Set the H to new one
            //origHSB[0] = newColHSB[0];

            //Difference between base
            origHSB[0] = (origHSB[0] + (newColHSB[0] - BASE_HUE)) % 1.0f;
            origHSB[1] = Math.min(1.0f, (origHSB[1] * newColHSB[1] * 2));
            origHSB[2] = Math.min(1.0f, (origHSB[2] * newColHSB[2] * 2));


            //Add alpha back
            int argb = (alpha << 24) + (Color.HSBtoRGB(origHSB[0], origHSB[1], origHSB[2]) & 0x00ffffff);
            //New color
            colored[i] = argb;
        }

        MemoryImageSource mis = new MemoryImageSource(width, height, colored, 0, width);
        Toolkit tk = Toolkit.getDefaultToolkit();
        return tk.createImage(mis);

    }

}
