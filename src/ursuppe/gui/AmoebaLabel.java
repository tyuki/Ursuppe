/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ursuppe.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JLabel;
import ursuppe.game.Amoeba;
import ursuppe.game.GameState.ATTACK_RESPONSE;
import ursuppe.gui.UrsuppeColor.TYPE;
import ursuppe.gui.effect.EffectListener;
import ursuppe.gui.icon.AnimatedIconInstance;
import ursuppe.gui.icon.IconContainer;

/**
 *
 * @author Personal
 */
public class AmoebaLabel extends JLabel implements EffectListener {

    private java.util.Timer _timer;
    private AnimatedIconInstance _icon;
    private Amoeba _amoeba;
    private boolean _HC;
    private boolean _MOU;
    private boolean _ALM;
    private boolean _ARM;
    private boolean _ESC;
    private boolean _DEF;
    private boolean _blocked;
    private boolean _none;
    private boolean _emphasis;
    private boolean _suppressed;
    private Component repaintTarget;
    private UrsuppeGUI _gui;

    public AmoebaLabel() {
        super();

        this.setSize(new Dimension(30, 30));
//        setDoubleBuffered(true);

    }

    public void setRepaintTarget(Component repaintTarget) {
        this.repaintTarget = repaintTarget;
    }
    
    public void setGUI(UrsuppeGUI gui) {
        _gui = gui;
    }

    public void setAmoeba(Amoeba amoeba) {
        _amoeba = amoeba;
        if (amoeba == null) {
            _icon = null;
            setIcon(null);
            //stop timer
            stopAnimation();

            _ALM = _ARM = _ESC = _DEF = _blocked = false;
        } else {
            _icon = IconContainer.getCustomIcon(IconContainer.CUSTOM_ICON_PREFIX + _amoeba.playerID, UrsuppeColor.get(_amoeba.colorID).toColor(TYPE.AMOEBA)).getInstance();
            setIcon(_icon);

            //_amoeba.playerID
        }
    }

    public Amoeba getAmoeba() {
        return _amoeba;
    }

    @Override
    public void paintComponent(Graphics g) {
        //Suppressed
        if (_suppressed) {
            return;
        }

        //Highlight is drawed below all other
        try {
            if (_emphasis) {
                g.drawImage(IconContainer.getIcon("highlight").getImage(), 0, 0, null);
            }
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }

        //Draw Icon
        super.paintComponent(g);

        //Draw amoeba status info
        if (_amoeba != null) {

            try {
                if (_amoeba.number > 0) {
                    g.drawImage(IconContainer.getCustomNumber(IconContainer.CUSTOM_ICON_PREFIX + _amoeba.playerID, UrsuppeColor.get(_amoeba.colorID).toColor(TYPE.AMOEBA), _amoeba.number), 0, 0, null);
                }
                if (_ARM) {
                    g.drawImage(IconContainer.getIcon("defense_ARM").getImage(), 0, 0, null);
                }
                if (_ALM) {
                    g.drawImage(IconContainer.getIcon("defense_ALM").getImage(), 0, 0, null);
                }
                if (_ESC) {
                    g.drawImage(IconContainer.getIcon("defense_ESC").getImage(), 0, 0, null);
                }
                if (_DEF) {
                    g.drawImage(IconContainer.getIcon("defense_DEF").getImage(), 0, 0, null);
                }
                if (_HC) {
                    g.drawImage(IconContainer.getIcon("defense_HARD").getImage(), 0, 0, null);
                }
                if (_MOU) {
                    g.drawImage(IconContainer.getIcon("defense_MOU").getImage(), 0, 0, null);
                }
                if (_blocked) {
                    g.drawImage(IconContainer.getIcon("defense_blocked").getImage(), 0, 0, null);
                }
                //if (_none) g.drawImage(IconContainer.getIcon("defense_none").getImage(), 0, 0, null);
            } catch (NullPointerException npe) {
                g.setColor(Color.black);
                g.setFont(new Font("SansSerif", Font.PLAIN, 16));
                g.drawString(Integer.toString(_amoeba.number), 10, 22);
                npe.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            g.setColor(Color.white);
            if (_amoeba.getDP() > 0) {
                g.fillRect(27, 27, 2, 2);
            }
            if (_amoeba.getDP() > 1) {
                g.fillRect(22, 27, 2, 2);
            }
            if (_amoeba.getDP() > 2) {
                g.fillRect(17, 27, 2, 2);
            }
            if (_amoeba.getDP() > 3) {
                g.fillRect(12, 27, 2, 2);
            }
        }
    }

    private void repaintParent() {
        if (_gui != null) {
            _gui.repaint();
        } else if (repaintTarget != null) {
            repaintTarget.repaint();
        } else {
            repaint();
        }
    }

    public void timerEvent() {
        if (_icon != null) {
            _icon.next();

            repaintParent();
        }
    }

    public void startAnimation() {
        if (_icon != null) {
            _icon.reset();
            if (_timer != null) {
                _timer.cancel();
            }
            _timer = new Timer();
            _timer.schedule(new TimerEvent(), 0, 100);
        }
    }

    public void stopAnimation() {
        if (_icon != null) {
            _icon.reset();
            if (_timer != null) {
                _timer.cancel();
            }
        }
    }

    public void setEmphasis(boolean em) {
        _emphasis = em;

        repaintParent();
    }

    public void setDefense(List<ATTACK_RESPONSE> res) {
        _HC = res.contains(ATTACK_RESPONSE.HARD_CRUST);
        _MOU = res.contains(ATTACK_RESPONSE.MORE_THAN_A_MOUTHFUL);
        _ALM = res.contains(ATTACK_RESPONSE.ALARM);
        _ARM = res.contains(ATTACK_RESPONSE.ARMOR);
        _ESC = res.contains(ATTACK_RESPONSE.ESCAPE);
        _DEF = res.contains(ATTACK_RESPONSE.DEFENSE);
        _blocked = res.contains(ATTACK_RESPONSE.BLOCKED);
        _none = res.contains(ATTACK_RESPONSE.NO_DEFENSE);
        _emphasis = res.size() > 0;

        repaintParent();
    }

    public void resetDefense() {
        _HC = false;
        _MOU = false;
        _ALM = false;
        _ARM = false;
        _ESC = false;
        _DEF = false;
        _blocked = false;
        _none = false;
        _emphasis = false;


        repaintParent();
    }

    public void setSuppressed(boolean suppressed) {
        _suppressed = suppressed;
    }

    @Override
    public void effectStarted() {
        setSuppressed(true);
    }

    @Override
    public void effectFinished() {
        setSuppressed(false);
    }

    private class TimerEvent extends TimerTask {

        @Override
        public void run() {
            timerEvent();
        }
    }
}
