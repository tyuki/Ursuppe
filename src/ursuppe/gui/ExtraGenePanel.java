/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ExtraGenePanel.java
 *
 * Created on 2010/01/13, 22:50:30
 */

package ursuppe.gui;

import java.awt.event.MouseEvent;
import ursuppe.gui.icon.IconContainer;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JLabel;
import ursuppe.game.Gene;
import ursuppe.game.GenePool;
import ursuppe.game.GenePool.GENE;

/**
 *
 * @author Personal
 */
public class ExtraGenePanel extends javax.swing.JPanel {

    private UrsuppeGUI _gui;
    private Map<GENE, JLabel> _geneToLabel;
    private GenePool _genePool;
    private boolean _availabilitySet;

    private boolean _RmousePressed;

    /** Creates new form ExtraGenePanel */
    public ExtraGenePanel() {
        initComponents();

        _geneToLabel = new HashMap<>();

        _geneToLabel.put(GENE.DandF, DandF);
        _geneToLabel.put(GENE.GLU, GLU);
        _geneToLabel.put(GENE.MIG, MIG);
        _geneToLabel.put(GENE.HARD, HARD);
        _geneToLabel.put(GENE.ALM, ALM);
        _geneToLabel.put(GENE.CLE, CLE);
        _geneToLabel.put(GENE.SOC, SOC);
        _geneToLabel.put(GENE.FAST, FAST);
        _geneToLabel.put(GENE.MOU, MOU);
        _geneToLabel.put(GENE.ADA, ADA);
        _geneToLabel.put(GENE.SUC1, SUC1);
        _geneToLabel.put(GENE.DIS, DIS);
        _geneToLabel.put(GENE.CAM1, CAM1);
        _geneToLabel.put(GENE.HEA1, HEA1);
        _geneToLabel.put(GENE.FLEX, FLE);
        _geneToLabel.put(GENE.SLS, SLS);
        _geneToLabel.put(GENE.TOX, TOX);
        _geneToLabel.put(GENE.BANG, BAN);
        _geneToLabel.put(GENE.REB, REB);
        _geneToLabel.put(GENE.POP, POP);
        _geneToLabel.put(GENE.ENE, ENE);
        _geneToLabel.put(GENE.PHD, PHD);
        _geneToLabel.put(GENE.ENV, ENV);
        _geneToLabel.put(GENE.CAM2, CAM2);
        _geneToLabel.put(GENE.HEA2, HEA2);
        _geneToLabel.put(GENE.SUC2, SUC2);
        
        //Set icons
        for (GENE gene : _geneToLabel.keySet()) {
            _geneToLabel.get(gene).setIcon(IconContainer.getMiniIcon(GenePool.getIconFilename(gene)));
            _geneToLabel.get(gene).setText(GenePool.toName(gene));
        }
    }

    public void setGUI(UrsuppeGUI gui) {
        _gui = gui;
    }


    public void setGene(GenePool pool) {
        _genePool = pool;
    }


    public void setAvailability(Map<GENE, Boolean> avail) {
        for (GENE gene : _geneToLabel.keySet()) {
            try {
                _geneToLabel.get(gene).setEnabled(avail.get(gene));
            } catch (NullPointerException npe) {
                _geneToLabel.get(gene).setEnabled(false);
            }
        }
        _availabilitySet = true;
    }

    public void clearAvailability() {
        _availabilitySet = false;
        for (GENE gene : _geneToLabel.keySet()) {
            _geneToLabel.get(gene).setEnabled(true);
        }
    }

    private void setDescription(Gene gene) {
        if (gene == null) return;
        try {
            _gui.setGeneDescription(gene.toDesciptionString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        extraGeneLabel = new javax.swing.JLabel();
        DandF = new javax.swing.JLabel();
        GLU = new javax.swing.JLabel();
        MIG = new javax.swing.JLabel();
        HARD = new javax.swing.JLabel();
        ALM = new javax.swing.JLabel();
        CLE = new javax.swing.JLabel();
        SOC = new javax.swing.JLabel();
        FAST = new javax.swing.JLabel();
        MOU = new javax.swing.JLabel();
        ADA = new javax.swing.JLabel();
        SUC1 = new javax.swing.JLabel();
        DIS = new javax.swing.JLabel();
        CAM1 = new javax.swing.JLabel();
        HEA1 = new javax.swing.JLabel();
        FLE = new javax.swing.JLabel();
        SLS = new javax.swing.JLabel();
        TOX = new javax.swing.JLabel();
        BAN = new javax.swing.JLabel();
        REB = new javax.swing.JLabel();
        POP = new javax.swing.JLabel();
        ENE = new javax.swing.JLabel();
        PHD = new javax.swing.JLabel();
        ENV = new javax.swing.JLabel();
        CAM2 = new javax.swing.JLabel();
        HEA2 = new javax.swing.JLabel();
        SUC2 = new javax.swing.JLabel();
        NOP1 = new javax.swing.JLabel();
        NOP2 = new javax.swing.JLabel();

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(ursuppe.UrsuppeApp.class).getContext().getResourceMap(ExtraGenePanel.class);
        setBackground(resourceMap.getColor("Form.background")); // NOI18N
        setMaximumSize(new java.awt.Dimension(210, 184));
        setMinimumSize(new java.awt.Dimension(210, 184));
        setName("Form"); // NOI18N
        setPreferredSize(new java.awt.Dimension(210, 184));
        setLayout(new java.awt.GridBagLayout());

        extraGeneLabel.setFont(resourceMap.getFont("geneLabel.font")); // NOI18N
        extraGeneLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        extraGeneLabel.setText(resourceMap.getString("extraGeneLabel.text")); // NOI18N
        extraGeneLabel.setMaximumSize(new java.awt.Dimension(210, 24));
        extraGeneLabel.setMinimumSize(new java.awt.Dimension(210, 24));
        extraGeneLabel.setName("extraGeneLabel"); // NOI18N
        extraGeneLabel.setPreferredSize(new java.awt.Dimension(210, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 7;
        add(extraGeneLabel, gridBagConstraints);

        DandF.setText(resourceMap.getString("DandF.text")); // NOI18N
        DandF.setMaximumSize(new java.awt.Dimension(30, 40));
        DandF.setMinimumSize(new java.awt.Dimension(30, 40));
        DandF.setName("DandF"); // NOI18N
        DandF.setPreferredSize(new java.awt.Dimension(30, 40));
        DandF.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        DandF.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        add(DandF, gridBagConstraints);

        GLU.setMaximumSize(new java.awt.Dimension(30, 40));
        GLU.setMinimumSize(new java.awt.Dimension(30, 40));
        GLU.setName("GLU"); // NOI18N
        GLU.setPreferredSize(new java.awt.Dimension(30, 40));
        GLU.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        GLU.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        add(GLU, gridBagConstraints);

        MIG.setMaximumSize(new java.awt.Dimension(30, 40));
        MIG.setMinimumSize(new java.awt.Dimension(30, 40));
        MIG.setName("MIG"); // NOI18N
        MIG.setPreferredSize(new java.awt.Dimension(30, 40));
        MIG.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        MIG.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        add(MIG, gridBagConstraints);

        HARD.setMaximumSize(new java.awt.Dimension(30, 40));
        HARD.setMinimumSize(new java.awt.Dimension(30, 40));
        HARD.setName("HARD"); // NOI18N
        HARD.setPreferredSize(new java.awt.Dimension(30, 40));
        HARD.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        HARD.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        add(HARD, gridBagConstraints);

        ALM.setMaximumSize(new java.awt.Dimension(30, 40));
        ALM.setMinimumSize(new java.awt.Dimension(30, 40));
        ALM.setName("ALM"); // NOI18N
        ALM.setPreferredSize(new java.awt.Dimension(30, 40));
        ALM.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        ALM.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        add(ALM, gridBagConstraints);

        CLE.setMaximumSize(new java.awt.Dimension(30, 40));
        CLE.setMinimumSize(new java.awt.Dimension(30, 40));
        CLE.setName("CLE"); // NOI18N
        CLE.setPreferredSize(new java.awt.Dimension(30, 40));
        CLE.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        CLE.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 1;
        add(CLE, gridBagConstraints);

        SOC.setMaximumSize(new java.awt.Dimension(30, 40));
        SOC.setMinimumSize(new java.awt.Dimension(30, 40));
        SOC.setName("SOC"); // NOI18N
        SOC.setPreferredSize(new java.awt.Dimension(30, 40));
        SOC.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        SOC.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 1;
        add(SOC, gridBagConstraints);

        FAST.setMaximumSize(new java.awt.Dimension(30, 40));
        FAST.setMinimumSize(new java.awt.Dimension(30, 40));
        FAST.setName("FAST"); // NOI18N
        FAST.setPreferredSize(new java.awt.Dimension(30, 40));
        FAST.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        FAST.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        add(FAST, gridBagConstraints);

        MOU.setMaximumSize(new java.awt.Dimension(30, 40));
        MOU.setMinimumSize(new java.awt.Dimension(30, 40));
        MOU.setName("MOU"); // NOI18N
        MOU.setPreferredSize(new java.awt.Dimension(30, 40));
        MOU.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        MOU.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        add(MOU, gridBagConstraints);

        ADA.setMaximumSize(new java.awt.Dimension(30, 40));
        ADA.setMinimumSize(new java.awt.Dimension(30, 40));
        ADA.setName("ADA"); // NOI18N
        ADA.setPreferredSize(new java.awt.Dimension(30, 40));
        ADA.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        ADA.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        add(ADA, gridBagConstraints);

        SUC1.setMaximumSize(new java.awt.Dimension(30, 40));
        SUC1.setMinimumSize(new java.awt.Dimension(30, 40));
        SUC1.setName("SUC1"); // NOI18N
        SUC1.setPreferredSize(new java.awt.Dimension(30, 40));
        SUC1.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        SUC1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        add(SUC1, gridBagConstraints);

        DIS.setMaximumSize(new java.awt.Dimension(30, 40));
        DIS.setMinimumSize(new java.awt.Dimension(30, 40));
        DIS.setName("DIS"); // NOI18N
        DIS.setPreferredSize(new java.awt.Dimension(30, 40));
        DIS.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        DIS.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        add(DIS, gridBagConstraints);

        CAM1.setMaximumSize(new java.awt.Dimension(30, 40));
        CAM1.setMinimumSize(new java.awt.Dimension(30, 40));
        CAM1.setName("CAM1"); // NOI18N
        CAM1.setPreferredSize(new java.awt.Dimension(30, 40));
        CAM1.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        CAM1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 2;
        add(CAM1, gridBagConstraints);

        HEA1.setMaximumSize(new java.awt.Dimension(30, 40));
        HEA1.setMinimumSize(new java.awt.Dimension(30, 40));
        HEA1.setName("HEA1"); // NOI18N
        HEA1.setPreferredSize(new java.awt.Dimension(30, 40));
        HEA1.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        HEA1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        add(HEA1, gridBagConstraints);

        FLE.setMaximumSize(new java.awt.Dimension(30, 40));
        FLE.setMinimumSize(new java.awt.Dimension(30, 40));
        FLE.setName("FLE"); // NOI18N
        FLE.setPreferredSize(new java.awt.Dimension(30, 40));
        FLE.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        FLE.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        add(FLE, gridBagConstraints);

        SLS.setMaximumSize(new java.awt.Dimension(30, 40));
        SLS.setMinimumSize(new java.awt.Dimension(30, 40));
        SLS.setName("SLS"); // NOI18N
        SLS.setPreferredSize(new java.awt.Dimension(30, 40));
        SLS.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        SLS.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        add(SLS, gridBagConstraints);

        TOX.setMaximumSize(new java.awt.Dimension(30, 40));
        TOX.setMinimumSize(new java.awt.Dimension(30, 40));
        TOX.setName("TOX"); // NOI18N
        TOX.setPreferredSize(new java.awt.Dimension(30, 40));
        TOX.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        TOX.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        add(TOX, gridBagConstraints);

        BAN.setMaximumSize(new java.awt.Dimension(30, 40));
        BAN.setMinimumSize(new java.awt.Dimension(30, 40));
        BAN.setName("BAN"); // NOI18N
        BAN.setPreferredSize(new java.awt.Dimension(30, 40));
        BAN.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        BAN.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 3;
        add(BAN, gridBagConstraints);

        REB.setMaximumSize(new java.awt.Dimension(30, 40));
        REB.setMinimumSize(new java.awt.Dimension(30, 40));
        REB.setName("REB"); // NOI18N
        REB.setPreferredSize(new java.awt.Dimension(30, 40));
        REB.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        REB.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 3;
        add(REB, gridBagConstraints);

        POP.setMaximumSize(new java.awt.Dimension(30, 40));
        POP.setMinimumSize(new java.awt.Dimension(30, 40));
        POP.setName("POP"); // NOI18N
        POP.setPreferredSize(new java.awt.Dimension(30, 40));
        POP.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        POP.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 3;
        add(POP, gridBagConstraints);

        ENE.setMaximumSize(new java.awt.Dimension(30, 40));
        ENE.setMinimumSize(new java.awt.Dimension(30, 40));
        ENE.setName("ENE"); // NOI18N
        ENE.setPreferredSize(new java.awt.Dimension(30, 40));
        ENE.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        ENE.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 3;
        add(ENE, gridBagConstraints);

        PHD.setMaximumSize(new java.awt.Dimension(30, 40));
        PHD.setMinimumSize(new java.awt.Dimension(30, 40));
        PHD.setName("PHD"); // NOI18N
        PHD.setPreferredSize(new java.awt.Dimension(30, 40));
        PHD.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        PHD.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        add(PHD, gridBagConstraints);

        ENV.setMaximumSize(new java.awt.Dimension(30, 40));
        ENV.setMinimumSize(new java.awt.Dimension(30, 40));
        ENV.setName("ENV"); // NOI18N
        ENV.setPreferredSize(new java.awt.Dimension(30, 40));
        ENV.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        ENV.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        add(ENV, gridBagConstraints);

        CAM2.setMaximumSize(new java.awt.Dimension(30, 40));
        CAM2.setMinimumSize(new java.awt.Dimension(30, 40));
        CAM2.setName("CAM2"); // NOI18N
        CAM2.setPreferredSize(new java.awt.Dimension(30, 40));
        CAM2.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        CAM2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        add(CAM2, gridBagConstraints);

        HEA2.setMaximumSize(new java.awt.Dimension(30, 40));
        HEA2.setMinimumSize(new java.awt.Dimension(30, 40));
        HEA2.setName("HEA2"); // NOI18N
        HEA2.setPreferredSize(new java.awt.Dimension(30, 40));
        HEA2.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        HEA2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 4;
        add(HEA2, gridBagConstraints);

        SUC2.setMaximumSize(new java.awt.Dimension(30, 40));
        SUC2.setMinimumSize(new java.awt.Dimension(30, 40));
        SUC2.setName("SUC2"); // NOI18N
        SUC2.setPreferredSize(new java.awt.Dimension(30, 40));
        SUC2.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                GeneMouseWheelMoved(evt);
            }
        });
        SUC2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GeneMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        add(SUC2, gridBagConstraints);

        NOP1.setMaximumSize(new java.awt.Dimension(30, 40));
        NOP1.setMinimumSize(new java.awt.Dimension(30, 40));
        NOP1.setName("NOP1"); // NOI18N
        NOP1.setPreferredSize(new java.awt.Dimension(30, 40));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 4;
        add(NOP1, gridBagConstraints);

        NOP2.setMaximumSize(new java.awt.Dimension(30, 40));
        NOP2.setMinimumSize(new java.awt.Dimension(30, 40));
        NOP2.setName("NOP2"); // NOI18N
        NOP2.setPreferredSize(new java.awt.Dimension(30, 40));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 4;
        add(NOP2, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void GeneMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_GeneMouseEntered
        try {
            if (_genePool != null) {
                setDescription(_genePool.getGene(GenePool.toGENE(((JLabel)evt.getSource()).getText())));
                _RmousePressed = false;
            }
        } catch (Exception e) {e.printStackTrace();}
    }//GEN-LAST:event_GeneMouseEntered

    private void GeneMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_GeneMousePressed
        try {
            //Left click
            if (evt.getButton() == MouseEvent.BUTTON1) {
                if (_genePool != null && _availabilitySet && evt.getComponent().isEnabled()) {
                    //setDescription(_genePool.getGene(GenePool.toGENE(((JLabel)evt.getSource()).getText())));
                    _gui.geneSelected(GenePool.toGENE(((JLabel)evt.getSource()).getText()));
                }
            } else if (evt.getButton() == MouseEvent.BUTTON2 || evt.getButton() == MouseEvent.BUTTON3) {
                if (_RmousePressed) {
                    _gui.setGeneDescriptionCarretToHead();
                } else {
                    _gui.setGeneDescriptionCarretToTail();
                }
                _RmousePressed = !_RmousePressed;
            }
        } catch (Exception e) {e.printStackTrace();}
    }//GEN-LAST:event_GeneMousePressed

    private void GeneMouseWheelMoved(java.awt.event.MouseWheelEvent evt) {//GEN-FIRST:event_GeneMouseWheelMoved
        try {
            if (_genePool != null) {
                if (evt.getWheelRotation() > 0) {
                    _gui.setGeneDescriptionCarretToTail();
                    _RmousePressed = true;
                } else {
                    _gui.setGeneDescriptionCarretToHead();
                    _RmousePressed = false;
                }
            }
        } catch (Exception e) {e.printStackTrace();}
    }//GEN-LAST:event_GeneMouseWheelMoved


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel ADA;
    private javax.swing.JLabel ALM;
    private javax.swing.JLabel BAN;
    private javax.swing.JLabel CAM1;
    private javax.swing.JLabel CAM2;
    private javax.swing.JLabel CLE;
    private javax.swing.JLabel DIS;
    private javax.swing.JLabel DandF;
    private javax.swing.JLabel ENE;
    private javax.swing.JLabel ENV;
    private javax.swing.JLabel FAST;
    private javax.swing.JLabel FLE;
    private javax.swing.JLabel GLU;
    private javax.swing.JLabel HARD;
    private javax.swing.JLabel HEA1;
    private javax.swing.JLabel HEA2;
    private javax.swing.JLabel MIG;
    private javax.swing.JLabel MOU;
    private javax.swing.JLabel NOP1;
    private javax.swing.JLabel NOP2;
    private javax.swing.JLabel PHD;
    private javax.swing.JLabel POP;
    private javax.swing.JLabel REB;
    private javax.swing.JLabel SLS;
    private javax.swing.JLabel SOC;
    private javax.swing.JLabel SUC1;
    private javax.swing.JLabel SUC2;
    private javax.swing.JLabel TOX;
    private javax.swing.JLabel extraGeneLabel;
    // End of variables declaration//GEN-END:variables

}
