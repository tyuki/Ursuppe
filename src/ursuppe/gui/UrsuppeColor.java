/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.gui;

import java.awt.Color;
import java.io.Serializable;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import ursuppe.UrsuppeApp;

/**
 *
 * @author Personal
 */
public class UrsuppeColor implements Serializable {

    public final int ID;
    public final float H;
    
    private Map<TYPE, Color> colors;
    
    public static enum TYPE {
        CHAT(0.55f, 1.0f),
        FOOD(0.6f, 1.0f),
        AMOEBA(0.5f, 0.5f),
        PLAYER(0.5f, 0.5f);
        
        public final float defaultS;
        public final float defaultV;

        private TYPE(float defaultS, float defaultV) {
            this.defaultS = defaultS;
            this.defaultV = defaultV;
        }
        
        
    }
    
    private static UrsuppeColor[] availableColors = new UrsuppeColor[6];
    
    static {
        
        final float[] Hs = new float[] {0.0f, 0.17f, 0.33f, 0.50f, 0.67f, 0.84f};

        for (int i = 0; i < Hs.length; i++) {
            availableColors[i] = (new UrsuppeColor(i, Hs[i]));
        }
    }



    public UrsuppeColor(int ID, float H) {
        this.ID = ID;
        this.H = H;
        
        colors = new EnumMap<>(TYPE.class);
        
        for (TYPE type : TYPE.values()) {
            colors.put(type, Color.getHSBColor(H, type.defaultS, type.defaultV));
        }
    }
    
    public static void modifyColor(TYPE type, float newS, float newV) {
        try {
            if (0 > newS || newS > 1) {
                newS = type.defaultS;
            }
            if (0 > newV || newV > 1) {
                newV = type.defaultV;
            }
            
            for (UrsuppeColor color : availableColors) {
                color.colors.put(type, Color.getHSBColor(color.H, newS, newV));
            }
        } catch (Exception e) { UrsuppeApp.debug(e); }
    }
    
    public Color toColor(TYPE type) {
        return colors.get(type);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof UrsuppeColor)
            return H == ((UrsuppeColor)obj).H;

        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.ID;
        return hash;
    }

    public static UrsuppeColor get(int id) {
        assert(0 <= id && id < availableColors.length);
        return availableColors[id];
    }


    public static List<UrsuppeColor> getPlayerColors() {
//        List<UrsuppeColor> playerColors = new LinkedList<UrsuppeColor>();
//
//        final float[] Hs = new float[] {0.0f, 0.17f, 0.33f, 0.50f, 0.67f, 0.84f};
//
//        for (int i = 0; i < Hs.length; i++) {
//            playerColors.add(new UrsuppeColor(i, Hs[i]));
//        }
//
////        playerColors.add(Color.decode("#e18a8a"));
////        playerColors.add(Color.decode("#fbff84"));
////        playerColors.add(Color.decode("#80ff81"));
////        playerColors.add(Color.decode("#82ffff"));
////        playerColors.add(Color.decode("#8380ff"));
////        playerColors.add(Color.decode("#ff80fd"));
//
//////        new UrsuppeColor()
////        playerColors.add(Color.getHSBColor(0.00f, 0.50f, 1.00f));  //#e18a8a
////        playerColors.add(Color.getHSBColor(0.17f, 0.48f, 1.00f));  //#fbff84
////        playerColors.add(Color.getHSBColor(0.33f, 0.50f, 1.00f));  //#80ff81
////        playerColors.add(Color.getHSBColor(0.50f, 0.49f, 1.00f));  //#82ffff
////        playerColors.add(Color.getHSBColor(0.67f, 0.50f, 1.00f));  //#8380ff
////        playerColors.add(Color.getHSBColor(0.84f, 0.50f, 1.00f));  //#ff80fd
//
//
////        for (Color color : playerColors) {
////            float[] hsv = Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
////            System.out.println(String.format("%.2f %.2f %.2f", hsv[0], hsv[1], hsv[2]));
////        }
//
        return Arrays.asList(availableColors);
    }

}
