/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.gui.effect;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.util.List;

/**
 *
 * @author Personal
 */
public class AnimationEffectInstance extends Effect {

    private final List<Image> _images;
    private int _current = 0;
    public final Point loc;


    public AnimationEffectInstance(List<Image>images, long rate, Point loc) {
        _images = images;
        _rate = rate;
        this.loc = loc;
    }

    @Override
    public boolean nextFrame() {
        if (_images == null || _images.size() <= _current+1) {
            return false;
        } else {
            _current++;
            return true;
        }
    }

    @Override
    public void paint(Graphics g) {
        try {
            g.translate(loc.x, loc.y);
            g.drawImage(_images.get(_current), 0, 0, null);
            g.translate(-loc.x, -loc.y);
        } catch (Exception e) {e.printStackTrace();}
    }

}
