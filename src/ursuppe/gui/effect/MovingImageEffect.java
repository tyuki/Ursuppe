/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.gui.effect;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.util.List;

/**
 *
 * @author Personal
 */
public class MovingImageEffect extends Effect {

    private final List<Image> _images;
    private int _current;
    private final long _totalTime;
    private final Point _start;
    private final Point _end;

    private Point _currentLoc;

    public MovingImageEffect(List<Image> images, Point start, Point end, long rate, long total) {
        _images = images;
        _start = start;
        _end = end;
        _rate = rate;
        _totalTime = total;
        _currentLoc = new Point(_start.x, _start.y);
    }

    @Override
    public void paint(Graphics g) {
        try {
            g.translate(_currentLoc.x, _currentLoc.y);
            g.drawImage(_images.get(_current % _images.size()), 0, 0, null);
            g.translate(-_currentLoc.x, -_currentLoc.y);
        } catch (Exception e) {e.printStackTrace();}
    }

    @Override
    protected boolean nextFrame() {
        if (_images == null || _images.isEmpty() || _current*_rate >= _totalTime) {
            return false;
        } else {
            _current++;
            double ratio = (double)(_current*_rate) / (double)_totalTime;
            _currentLoc = new Point(_start.x+(int)(((double)(_end.x-_start.x))*ratio), _start.y+(int)(((double)(_end.y-_start.y))*ratio));
            return true;
        }
    }

    /*public MovingImageEffectInstance createInstance(long rate, Point loc) {
        return new AnimationEffectInstance(_images, rate, loc);
    }*/
}
