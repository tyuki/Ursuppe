/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.gui.effect;

import java.awt.Image;
import java.awt.Point;
import java.util.List;

/**
 *
 * @author Personal
 */
public class AnimationEffect {

    private final List<Image> _images;

    public AnimationEffect(List<Image> images) {
        _images = images;
    }

    public AnimationEffectInstance createInstance(long rate, Point loc) {
        return new AnimationEffectInstance(_images, rate, loc);
    }

    
}
