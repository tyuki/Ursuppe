/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.gui.effect;

import java.awt.Component;
import java.awt.Graphics;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Personal
 */
public abstract class Effect extends TimerTask {

    private Component _component;
    private List<Effect> _regsiteredList;
    private Timer _timer;
    protected long _rate = 100;

    private List<EffectListener> _listener = new LinkedList<>();

    public abstract void paint(Graphics g);

    public void addEffectListener(EffectListener listener) {
        _listener.add(listener);
    }

    public final void register(Component component, List<Effect> list) {
        _component = component;
        _regsiteredList = list;
        _regsiteredList.add(this);
        start();
    }

    protected final void start() {
        _timer = new Timer();
        _timer.schedule(this, 0, _rate);
        for (EffectListener listener : _listener) {
            listener.effectStarted();
        }
    }

    protected abstract boolean nextFrame();

    @Override
    public final void run() {
        try {
            if (nextFrame()) {
                _component.repaint();
            } else {
                _timer.cancel();
                _timer = null;
                _regsiteredList.remove(this);
                for (EffectListener listener : _listener) {
                    listener.effectFinished();
                }
                _component.repaint();
            }
        } catch (Exception e) {
            for (EffectListener listener : _listener) {
                listener.effectFinished();
            }
            e.printStackTrace();
        }
    }
}
