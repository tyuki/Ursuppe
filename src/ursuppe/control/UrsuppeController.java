/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ursuppe.control;

import java.util.List;
import ursuppe.game.EnvironmentCard;
import ursuppe.game.GameState;
import ursuppe.game.GameState.DIRECTION;

/**
 * Main Controller that defines the set of queries to players during the game.
 * 
 * @author tyuki
 */
public interface UrsuppeController {
    
    public void setID(int id);
    public void gameReset();

    public void selectInitialScore(int playerID, GameState game);
    public void selectInitialAmoeba(int playerID, GameState game);
    public void moveResponse(int playerID, int amoebaIndex, List<DIRECTION> dir, GameState game);
    public void selectPhase1Action(int playerID, int amoebaIndex, GameState game);
    public void handleGeneDefect(int playerID, GameState game);
    public void selectPhase3Action(int playerID, GameState game);
    public void selectPhase4Action(int playerID, GameState game);
    //public void selectPhase5Action(int playerID, GameState game);
    public void death(int playerID, int amoebaID, GameState game);
    public void scoring(int playerID, GameState game);

    //HOL (stick)
    public void decideHOL(int playerID, int amoebaIndex, DIRECTION dir, GameState game);

    //POP
    public void decidePOP(int playerID, GameState game);

    //MIG&ENV
    public void showNextEnvironment(EnvironmentCard env);

    //AGR
    public void decideAGR(int playerID, GameState game);

    public void defenderAction(int attackerID, int attackerAmoebaNum, int defenderID, int defenderAmoebaNum, GameState game);
    public void attackerAction(int attackerID, int attackerAmoebaNum, int defenderID, int defenderAmoebaNum, boolean HARD, boolean MOUexcretes, GameState game);
    public void selectEscapeDirection(int defenderID, int defenderAmoebaNum, List<DIRECTION> dir, GameState game);
    public void decideESCSPD(int defenderID, GameState game);
    public void decideSFSBANG(int defenderID, int defenderAmoebaNum, GameState game);
    
}
