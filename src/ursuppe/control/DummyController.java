package ursuppe.control;

import java.util.List;
import ursuppe.game.EnvironmentCard;
import ursuppe.game.GameState;
import ursuppe.game.GameState.DIRECTION;

/**
 * Dummy controller for local mode
 *
 * @author Personal
 */
public class DummyController implements UrsuppeController {
    protected Organizer _organizer;
    protected final String _name;

    protected int _wait = 500;

    private int _playerID = -1;

    public DummyController(String name, int port, String iconName, String bgName, int preferredColor1, int preferredColor2) {
        _name = name;
        _organizer = new Organizer();
        _organizer.setController(this);
        _organizer.connectServer(_name, "127.0.0.1", port, true, iconName, bgName, preferredColor1, preferredColor2);
    }

    public Organizer getOrganizer() {
        return _organizer;
    }

    public int getPlayerID() {
        return _playerID;
    }

    @Override
    public void setID(int id) {
        _playerID = id;
    }

    @Override
    public void gameReset() {
        
    }

    @Override
    public void selectInitialScore(int playerID, GameState game) {
        
    }

    @Override
    public void selectInitialAmoeba(int playerID, GameState game) {

    }

    @Override
    public void moveResponse(int playerID, int amoebaIndex, List<DIRECTION> dir, GameState game) {
        
    }

    @Override
    public void selectPhase1Action(int playerID, int amoebaIndex, GameState game) {
        
    }

    @Override
    public void handleGeneDefect(int playerID, GameState game) {
        
    }

    @Override
    public void selectPhase3Action(int playerID, GameState game) {
        
    }

    @Override
    public void selectPhase4Action(int playerID, GameState game) {
        
    }

    @Override
    public void death(int playerID, int amoebaID, GameState game) {
        
    }

    @Override
    public void scoring(int playerID, GameState game) {
        
    }

    @Override
    public void decideHOL(int playerID, int amoebaIndex, DIRECTION dir, GameState game) {
        
    }

    @Override
    public void decidePOP(int playerID, GameState game) {
        
    }

    @Override
    public void showNextEnvironment(EnvironmentCard env) {
        
    }

    @Override
    public void decideAGR(int playerID, GameState game) {
        
    }

    @Override
    public void defenderAction(int attackerID, int attackerAmoebaNum, int defenderID, int defenderAmoebaNum, GameState game) {
        
    }

    @Override
    public void attackerAction(int attackerID, int attackerAmoebaNum, int defenderID, int defenderAmoebaNum, boolean HARD, boolean MOUexcretes, GameState game) {
        
    }

    @Override
    public void selectEscapeDirection(int defenderID, int defenderAmoebaNum, List<DIRECTION> dir, GameState game) {
        
    }

    @Override
    public void decideESCSPD(int defenderID, GameState game) {
        
    }

    @Override
    public void decideSFSBANG(int defenderID, int defenderAmoebaNum, GameState game) {
        
    }
}
