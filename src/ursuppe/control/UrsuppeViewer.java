package ursuppe.control;

import java.awt.Point;
import javax.swing.text.SimpleAttributeSet;
import sound.SOUND_EVENT;
import ursuppe.game.Amoeba;
import ursuppe.game.GameState;
import ursuppe.game.GenePool.GENE;

/**
 * Viewer is responsible for visualizing the game state.
 * 
 * 
 * @author tyuki
 */
public interface UrsuppeViewer {

    public void setGameState(GameState game);
    public void addUserMessage(String text, SimpleAttributeSet attr);
    public void addSystemMessage(String text, SimpleAttributeSet attr);
    public void geneUsed(int playerID, GENE gene);
    public void soundEvent(SOUND_EVENT event);
    public void moveEvent(Amoeba amoeba, Point src, int srcIndex, Point dst, int dstIndex);

    public void errorReport(String message);
}
