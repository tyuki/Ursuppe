package ursuppe.control;

import sound.SOUND_EVENT;
import java.awt.Color;
import java.awt.Point;
import ursuppe.game.EnvironmentCard;
import ursuppe.game.message.GameTextMessage;
import ursuppe.game.message.ServerMessage;
import ursuppe.game.GameState;
import ursuppe.game.GameManager;
import java.io.IOException;
import java.nio.channels.UnresolvedAddressException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import network.IClient;
import network.Message;
import network.MinaClient;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoderException;
import ursuppe.UrsuppeApp;
import ursuppe.UrsuppeResource;
import ursuppe.game.Amoeba;
import ursuppe.game.GameOption;
import ursuppe.game.GameState.DIRECTION;
import ursuppe.game.GameState.PHASE1_ACTION;
import ursuppe.game.GenePool.GENE;
import ursuppe.game.Player;
import ursuppe.game.Progress;
import ursuppe.game.Progress.DICE_STATE;
import ursuppe.game.Replay;
import ursuppe.gui.UrsuppeColor;
import ursuppe.game.message.GameEventMessage;
import ursuppe.game.message.RelevantAmoeba;
import ursuppe.game.message.RelevantGene;
import ursuppe.gui.UrsuppeColor.TYPE;
import ursuppe.gui.icon.AnimatedColorableIconBase;
import ursuppe.gui.icon.CustomIconSet;
import ursuppe.gui.icon.IconContainer;
import ursuppe.gui.icon.PixelImage;

/**
 * Organizer is the interface between the Controller, Viewer, and messages exchanged over the network.
 * 
 * @author tyuki
 */
public class Organizer extends IoHandlerAdapter {

    private UrsuppeViewer _viewer;
    private UrsuppeController _controller = null;

    private int _playerID = -1;

    private GameManager _server;
    private IClient _client;
    private boolean _ackResponse;
    private boolean _ackArrived;

    private GameState _currentState;

//    @SuppressWarnings("over-ann'");
    public Organizer() {
        
        //Initialize with default viewer that does nothing
        _viewer = new UrsuppeViewer() {

            @Override
            public void setGameState(GameState game) {}
            @Override
            public void addUserMessage(String text, SimpleAttributeSet attr) {}
            public void addGameMessage(GameTextMessage msg) {}
            public void addServerMessage(ServerMessage msg) {}
            @Override
            public void addSystemMessage(String text, SimpleAttributeSet attr) {}            
            @Override
            public void geneUsed(int playerID, GENE gene) {}
            @Override
            public void soundEvent(SOUND_EVENT event) {}
            @Override
            public void moveEvent(Amoeba amoeba, Point src, int srcIndex, Point dst, int dstIndex){}
            @Override
            public void errorReport(String message) {
                UrsuppeApp.debug("Error reported to default viewer :" + message);
            }
        };
        _controller = new UrsuppeController() {
            @Override
            public void setID(int id) {}
            @Override
            public void gameReset() {}
            @Override
            public void selectInitialScore(int playerID, GameState game) {}
            @Override
            public void selectInitialAmoeba(int playerID, GameState game) {}
            @Override
            public void moveResponse(int playerID, int amoebaIndex, List<DIRECTION> dir, GameState game) {}
            @Override
            public void selectPhase1Action(int playerID, int amoebaIndex, GameState game) {}
            @Override
            public void handleGeneDefect(int playerID, GameState game) {}
            @Override
            public void selectPhase3Action(int playerID, GameState game) {}
            @Override
            public void selectPhase4Action(int playerID, GameState game) {}
            @Override
            public void death(int playerID, int amoebaID, GameState game) {}
            @Override
            public void scoring(int playerID, GameState game) {}
            @Override
            public void decideHOL(int playerID, int amoebaIndex, DIRECTION dir, GameState game) {}
            @Override
            public void decidePOP(int playerID, GameState game) {}
            @Override
            public void showNextEnvironment(EnvironmentCard env) {}
            @Override
            public void decideAGR(int playerID, GameState game) {}
            @Override
            public void defenderAction(int attackerID, int attackerAmoebaNum, int defenderID, int defenderAmoebaNum, GameState game) {}
            @Override
            public void attackerAction(int attackerID, int attackerAmoebaNum, int defenderID, int defenderAmoebaNum, boolean HARD, boolean MOUexcretes, GameState game) {}
            @Override
            public void selectEscapeDirection(int defenderID, int defenderAmoebaNum, List<DIRECTION> dir, GameState game) {}
            @Override
            public void decideESCSPD(int defenderID, GameState game) {}
            @Override
            public void decideSFSBANG(int defenderID, int defenderAmoebaNum, GameState game) {}
        };
    }

    public void setViewer(UrsuppeViewer viewer) {
        _viewer = viewer;
    }

    public void setController(UrsuppeController controller) {
        _controller = controller;
    }

    /**
     * Starts a server
     *
     * @param port
     * @param option
     * @return
     */
    public boolean startServer(int port, GameOption option) {
        UrsuppeApp.trace("[Organizer] startServer");

        _server = new GameManager(port, option);

        UrsuppeApp.trace("[Organizer] startServer : server created");

        return _server.start();
    }

    public void stopServer() {
        if (_server != null) _server.stop();
    }


    @Override
    public void exceptionCaught( IoSession session, Throwable cause ) throws Exception
    {
        UrsuppeApp.debug(cause);
       
        if (cause instanceof ProtocolDecoderException) {
//            org.apache.mina.core.
        } else if (cause instanceof IOException) {
//            disconnectFromServer();
            disconnectedFromServer((IOException)cause);
        } else if (cause instanceof Exception) {
            disconnectFromServer();
            UrsuppeApp.dump(cause);
            _viewer.errorReport(UrsuppeResource.getString("error.OtherException"));
        }
    }

    @Override
    public void messageReceived( IoSession session, Object message ) throws Exception
    {
        
        if (message instanceof Message) {
            messageHandling(session, (Message)message);
        }
    }

    @Override
    public void sessionIdle( IoSession session, IdleStatus status ) throws Exception
    {
    }


    public boolean connectServer(String playerName, String host, int port, boolean join, String iconName, String bgName, int preferredColor1, int preferredColor2) {
        _client = new MinaClient(this, host, port);

        try {
            if (_client.connect()) {
                    _ackArrived = false;
                    _ackResponse = false;
                    _client.write(new Message(Message.COMMAND_SETUP, Message.CONNECT, playerName ));
                    //Wait for response
                    while (!_ackArrived) {
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException ex) {
                        }
                    }
                    //Check if acked
                    if (!_ackResponse) {
                        _viewer.errorReport(UrsuppeResource.getString("error.NameUsed"));
                        return false;
                    }
                    if (join) {
                        joinGame(playerName, iconName, bgName, preferredColor1, preferredColor2);
                    }
                    return _ackResponse;
            }
        } catch (UnresolvedAddressException uae) {
            _viewer.errorReport(UrsuppeResource.getString("error.UnknownHost"));
        } catch (Exception e) {
            _viewer.errorReport(UrsuppeResource.getString("error.ConnectException"));
        }
        return false;
    }


    /**
     * Connect to a server
     *
     * @param playerName
     * @param host
     * @param port
     * @param join
     * @return
     */
    public boolean connectServer(String host, int port) {
        _client = new MinaClient(this, host, port);

        try {
            if (_client.connect()) {
                    _ackArrived = false;
                    _ackResponse = false;
                    _client.write(new Message(Message.COMMAND_SETUP, Message.CONNECT, "localhost" ));
                    //Wait for response
                    while (!_ackArrived) {
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException ex) {
                        }
                    }
                    //Check if acked
                    if (!_ackResponse) {
                        _viewer.errorReport(UrsuppeResource.getString("error.NameUsed"));
                        return false;
                    }
                    return _ackResponse;
            }
        } catch (UnresolvedAddressException uae) {
            _viewer.errorReport(UrsuppeResource.getString("error.UnknownHost"));
        } catch (Exception e) {
            _viewer.errorReport(UrsuppeResource.getString("error.ConnectException"));
        }
        return false;
    }

    public void disconnectFromServer(){
        if (_client != null) {
            _client.write(new Message(Message.COMMAND_SETUP, Message.DISCONNECT, _playerID));
            _playerID = -1;
            _controller.setID(_playerID);
            _client.disconnect();
            _viewer.addUserMessage("Disconnected from server", null);
        }
    }

    public boolean isServerRunning() {
        if (_server != null) return _server.isRunning();

        return false;
    }

    public boolean isConnected() {
        if (_client != null) return _client.isConnected();

        return false;
    }

    public int getPort() {
        if (_server != null && _server.isRunning()) return _server.getPort();

        return -1;
    }

    /**
     *
     * @param text
     */
    public void sendChatMessage(String text) {
            if (_client != null) {
                _client.write(new Message(Message.COMMAND_CHAT, Message.USER_MESSAGE, text));
            }
    }

    public void joinGame(String playerName, String iconName, String bgName, int preferredColor1, int preferredColor2) {
            AnimatedColorableIconBase base = null;
            PixelImage[] numBase = null;
            PixelImage scoreBase = null;
            PixelImage PGbase = null;
            if (iconName != null) base = IconContainer.getCustomIconBase(iconName);
            if (iconName != null) numBase = IconContainer.getCustomNumberBase(iconName);
            if (iconName != null) scoreBase = IconContainer.getCustomScoreBase(iconName);
            if (bgName != null) PGbase = IconContainer.getCustomPlayerBackgroundBase(bgName);
            CustomIconSet customIcon = new CustomIconSet(base, numBase, scoreBase, PGbase);
            _client.write(new Message(Message.COMMAND_SETUP, Message.JOIN, new Object[]{playerName, customIcon, preferredColor1, preferredColor2}));
    }

    public void leaveGame() {
        if (_playerID >= 0) {
                _client.write(new Message(Message.COMMAND_SETUP, Message.LEAVE, _playerID));
                _playerID = -1;
                _controller.setID(_playerID);
        }
    }

    public void setCommandAccept(boolean accept) {
        if (_server != null) {
            _server.setAcceptCommand(accept);
        }
    }

    public void sendAddAICommand() {
         _client.write(new Message(Message.COMMAND_SERVER, Message.ADD_AI, _playerID));
    }

    public void sendRemoveAICommand() {
         _client.write(new Message(Message.COMMAND_SERVER, Message.REMOVE_AI, _playerID));
    }

    public void sendStartGameCommand() {
            if (_currentState.getPlayers().size() > 2) {
                _client.write(new Message(Message.COMMAND_SERVER, Message.START_GAME, _playerID));
            }
    }

    public void sendResetGameCommand() {
        _client.write(new Message(Message.COMMAND_SERVER, Message.RESET_GAME, _playerID));
    }

    public void sendCancelResetCommand() {
        _client.write(new Message(Message.COMMAND_SERVER, Message.CANCEL_RESET, _playerID));
    }
    
    public void sendFetchReplayCommand() {
        _client.write(new Message(Message.COMMAND_SERVER, Message.FETCH_REPLAY, _playerID));
    }
    
    public void sendToggleExtraGeneCommand() {
        _client.write(new Message(Message.COMMAND_SERVER, Message.TOGGLE_EXGENE, _playerID));
    }
    public void sendToggleSFSCommand() {
        _client.write(new Message(Message.COMMAND_SERVER, Message.TOGGLE_SFS, _playerID));
    }
    public void sendToggleHOLCommand() {
        _client.write(new Message(Message.COMMAND_SERVER, Message.TOGGLE_HOL, _playerID));
    }
    public void sendToggleBANGCommand() {
        _client.write(new Message(Message.COMMAND_SERVER, Message.TOGGLE_BANG, _playerID));
    }
    public void sendTogglePARCommand() {
        _client.write(new Message(Message.COMMAND_SERVER, Message.TOGGLE_PAR, _playerID));
    }

    /**
     * 
     */
    public boolean startGame() {
        return _server.startGame();
    }

    public void resetGame() {
        _server.resetGame();
    }

    private void disconnectedFromServer(IOException ioe) {
        UrsuppeApp.debug(ioe);
        SimpleAttributeSet attr = new SimpleAttributeSet();
        StyleConstants.setFontSize(attr, 14);
        StyleConstants.setForeground(attr, Color.red);
        _viewer.addSystemMessage(UrsuppeResource.getString("error.ConnectionClosed"), attr);
        _viewer.errorReport(UrsuppeResource.getString("error.ConnectionClosed"));
    }

    public GameState getCurrentGameState() {
        return _currentState;
    }


    private synchronized void messageHandling(IoSession session, Message msg) {
        if (msg.getMessageType() != Message.CHECKALIVE) UrsuppeApp.debug(_playerID+":"+msg.toString());
        switch (msg.getCommandType()) {
            case Message.COMMAND_SETUP:
                setupMessageHandling(session, msg);
                break;
            case Message.COMMAND_CHAT:
                chatMessageHandling(session, msg);
                break;
            case Message.COMMAND_GAME:
                gameMessageHandling(session, msg);
                break;
            case Message.COMMAND_ERROR:
                _viewer.errorReport((String)msg.getItems()[0]);
                break;
            case Message.COMMAND_SERVER:
                serverCommandHandling(session, msg);
                break;
            default:
                _viewer.errorReport(UrsuppeResource.getString("error.UnhandledCommandType") + ":" + msg.getCommandType());
                break;
        }
    }

    private void setupMessageHandling(IoSession session, Message msg) {
        switch (msg.getMessageType()) {
            case Message.NEWGAME:
                _controller.gameReset();
                IconContainer.clearCustomIconCache();
                IconContainer.clearExternalCustomIcons();
                _playerID = -1;
                _controller.setID(_playerID);
                break;
            //Check alive
            case Message.CHECKALIVE:
                break;
            //Connection ack
            case Message.CONNECT:
                _ackArrived = true;
                _ackResponse = (Boolean)msg.getItems()[0];
                break;
            //Join ack, player ID returned
            case Message.JOIN:
                _playerID = (Integer)msg.getItems()[0];
                _controller.setID(_playerID);
                break;
            case Message.CUSTOM_ICONS:
                //id, iconBase, numBase, scoreBase, BGbase
                int id = (Integer)msg.getItems()[0];
                CustomIconSet customIcons = (CustomIconSet)msg.getItems()[1];

                IconContainer.addCustomIcon(IconContainer.CUSTOM_ICON_PREFIX+id, customIcons.iconBase);
                IconContainer.addCustomNumberBase(IconContainer.CUSTOM_ICON_PREFIX+id, customIcons.numBase);
                IconContainer.addCustomScoreBase(IconContainer.CUSTOM_SCORE_ICON_NAME+id, customIcons.scoreBase);
                IconContainer.addCustomPlayerBackgroundBase(IconContainer.CUSTOM_BACKGROUND_PREFIX+id, customIcons.BGbase);
                break;
            case Message.OPTION_CHANGE_NOTIFICATION:
                GameOption newOpts = (GameOption)msg.getItems()[0];
                int modOpt = (Integer)msg.getItems()[1];
                _viewer.addSystemMessage(newOpts.toShortString(modOpt), null);
                break;
            default:
                _viewer.errorReport(UrsuppeResource.getString("error.UnhandledClientMessageType") + ":" + msg.getMessageType());
                break;
        }
    }

    private void chatMessageHandling(IoSession session, Message msg) {
        switch (msg.getMessageType()) {
            case Message.USER_MESSAGE:
                int colorID = (Integer)msg.getItems()[1];
                SimpleAttributeSet attr = new SimpleAttributeSet();
                if (colorID != -1) {
                    StyleConstants.setForeground(attr, UrsuppeColor.get(colorID).toColor(TYPE.CHAT));
                }
                _viewer.addUserMessage((String)msg.getItems()[0], attr);
                break;
            case Message.SERVER_MESSAGE:
                _viewer.addSystemMessage((String)msg.getItems()[0], (SimpleAttributeSet)msg.getItems()[1]);
                break;
            default:
                _viewer.errorReport(UrsuppeResource.getString("error.UnhandledClientMessageType") + ":" + msg.getMessageType());
                break;
        }

    }

    /**
     * Moved as static method so that these process are reused when playing replay files.
     *
     * @param viewer
     * @param controller
     * @param playerID
     * @param currentState
     * @param gm
     */
    public static void gameTextMessageHandling(UrsuppeViewer viewer, UrsuppeController controller, int playerID, GameState currentState, GameTextMessage gm) {
//            GameTextMessage gm = (GameTextMessage)msg.getItems()[0];
            //String message
            SimpleAttributeSet attr = new SimpleAttributeSet();
            StyleConstants.setFontSize(attr, 14);
            switch (gm.colorType) {
                case DEFAULT:
                    break;
                case PLAYER:
                    StyleConstants.setForeground(attr, UrsuppeColor.get(currentState.getPlayerByID(gm.colorID).getColorID()).toColor(TYPE.CHAT));
                    break;
                case EMPHASIZE:
                        StyleConstants.setBold(attr, true);
                        StyleConstants.setFontSize(attr, 18);
                    break;
            }
            if (gm.toString().length() > 0) {
                viewer.addSystemMessage(gm.toString(), attr);
            }
            //_viewer.addUserMessage(gm.toString(), attr);
            //Sound
            //gm.message.
            switch (gm.message) {
                case MOVE:
                case DRIFT:
                    viewer.soundEvent(SOUND_EVENT.MOVE);
                    break;
                case STARVE:
                case GENE_TOX_HIT:
                    viewer.soundEvent(SOUND_EVENT.DAMAGE);
                    break;
                case ADD_AMOEBA:
                    viewer.soundEvent(SOUND_EVENT.DIVISION);
                    break;
                case GENE_HEA1:
                case GENE_HEA2:
                    viewer.soundEvent(SOUND_EVENT.HEAL);
                    break;
                case GENE_SFS_SUCCESS:
                case GENE_SFS_SUCCESS_MOU:
                    viewer.soundEvent(SOUND_EVENT.SFS_DAMAGE);
                    break;
                case GENE_BANG:
                    viewer.soundEvent(SOUND_EVENT.BANG);
                    break;
                case GENE_TEN:
                    viewer.soundEvent(SOUND_EVENT.TEN);
                    break;
                case TURN_SIGNAL:
                    if (gm.colorID == playerID) viewer.soundEvent(SOUND_EVENT.TURN);
                    break;
                case END_PHASE_1:
                case END_GENE_PURCHASE:
                case END_CELL_DIVISION:
                    if (gm.colorID == playerID) viewer.soundEvent(SOUND_EVENT.TURN_END);
                    break;
                case DIE_ROLL:
                case DICE_ROLL:
                case DICE_DEF:
                    viewer.soundEvent(SOUND_EVENT.DICEROLL);
                    break;
                case GAME_END:
                    viewer.soundEvent(SOUND_EVENT.GAME_END);
                    break;
            }

            //Other effects
            relevantAmoebaHandling(viewer, controller, playerID, currentState, gm.amoebas);
            relevantGeneHandling(viewer, controller, playerID, currentState, gm.genes);
    }

    public static void gameEventMessageHandling(UrsuppeViewer viewer, GameEventMessage gem) {
                switch (gem.event) {
                    case MOVE:
                        viewer.moveEvent((Amoeba)gem.data[0], (Point)gem.data[1], (Integer)gem.data[2], (Point)gem.data[3], (Integer)gem.data[4]);
                        break;
                    case SCORE_STEP:
                        viewer.soundEvent(SOUND_EVENT.SCORE);
                        break;
                }
    }

    private void gameMessageHandling(IoSession session, Message msg) {
        switch (msg.getMessageType()) {
            case Message.ENTIRE_GAME_STATE:
                _currentState = (GameState)msg.getItems()[0];
                UrsuppeApp.debug(_playerID+":"+_currentState.getProgress().dumpProgress());
                _viewer.setGameState(_currentState);
                if (_controller != null && _playerID >= 0) {
                    askController(_currentState);
                }
                break;
            case Message.GAME_TEXT_MESSAGE:
                GameTextMessage gm = (GameTextMessage)msg.getItems()[0];
                gameTextMessageHandling(_viewer, _controller, _playerID, _currentState, gm);
                break;
            case Message.GAME_EVENT_MESSAGE:
                GameEventMessage gem = (GameEventMessage)msg.getItems()[0];
                gameEventMessageHandling(_viewer, gem);
                break;
            default:
                _viewer.errorReport(UrsuppeResource.getString("error.UnhandledClientMessageType") + ":" + msg.getMessageType());
                break;
        }

    }

    private void serverCommandHandling(IoSession session, Message msg) {
        
        if (msg.getMessageType() == Message.FETCHED_REPLAY) {
                Replay rep = (Replay)msg.getItems()[0];
                rep.save();
                _viewer.addSystemMessage("fetched: " + rep.replayName, null);
                return;
        }
        
        if (_server == null || !_server.isRunning()) return;

        switch (msg.getMessageType()) {
            case Message.START_GAME:
                startGame();
                break;
            case Message.RESET_GAME:
                _server.resetGame((String)msg.getItems()[0]);
                break;
            case Message.CANCEL_RESET:
                _server.cancelReset((String)msg.getItems()[0]);
                break;
            case Message.ADD_AI:
                addAI();
                break;
            case Message.REMOVE_AI:
                removeAI();
                break;
            case Message.TOGGLE_EXGENE:
            case Message.TOGGLE_SFS:
            case Message.TOGGLE_HOL:
            case Message.TOGGLE_BANG:
            case Message.TOGGLE_PAR:
                 _server.toggleGameOption(msg.getMessageType());
                break;
        }
    }

    private static void relevantAmoebaHandling(UrsuppeViewer viewer, UrsuppeController controller, int playerID, GameState currentState, RelevantAmoeba[] amoebas) {

    }

    private static void relevantGeneHandling(UrsuppeViewer viewer, UrsuppeController controller, int playerID, GameState currentState, RelevantGene[] genes) {
        if (genes == null) return;

        for (RelevantGene gene : genes) {
            switch (gene.gene) {
                case MIG:
                    if (playerID == gene.playerID) controller.showNextEnvironment(currentState.peekNextEnvironment(GENE.MIG));
                    viewer.geneUsed(gene.playerID, gene.gene);
                    break;
                case ENV:
                    if (playerID == gene.playerID) controller.showNextEnvironment(currentState.peekNextEnvironment(GENE.ENV));
                    viewer.geneUsed(gene.playerID, gene.gene);
                    break;
                default:
                    viewer.geneUsed(gene.playerID, gene.gene);
                    break;
            }
        }
    }

    public void addAI() {
        _server.addAI();
    }

    public void removeAI() {
        _server.removeAI();
    }

    public int getAIcount() {
        return _server.getAIcount();
    }

    private void askController(GameState game) {

        Progress progress = game.getProgress();
        
        if (!progress.started) return;

        //End game
        if (progress.ended) return;


        if (progress.currentOrder >= 0 && progress.currentOrder < game.getPlayers().size()) {
            Player currentPlayer =  game.getPlayerByOrder(progress.currentOrder);

            //Dice roll has more priority
            if (progress.diceState != DICE_STATE.NONE) {
                //Defense
                if (progress.diceState == DICE_STATE.DEF) {
                    if (_playerID == progress.defenderID) {
                        DEFrollAck();
                    }
                //Escape
                } else if (progress.diceState == DICE_STATE.ESC || progress.diceState == DICE_STATE.ESC_MV1 || progress.diceState == DICE_STATE.ESC_MV2) {
                    _controller.selectEscapeDirection(progress.defenderID, progress.defenderAmoebaNum, game.getMovableDirection(progress.diceState, progress.dice1, progress.dice2), game);
                } else {
                    _controller.moveResponse(currentPlayer.getID(), progress.currentAmoeba, game.getMovableDirection(progress.diceState, progress.dice1, progress.dice2), game);
                }
                return;
            }

            //HOLD has priority
            if (progress.HOLactive) {
                _controller.decideHOL(progress.HOLPlayerID, progress.HOLAmoebaIndex, progress.HOLdirection, game);
                return;
            }

            //ESC may have 2nd move
            if (progress.ESCSPDdecide) {
                _controller.decideESCSPD(progress.defenderID, game);
                return;
            }

            //ESC can finish after HOLD
            if (progress.ESCfinished) {
                if (progress.defenderID == _playerID) ESCAck();
                return;
            }

            //Defense sequence
            //Defender
            if (progress.defenderActive) {
                _controller.defenderAction(progress.attackerID, progress.attackerAmoebaNum, progress.defenderID, progress.defenderAmoebaNum, game);
                return;
            }

            //BANG
            if (progress.defenderBANG) {
                _controller.decideSFSBANG(progress.defenderID, progress.defenderAmoebaNum, game);
                return;
            }
            
            //Attacker
            if (progress.attackerHARD || progress.attackerMOUexcretes) {
                _controller.attackerAction(progress.attackerID, progress.attackerAmoebaNum, progress.defenderID, progress.defenderAmoebaNum, progress.attackerHARD, progress.attackerMOUexcretes, game);
                return;
            }

            //POP decision needs to be made
            if (progress.POPdecide) {
                _controller.decidePOP(currentPlayer.getID(), game);
                return;
            }

            //AGR decision needs to be made
            if (progress.AGRactive) {
                _controller.decideAGR(currentPlayer.getID(), game);
                return;
            }

            //Initial selection
            switch (progress.currentPhase) {
                case Progress.PHASE_INITIAL_SCORE:
                    _controller.selectInitialScore(currentPlayer.getID(), game);
                    break;
                case Progress.PHASE_FIRST_AMOEBA:
                case Progress.PHASE_SECOND_AMOEBA:
                    _controller.selectInitialAmoeba(currentPlayer.getID(), game);
                    break;
                case Progress.PHASE_1_MOVE_AND_FEED:
                    if (_playerID == currentPlayer.getID()) {
                        List<PHASE1_ACTION> actions = game.getAvailablePhase1Actions(currentPlayer.getID(), progress.currentAmoeba);

                        //auto starve
                        if (actions.size() == 1 && actions.contains(PHASE1_ACTION.STARVE)) {
                            _controller.selectPhase1Action(-1, progress.currentAmoeba, game);
                            starveSelected();
                            return;
                        //auto eat
                        } else if (actions.size() == 1 && actions.contains(PHASE1_ACTION.EAT) && !currentPlayer.hasGene(GENE.CLE) && !currentPlayer.hasGene(GENE.TOX)) {
                            _controller.selectPhase1Action(-1, progress.currentAmoeba, game);
                            eatSelected(null, false);
                            return;
                        } 
                    }
                     _controller.selectPhase1Action(currentPlayer.getID(), progress.currentAmoeba, game);
                    break;
                case Progress.PHASE_2_OZONE_CHANGE:
                    //
                    boolean noDefect = game.getCurrentEnvironment().maxMP >= currentPlayer.getMP();
                    if (currentPlayer.getID() == _playerID) {
                        //If no gene defect, don't bother the controller
                        if (noDefect) {
                            _controller.handleGeneDefect(-1, game);
                            geneDefectsHandled(null);
                            return;
                        }
                    }
                    if (noDefect) {
                        _controller.handleGeneDefect(-1, game);
                    } else {
                        _controller.handleGeneDefect(currentPlayer.getID(), game);
                    }
                    break;
                case Progress.PHASE_3_NEW_GENES:
                    //check if there is any money left
                    if (_playerID == currentPlayer.getID()) {
                        Map<GENE, Boolean> map = game.getPurchasableGenes(currentPlayer);
                        //check if some thing could be bought
                        boolean purchasable = false;
                        for (boolean b : map.values()) purchasable |= b;
                        //Has something to purchase or ADA
                        if (!purchasable && !currentPlayer.hasGene(GENE.ADA)) {
                            _controller.selectPhase3Action(-1, game);
                            endGenePurchase();
                            return;
                        }
                    }
                    _controller.selectPhase3Action(currentPlayer.getID(), game);
                    break;
                case Progress.PHASE_4_DIVISION:
                    if (currentPlayer.getID() == _playerID) {
                        boolean division = game.getDivisionCost(currentPlayer) <= currentPlayer.getBP() && currentPlayer.getNumAmoebas() < game.maximumAmoebaCount();
                        boolean damagedAmoeba = false;
                        for (Amoeba amoeba : currentPlayer.getAmoebas()) {
                            if (amoeba.getDP() > 0) {
                                damagedAmoeba = true;
                                break;
                            }
                        }
                        boolean heal = (currentPlayer.hasGene(GENE.HEA1) || currentPlayer.hasGene(GENE.HEA2) ) && currentPlayer.getBP() >= 3 && damagedAmoeba;
                        if (heal || division) {
                            _controller.selectPhase4Action(currentPlayer.getID(), game);
                        } else {
                            endCellDivision();
                        }
                    } else {
                        _controller.selectPhase4Action(currentPlayer.getID(), game);
                    }
                    break;
                case Progress.PHASE_5_DEATHS:
                    _controller.death(currentPlayer.getID(), progress.currentAmoeba, game);
                    break;
                case Progress.PHASE_6_SCORING:
                    _controller.scoring(currentPlayer.getID(), game);
                    scoreAck(progress.ackedScore);
                    break;
            }
        }
    }


    public void initialScoreSelected(int score) {
            _client.write(new Message(Message.COMMAND_GAME, Message.INITIAL_SCORE, new Object[]{_playerID, score}));
    }

    public void initialAmoebaSelected(int num, Point loc) {
            _client.write(new Message(Message.COMMAND_GAME, Message.INITIAL_AMOEBA, new Object[]{_playerID, num, loc}));
    }

    
    public void amoebaDriftSelected(int[] TENfoods, boolean useREB) {
            if (TENfoods == null) TENfoods = autoTENdrift(useREB);
            _client.write(new Message(Message.COMMAND_GAME, Message.DRIFT, new Object[]{_playerID, TENfoods, useREB}));
    }

    public void amoebaMoveSelected() {
            _client.write(new Message(Message.COMMAND_GAME, Message.MOVE_REQ, _playerID));
    }

    public void amoebaMoveSelected(DIRECTION dir, int[] TENfoods, boolean useREB) {
            if (TENfoods == null) TENfoods = autoTEN(dir, useREB);
            _client.write(new Message(Message.COMMAND_GAME, Message.MOVE, new Object[]{_playerID, dir, TENfoods, useREB}));
    }

    public void amoebaMoveSelected(Point newLoc, int[] TENfoods) {
        
        Progress progress = _currentState.getProgress();
        Player player = _currentState.getPlayerByID(_playerID);
        Amoeba amoeba = player.getAmoebas().get(progress.currentAmoeba);

        DIRECTION dir = _currentState.getDirectionToReachNewLocation(amoeba.getLocation(), newLoc);
        List<DIRECTION> dirs = _currentState.getMovableDirection(progress.diceState, progress.dice1, progress.dice2);

        //When the direction given is in the available list
        if (dirs.contains(dir)) {
            amoebaMoveSelected(dir, TENfoods, false);
            return;
        //If the movement is to not move, it may be hitting the wall
        } else if (dir == DIRECTION.NONE) {
            List<DIRECTION> wallDirs = _currentState.getDirectionsToHitWall(amoeba.getLocation());
            for (DIRECTION d : wallDirs) {
                if (dirs.contains(d)) {
                    amoebaMoveSelected(d, TENfoods, false);
                    return;
                }
            }

        //If not and player has REB gene
        } else if (player.hasGene(GENE.REB)) {
            //get list of directions to hit wall
            List<DIRECTION> wallDirs = _currentState.getDirectionsToHitWall(amoeba.getLocation());
            //check if the opposite direction is what you wan
            for (DIRECTION d : wallDirs) {
                if (_currentState.getOppositeDirection(d) == dir) {
                    amoebaMoveSelected(d, TENfoods, true);
                    return;
                }
            }
        }

        _viewer.errorReport("Unexpected Error at amoebaMove");
        
    }

    public void eatSelected(Point exLoc, boolean tox) {
            _client.write(new Message(Message.COMMAND_GAME, Message.EAT, new Object[]{_playerID, exLoc, tox}));
    }

    public void eatSelected(int[] foods, int[] parPlayerIDs, Point sucLoc, int[] sucFoods, Point exLoc, boolean tox) {
            _client.write(new Message(Message.COMMAND_GAME, Message.EAT_WITH_GENES, new Object[]{_playerID, foods, parPlayerIDs, sucLoc, sucFoods, exLoc, tox}));
      
    }

    public void autoEat(Player player, Amoeba amoeba, Point sucLoc, int[] sucFoods, Point exLoc, boolean tox) {
        List<int[]> eatingCombinations = _currentState.getSubOptimalEatingCombinations(player, amoeba, sucLoc, sucFoods);
        List<int[]> parCombinations = _currentState.getPossiblePARcombinations(player, amoeba.getLocation());

        //Exceptional case
        if (eatingCombinations.isEmpty() || parCombinations.isEmpty()) {
            starveSelected();
        //Else pick randomly
        } else {
            Collections.shuffle(eatingCombinations);
            Collections.shuffle(parCombinations);
            eatSelected(eatingCombinations.get(0), parCombinations.get(0), sucLoc, sucFoods, exLoc, tox);
        }
    }

    private int[] autoTENHold() {
        Progress progress = _currentState.getProgress();
        Player player = _currentState.getPlayerByID(progress.HOLPlayerID);
        Point src = progress.HOLlocation;
        Point dst = _currentState.getDestiation(src, progress.HOLdirection);
        
        return autoTEN(player, _currentState.getSoup().getCell(src).getFood(), _currentState.getSoup().getCell(dst).getFood());
    }

    private int[] autoTENdrift(boolean useREB) {
        Player player = _currentState.getCurrentPlayer();
        Point src = _currentState.getCurrentAmoeba().getLocation();
        DIRECTION dir = _currentState.getCurrentEnvironment().direction;
        if (useREB) dir = _currentState.getOppositeDirection(dir);
        Point dst = _currentState.getDestiation(src, dir);

        return autoTEN(player, _currentState.getSoup().getCell(src).getFood(), _currentState.getSoup().getCell(dst).getFood());
    }

    private int[] autoTEN(DIRECTION dir, boolean useREB) {
        Player player = _currentState.getCurrentPlayer();
        Point src = _currentState.getCurrentAmoeba().getLocation();
        if (useREB) dir = _currentState.getOppositeDirection(dir);
        Point dst = _currentState.getDestiation(src, dir);

        return autoTEN(player, _currentState.getSoup().getCell(src).getFood(), _currentState.getSoup().getCell(dst).getFood());
    }

    private int[] autoTEN(Player player, int[] srcFood, int[] dstFood) {
        int[] foodColors = _currentState.getSoup().getFoodColors();
        int[] tenfood = new int[foodColors.length];

        if (!player.hasGene(GENE.TEN)) return tenfood;

        int ownColor = 0;
        for (int i = 0; i < foodColors.length; i++) {
            if (foodColors[i] == (player.getColorID())) {
                ownColor = i;
                break;
            }
        }

        int maxTotalOtherColor = 0;
        int maxOther = 0;
        for (int i = 0; i < srcFood.length; i++) {
            //skip own
            if (i == ownColor) continue;
            maxTotalOtherColor += srcFood[i];
            maxOther = Math.max(maxOther, srcFood[i]);
        }

        List<Integer> selected = new LinkedList<>();
        int TENnum = Math.min(maxTotalOtherColor, 3);

        while (TENnum > 0) {
            //Find out the colors that has minimal availability
            int minAvail = 999;
            List<Integer> valid = new LinkedList<>();
            for (int i = 0; i < dstFood.length; i++) {
                //skip own
                if (i == ownColor) continue;
                //skip if you can't get any more
                int count = 0;
                for (int c : selected) if (c==i) count++;
                if (srcFood[i] <= count) continue;
                //take the min
                valid.add(i);
                minAvail = Math.min(minAvail, dstFood[i]+count);
            }
            //find out the colors with minimum availability
            List<Integer> candidate = new LinkedList<>();
            for (int i = 0; i < dstFood.length; i++) {
                if (!valid.contains(i)) continue;
                //count number of foods already selected for this color
                int count = 0;
                for (int c : selected) if (c==i) count++;
                //see fi the sum matches
                if (minAvail == dstFood[i]+count) candidate.add(i);
            }
            if (candidate.isEmpty()) break;
            Collections.shuffle(candidate);
            selected.add(candidate.get(0));
            TENnum--;
        }


        for (int sel : selected) tenfood[sel]++;

        return tenfood;
    }

    public void starveSelected() {
            _client.write(new Message(Message.COMMAND_GAME, Message.STARVE, _playerID));
    }

    public void cancelSPDSelected() {
            _client.write(new Message(Message.COMMAND_GAME, Message.CANCEL_SPEED, _playerID));
    }

    public void HOLSelected() {
            _client.write(new Message(Message.COMMAND_GAME, Message.HOLD_STAY, _playerID));
    }

    public void HOLSelected(boolean hold, int[] TENfoods) {
            if (TENfoods == null && hold) TENfoods = autoTENHold();
            _client.write(new Message(Message.COMMAND_GAME, Message.HOLD,  new Object[]{_playerID, hold, TENfoods}));
    }

    public void geneDefectsHandled(GENE[] discards) {
            _client.write(new Message(Message.COMMAND_GAME, Message.GENE_DEFECT, new Object[]{_playerID, discards}));
    }

    public void genePurchase(GENE gene) {
            _client.write(new Message(Message.COMMAND_GAME, Message.GENE_PURCHASE, new Object[]{_playerID, gene}));
    }

    public void ADASelected(GENE exchange, GENE newGene) {
            _client.write(new Message(Message.COMMAND_GAME, Message.ADA, new Object[]{_playerID, exchange, newGene}));
    }

    public void endGenePurchase() {
            _client.write(new Message(Message.COMMAND_GAME, Message.END_GENE_PURCHASE, _playerID));
    }

    public void cellDivision(int num, Point loc) {
            _client.write(new Message(Message.COMMAND_GAME, Message.CELL_DIVISION, new Object[]{_playerID, num, loc}));
        
    }

    public void endCellDivision() {
            _client.write(new Message(Message.COMMAND_GAME, Message.END_CELL_DIVISION, _playerID));
    }

    public void POPdecided(boolean usePOP) {
            _client.write(new Message(Message.COMMAND_GAME, Message.POP_DECISION,  new Object[]{_playerID, usePOP}));
    }

    public void HEALselected(Amoeba heal, Amoeba transfer) {
            _client.write(new Message(Message.COMMAND_GAME, Message.HEAL,  new Object[]{_playerID, heal, transfer}));
    }

    public void deathAck(boolean useBANG) {
            _client.write(new Message(Message.COMMAND_GAME, Message.DEATH_ACK,  new Object[]{_playerID, useBANG}));
    }

    private void scoreAck(int score) {
            _client.write(new Message(Message.COMMAND_GAME, Message.SCORE_ACK, new Object[]{_playerID, score}));
    }


    public void SFSSelected(int targetPlayerID, int targetAmoebaNum) {
            _client.write(new Message(Message.COMMAND_GAME, Message.SFS_ATTACK, new Object[]{_playerID, targetPlayerID, targetAmoebaNum}));
    }
    
    public void AGRCanceled() {
            _client.write(new Message(Message.COMMAND_GAME, Message.AGR_CANCEL, _playerID));
    }

    public void AGRSelected(int attackingAmoebaNum, int targetPlayerID, int targetAmoebaNum) {
            _client.write(new Message(Message.COMMAND_GAME, Message.AGR_ATTACK, new Object[]{_playerID, attackingAmoebaNum, targetPlayerID, targetAmoebaNum}));
       
    }

    public void defenderGiveup() {
            _client.write(new Message(Message.COMMAND_GAME, Message.DEFENDER_GIVEUP, _playerID));
    }

    public void attackerActionSelected(boolean payHC,Point exLoc,  boolean useTOX) {
            _client.write(new Message(Message.COMMAND_GAME, Message.ATTACKER_ACTION, new Object[]{_playerID, payHC, exLoc, useTOX}));
    }

    public void DEFSelected() {
            _client.write(new Message(Message.COMMAND_GAME, Message.DEF_USE, _playerID));
    }

    public void DEFrollAck() {
            _client.write(new Message(Message.COMMAND_GAME, Message.DEF_ACK, _playerID));
    }

    public void ESCSelected() {
            _client.write(new Message(Message.COMMAND_GAME, Message.ESC_USE, _playerID));
    }

    public void ESCDirectionSelected(DIRECTION dir, int[] TENfoods, boolean useREB) {
            if (TENfoods == null) TENfoods = autoTEN(dir, useREB);
            _client.write(new Message(Message.COMMAND_GAME, Message.ESCAPE, new Object[]{_playerID, dir, TENfoods, useREB}));
    }


    public void ESCDirectionSelected(Point newLoc, int[] TENfoods) {

        Progress progress = _currentState.getProgress();
        Player defender = _currentState.getPlayerByID(progress.defenderID);
        Amoeba defenderAmoeba = defender.getAmoebaByNumber(progress.defenderAmoebaNum);

        DIRECTION dir = _currentState.getDirectionToReachNewLocation(defenderAmoeba.getLocation(), newLoc);
        List<DIRECTION> dirs = _currentState.getMovableDirection(progress.diceState, progress.dice1, progress.dice2);

        //When the direction given is in the available list
        if (dirs.contains(dir)) {
            ESCDirectionSelected(dir, TENfoods, false);
            return;
        //If the movement is to not move, it may be hitting the wall
        } else if (dir == DIRECTION.NONE) {
            List<DIRECTION> wallDirs = _currentState.getDirectionsToHitWall(defenderAmoeba.getLocation());
            for (DIRECTION d : wallDirs) {
                if (dirs.contains(d)) {
                    ESCDirectionSelected(d, TENfoods, false);
                    return;
                }
            }

        //If not and player has REB gene
        } else if (defender.hasGene(GENE.REB)) {
            //get list of directions to hit wall
            List<DIRECTION> wallDirs = _currentState.getDirectionsToHitWall(defenderAmoeba.getLocation());
            //check if the opposite direction is what you wan
            for (DIRECTION d : wallDirs) {
                if (_currentState.getOppositeDirection(d) == dir) {
                    ESCDirectionSelected(d, TENfoods, true);
                    return;
                }
            }
        }

        _viewer.errorReport( "Unexpected Error at amoebaMove");

    }

    public void ESCAck() {
        _client.write(new Message(Message.COMMAND_GAME, Message.ESC_ACK, _playerID));
    }

    public void SFSAck(boolean useBANG) {
        _client.write(new Message(Message.COMMAND_GAME, Message.SFS_ACK, new Object[]{_playerID, useBANG}));
    }
}
