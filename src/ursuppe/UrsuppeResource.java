package ursuppe;

/**
 * Class for accessing resources that define text for GUI. 
 * 
 * @author tyuki
 */
public class UrsuppeResource {

    private static final org.jdesktop.application.ResourceMap RESOURCE_MAP;

    static {
        RESOURCE_MAP = org.jdesktop.application.Application.getInstance(ursuppe.UrsuppeApp.class).getContext().getResourceMap(UrsuppeApp.class);
    }

    public static String getString(String key) {
        return RESOURCE_MAP.getString(key);
    }
}
