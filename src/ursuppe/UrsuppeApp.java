/*
 * UrsuppeApp.java
 */

package ursuppe;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import javax.swing.JOptionPane;
import org.apache.mina.core.write.WriteToClosedSessionException;
import ursuppe.control.Organizer;
import org.jdesktop.application.Application;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.SingleFrameApplication;
import sound.SoundContainer;
import ursuppe.gui.icon.IconContainer;
import ursuppe.gui.icon.SoupBackgroundContainer;

/**
 * The main class of the application.
 */
public class UrsuppeApp extends SingleFrameApplication {

    private static PrintStream _debugOut = null;
    public static final boolean DEBUG = false;
    public static final boolean DUMP = false;
    public static final boolean TRACE = false;

    protected FrameView view;

    /**
     * At startup create and show the main frame of the application.
     */
    @Override protected void startup() {
        show(view);
        if (view instanceof UrsuppeServerView) {
            ((UrsuppeServerView)view).start();
        }
    }

    @Override
    public void initialize(String[] argv) {
        UrsuppeApp.loadResources();
        Organizer organizer = new Organizer();

        if (System.getProperty("java.version").matches("^1\\.[12345].+")) {
            JOptionPane.showMessageDialog(null, "Java version is < 1.6");
            return;
        }

        if (argv.length > 0 && argv[0].contentEquals("-small")) {
            UrsuppeApp.debug(argv[0]);
            view = new UrsuppeSmallView(this, organizer);
        } else if (argv.length > 0 && argv[0].contentEquals("-local")) {
            view = new UrsuppeLocalView(this, organizer);
        } else if (argv.length > 0 && argv[0].contentEquals("-server")) {
            view = new UrsuppeServerView(this, organizer);
        } else if (argv.length > 0 && argv[0].contentEquals("-normal")) {
            view = new UrsuppeServerView(this, organizer);
        } else {
            view = new UrsuppeView(this, organizer);
        }
    }

    public static void loadResources() {
        //images
        IconContainer.loadDirectory("./images/", IconContainer.IMAGE_TYPE.OTHER);
        IconContainer.loadDirectory("./images/gene/", IconContainer.IMAGE_TYPE.GENE);
        IconContainer.loadDirectory("./images/control/", IconContainer.IMAGE_TYPE.CONTROL);
        IconContainer.loadDirectory("./images/environment", IconContainer.IMAGE_TYPE.ENVIRONMENT);
        IconContainer.loadEffects("./images/effect");
        //IconContainer.loadDirectoryOfColoredIconBase("./images/amoeba/");
        IconContainer.loadDirectory("./images/amoeba", IconContainer.IMAGE_TYPE.OTHER);
        IconContainer.loadCustomIcons("./images/amoeba/");
        IconContainer.loadCustomPlayerBackground("./images/player");

        SoupBackgroundContainer.loadDirectory("./images/soup/");
        
        SoundContainer.loadSounds();

        //Properties
        PropertyContainer.registerProperties(PropertyContainer.HISTORY_FILE);
    }

    /**
     * This method is to initialize the specified window by injecting resources.
     * Windows shown in our application come fully initialized from the GUI
     * builder, so this additional configuration is not needed.
     */
    @Override protected void configureWindow(java.awt.Window root) {
    }

    /**
     * A convenient static getter for the application instance.
     * @return the instance of UrsuppeApp
     */
    public static UrsuppeApp getApplication() {
        return Application.getInstance(UrsuppeApp.class);
    }

    /**
     * Main method launching the application.
     */
    public static void main(String[] args) {
        launch(UrsuppeApp.class, args);
    }

    public static void debug(Throwable e) {
        if (DEBUG) {
            Throwable tracker = new Throwable();
            tracker.fillInStackTrace();
            //commonly occuring exception, use short hand
            if (e instanceof WriteToClosedSessionException) {
                System.err.println("WriteToClosedSessionException");
            } else {
                e.printStackTrace();
            }
            System.err.println( "reported at:" );
            tracker.printStackTrace();
            System.err.flush();
        }
    }

    public static void dump(String str) {
        if (DUMP) {
            try {
                File dump = new File("./dump" + System.currentTimeMillis());
                Throwable tracker = new Throwable();
                tracker.fillInStackTrace();
                PrintWriter writer = new PrintWriter(dump);
                writer.print(str);
                writer.print("reported at:");
                tracker.printStackTrace(writer);
                writer.close();
            } catch (Exception ex) {}
        }
    }

    public static void dump(Throwable e) {
        if (DUMP) {
            try {
                File dump = new File("./dump" + System.currentTimeMillis());
                Throwable tracker = new Throwable();
                tracker.fillInStackTrace();
                PrintWriter writer = new PrintWriter(dump);
                e.printStackTrace(writer);
                writer.print("reported at:");
                tracker.printStackTrace(writer);
                writer.close();
            } catch (Exception ex) {}
        }
    }

    @Override
    public void quit(ActionEvent e) {
        exit();
    }

    public static void debug(String out) {
        if (!DEBUG) return;
        try {
            if (_debugOut == null)  _debugOut = new PrintStream(new FileOutputStream(new File("debug.out")));
            _debugOut.println(out);
            _debugOut.flush();
        } catch (FileNotFoundException ex) {
            dump(ex);
        } catch (Exception e) {
            dump(e);
        }
    }


    public static void trace(String out) {
        if (!TRACE) return;
        try {
            if (_debugOut == null)  _debugOut = new PrintStream(new FileOutputStream(new File("debug.out")));
            _debugOut.println(out);
            _debugOut.flush();
        } catch (FileNotFoundException ex) {
            dump(ex);
        } catch (Exception e) {
            dump(e);
        }
    }
}
