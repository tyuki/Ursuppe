package ursuppe;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

/**
 * Class with static methods for accessing properties.
 * 
 * Properties are used to store past choices by the player
 * such as the ip, port to use.
 * 
 * @author tyuki
 */
public class PropertyContainer {

    public static final String HISTORY_FILE = "history.p";

    protected static final Map<String, Properties> PROPERTIES = new TreeMap<>();

    public static String getProperty(String filename, String propertyName) {
        if (PROPERTIES.containsKey(filename)) {
            return org.apache.commons.lang.StringEscapeUtils.unescapeJava(PROPERTIES.get(filename).getProperty(propertyName));
        }

        return null;
    }

    public static void setProperty(String filename, String propertyName, String value) {
        if (!PROPERTIES.containsKey(filename)) {
            PROPERTIES.put(filename, new Properties());
        }

        PROPERTIES.get(filename).setProperty(propertyName, org.apache.commons.lang.StringEscapeUtils.escapeJava(value));
    }

    public static void removeProperty(String filename, String propertyName) {
        if (PROPERTIES.containsKey(filename)) {
           PROPERTIES.get(filename).remove(propertyName);
        }
    }
    
    public static Properties loadProperties(String filename) {
        Properties prop = new Properties();

        try {
          prop.load(new FileInputStream(filename));
        } catch (IOException e) {
          return null;
        }
        
        return prop;
    }

    public static void registerProperties(String filename) {
        Properties prop = loadProperties(filename);

        if (prop != null) {
            PROPERTIES.put(filename, prop);
        }
//        return properties.get(filename);
    }
    

    public static void saveProperties() {
        for (String filename : PROPERTIES.keySet()) {
            try {
                PROPERTIES.get(filename).store(new BufferedWriter(new FileWriter(new File(filename))), null);
            } catch (IOException ioe) {
                
            }
        }
    }


}
