package network;

import java.net.InetSocketAddress;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoConnector;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.serialization.ObjectSerializationCodecFactory;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

/**
 * Client using Apache MINA
 * https://mina.apache.org/mina-project/
 *
 * @author tyuki
 */
public class MinaClient implements IClient {
    protected final IoHandlerAdapter handler;
    protected final String hostname;
    protected final int port;
    protected IoConnector connector;
    protected ConnectFuture connectFuture;
    protected IoSession session;

    public MinaClient(IoHandlerAdapter handler, String hostname, int port) {
        this.handler = handler;
        this.port = port;
        
        if (hostname.trim().matches("^localhost$")) {
            this.hostname = "127.0.0.1";
        } else {
            this.hostname = hostname;
        }


    }

    @Override
    public boolean connect() {
        connector = new NioSocketConnector();

        ObjectSerializationCodecFactory factory = new ObjectSerializationCodecFactory();
        factory.setDecoderMaxObjectSize(Integer.MAX_VALUE);
        factory.setEncoderMaxObjectSize(Integer.MAX_VALUE);
        
//        connector.getFilterChain().addLast( "logger", new LoggingFilter() );
        connector.getFilterChain().addLast("codec", new ProtocolCodecFilter(factory));

        connector.setHandler(handler);

        connector.setConnectTimeoutMillis(2000);
        connector.getSessionConfig().setWriteTimeout(60);
        connector.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, 10);

        InetSocketAddress sa = new InetSocketAddress(hostname, port);
        connectFuture = connector.connect(sa);
        connectFuture.awaitUninterruptibly();
        session = connectFuture.getSession();

        return true;
    }

    @Override
    public void disconnect() {
        if (session != null) {
            session.close(true);
        }
    }

    @Override
    public boolean isConnected() {
        if (session != null)
            return session.isConnected();

        return false;
    }

    @Override
    public synchronized void write(final Message msg) {
        if (session != null && session.isConnected() && !session.isClosing()) {
            session.write(msg);
        }
    }
}
