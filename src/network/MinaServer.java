/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package network;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.service.IoHandler;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.serialization.ObjectSerializationCodecFactory;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

/**
 * Sever using Apache MINA
 * https://mina.apache.org/mina-project/
 * 
 * @author tyuki
 */
public class MinaServer implements IServer
{
   private final int port;
   private final IoHandler handler;
   private  IoAcceptor acceptor;

    public MinaServer(IoHandler handler, int port) {
        this.handler = handler;
        this.port = port;
    }


    @Override
    public boolean start() throws IOException {
        try {
            acceptor = new NioSocketAcceptor();

            ObjectSerializationCodecFactory factory = new ObjectSerializationCodecFactory();
            factory.setDecoderMaxObjectSize(Integer.MAX_VALUE);
            factory.setEncoderMaxObjectSize(Integer.MAX_VALUE);
            
//            acceptor.getFilterChain().addLast("logger", new LoggingFilter());
            acceptor.getFilterChain().addLast("codec", new ProtocolCodecFilter(factory));
            acceptor.setHandler(handler);
            
            acceptor.getSessionConfig().setWriteTimeout(60);
            acceptor.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, 10);
            acceptor.bind(new InetSocketAddress(port));
            return true;
        } catch (IOException ex) {
            throw ex;
        }
    }

    @Override
    public void stop() {
        acceptor.unbind();
        acceptor.dispose();
    }

    public boolean isActive() {
        if (acceptor != null)
            return acceptor.isActive();

        return false;
    }

    public Map<Long, IoSession> getSessions() {
        if (isActive()) {
            return acceptor.getManagedSessions();
        } else {
            return new HashMap<>();
        }
    }
    
    public void sendMessage(final IoSession session, final Message msg) {
        synchronized (session) {
            if (session != null && session.isConnected() && !session.isClosing()) {
                session.write(msg);
            }
        }
    }
}
