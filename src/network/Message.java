package network;

import java.io.Serializable;
import java.util.Collection;

public class Message implements Serializable {
    
    public static final long serialVersionUID = 5481857335L;

    public static final int COMMAND_SETUP = 1;
    public static final int COMMAND_CHAT = 2;
    public static final int COMMAND_GAME = 3;
    public static final int COMMAND_ERROR = 4;
    public static final int COMMAND_SERVER = 5;


    public static final int START_GAME = 5000;
    public static final int RESET_GAME = 5001;
    public static final int CANCEL_RESET = 5002;
    public static final int ADD_AI = 5010;
    public static final int REMOVE_AI = 5011;
    public static final int FETCH_REPLAY = 5020;
    public static final int FETCHED_REPLAY = 5021;
    
    public static final int TOGGLE_EXGENE = 5100;
    public static final int TOGGLE_SFS = 5110;
    public static final int TOGGLE_HOL = 5120;
    public static final int TOGGLE_BANG = 5130;
    public static final int TOGGLE_PAR = 5140;


    public static final int NEWGAME = 999;

    public static final int CONNECT = 100;
    public static final int DISCONNECT = 101;
    public static final int JOIN = 110;
    public static final int LEAVE = 111;
    public static final int CHECKALIVE = 120;
    public static final int CUSTOM_ICONS = 130;
    public static final int OPTION_CHANGE_NOTIFICATION = 140;

    public static final int SERVER_MESSAGE = 200;
    public static final int GAME_TEXT_MESSAGE = 210;
    public static final int GAME_EVENT_MESSAGE = 211;
    public static final int USER_MESSAGE = 220;

    public static final int ENTIRE_GAME_STATE = 399;

    public static final int INITIAL_SCORE = 300;
    public static final int INITIAL_AMOEBA = 301;
    
    public static final int DRIFT = 310;
    public static final int MOVE_REQ = 311;
    public static final int MOVE = 312;
    public static final int STARVE = 313;
    public static final int EAT = 314;
    public static final int EAT_WITH_GENES = 315;
    public static final int HOLD_STAY = 316;
    public static final int HOLD = 317;
    public static final int CANCEL_SPEED = 319;

    public static final int GENE_DEFECT = 320;

    public static final int GENE_PURCHASE = 330;
    public static final int ADA = 331;
    public static final int END_GENE_PURCHASE = 321;

    public static final int CELL_DIVISION = 340;
    public static final int END_CELL_DIVISION = 341;
    public static final int POP_DECISION = 342;
    public static final int HEAL = 343;

    public static final int SFS_ATTACK = 500;
    public static final int AGR_ATTACK = 510;
    public static final int AGR_CANCEL = 511;
    public static final int DEFENDER_GIVEUP = 520;
    public static final int ATTACKER_ACTION = 530;
    public static final int DEF_USE = 540;
    public static final int DEF_ACK = 541;
    public static final int ESC_USE = 550;
    public static final int ESCAPE = 551;
    public static final int ESC_ACK = 552;
    public static final int SFS_ACK = 560;
    
    public static final int DEATH_ACK = 350;
    
    public static final int SCORE_ACK = 360;

    public static final int ERROR_DIALOG = 400;

    //Typet
    private final int _commandType; //Connection Type
    private final int _messageType; //Message Type
    private long _time;
    private Object[] _items;
    

    public Message (int type, int type2, Object... items) {
            _commandType = type;
            _messageType = type2;
            _items = items;
            _time = System.currentTimeMillis();
    }

    public Message (int type, int type2, Collection<Object> collection) {
            _commandType = type;
            _messageType = type2;
            _items = collection.toArray();
            _time = System.currentTimeMillis();
    }

    public int getCommandType() {
            return _commandType;
    }

    public int getMessageType() {
            return _messageType;
    }

    public Object[] getItems() {
            return _items;
    }

    public int getLength() {
            return _items.length;
    }

    public long getTime() {
            return _time;
    }

    public void setTime(long time) {
            _time = time;
    }

    public void setItems(Object[] items) {
            _items = items;
    }

    public void setItems(Collection<Object> items) {
            _items = items.toArray();
    }

    @Override
    public String toString() {
            String items = "";
            for (Object item : _items) {
                    items += item + " ";
            }
            return "Type: " + _commandType + ":" + _messageType + " items  " + items;
    }
}