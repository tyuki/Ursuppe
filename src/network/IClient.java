package network;

public interface IClient {

    boolean connect();

    void disconnect();

    boolean isConnected();

    void write(Message msg);

}
