package network;

import java.io.IOException;

/**
 *
 * @author Personal
 */
public interface IServer {

    boolean start() throws IOException;

    void stop();

}
