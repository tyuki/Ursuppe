# Ursuppe - Primordial Soup

Ursuppe is a board game designed by Doris Matthaus & Frank Nestel, where the main theme is amoebas floating around in the primordial soup, acquiring new abilities (genes) to survive. It is still the favorite game of mine, and I did a java implementation of the game.

[Original Game](http://doris-frank.de/GamesUrsuppe.html)

### Main Features
The Java implementation is based on the original game plus its expansion. Its features are:
* Support for 3-6 player games.
* Support for all 46 genese. 
* There is a reasonably good AI to add more players to the game.
* Visuals of amoebas, player panels, and game boards are customizable. 
* Sounds are customizable as well.

### Requirements
Java Runtime Environment 1.8 or higher

### Install/Build
Pre-built distribution: simply double click on Ursuppe.jar to start the game.

Latest: https://gitlab.com/tyuki/Ursuppe/tags/v2.02

You need to run the jar with an extra argument to start the app in different modes. Shortcuts are provided for Windows users; Mac/Linux users can run them from the command line, e.g., "java -jar Ursuppe.jar -server" starts the app in server mode.

If you want to build yourself, you should be able to open the top-level directory as a project in Net Beans. In fact, NetBeans IDE discontinued the GUI development environment that was used to make this app. You will have to use NetBeans 7 to edit the GUI. 

### How to Play

The intended audience is those who are already familiar with the game. Please refer to other sources for rules of the game.

#### Interface

* Once the game has started, go to the top-left menu and select 'Connect to Server'.
* One of the players must host the game by starting an instance of the app in server mode.
* After all players have joined the game, the host can select 'Start Game' from the menu. If you want to add AI players, you can also do that from the menu before your start.
* If you lose connection in the middle, you should be able to get back to the game by connecting again with the same player name.
* Most of the controls are through clicking boards, amoebas, or buttons that show up in the bottom-right.
* The expansion cards/rules can be selected as you start the server.

Some additional descriptions and screenshots are available in the distribution.

### Notes

I have played a lot of board games, but Ursuppe remains to be my favorite. I implemented it in Java so that I can play with my friends online. I decided to make it available on gitlab as I was cleaning up my old projects (this code comes from 2012 or so.)

The application supports English and Japanese. It should switch to Japanese when the locale is JP. If someone wants to add support for different languages, it should not be too difficult: the property files in ''ursuppe.resources'' is where the texts are defined.

### License
MIT

Source code is made publicly available at GitLab:
  https://gitlab.com/tyuki/Ursuppe

