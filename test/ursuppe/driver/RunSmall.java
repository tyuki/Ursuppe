package ursuppe.driver;

import static org.jdesktop.application.Application.launch;
import ursuppe.UrsuppeApp;



/**
 *
 * @author tyuki
 */
public class RunSmall {
    
    public static void main(String[] args) {
        launch(UrsuppeApp.class, new String[] { "-small" });
    }
}
